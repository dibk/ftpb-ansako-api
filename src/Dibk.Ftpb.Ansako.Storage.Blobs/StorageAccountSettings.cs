﻿namespace Dibk.Ftpb.Ansako.Storage.BlobStorage
{
    public abstract class StorageAccountSettings : IStorageAccountSettings
    {        
        public string ConnectionString { get; set; }
    }
}
