﻿using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Ansako.Storage
{

    public class KeyValuePairComparer : IEqualityComparer<KeyValuePair<string, string>>
    {
        public bool Equals(KeyValuePair<string, string> x, KeyValuePair<string, string> y)
        {
            if (x.Key.Equals(y.Key, StringComparison.OrdinalIgnoreCase) && (x.Value.Equals(y.Value, StringComparison.OrdinalIgnoreCase)))
                return true;

            return false;
        }

        public int GetHashCode(KeyValuePair<string, string> obj)
        {
            var kv = $"{obj.Key.ToUpper()}-{obj.Value.ToUpper()}";
            return kv.GetHashCode();
        }
    }
}
