﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Dibk.Ftpb.Ansako.Storage.BlobStorage
{
    public class BlobItem : IDisposable
    {
        private bool _disposed;

        public string FileName { get; set; }
        public string MimeType { get; set; }
        public IDictionary<string, string> Metadata { get; set; }
        public Stream ContentStream { get; set; }
        public DateTime LastModified { get; set; }

        public BlobItem() { }

        public BlobItem(string fileName, string mimeType, IDictionary<string, string> metadata, Stream contentStream, DateTime lastModified)
        {
            FileName = fileName;
            MimeType = mimeType;
            Metadata = metadata;
            ContentStream = contentStream;
            LastModified = lastModified;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    ContentStream?.Dispose();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
