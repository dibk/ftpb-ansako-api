﻿namespace Dibk.Ftpb.Ansako.Storage.BlobStorage
{
    public interface IStorageAccountSettings
    {
        string ConnectionString { get; set; }
    }
}