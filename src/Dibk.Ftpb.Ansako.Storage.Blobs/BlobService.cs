using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Azure.Storage.Blobs.Specialized;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Dibk.Ftpb.Ansako.Storage.BlobStorage
{
    public abstract class BlobService : IBlobService
    {
        private readonly string _connectionString;
        private readonly ILogger<BlobService> _logger;

        public BlobService(ILogger<BlobService> logger, IOptions<IStorageAccountSettings> options)
        {
            _connectionString = options.Value.ConnectionString;
            _logger = logger;
        }

        private BlobContainerClient GetBlobContainerClient(string containerName)
        {
            BlobServiceClient blobServiceClient = new BlobServiceClient(_connectionString);
            BlobContainerClient containerClient = blobServiceClient.GetBlobContainerClient(containerName.ToLower());
            if (!containerClient.Exists())
                containerClient.CreateIfNotExists(PublicAccessType.None);

            return containerClient;
        }

        private BlobClient GetBlobClient(string containerName, string fileName)
        {
            try
            {
                BlobContainerClient containerClient = GetBlobContainerClient(containerName.ToLower());
                return containerClient.GetBlobClient(fileName);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<string> AddOrUpdateFileAsync(string containerName, string filename, string mimeType, byte[] blobContent)
        {
            BlobClient blobClient = GetBlobClient(containerName.ToLower(), filename.ToLower());            

            var blobHttpHeader = new BlobHttpHeaders { ContentType = mimeType };
            var binaryData = new BinaryData(blobContent);

            try
            {
                _logger.LogDebug("Adds {FileName} to {ContainerName} in storage {AccountName}", filename, containerName, blobClient.AccountName);
                Response<BlobContentInfo> response = await blobClient.UploadAsync(binaryData, new BlobUploadOptions() { HttpHeaders = blobHttpHeader });

                return blobClient.Uri.AbsoluteUri.ToString();
            }
            catch (RequestFailedException rfex)
            {
                _logger.LogError(rfex, "Unable to save {FileName} blob to container {ContainerName} in storage {AccountName}", filename, containerName, blobClient.AccountName);
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to save {FileName} blob to container {ContainerName} in storage {AccountName}", filename, containerName, blobClient.AccountName);
                throw;
            }
        }

        public async Task<string> AddOrUpdateFileAsync(string containerName, string filename, string mimeType, byte[] blobContent, IDictionary<string, string> metadata)
        {
            BlobClient blobClient = GetBlobClient(containerName.ToLower(), filename.ToLower());

            var blobHttpHeader = new BlobHttpHeaders { ContentType = mimeType };
            var binaryData = new BinaryData(blobContent);

            try
            {
                _logger.LogDebug("Adds {FileName} to {ContainerName} in storage {AccountName}", filename, containerName, blobClient.AccountName);
                Response<BlobContentInfo> response = await blobClient.UploadAsync(binaryData,
                                                                                  new BlobUploadOptions()
                                                                                  {
                                                                                      HttpHeaders = blobHttpHeader,
                                                                                      Metadata = metadata
                                                                                  }).ConfigureAwait(false);

                return blobClient.Uri.AbsoluteUri.ToString();
            }
            catch (RequestFailedException rfex)
            {
                _logger.LogError(rfex, "Unable to save {FileName} blob to container {ContainerName} in storage {AccountName}", filename, containerName, blobClient.AccountName);
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to save {FileName} blob to container {ContainerName} in storage {AccountName}", filename, containerName, blobClient.AccountName);
                throw;
            }
        }

        public Task<string> AddOrUpdateFileAsync(string containerName, string filename, string mimeType, Stream blobContent)
        {
            return AddOrUpdateFileAsync(containerName, filename, mimeType, blobContent, null);
        }

        public async Task<string> AddOrUpdateFileAsync(string containerName, string filename, string mimeType, Stream blobContent, IDictionary<string, string> metadata)
        {
            BlobClient blobClient = GetBlobClient(containerName.ToLower(), filename.ToLower());

            var blobHttpHeader = new BlobHttpHeaders { ContentType = mimeType };

            try
            {
                _logger.LogDebug("Adds {FileName} to {containerName} in storage {AccountName}", filename, containerName, blobClient.AccountName);
                Response<BlobContentInfo> response = await blobClient.UploadAsync(blobContent, blobHttpHeader, metadata).ConfigureAwait(false);

                return blobClient.Uri.AbsoluteUri.ToString();
            }
            catch (RequestFailedException rfex)
            {
                _logger.LogError(rfex, "Unable to save {FileName} blob to container {ContainerName} in storage {AccountName}", filename, containerName, blobClient.AccountName);
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to save {FileName} blob to container {ContainerName} in storage {AccountName}", filename, containerName, blobClient.AccountName);
                throw;
            }
        }

        public async Task DeleteFileAsync(string containerName, string filename)
        {
            BlobClient blobClient = GetBlobClient(containerName.ToLower(), filename.ToLower());

            _logger.LogDebug("Deletes {FileName} from {ContainerName} in storage {AccountName}", filename, containerName, blobClient.AccountName);
            Response response = await blobClient.DeleteAsync().ConfigureAwait(false);
        }

        public async Task<BlobItem> GetFileAsync(string containerName, string fileName)
        {
            BlobClient blobClient = GetBlobClient(containerName.ToLower(), fileName.ToLower());

            if(!blobClient.Exists())
                blobClient = GetBlobClient(containerName.ToLower(), fileName);

            try
            {
                _logger.LogDebug("Retrieves file {FileName} in {ContainerName} from storage {AccountName}", fileName, containerName, blobClient.AccountName);
                var result = await blobClient.DownloadStreamingAsync().ConfigureAwait(false);
                return MapToBlobItem(result, blobClient.Name);
            }
            catch (RequestFailedException rfe)
            {
                if (rfe.Status == 404)
                    return null;

                throw;
            }
        }

        public async Task<IEnumerable<BlobItem>> GetFilesByFilterAsync(string containerName, IDictionary<string, string> filter)
        {
            _logger.LogDebug("Retrieves file by filter. {Filter} in {ContainerName}", string.Join(", ", filter.Select(kvp => kvp.Key + " = " + kvp.Value)), containerName);
            BlobContainerClient containerClient = GetBlobContainerClient(containerName.ToLower());
            var blobs = containerClient.GetBlobs(BlobTraits.Metadata).Where(p => p.Metadata.Intersect(filter, new KeyValuePairComparer()).Count() == filter.Count());
            try
            {
                var myblobs = new List<BlobItem>();
                foreach (var blob in blobs)
                {
                    var result = await GetBlobClient(containerName, blob.Name).DownloadStreamingAsync().ConfigureAwait(false);

                    myblobs.Add(MapToBlobItem(result, blob.Name));
                }
                return myblobs;
            }
            catch (RequestFailedException rfe)
            {
                if (rfe.Status == 404)
                    return null;

                throw;
            }
        }

        public async Task CreateContainerAsync(string containerName)
        {
            BlobContainerClient blobContainer = GetBlobContainerClient(containerName.ToLower());
            await blobContainer.CreateIfNotExistsAsync().ConfigureAwait(false);
        }

        public async Task<bool> DeleteContainerAsync(string containerName)
        {
            var blobServiceClient = new BlobServiceClient(_connectionString);
            BlobContainerClient containerClient = blobServiceClient.GetBlobContainerClient(containerName.ToLower());

            try
            {
                Response<bool> response = await containerClient.DeleteIfExistsAsync().ConfigureAwait(false);

                return response.Value;
            }
            catch (RequestFailedException ex)
            {
                _logger.LogError(ex, ex.Message);

                return false;
            }
        }

        public async Task<BlobLeaseResult> AquireLeaseAsync(string containerName, TimeSpan leaseDuration, CancellationToken cancelationToken)
        {
            var result = new BlobLeaseResult();
            try
            {
                var client = GetBlobContainerClient(containerName);
                var leaseClient = client.GetBlobLeaseClient();
                Response<BlobLease> lease;

                lease = await leaseClient.AcquireAsync(leaseDuration, null, cancelationToken).ConfigureAwait(false);
                _logger.LogInformation($"Lease for {containerName} aquired");
                result.SuccessfullLease = true;
                result.LeaseId = lease.Value.LeaseId;
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while aquiring lease on container");
                result.SuccessfullLease = false;
                result.Message = ex.Message;
                return result;
            }
        }

        public async Task<bool> ReleaseLeaseAsync(string containerName, string leaseId, CancellationToken cancellationToken)
        {
            try
            {
                var client = GetBlobContainerClient(containerName);
                var leaseClient = client.GetBlobLeaseClient(leaseId);

                var lease = await leaseClient.ReleaseAsync(null, cancellationToken).ConfigureAwait(false);
                _logger.LogInformation($"Lease for {containerName} released");

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred while releasing lease on container");
                throw;
            }
        }

        private BlobItem MapToBlobItem(BlobDownloadStreamingResult blob, string fileName)
        {
            var blobItem = new BlobItem()
            {
                ContentStream = blob.Content,
                FileName = fileName,
                MimeType = blob.Details.ContentType,
                Metadata = blob.Details.Metadata,
                LastModified = blob.Details.LastModified.DateTime
            };

            return blobItem;
        }
    }

    public class BlobLeaseResult
    {
        public string LeaseId { get; set; }
        public bool SuccessfullLease { get; set; }
        public string Message { get; set; }
    }
}