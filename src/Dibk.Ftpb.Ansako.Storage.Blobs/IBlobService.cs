﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Storage.BlobStorage
{
    public interface IBlobService
    {
        Task<string> AddOrUpdateFileAsync(string containerName, string filename, string mimeType, byte[] blobContent);

        Task<string> AddOrUpdateFileAsync(string containerName, string filename, string mimeType, byte[] blobContent, IDictionary<string, string> metadata);

        Task<string> AddOrUpdateFileAsync(string containerName, string filename, string mimeType, Stream blobContent);

        Task<string> AddOrUpdateFileAsync(string containerName, string filename, string mimeType, Stream blobContent, IDictionary<string, string> metadata = null);

        Task<BlobItem> GetFileAsync(string containerName, string fileName);

        Task<IEnumerable<BlobItem>> GetFilesByFilterAsync(string containerName, IDictionary<string, string> filter);

        Task DeleteFileAsync(string containerName, string filename);

        Task<bool> DeleteContainerAsync(string containerName);

        Task CreateContainerAsync(string containerName);

        Task<BlobLeaseResult> AquireLeaseAsync(string containerName, TimeSpan leaseDuration, CancellationToken cancelationToken);

        Task<bool> ReleaseLeaseAsync(string containerName, string leaseId, CancellationToken cancellationToken);
    }
}