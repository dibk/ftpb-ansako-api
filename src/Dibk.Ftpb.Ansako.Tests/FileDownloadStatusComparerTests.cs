﻿using Dibk.Ftpb.Api.Client.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dibk.Ftpb.Ansako.Tests
{
    [TestClass]
    public class FileDownloadStatusComparerTests
    {
        private readonly FileDownloadStatusEqualtiyComparer _comparer;

        public FileDownloadStatusComparerTests()
        {
            _comparer = new FileDownloadStatusEqualtiyComparer();
        }

        [TestMethod]
        public void Equals_SameObjects_ReturnsTrue()
        {
            var file1 = new FileDownloadStatus
            {
                ArchiveReference = "ref1",
                BlobLink = "link1",
                Filename = "file1",
                FileType = Api.Models.FilTyperForNedlasting.SkjemaPdf,
                FormName = "form1",
                ContainerName = "container1",
                MimeType = "mime1"
            };

            var file2 = new FileDownloadStatus
            {
                ArchiveReference = "ref1",
                BlobLink = "link1",
                Filename = "file1",
                FileType = Api.Models.FilTyperForNedlasting.SkjemaPdf,
                FormName = "form1",
                ContainerName = "container1",
                MimeType = "mime1"
            };

            var result = _comparer.Equals(file1, file2);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Equals_DifferentObjects_ReturnsFalse()
        {
            var file1 = new FileDownloadStatus
            {
                ArchiveReference = "ref1",
                BlobLink = "link1",
                Filename = "file1",
                FileType = Api.Models.FilTyperForNedlasting.SkjemaPdf,
                FormName = "form1",
                ContainerName = "container1",
                MimeType = "mime1"
            };

            var file2 = new FileDownloadStatus
            {
                ArchiveReference = "ref2",
                BlobLink = "link2",
                Filename = "file2",
                FileType = Api.Models.FilTyperForNedlasting.MaskinlesbarXml,
                FormName = "form2",
                ContainerName = "container2",
                MimeType = "mime2"
            };

            var result = _comparer.Equals(file1, file2);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void GetHashCode_SameObjects_ReturnsSameHashCode()
        {
            var file1 = new FileDownloadStatus
            {
                Id = 1,
                ArchiveReference = "ref1",
                BlobLink = "link1",
                Filename = "file1",
                FileType = Api.Models.FilTyperForNedlasting.SkjemaPdf,
                FormName = "form1",
                ContainerName = "container1",
                MimeType = "mime1"
            };

            var file2 = new FileDownloadStatus
            {
                Id = 1,
                ArchiveReference = "ref1",
                BlobLink = "link1",
                Filename = "file1",
                FileType = Api.Models.FilTyperForNedlasting.SkjemaPdf,
                FormName = "form1",
                ContainerName = "container1",
                MimeType = "mime1"
            };

            var hash1 = _comparer.GetHashCode(file1);
            var hash2 = _comparer.GetHashCode(file2);

            Assert.AreEqual(hash1, hash2);
        }

        [TestMethod]
        public void GetHashCode_DifferentObjects_ReturnsDifferentHashCodes()
        {
            var file1 = new FileDownloadStatus
            {
                Id = 1,
                ArchiveReference = "ref1",
                BlobLink = "link1",
                Filename = "file1",
                FileType = Api.Models.FilTyperForNedlasting.SkjemaPdf,
                FormName = "form1",
                ContainerName = "container1",
                MimeType = "mime1"
            };

            var file2 = new FileDownloadStatus
            {
                Id = 2,
                ArchiveReference = "ref2",
                BlobLink = "link2",
                Filename = "file2",
                FileType = Api.Models.FilTyperForNedlasting.MaskinlesbarXml,
                FormName = "form2",
                ContainerName = "container2",
                MimeType = "mime2"
            };

            var hash1 = _comparer.GetHashCode(file1);
            var hash2 = _comparer.GetHashCode(file2);

            Assert.AreNotEqual(hash1, hash2);
        }
    }
}