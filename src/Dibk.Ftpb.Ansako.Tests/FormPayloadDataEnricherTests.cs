using Dibk.Ftpb.Ansako.App.Models;
using Dibk.Ftpb.Ansako.App.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Dibk.Ftpb.Ansako.Tests
{
    [TestClass]
    public class FormPayloadDataEnricherTests
    {
        [TestMethod]
        public void SetSigningDeadlineIfMissing_metaDataNullValue_success()
        {
            var payload = new CreateFormPayload();
            FormPayloadDataEnricher.SetSigningDeadlineIfMissing(payload, 1);
            var expectedDeadline = DateTime.Now.Date.AddDays(1).AddHours(23).AddMinutes(59).AddSeconds(59);
            Assert.AreEqual(expectedDeadline, payload.FormMetadata.Signeringsfrist);
        }

        [TestMethod]
        public void SetSigningDeadlineIfMissing_deadlineDateNullValue_success()
        {
            var payload = new CreateFormPayload()
            {
                FormMetadata = new CreateFormMetadata()
            };
            FormPayloadDataEnricher.SetSigningDeadlineIfMissing(payload, 1);
            var expectedDeadline = DateTime.Now.Date.AddDays(1).AddHours(23).AddMinutes(59).AddSeconds(59);
            Assert.AreEqual(expectedDeadline, payload.FormMetadata.Signeringsfrist);
        }

        [TestMethod]
        public void SetSigningDeadlineIfMissing_deadlineDateSet_returnsAddedHoursMinutesAndSecconds()
        {
            var inputDate = DateTime.Now.AddDays(3);
            var payload = new CreateFormPayload()
            {
                FormMetadata = new CreateFormMetadata() { Signeringsfrist = inputDate }
            };
            FormPayloadDataEnricher.SetSigningDeadlineIfMissing(payload, 1);
            var expectedDeadline = inputDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            Assert.AreEqual(expectedDeadline, payload.FormMetadata.Signeringsfrist);
        }
    }
}