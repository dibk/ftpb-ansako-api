﻿namespace Dibk.Ftpb.Api.Client.Interfaces
{
    public interface ILogEntryRepository
    {
        Task SaveAsync();

        void AddInfo(string message);

        void AddInfo(string message, string eventId);

        void AddInfoInternal(string message, string eventId);

        void AddErrorInternal(string message, string eventId);

        void AddError(string message);

        void AddNewError(string message, string eventId);

        Task InitateAsync(string ftpbReferenceId, string localStorageContainerName);

        Task SyncToAlfa();
    }
}