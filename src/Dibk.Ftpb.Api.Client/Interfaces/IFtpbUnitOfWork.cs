﻿namespace Dibk.Ftpb.Api.Client.Interfaces
{
    public interface IFtpbUnitOfWork
    {
        IFormMetadataRepository FormMetadata { get; }
        ILogEntryRepository LogEntries { get; }
        IDistributionFormsRepository DistributionForms { get; }
        IFileDownloadStatusRepository FileDownloadStatuses { get; }

        Task SaveFormMetadataAsync();

        Task SaveLogEntriesAsync();

        Task SaveDistributionFormsAsync();

        Task SaveFileDownloadStatusAsync();        

        Task InitiateAsync(string ftpbReferenceId, string localStorageContainerName);

        Task SaveAsync();

        Task SyncToAlfaAsync();
    }
}