﻿using Dibk.Ftpb.Api.Client.Models;

namespace Dibk.Ftpb.Api.Client.Interfaces
{
    public interface IDistributionFormsRepository
    {
        void AddAsync(DistributionForm distributionForm);

        Task<DistributionForm> GetAsync(Guid id);

        Task InitiateAsync(string ftpbReferenceId, string localStorageContainerName);

        Task<bool> SaveAsync();

        Task SyncToAlfa();
    }
}