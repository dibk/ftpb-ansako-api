﻿using Dibk.Ftpb.Api.Client.Models;

namespace Dibk.Ftpb.Api.Client.Interfaces
{
    public interface IFileDownloadStatusRepository
    {
        Task<List<FileDownloadStatus>> GetAsync();

        Task<FileDownloadStatus> AddOrUpdateAsync(FileDownloadStatus fileDownloadStatus);

        Task InitiateAsync(string ftpbReferenceId, string localStorageContainerName);

        Task SaveAsync();

        Task SyncToAlfa();
    }
}