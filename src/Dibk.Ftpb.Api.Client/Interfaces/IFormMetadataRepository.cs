﻿using Dibk.Ftpb.Api.Client.Models;

namespace Dibk.Ftpb.Api.Client.Interfaces
{
    public interface IFormMetadataRepository
    {
        Task<FormMetadata> GetAsync();

        Task<FormMetadata> AddOrUpdateAsync(FormMetadata formMetadata);

        Task SaveAsync();

        Task IntiateAsync(string ftpbReferenceId, string localStorageContainerName);

        Task SyncToAlfa();
    }
}