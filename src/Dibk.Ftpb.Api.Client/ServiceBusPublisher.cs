﻿using Azure.Messaging.ServiceBus;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Text.Json;

namespace Dibk.Ftpb.Api.Client
{
    public class ServiceBusPublisher
    {
        private readonly ILogger<ServiceBusPublisher> _logger;
        private readonly IAzureClientFactory<ServiceBusClient> _azureClientFactory;
        public static readonly string ServiceBusClientName = "AnSaKoQueueClient";
        private readonly string _serviceBusQueueName = "sbq-ftpb-ansako-alfa-sync";

        public ServiceBusPublisher(ILogger<ServiceBusPublisher> logger,
                                     IAzureClientFactory<ServiceBusClient> azureClientFactory,
                                     IConfiguration configuration)
        {
            _logger = logger;
            _azureClientFactory = azureClientFactory;

            var serviceBusQueueName = configuration["FormProcessAPISettings:QueueName"];
            if (!string.IsNullOrWhiteSpace(serviceBusQueueName))
                _serviceBusQueueName = serviceBusQueueName;

        }

        public async Task PublishAsync(SyncToAlfaMessage message)
        {
            await PublishAsync(message, CancellationToken.None);
        }

        public async Task PublishAsync(SyncToAlfaMessage message, CancellationToken cancellationToken)
        {
            var client = _azureClientFactory.CreateClient(ServiceBusClientName);
            var sender = client.CreateSender(_serviceBusQueueName);

            var opts = new JsonSerializerOptions() { WriteIndented = false, Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping };
            var messageBody = JsonSerializer.Serialize(message, opts);
            var serviceBusMessage = new ServiceBusMessage(messageBody);

            try
            {
                _logger.LogDebug("Publishing message to ServiceBus queue {QueueName}", _serviceBusQueueName);
                await sender.SendMessageAsync(serviceBusMessage, cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while publishing message to ServiceBus queue {QueueName}", _serviceBusQueueName);
                throw;
            }
        }
    }

    public class SyncToAlfaMessage
    {
        public string AnSaKoReferenceId { get; set; }
        public string FtpbReferenceId { get; set; }
    }
}