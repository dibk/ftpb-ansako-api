﻿using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace DibkFtpb.Api.Client.LocalStore
{
    public class LocalStoreBlobService : BlobService
    {
        public LocalStoreBlobService(ILogger<BlobService> logger, IOptions<LocalStoreBlobStorageAccountSettings> options) : base(logger, options)
        { }
    }
}