﻿using Dibk.Ftpb.Ansako.Storage.BlobStorage;

namespace DibkFtpb.Api.Client.LocalStore
{
    public class LocalStoreBlobStorageAccountSettings : StorageAccountSettings
    {
        public static string ConfigSection => "AzureStorage";
    }
}