﻿using DibkFtpb.Api.Client.LocalStore;

internal static class LocalStoreServiceHelpers
{
    public static string GetFilename<T>(string key) where T : ILocalStoreEntity
    {
        var type = typeof(T);
        LocalStoreAttribute? attribute = type.GetCustomAttributes(typeof(LocalStoreAttribute), false).FirstOrDefault() as LocalStoreAttribute;

        if (attribute != null)
            return $"{key}/{attribute.Filename}";
        else
        {
            throw new Exception("Attribute not found");
        }
    }
}