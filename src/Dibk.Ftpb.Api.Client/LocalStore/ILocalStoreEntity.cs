﻿namespace DibkFtpb.Api.Client.LocalStore
{
    public interface ILocalStoreEntity
    {
        public DateTime? DateUpdated { get; set; }
    }
}