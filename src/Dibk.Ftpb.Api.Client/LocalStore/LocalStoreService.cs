﻿using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using Microsoft.Extensions.Logging;
using System.Text;

namespace DibkFtpb.Api.Client.LocalStore
{
    public class LocalStoreService
    {
        private readonly ILogger<LocalStoreBlobService> _logger;
        private readonly LocalStoreBlobService _blobService;

        public LocalStoreService(ILogger<LocalStoreBlobService> logger, LocalStoreBlobService blobService)
        {
            _logger = logger;
            _blobService = blobService;
        }

        public async Task<T?> GetAsync<T>(string containerName, string filePrefix) where T : ILocalStoreEntity, new()
        {
            using (BlobItem result = await _blobService.GetFileAsync(containerName, LocalStoreServiceHelpers.GetFilename<T>(filePrefix)))
            {
                if (result == null)
                    return default(T);

                using (var reader = new StreamReader(result.ContentStream))
                {
                    var json = await reader.ReadToEndAsync();

                    return System.Text.Json.JsonSerializer.Deserialize<T>(json);
                }
            }
        }

        public async Task<T?> SaveAsync<T>(string containerName, string filePrefix, T data) where T : ILocalStoreEntity, new()
        {
            var dateUpdated = DateTime.Now;
            data.DateUpdated = dateUpdated;
            var filename = LocalStoreServiceHelpers.GetFilename<T>(filePrefix);

            _logger.LogInformation("Saving {LocalStoreType} to local store {Filename} in container {ContainerName} - dateUpdated: {DateUpdated}", typeof(T).Name, filename, containerName, dateUpdated);

            var json = System.Text.Json.JsonSerializer.Serialize(data);
            using (Stream stream = new MemoryStream(Encoding.UTF8.GetBytes(json)))
                await _blobService.AddOrUpdateFileAsync(containerName, filename, "application/json", stream);

            return await GetAsync<T>(containerName, filePrefix);
        }
    }
}