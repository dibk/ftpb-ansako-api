﻿namespace DibkFtpb.Api.Client.LocalStore
{
    [AttributeUsage(AttributeTargets.Class)]
    public class LocalStoreAttribute : Attribute
    {
        public string Filename { get; }

        public LocalStoreAttribute(string filename)
        {
            Filename = filename;
        }
    }
}