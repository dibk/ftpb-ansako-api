﻿using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace DibkFtpb.Api.Client
{
    public class FtpbBlobService : BlobService
    {
        public FtpbBlobService(ILogger<BlobService> logger, IOptions<FtpbBlobStorageAccountSettings> options) : base(logger, options)
        {
        }
    }

    public class FtpbBlobStorageAccountSettings : StorageAccountSettings
    {
        public static string ConfigSection => "FormProcessAPISettings:StorageAccount";
    }
}