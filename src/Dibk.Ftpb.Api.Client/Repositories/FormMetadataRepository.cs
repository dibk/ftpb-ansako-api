﻿using AutoMapper;
using Dibk.Ftpb.Api.Client.Interfaces;
using Dibk.Ftpb.Api.Client.Models;
using Dibk.Ftpb.Api.Clients.HttpClients;
using Dibk.Ftpb.Api.Models;
using DibkFtpb.Api.Client.LocalStore;
using Microsoft.Extensions.Logging;
using System.Net;

namespace Dibk.Ftpb.Api.Client.Repositories
{
    public class FormMetadataRepository : FtpbApiRepositoryBase, IFormMetadataRepository
    {
        private readonly ILogger<FormMetadataRepository> _logger;
        private readonly FormMetadataHttpClient _formMetadataClient;
        private readonly LocalStoreService _localStoreService;
        private FormMetadataLocalStoreEntity _localStoreEntity;

        public FormMetadataRepository(ILogger<FormMetadataRepository> logger,
                                      FormMetadataHttpClient formMetadataClient,
                                      LocalStoreService localStoreService,
                                      IMapper map) : base(map)
        {
            _logger = logger;
            _formMetadataClient = formMetadataClient;
            _localStoreService = localStoreService;
        }

        public async Task IntiateAsync(string ftpbReferenceId, string localStorageContainerName)
        {
            _localStoreEntity = null;
            _ftpbReferenceId = ftpbReferenceId;
            _localStorageContainerName = localStorageContainerName;
            await LoadLocalStore();
        }

        private async Task LoadLocalStore()
        {
            var fm = await _localStoreService.GetAsync<FormMetadataLocalStoreEntity>(_localStorageContainerName, _ftpbReferenceId);

            if (fm == null)
            {
                FormMetadataApiModel result = null;
                try
                {
                    result = await _formMetadataClient.GetAsync(_ftpbReferenceId);
                }
                catch (HttpRequestException ex) when (ex.StatusCode == HttpStatusCode.NotFound)
                {
                    _logger.LogWarning("No FormMetadata found in Alfa");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error while loading FormMetadata from Alfa");
                }

                if (result != null)
                {
                    fm = new FormMetadataLocalStoreEntity(_ftpbReferenceId)
                    {
                        FormMetadata = _mapper.Map<FormMetadata>(result)
                    };
                    fm = await _localStoreService.SaveAsync(_localStorageContainerName, _ftpbReferenceId, fm);
                }
                else
                {
                    fm = new FormMetadataLocalStoreEntity(_ftpbReferenceId);
                }
            }

            _localStoreEntity = fm;
        }

        public async Task<FormMetadata> GetAsync()
        {
            ThrowIfNotInitiated();

            return _localStoreEntity.FormMetadata;
        }

        public async Task<FormMetadata> AddOrUpdateAsync(FormMetadata formMetadata)
        {
            ThrowIfNotInitiated();

            if (formMetadata != null)
            {
                _localStoreEntity.FormMetadata = formMetadata;
                _localStoreEntity = await _localStoreService.SaveAsync(_localStorageContainerName, _ftpbReferenceId, _localStoreEntity);
            }

            return await GetAsync();
        }

        public async Task SaveAsync()
        {
            ThrowIfNotInitiated();

            if (_localStoreEntity != null)
            {
                await _localStoreService.SaveAsync(_localStorageContainerName, _ftpbReferenceId, _localStoreEntity);                
            }
        }

        public async Task SyncToAlfa()
        {
            if (_localStoreEntity != null)
            {
                FormMetadataApiModel alfaValue = null;
                try
                {
                    alfaValue = await _formMetadataClient.GetAsync(_ftpbReferenceId);
                }
                catch (HttpRequestException ex) when (ex.StatusCode == HttpStatusCode.NotFound)
                {
                    _logger.LogWarning("No FormMetadata found in Alfa");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error while loading FormMetadata from Alfa");
                    throw;
                }

                if (alfaValue == null)
                    await _formMetadataClient.CreateAsync(_mapper.Map<FormMetadataApiModel>(_localStoreEntity.FormMetadata));
                else
                    await _formMetadataClient.UpdateAsync(_mapper.Map<FormMetadataApiModel>(_localStoreEntity.FormMetadata));
            }
        }
    }
}