﻿namespace Dibk.Ftpb.Api.Client.Repositories
{
    public enum FtpbAlfaEntityState
    {
        Unchanged = 0,
        Modified = 1,
        Added = 2
    }
}