﻿using AutoMapper;

namespace Dibk.Ftpb.Api.Client.Repositories
{
    public class FtpbApiRepositoryBase(IMapper mapper)
    {
        protected readonly IMapper _mapper = mapper;
        protected string _ftpbReferenceId;
        protected string _localStorageContainerName;

        protected void ThrowIfNotInitiated()
        {
            if (string.IsNullOrEmpty(_ftpbReferenceId))
                throw new System.ArgumentNullException("Repository is not initiated. Please initiate it with a reference id");

            if (string.IsNullOrEmpty(_localStorageContainerName))
                throw new System.ArgumentNullException("Repository is not initiated. Please initiate it with a local storage container name");
        }
    }
}