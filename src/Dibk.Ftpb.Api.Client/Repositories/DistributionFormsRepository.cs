﻿using AutoMapper;
using Dibk.Ftpb.Api.Client.Interfaces;
using Dibk.Ftpb.Api.Client.Models;
using Dibk.Ftpb.Api.Clients.HttpClients;
using Dibk.Ftpb.Api.Models;
using DibkFtpb.Api.Client.LocalStore;
using Microsoft.Extensions.Logging;
using System.Net;

namespace Dibk.Ftpb.Api.Client.Repositories
{
    public class DistributionFormsRepository : FtpbApiRepositoryBase, IDistributionFormsRepository
    {
        private readonly ILogger<DistributionFormsRepository> _logger;
        private readonly DistributionFormsHttpClient _distributionFormsClient;
        private readonly LocalStoreService _localStoreService;
        private DistributionFormsLocalStoreEntity _localStoreEntity;

        public DistributionFormsRepository(ILogger<DistributionFormsRepository> logger,
                                           DistributionFormsHttpClient distributionFormsClient,
                                           LocalStoreService localStoreService,
                                           IMapper map) : base(map)
        {
            _logger = logger;
            _distributionFormsClient = distributionFormsClient;
            _localStoreService = localStoreService;
            _localStoreEntity = new DistributionFormsLocalStoreEntity();
        }

        public async Task InitiateAsync(string ftpbReferenceId, string localStorageContainerName)
        {
            _ftpbReferenceId = ftpbReferenceId;
            _localStorageContainerName = localStorageContainerName;
            await LoadLocalStore();
        }

        public async Task<DistributionForm?> GetAsync(Guid id)
        {
            ThrowIfNotInitiated();

            _logger.LogDebug("Retrieves distributionForm with ID {DistributionFormId}", id.ToString());

            var result = _localStoreEntity?.DistributionForms?.FirstOrDefault(p => p.Id == id);

            return result;
        }

        private async Task LoadLocalStore()
        {
            var storeEntry = await _localStoreService.GetAsync<DistributionFormsLocalStoreEntity>(_localStorageContainerName, _ftpbReferenceId);

            if (storeEntry == null)
            {
                IEnumerable<DistributionFormApiModel> dfs = null;

                try
                {
                    dfs = await _distributionFormsClient.GetAllAsync(_ftpbReferenceId);
                }
                catch (HttpRequestException ex) when (ex.StatusCode == HttpStatusCode.NotFound)
                {
                    _logger.LogWarning("No distribution forms found in Alfa");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Failed to load distribution forms from Alfa");
                }

                if (dfs != null)
                {
                    storeEntry = new DistributionFormsLocalStoreEntity()
                    {
                        DistributionForms = dfs.Select(p => _mapper.Map<DistributionForm>(p)).ToList()
                    };
                    storeEntry = await _localStoreService.SaveAsync(_localStorageContainerName, _ftpbReferenceId, storeEntry);
                }
            }

            if (storeEntry != null)
                _localStoreEntity = storeEntry;
            else
                _localStoreEntity = new DistributionFormsLocalStoreEntity();
        }

        public void AddAsync(DistributionForm distributionForm)
        {
            ThrowIfNotInitiated();
            ValidateInput(distributionForm);

            distributionForm.InitialArchiveReference = _ftpbReferenceId;
            _localStoreEntity.DistributionForms.Add(distributionForm);
        }

        public async Task<bool> SaveAsync()
        {
            if (_localStoreEntity.DistributionForms?.Count == 0)
            {
                _logger.LogWarning("No forms to persist!");
                return true;
            }

            _localStoreEntity = await _localStoreService.SaveAsync(_localStorageContainerName, _ftpbReferenceId, _localStoreEntity);            

            return true;
        }

        public async Task SyncToAlfa()
        {
            if(_localStoreEntity.DistributionForms?.Count == 0)
            {
                _logger.LogWarning("No distribution forms to sync");
                return;
            }

            foreach (var localDf in _localStoreEntity.DistributionForms)
            {
                bool alreadyExistsInAlfa = false;

                var dfInAlfa = await _distributionFormsClient.GetAsync(localDf.Id);


                if (dfInAlfa != null)
                    alreadyExistsInAlfa = true;

                if (alreadyExistsInAlfa)
                {
                    try
                    {
                        await _distributionFormsClient.UpdateAsync(localDf.InitialArchiveReference, localDf.Id, _mapper.Map<DistributionFormApiModel>(localDf));
                    }
                    catch
                    {
                        _logger.LogError("Persisting distributionForm failed");
                        throw;
                    }
                }
                else
                {
                    try
                    {
                        await _distributionFormsClient.CreateAsync(_ftpbReferenceId, new List<DistributionFormApiModel>() { _mapper.Map<DistributionFormApiModel>(localDf) });
                    }
                    catch
                    {
                        _logger.LogError("Persisting distributionForm failed");
                    }
                }
            }
        }

        private void ValidateInput(DistributionForm df)
        {
            if (!df.InitialArchiveReference.Equals(_ftpbReferenceId, StringComparison.OrdinalIgnoreCase))
                throw new ArgumentException("DistributionForm.InitialArchiveReference must be equal to the repositories initiated referenceId");

            if (df.Id == default)
                throw new ArgumentException("DistributionForm.Id is required to have a value");
        }
    }
}