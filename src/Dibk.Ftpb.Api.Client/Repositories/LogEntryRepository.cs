﻿using AutoMapper;
using Dibk.Ftpb.Api.Client.Interfaces;
using Dibk.Ftpb.Api.Client.Models;
using Dibk.Ftpb.Api.Clients.HttpClients;
using Dibk.Ftpb.Api.Models;
using DibkFtpb.Api.Client.LocalStore;
using Microsoft.Extensions.Logging;

namespace Dibk.Ftpb.Api.Client.Repositories
{
    public class LogEntryRepository : FtpbApiRepositoryBase, ILogEntryRepository
    {
        private readonly ILogger<LogEntryRepository> _logger;
        private readonly LogEntriesHttpClient _logEntryClient;
        private readonly LocalStoreService _localStoreService;
        private LogEntriesLocalStoreEntity _localStoreEntity;

        public LogEntryRepository(ILogger<LogEntryRepository> logger,
                                  LogEntriesHttpClient logEntryClient,
                                  LocalStoreService localStoreService,
                                  IMapper map) : base(map)
        {
            _logger = logger;
            _logEntryClient = logEntryClient;
            _localStoreService = localStoreService;
        }

        public async Task InitateAsync(string ftpbReferenceId, string localStorageContainerName)
        {
            _ftpbReferenceId = ftpbReferenceId;
            _localStorageContainerName = localStorageContainerName;
            await LoadLocalStore();
        }

        public async Task LoadLocalStore()
        {
            _localStoreEntity = await _localStoreService.GetAsync<LogEntriesLocalStoreEntity>(_localStorageContainerName, _ftpbReferenceId);

            if (_localStoreEntity == null)
            {
                _localStoreEntity = new LogEntriesLocalStoreEntity(new List<LogEntity>());
            }
        }

        public async Task SaveAsync()
        {
            await _localStoreService.SaveAsync(_localStorageContainerName, _ftpbReferenceId, _localStoreEntity);            
        }

        public async Task SyncToAlfa()
        {
            if (_localStoreEntity.LogEntries?.Count > 0)
            {
                var addedLogEntries = _localStoreEntity.LogEntries.Where(p => p.State == FtpbAlfaEntityState.Added).Select(p => p.Log).ToList();
                if (addedLogEntries != null)
                {
                    _logger.LogDebug($"#{addedLogEntries.Count} logentires posted");
                    try
                    {
                        await _logEntryClient.CreateAsync(_ftpbReferenceId, addedLogEntries.Select(_mapper.Map<LogEntryApiModel>).ToList());
                        foreach (var logEntity in _localStoreEntity.LogEntries.Where(p => p.State == FtpbAlfaEntityState.Added))
                        {
                            logEntity.State = FtpbAlfaEntityState.Unchanged;
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, "Error posting logentries");
                    }
                }

                // Save "saved" state to local storage
                await _localStoreService.SaveAsync(_localStorageContainerName, _ftpbReferenceId, _localStoreEntity);
            }
        }

        public void AddInfo(string message)
        {
            _localStoreEntity.LogEntries.Add(new LogEntity(new LogEntry(_ftpbReferenceId, message, LogEntry.Info)));
        }

        public void AddInfo(string message, string eventId)
        {
            _localStoreEntity.LogEntries.Add(new LogEntity(new LogEntry(_ftpbReferenceId, message, LogEntry.Info) { EventId = eventId }));
        }

        public void AddInfoInternal(string message, string eventId)
        {
            _localStoreEntity.LogEntries.Add(new LogEntity(new LogEntry(_ftpbReferenceId, message, LogEntry.Info, LogEntry.InternalMsg) { EventId = eventId, }));
        }

        public void AddErrorInternal(string message, string eventId)
        {
            _localStoreEntity.LogEntries.Add(new LogEntity(new LogEntry(_ftpbReferenceId, message, LogEntry.Error, LogEntry.InternalMsg) { EventId = eventId }));
        }

        public void AddError(string message)
        {
            _localStoreEntity.LogEntries.Add(new LogEntity(new LogEntry(_ftpbReferenceId, message, LogEntry.Error)));
        }

        public void AddNewError(string message, string eventId)
        {
            _localStoreEntity.LogEntries.Add(new LogEntity(new LogEntry(_ftpbReferenceId, message, LogEntry.Error) { EventId = eventId, }));
        }
    }
}