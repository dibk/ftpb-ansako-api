﻿using AutoMapper;
using Dibk.Ftpb.Api.Client.Interfaces;
using Dibk.Ftpb.Api.Client.Models;
using Dibk.Ftpb.Api.Clients.HttpClients;
using Dibk.Ftpb.Api.Models;
using DibkFtpb.Api.Client.LocalStore;
using Microsoft.Extensions.Logging;
using System.Net;

namespace Dibk.Ftpb.Api.Client.Repositories
{
    public class FileDownloadStatusRepository : FtpbApiRepositoryBase, IFileDownloadStatusRepository
    {
        private readonly ILogger<FileDownloadStatusRepository> _logger;
        private readonly FileDownloadStatusHttpClient _fileDownloadStatusClient;
        private readonly LocalStoreService _localStoreService;
        private FileDownloadStatusLocalStoreEntity _localStoreEntity;

        public FileDownloadStatusRepository(ILogger<FileDownloadStatusRepository> logger,
                                            FileDownloadStatusHttpClient fileDownloadStatusClient,
                                            LocalStoreService localStoreService,
                                            IMapper map) : base(map)
        {
            _logger = logger;
            _fileDownloadStatusClient = fileDownloadStatusClient;
            _localStoreService = localStoreService;
            _localStoreEntity = new FileDownloadStatusLocalStoreEntity();
        }

        public async Task InitiateAsync(string ftpbReferenceId, string localStorageContainerName)
        {
            _ftpbReferenceId = ftpbReferenceId;
            _localStorageContainerName = localStorageContainerName;
            await LoadLocalStoreAsync();
        }

        private async Task LoadLocalStoreAsync()
        {
            var storeEntry = await _localStoreService.GetAsync<FileDownloadStatusLocalStoreEntity>(_localStorageContainerName, _ftpbReferenceId);
            if (storeEntry == null)
            {
                IEnumerable<FileDownloadStatusApiModel> dfs = null;
                try
                {
                    dfs = await _fileDownloadStatusClient.GetAsync(_ftpbReferenceId);
                }
                catch (HttpRequestException ex) when (ex.StatusCode == HttpStatusCode.NotFound)
                {
                    _logger.LogWarning("No file download statuses found in Alfa");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error while loading file download statuses from Alfa");
                }

                if (dfs != null)
                {
                    storeEntry = new FileDownloadStatusLocalStoreEntity()
                    {
                        FileDownloadStatuses = dfs.Select(_mapper.Map<FileDownloadStatus>).ToList()
                    };
                    storeEntry = await _localStoreService.SaveAsync(_localStorageContainerName, _ftpbReferenceId, storeEntry);
                }
            }

            if (storeEntry != null)
                _localStoreEntity = storeEntry;
            else
                _localStoreEntity = new FileDownloadStatusLocalStoreEntity();
        }

        public async Task<List<FileDownloadStatus>> GetAsync()
        {
            ThrowIfNotInitiated();
            return _localStoreEntity.FileDownloadStatuses;
        }

        public async Task<FileDownloadStatus> AddOrUpdateAsync(FileDownloadStatus fileDownloadStatus)
        {
            ThrowIfNotInitiated();
            ValidateInput(fileDownloadStatus);

            FileDownloadStatus retval = null;
            if (_localStoreEntity.FileDownloadStatuses.Contains(fileDownloadStatus, new FileDownloadStatusEqualtiyComparer()))
            {
                //Update the existing fileDownloadStatus
                var entry = _localStoreEntity.FileDownloadStatuses.First(p => new FileDownloadStatusEqualtiyComparer().Equals(fileDownloadStatus, p));
                entry = fileDownloadStatus;
                await _localStoreService.SaveAsync(_localStorageContainerName, _ftpbReferenceId, _localStoreEntity);
                retval = entry;
            }
            else
            {
                //Add the new fileDownloadStatus
                _localStoreEntity.FileDownloadStatuses.Add(fileDownloadStatus);
                await _localStoreService.SaveAsync(_localStorageContainerName, _ftpbReferenceId, _localStoreEntity);
                retval = fileDownloadStatus;
            }

            return retval;
        }

        public async Task SaveAsync()
        {
            ThrowIfNotInitiated();
            await _localStoreService.SaveAsync(_localStorageContainerName, _ftpbReferenceId, _localStoreEntity);
        }

        public async Task SyncToAlfa()
        {
            foreach (var fd in _localStoreEntity.FileDownloadStatuses)
            {
                if (fd.Id == 0) //Doesn't exist in Alfa
                {
                    await _fileDownloadStatusClient.CreateAsync(_mapper.Map<FileDownloadStatusApiModel>(fd));
                }
                else
                {
                    await _fileDownloadStatusClient.UpdateAsync(_mapper.Map<FileDownloadStatusApiModel>(fd));
                }
            }

            //Update the localstore with the updated values from Alfa
            IEnumerable<FileDownloadStatusApiModel> dfs = await _fileDownloadStatusClient.GetAsync(_ftpbReferenceId);
            if (dfs != null)
            {
                _localStoreEntity = new FileDownloadStatusLocalStoreEntity()
                {
                    FileDownloadStatuses = dfs.Select(_mapper.Map<FileDownloadStatus>).ToList()
                };
                _localStoreEntity = await _localStoreService.SaveAsync(_localStorageContainerName, _ftpbReferenceId, _localStoreEntity);
            }
        }

        private void ValidateInput(FileDownloadStatus fileDownloadStatus)
        {
            if (!fileDownloadStatus.ArchiveReference.Equals(_ftpbReferenceId, StringComparison.OrdinalIgnoreCase))
                throw new ArgumentException("FiledownloadStatis.ArchiveReference must be equal to the repositories initiated referenceId");
        }
    }
}