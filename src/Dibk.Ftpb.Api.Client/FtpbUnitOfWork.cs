﻿using Dibk.Ftpb.Api.Client.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Dibk.Ftpb.Api.Client
{
    public class FtpbUnitOfWork : IFtpbUnitOfWork
    {
        private readonly ILogger<FtpbUnitOfWork> _logger;
        private readonly ServiceBusPublisher _serviceBusPublisher;
        private bool _useEnqueing = true;

        public FtpbUnitOfWork(ILogger<FtpbUnitOfWork> logger,
                ILogEntryRepository logEntryRepository,
                IFormMetadataRepository formMetadataRepository,
                IDistributionFormsRepository distributionFormRepository,
                IFileDownloadStatusRepository fileDownloadStatusRepository,
                ServiceBusPublisher serviceBusPublisher,
                IConfiguration configuration)
        {
            _logger = logger;
            LogEntries = logEntryRepository;
            FormMetadata = formMetadataRepository;
            DistributionForms = distributionFormRepository;
            FileDownloadStatuses = fileDownloadStatusRepository;
            _serviceBusPublisher = serviceBusPublisher;

            var useEnqueingConfig = configuration["FormProcessAPISettings:EnqueingEnabled"];
            if (!string.IsNullOrWhiteSpace(useEnqueingConfig))
                _useEnqueing = bool.Parse(useEnqueingConfig);
        }

        private string? _ftpbReferenceId;
        private string? _localStorageContainerName;

        public async Task InitiateAsync(string ftpbReferenceId, string localStorageContainerName)
        {
            _logger.LogDebug("Inititates unitOfWork for referenceId: {0}", ftpbReferenceId);
            _ftpbReferenceId = ftpbReferenceId;
            _localStorageContainerName = localStorageContainerName;
            await FormMetadata.IntiateAsync(_ftpbReferenceId, localStorageContainerName);
            await LogEntries.InitateAsync(_ftpbReferenceId, localStorageContainerName);
            await DistributionForms.InitiateAsync(_ftpbReferenceId, localStorageContainerName);
            await FileDownloadStatuses.InitiateAsync(_ftpbReferenceId, localStorageContainerName);
        }

        public IFormMetadataRepository FormMetadata { get; }
        public ILogEntryRepository LogEntries { get; }
        public IDistributionFormsRepository DistributionForms { get; }
        public IFileDownloadStatusRepository FileDownloadStatuses { get; }

        public async Task SaveFormMetadataAsync()
        {
            await FormMetadata.SaveAsync();
        }

        public async Task SaveLogEntriesAsync()
        {
            await LogEntries.SaveAsync();
        }

        public async Task SaveDistributionFormsAsync()
        {
            await DistributionForms.SaveAsync();
        }

        public async Task SaveFileDownloadStatusAsync()
        {
            await FileDownloadStatuses.SaveAsync();
        }

        public async Task SaveAsync()
        {
            _logger.LogDebug("Saves unitOfWork for {ReferenceId}", _ftpbReferenceId);
            await SaveFormMetadataAsync();
            await SaveLogEntriesAsync();
            await SaveDistributionFormsAsync();
            await SaveFileDownloadStatusAsync();

            if (_useEnqueing)
            {
                var message = new SyncToAlfaMessage
                {
                    FtpbReferenceId = _ftpbReferenceId,
                    AnSaKoReferenceId = _localStorageContainerName
                };

                await _serviceBusPublisher.PublishAsync(message);
            }
            else
                await SyncToAlfaAsync();
        }

        public async Task SyncToAlfaAsync()
        {
            _logger.LogInformation("Syncing {ReferenceId} to Alfa", _ftpbReferenceId);
            await FormMetadata.SyncToAlfa();
            await DistributionForms.SyncToAlfa();
            await FileDownloadStatuses.SyncToAlfa();
            await LogEntries.SyncToAlfa();
        }
    }
}