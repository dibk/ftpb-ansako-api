﻿using Azure.Identity;
using Dibk.Ftpb.Api.Client;
using Dibk.Ftpb.Api.Client.Interfaces;
using Dibk.Ftpb.Api.Client.Models.Mapping;
using Dibk.Ftpb.Api.Client.Repositories;
using Dibk.Ftpb.Api.Clients;
using DibkFtpb.Api.Client.LocalStore;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Text;

namespace DibkFtpb.Api.Client
{
    public static class FtpbApiExtensions
    {
        public static IServiceCollection AddFtpbApiClient(this IServiceCollection services, Uri baseUri, string userName, string password, int timeoutSeconds)
        {
            var auth = Encoding.ASCII.GetBytes($"{userName}:{password}");
            var authHeader = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(auth));

            services.AddScoped<IFtpbUnitOfWork, FtpbUnitOfWork>();
            services.AddScoped<IDistributionFormsRepository, DistributionFormsRepository>();
            services.AddScoped<ILogEntryRepository, LogEntryRepository>();
            services.AddScoped<IFormMetadataRepository, FormMetadataRepository>();
            services.AddScoped<IFileDownloadStatusRepository, FileDownloadStatusRepository>();

            services.AddFtpbApiClient(baseUri, userName, password);

            return services;
        }

        public static IServiceCollection AddLocalStore(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<LocalStoreService>();
            services.AddScoped<LocalStoreBlobService>();
            services.Configure<LocalStoreBlobStorageAccountSettings>(configuration.GetSection(LocalStoreBlobStorageAccountSettings.ConfigSection));

            services.AddAutoMapper(typeof(FormMetadataProfile));

            services.AddScoped<ServiceBusPublisher>();
            services.AddAzureClients(builder =>
            {
                var servuceBusNamespace = configuration["ServiceBus:Namespace"];
                builder.AddServiceBusClientWithNamespace(servuceBusNamespace)
                    .WithCredential(GetAzureCredentials(configuration))
                    .WithName(ServiceBusPublisher.ServiceBusClientName)
                    .ConfigureOptions(opts =>
                    { });
            });

            return services;
        }

        private static DefaultAzureCredential GetAzureCredentials(IConfiguration configuration)
        {
            var tenantId = configuration["Azure:TenantId"];
            if (!string.IsNullOrWhiteSpace(tenantId))
                return new DefaultAzureCredential(new DefaultAzureCredentialOptions() { TenantId = tenantId });
            else
                return new DefaultAzureCredential();
        }
    }
}