﻿using System.Diagnostics.CodeAnalysis;

namespace Dibk.Ftpb.Api.Client.Models
{
    public class FileDownloadStatusEqualtiyComparer : IEqualityComparer<FileDownloadStatus>
    {
        public bool Equals([AllowNull] FileDownloadStatus x, [AllowNull] FileDownloadStatus y)
        {
            bool result = x.ArchiveReference.Equals(y.ArchiveReference, StringComparison.OrdinalIgnoreCase)
                            && x.BlobLink.Equals(y.BlobLink, StringComparison.OrdinalIgnoreCase)
                            && x.Filename.Equals(y.Filename, StringComparison.OrdinalIgnoreCase)
                            && x.FileType == y.FileType
                            && x.FormName.Equals(y.FormName, StringComparison.OrdinalIgnoreCase)
                            && x.ContainerName.Equals(y.ContainerName, StringComparison.OrdinalIgnoreCase)
                            && x.MimeType.Equals(y.MimeType, StringComparison.OrdinalIgnoreCase);
            return result;
        }

        public int GetHashCode([DisallowNull] FileDownloadStatus obj)
        {
            var val = $"{obj.Id}{obj.ArchiveReference}{obj.BlobLink}{obj.Filename}{obj.FileType}{obj.FormName}{obj.ContainerName}{obj.MimeType}";
            return val.GetHashCode();
        }
    }
}