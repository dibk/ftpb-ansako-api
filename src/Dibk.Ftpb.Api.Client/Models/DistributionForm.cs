﻿using Dibk.Ftpb.Api.Models;
using DibkFtpb.Api.Client.LocalStore;

namespace Dibk.Ftpb.Api.Client.Models
{
    public class DistributionForm : DistributionFormApiModel
    { }

    [LocalStore("distribution-form.json")]
    public class DistributionFormsLocalStoreEntity : ILocalStoreEntity
    {
        public DistributionFormsLocalStoreEntity()
        {
            DistributionForms = new List<DistributionForm>();
        }

        public DateTime? DateUpdated { get; set; }
        public List<DistributionForm> DistributionForms { get; set; }
    }
}