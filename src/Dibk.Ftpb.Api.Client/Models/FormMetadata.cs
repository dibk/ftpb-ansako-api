﻿using Dibk.Ftpb.Api.Models;
using DibkFtpb.Api.Client.LocalStore;

namespace Dibk.Ftpb.Api.Client.Models
{
    public class FormMetadata : FormMetadataApiModel
    {
        public FormMetadata() : base()
        { }

        public FormMetadata(string archiveReference) : base()
        {
            ArchiveReference = archiveReference;
        }
    }

    [LocalStore("form-metadata.json")]
    public class FormMetadataLocalStoreEntity : ILocalStoreEntity
    {
        public FormMetadataLocalStoreEntity()
        {
            FormMetadata = new FormMetadata();
        }
        public FormMetadataLocalStoreEntity(string archiveReference)
        {
            FormMetadata = new FormMetadata(archiveReference);
        }

        public FormMetadataLocalStoreEntity(FormMetadata formMetadata)
        {
            FormMetadata = formMetadata;
        }

        public DateTime? DateUpdated { get; set; }
        public FormMetadata FormMetadata { get; set; }
    }
}