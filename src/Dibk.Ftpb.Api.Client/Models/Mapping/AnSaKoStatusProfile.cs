﻿using Dibk.Ftpb.Api.Models;

namespace Dibk.Ftpb.Api.Client.Models.Mapping
{
    public class AnSaKoStatusProfile : AutoMapper.Profile
    {
        public AnSaKoStatusProfile()
        {
            CreateMap<AnSaKoStatus, AnSaKoStatusApiModel>().ReverseMap();
        }
    }
}