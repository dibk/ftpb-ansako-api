﻿using Dibk.Ftpb.Api.Models;

namespace Dibk.Ftpb.Api.Client.Models.Mapping
{
    public class DistributionFormProfile : AutoMapper.Profile
    {
        public DistributionFormProfile()
        {
            CreateMap<DistributionForm, DistributionFormApiModel>().ReverseMap();
        }
    }
}