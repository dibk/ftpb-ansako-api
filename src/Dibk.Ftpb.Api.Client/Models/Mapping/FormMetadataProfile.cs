﻿using Dibk.Ftpb.Api.Models;

namespace Dibk.Ftpb.Api.Client.Models.Mapping
{
    public class FormMetadataProfile : AutoMapper.Profile
    {
        public FormMetadataProfile()
        {
            CreateMap<FormMetadata, FormMetadataApiModel>().ReverseMap();
        }
    }
}