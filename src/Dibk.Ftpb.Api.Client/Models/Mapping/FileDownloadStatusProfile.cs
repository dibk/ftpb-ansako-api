﻿using Dibk.Ftpb.Api.Models;

namespace Dibk.Ftpb.Api.Client.Models.Mapping
{
    public class FileDownloadStatusProfile : AutoMapper.Profile
    {
        public FileDownloadStatusProfile()
        {
            CreateMap<FileDownloadStatus, FileDownloadStatusApiModel>().ReverseMap();
        }
    }
}