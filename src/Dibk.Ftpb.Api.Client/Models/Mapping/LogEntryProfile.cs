﻿using Dibk.Ftpb.Api.Models;

namespace Dibk.Ftpb.Api.Client.Models.Mapping
{
    public class LogEntryProfile : AutoMapper.Profile
    {
        public LogEntryProfile()
        {
            CreateMap<LogEntry, LogEntryApiModel>().ReverseMap();
        }
    }
}