﻿using Dibk.Ftpb.Api.Client.Repositories;
using Dibk.Ftpb.Api.Models;
using DibkFtpb.Api.Client.LocalStore;

namespace Dibk.Ftpb.Api.Client.Models
{
    public class LogEntry : LogEntryApiModel
    {
        public const string Info = "Info";
        public const string Error = "Error";
        public const bool InternalMsg = true;
        public const bool ExternalMsg = false;

        public LogEntry() : base()
        { }

        public LogEntry(string archiveReference, string message, string type = "") : base()
        {
            ArchiveReference = archiveReference;
            Message = message;
            Type = type;
            Timestamp = DateTime.Now;
        }

        public LogEntry(string archiveReference, string message, string type, bool onlyInternal) : base()
        {
            ArchiveReference = archiveReference;
            Message = message;
            Type = type;
            Timestamp = DateTime.Now;
            OnlyInternal = onlyInternal;
        }
    }

    public class LogEntity
    {
        public LogEntry Log { get; set; }
        public FtpbAlfaEntityState State { get; set; }

        public LogEntity()
        { }

        public LogEntity(LogEntry log, FtpbAlfaEntityState state)
        {
            Log = log;
            State = state;
        }

        public LogEntity(LogEntry log)
        {
            Log = log;
            State = FtpbAlfaEntityState.Added;
        }
    }

    [LocalStore("log-entry.json")]
    public class LogEntriesLocalStoreEntity : ILocalStoreEntity
    {
        public DateTime? DateUpdated { get; set; }
        public List<LogEntity> LogEntries { get; set; }

        public LogEntriesLocalStoreEntity()
        { }

        public LogEntriesLocalStoreEntity(List<LogEntity> logEntries)
        {
            LogEntries = logEntries;
        }
    }
}