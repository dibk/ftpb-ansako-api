﻿using Dibk.Ftpb.Api.Models;
using DibkFtpb.Api.Client.LocalStore;

namespace Dibk.Ftpb.Api.Client.Models
{
    public class FileDownloadStatus : FileDownloadStatusApiModel
    { }

    [LocalStore("file-download-status.json")]
    public class FileDownloadStatusLocalStoreEntity : ILocalStoreEntity
    {
        public DateTime? DateUpdated { get; set; }

        public List<FileDownloadStatus> FileDownloadStatuses { get; set; }

        public FileDownloadStatusLocalStoreEntity()
        {
            FileDownloadStatuses = new List<FileDownloadStatus>();
        }

        public FileDownloadStatusLocalStoreEntity(List<FileDownloadStatus> fileDownloadStatuses)
        {
            FileDownloadStatuses = fileDownloadStatuses;
        }
    }
}