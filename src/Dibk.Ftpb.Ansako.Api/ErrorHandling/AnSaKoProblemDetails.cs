﻿using Microsoft.AspNetCore.Mvc;

namespace Dibk.Ftpb.Ansako.Api.ErrorHandling
{
    public static class AnSaKoProblemDetails
    {
        public static ProblemDetails Create<T>(T problemType, string title)
        {
            var pd = new ProblemDetails() { Title = title };
            pd.Extensions.Add("problemType", problemType.ToString());
            return pd;
        }
    }

    public enum SigningProblemType
    {
        ActiveSigningJobExists
    }

    public enum AttachmentProblemType
    {
        AttachmentProtected,
        AttachmentInfected,
        AttachmentFilenameTooLong,
        AttachmentEmpty,
        UnsupportedFileType,
    }
}
