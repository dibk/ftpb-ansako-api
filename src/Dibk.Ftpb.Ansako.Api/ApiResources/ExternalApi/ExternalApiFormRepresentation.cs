﻿using Dibk.Ftpb.Ansako.App.Models;
using Dibk.Ftpb.Ansako.Storage.Database.Models;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using WebApi.Hal;
using static Dibk.Ftpb.Ansako.App.Models.CreateFormResult;

namespace Dibk.Ftpb.Ansako.Api.ApiResources.ExternalApi
{
    public class ExternalApiFormRepresentation : Representation
    {
        [JsonIgnore]
        public ErklaeringType FormType { get; set; }
        public string ReferanseId { get; set; }
        public string DataFormatId { get; set; }
        public string DataFormatVersion { get; set; }
        public CreateFormMetadata FormMetadata { get; set; }
            
        public IEnumerable<AnsvarsomraadeResult> AnsvarsomraadeReferanser { get; set; }

        private string _frontendHost;
        private readonly string _ansakoReferenceId;

        public ExternalApiFormRepresentation(string frontendHost, string ansakoReferenceId)
        {
            _frontendHost = frontendHost;
            _ansakoReferenceId = ansakoReferenceId;
            FormMetadata = new CreateFormMetadata();
        }

        protected override void CreateHypermedia()
        {
            var urlTemplate = _frontendHost + "/skjema/{ansakoReferenceId}";
            Links.Add(new Link("utfylling_av_skjema", urlTemplate).CreateLink(new { ansakoReferenceId = _ansakoReferenceId }));
            Links.Add(ExternalApiLinkTemplates.Innsendinger.SetStatus.CreateLink(new { ftpbReferenceId = ReferanseId }));
        }
    }
}
