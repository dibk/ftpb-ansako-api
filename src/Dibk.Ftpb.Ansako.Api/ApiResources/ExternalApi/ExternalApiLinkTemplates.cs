﻿using WebApi.Hal;

namespace Dibk.Ftpb.Ansako.Api.ApiResources.ExternalApi
{
    public static class ExternalApiLinkTemplates
    {
        public static class Innsendinger
        {
            public static Link SetStatus { get { return new Link("sett_status", "~/api/v1/skjemainnsending/{ftpbReferenceId}/status"); } }

        }
    }
}
