﻿using System;

namespace Dibk.Ftpb.Ansako.Api.ApiResources.InternalApi
{
    public class Attachment
    {
        public string Filnavn { get; set; }
        public string Vedleggstype { get; set; }
        public string MimeType { get; set; }
    }
}
