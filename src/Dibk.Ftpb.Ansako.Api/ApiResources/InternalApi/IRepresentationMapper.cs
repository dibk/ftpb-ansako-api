﻿using Dibk.Ftpb.Ansako.App.Models.InternalApi;

namespace Dibk.Ftpb.Ansako.Api.ApiResources.InternalApi
{
    public interface IRepresentationMapper<TRep, TDto> where TRep : IInnsendingRepresentation where TDto : IFormDto
    {
        TRep Map(TDto dto);
        TDto Map(TRep rep);
    }
}
