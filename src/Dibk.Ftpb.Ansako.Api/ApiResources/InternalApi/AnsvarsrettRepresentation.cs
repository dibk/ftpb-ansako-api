﻿using Dibk.Ftpb.Ansako.App.Models.InternalApi;

namespace Dibk.Ftpb.Ansako.Api.ApiResources.InternalApi
{
    public class AnsvarsrettRepresentation : InnsendingsTypeRepresentation<AnsvarsrettModel>
    {
        public AnsvarsrettRepresentation()
        {
            Innsendingstype = Storage.Database.Models.ErklaeringType.Ansvarsrett;
        }
        public override string Href
        {

            get { return InternalApiLinkTemplates.Ansvarsretter.Ansvarsrett.CreateLink(new { ansakoReferenceId = ReferanseId }).Href; }
            set { }
        }

        public override string Rel
        {
            get { return InternalApiLinkTemplates.Ansvarsretter.Ansvarsrett.Rel; }
            set { }
        }
        protected override void CreateHypermedia()
        {
            Links.Add(InternalApiLinkTemplates.Ansvarsretter.Ansvarsrett.CreateLink(new { ansakoReferenceId = ReferanseId }));
            Links.Add(InternalApiLinkTemplates.Ansvarsretter.AnsvarsrettReject.CreateLink(new { ansakoReferenceId = ReferanseId }));
            Links.Add(InternalApiLinkTemplates.Ansvarsretter.AnsvarsrettSignering.CreateLink(new { ansakoReferenceId = ReferanseId }));
        }
    }
}
