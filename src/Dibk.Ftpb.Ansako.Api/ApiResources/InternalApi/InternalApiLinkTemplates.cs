﻿using WebApi.Hal;

namespace Dibk.Ftpb.Ansako.Api.ApiResources.InternalApi
{
    public static class InternalApiLinkTemplates
    {
        public static class Innsendinger
        {
            public static Link Innsending { get { return new Link("innsending", "~/api/v1/innsending/{ansakoReferenceId}"); } }
        }

        public static class Ansvarsretter
        {
            public static Link Ansvarsrett { get { return new Link("ansvarsrett", "~/api/v1/ansvarsrett/{ansakoReferenceId}"); } }
            public static Link AnsvarsrettReject { get { return new Link("avvis", "~/api/v1/ansvarsrett/{ansakoReferenceId}/reject"); } }
            public static Link AnsvarsrettSignering { get { return new Link("signering", "~/api/v1/ansvarsrett/{ansakoReferenceId}/signering"); } }
        }

        public static class Samsvarserklaeringer
        {
            public static Link Samsvarserklaering { get { return new Link("samsvarserklaering", "~/api/v1/samsvarserklaering/{ansakoReferenceId}"); } }
            public static Link SamsvarserklaeringReject { get { return new Link("avvis", "~/api/v1/samsvarserklaering/{ansakoReferenceId}/reject"); } }
            public static Link SamsvarserklaeringSignering { get { return new Link("signering", "~/api/v1/samsvarserklaering/{ansakoReferenceId}/signering"); } }
        }

        public static class Kontrollerklaeringer
        {
            public static Link Kontrollerklaering { get { return new Link("kontrollerklaering", "~/api/v1/kontrollerklaering/{ansakoReferenceId}"); } }
            public static Link KontrollerklaeringReject { get { return new Link("avvis", "~/api/v1/kontrollerklaering/{ansakoReferenceId}/reject"); } }
            public static Link KontrollerklaeringSignering { get { return new Link("signering", "~/api/v1/kontrollerklaering/{ansakoReferenceId}/signering"); } }
            public static Link KontrollerklaeringAttachment { get { return new Link("vedlegg", "~/api/v1/kontrollerklaering/{ansakoReferenceId}/attachment"); } }
        }
    }
}
