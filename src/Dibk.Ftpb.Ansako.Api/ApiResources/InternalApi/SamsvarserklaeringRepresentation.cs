﻿using Dibk.Ftpb.Ansako.App.Models.InternalApi;

namespace Dibk.Ftpb.Ansako.Api.ApiResources.InternalApi
{
    public class SamsvarserklaeringRepresentation : InnsendingsTypeRepresentation<SamsvarserklaeringModel>
    {
        public SamsvarserklaeringRepresentation()
        {
            Innsendingstype = Storage.Database.Models.ErklaeringType.Samsvarserklaering;
        }
        public override string Href
        {

            get { return InternalApiLinkTemplates.Samsvarserklaeringer.Samsvarserklaering.CreateLink(new { ansakoReferenceId = ReferanseId }).Href; }
            set { }
        }

        public override string Rel
        {
            get { return InternalApiLinkTemplates.Samsvarserklaeringer.Samsvarserklaering.Rel; }
            set { }
        }

        protected override void CreateHypermedia()
        {
            Links.Add(InternalApiLinkTemplates.Samsvarserklaeringer.Samsvarserklaering.CreateLink(new { ansakoReferenceId = ReferanseId }));
            Links.Add(InternalApiLinkTemplates.Samsvarserklaeringer.SamsvarserklaeringReject.CreateLink(new { ansakoReferenceId = ReferanseId }));
            Links.Add(InternalApiLinkTemplates.Samsvarserklaeringer.SamsvarserklaeringSignering.CreateLink(new { ansakoReferenceId = ReferanseId }));
        }
    }
}
