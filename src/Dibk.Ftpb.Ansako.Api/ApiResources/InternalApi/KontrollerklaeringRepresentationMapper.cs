﻿using Dibk.Ftpb.Ansako.App.Mappers;
using Dibk.Ftpb.Ansako.App.Models.InternalApi;

namespace Dibk.Ftpb.Ansako.Api.ApiResources.InternalApi
{
    public class KontrollerklaeringRepresentationMapper : IRepresentationMapper<KontrollerklaeringRepresentation, KontrollerklaeringDto>
    {
        public KontrollerklaeringRepresentation Map(KontrollerklaeringDto dto)
        {
            var kr = new KontrollerklaeringRepresentation()
            {
                ReferanseId = dto.AnSaKoReferenceId,
                Status = FormStatusToAnSaKoProcessStatusMapper.GetStatus(dto.Status),
                FormData = dto.FormData,
                Signeringsstatus = dto.SigningJobStatus,
                StatusReason = dto.StatusDetails,
                Signeringsfrist = dto.Signeringsfrist,
                SignertDato = dto.SignertDato,
                FtpbReferanseId = dto.FtpbReferenceId
            };
            if (dto.Attachment != null)
                kr.Vedlegg = new Attachment()
                {
                    Filnavn = dto.Attachment.FileName,
                    Vedleggstype = dto.Attachment.AttachmentType,
                    MimeType = dto.Attachment.MimeType
                };

            return kr;
        }

        public KontrollerklaeringDto Map(KontrollerklaeringRepresentation rep)
        {
            return new KontrollerklaeringDto()
            {
                AnSaKoReferenceId = rep.ReferanseId,
                Status = FormStatusToAnSaKoProcessStatusMapper.GetStatus(rep.Status),
                FormData = rep.FormData,
                SigningJobStatus = rep.Signeringsstatus,
                StatusDetails = rep.StatusReason,
                FtpbReferenceId = rep.FtpbReferanseId
            };
        }
    }
}
