﻿using Dibk.Ftpb.Ansako.App.Mappers;
using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Dibk.Ftpb.Api.Models;
using System;
using WebApi.Hal;

namespace Dibk.Ftpb.Ansako.Api.ApiResources.InternalApi
{
    public interface IInnsendingRepresentation
    {
        ErklaeringType Innsendingstype { get; set; }
        string ReferanseId { get; set; }
        AnSaKoProcessStatusType Status { get; set; }
        SigningJobStatus? Signeringsstatus { get; set; }
        string StatusReason { get; set; }
        DateTime? Signeringsfrist { get; set; }
        DateTimeOffset? SignertDato { get; set; }
        string FtpbReferanseId { get; set; }
    }

    public class InnsendingsTypeRepresentation<T> : Representation, IInnsendingRepresentation
    {
        public ErklaeringType Innsendingstype { get; set; }
        public string ReferanseId { get; set; }
        public AnSaKoProcessStatusType Status { get; set; }
        public SigningJobStatus? Signeringsstatus { get; set; }
        public string StatusReason { get; set; }
        public DateTime? Signeringsfrist { get; set; }
        public DateTimeOffset? SignertDato { get; set; }
        public string FtpbReferanseId { get; set; }
        public T FormData { get; set; }

        internal static FormStatus GetStatus(AnSaKoProcessStatusType anSaKoProcessStatusType)
        {
            return FormStatusToAnSaKoProcessStatusMapper.GetStatus(anSaKoProcessStatusType);
        }

        internal static AnSaKoProcessStatusType GetStatus(FormStatus formStatus)
        {
            return FormStatusToAnSaKoProcessStatusMapper.GetStatus(formStatus);
        }
    }
}