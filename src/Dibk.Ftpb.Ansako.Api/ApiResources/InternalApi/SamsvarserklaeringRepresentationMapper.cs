﻿using Dibk.Ftpb.Ansako.App.Mappers;
using Dibk.Ftpb.Ansako.App.Models.InternalApi;

namespace Dibk.Ftpb.Ansako.Api.ApiResources.InternalApi
{
    public class SamsvarserklaeringRepresentationMapper : IRepresentationMapper<SamsvarserklaeringRepresentation, SamsvarserklaeringDto>
    {
        public SamsvarserklaeringRepresentation Map(SamsvarserklaeringDto dto)
        {
            return new SamsvarserklaeringRepresentation()
            {
                ReferanseId = dto.AnSaKoReferenceId,
                Status = FormStatusToAnSaKoProcessStatusMapper.GetStatus(dto.Status),
                FormData = dto.FormData,
                Signeringsstatus = dto.SigningJobStatus,
                StatusReason = dto.StatusDetails,
                Signeringsfrist = dto.Signeringsfrist,
                SignertDato = dto.SignertDato,
                FtpbReferanseId = dto.FtpbReferenceId
            };
        }

        public SamsvarserklaeringDto Map(SamsvarserklaeringRepresentation rep)
        {
            return new SamsvarserklaeringDto()
            {
                AnSaKoReferenceId = rep.ReferanseId,
                Status = FormStatusToAnSaKoProcessStatusMapper.GetStatus(rep.Status),
                FormData = rep.FormData,
                SigningJobStatus = rep.Signeringsstatus,
                StatusDetails = rep.StatusReason,
                FtpbReferenceId = rep.FtpbReferanseId
            };
        }
    }
}
