﻿using Dibk.Ftpb.Ansako.App.Mappers;
using Dibk.Ftpb.Ansako.App.Models.InternalApi;
using System;
using System.Linq;

namespace Dibk.Ftpb.Ansako.Api.ApiResources.InternalApi
{
    public class AnsvarsrettRepresentationMapper : IRepresentationMapper<AnsvarsrettRepresentation, AnsvarsrettDto>
    {
        public AnsvarsrettRepresentation Map(AnsvarsrettDto dto)
        {
            var rep = new AnsvarsrettRepresentation()
            {
                ReferanseId = dto.AnSaKoReferenceId,
                Status = FormStatusToAnSaKoProcessStatusMapper.GetStatus(dto.Status),
                FormData = dto.FormData,
                StatusReason = dto.StatusDetails,
                Signeringsfrist = dto.Signeringsfrist,
                SignertDato = dto.SignertDato,
                FtpbReferanseId = dto.FtpbReferenceId
            };

            rep.Signeringsstatus = dto.SigningJobStatus;

            string[] order = new string[] { "PRO", "UTF", "KONTROLL", "SØK" };

            //Sort ansvarsområder
            var ans = rep.FormData.Ansvarsomraader.OrderBy(a => Array.IndexOf(order, a.FunksjonKode)).ToList();
            rep.FormData.Ansvarsomraader = ans;
            return rep;
        }

        public AnsvarsrettDto Map(AnsvarsrettRepresentation rep)
        {
            return new AnsvarsrettDto()
            {
                AnSaKoReferenceId = rep.ReferanseId,
                Status = FormStatusToAnSaKoProcessStatusMapper.GetStatus(rep.Status),
                FormData = rep.FormData,
                SigningJobStatus = rep.Signeringsstatus,
                StatusDetails = rep.StatusReason,
                Signeringsfrist = rep.Signeringsfrist,
                FtpbReferenceId = rep.FtpbReferanseId,
            };
        }
    }
}
