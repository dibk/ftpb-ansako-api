﻿using Dibk.Ftpb.Ansako.Storage.Database.Models;
using System;
using WebApi.Hal;

namespace Dibk.Ftpb.Ansako.Api.ApiResources.InternalApi
{
    public class AnSaKoRepresentation : Representation
    {
        public ErklaeringType Innsendingstype { get; set; }
        public string ReferanseId { get; set; }
        public DateTime? MottattDato { get; set; }
        public FormStatus Status { get; set; }
        public string DataFormatId { get; set; }
        public string DataFormatVersion { get; set; }
        public DateTime? SlettetDato { get; set; }        

        public override string Href
        {

            get { return InternalApiLinkTemplates.Innsendinger.Innsending.CreateLink(new { ansakoReferenceId = ReferanseId }).Href; }
            set { }
        }

        public override string Rel
        {
            get { return InternalApiLinkTemplates.Innsendinger.Innsending.Rel; }
            set { }
        }

        protected override void CreateHypermedia()
        {
            switch (Innsendingstype)
            {
                case ErklaeringType.Ansvarsrett:
                    Links.Add(InternalApiLinkTemplates.Ansvarsretter.Ansvarsrett.CreateLink(new { ansakoReferenceId = ReferanseId }));
                    Links.Add(InternalApiLinkTemplates.Ansvarsretter.AnsvarsrettReject.CreateLink(new { ansakoReferenceId = ReferanseId }));
                    Links.Add(InternalApiLinkTemplates.Ansvarsretter.AnsvarsrettSignering.CreateLink(new { ansakoReferenceId = ReferanseId }));
                    break;
                case ErklaeringType.Samsvarserklaering:
                    Links.Add(InternalApiLinkTemplates.Samsvarserklaeringer.Samsvarserklaering.CreateLink(new { ansakoReferenceId = ReferanseId }));
                    Links.Add(InternalApiLinkTemplates.Samsvarserklaeringer.SamsvarserklaeringReject.CreateLink(new { ansakoReferenceId = ReferanseId }));
                    Links.Add(InternalApiLinkTemplates.Samsvarserklaeringer.SamsvarserklaeringSignering.CreateLink(new { ansakoReferenceId = ReferanseId }));
                    break;
                case ErklaeringType.Kontrollerklaering:
                    Links.Add(InternalApiLinkTemplates.Kontrollerklaeringer.Kontrollerklaering.CreateLink(new { ansakoReferenceId = ReferanseId }));
                    Links.Add(InternalApiLinkTemplates.Kontrollerklaeringer.KontrollerklaeringReject.CreateLink(new { ansakoReferenceId = ReferanseId }));
                    Links.Add(InternalApiLinkTemplates.Kontrollerklaeringer.KontrollerklaeringSignering.CreateLink(new { ansakoReferenceId = ReferanseId }));
                    break;
                default:
                    break;
            }
        }
    }
}
