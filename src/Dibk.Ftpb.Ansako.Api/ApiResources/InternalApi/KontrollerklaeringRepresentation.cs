﻿using Dibk.Ftpb.Ansako.App.Models.InternalApi;

namespace Dibk.Ftpb.Ansako.Api.ApiResources.InternalApi
{
    public class KontrollerklaeringRepresentation : InnsendingsTypeRepresentation<KontrollerklaeringModel>
    {
        public KontrollerklaeringRepresentation()
        {
            Innsendingstype = Storage.Database.Models.ErklaeringType.Kontrollerklaering;
        }

        public Attachment Vedlegg { get; set; }

        public override string Href
        {

            get { return InternalApiLinkTemplates.Kontrollerklaeringer.Kontrollerklaering.CreateLink(new { ansakoReferenceId = ReferanseId }).Href; }
            set { }
        }

        public override string Rel
        {
            get { return InternalApiLinkTemplates.Kontrollerklaeringer.Kontrollerklaering.Rel; }
            set { }
        }

        protected override void CreateHypermedia()
        {
            Links.Add(InternalApiLinkTemplates.Kontrollerklaeringer.Kontrollerklaering.CreateLink(new { ansakoReferenceId = ReferanseId }));
            Links.Add(InternalApiLinkTemplates.Kontrollerklaeringer.KontrollerklaeringReject.CreateLink(new { ansakoReferenceId = ReferanseId }));
            Links.Add(InternalApiLinkTemplates.Kontrollerklaeringer.KontrollerklaeringSignering.CreateLink(new { ansakoReferenceId = ReferanseId }));
            Links.Add(InternalApiLinkTemplates.Kontrollerklaeringer.KontrollerklaeringAttachment.CreateLink(new { ansakoReferenceId = ReferanseId }));
        }
    }
}
