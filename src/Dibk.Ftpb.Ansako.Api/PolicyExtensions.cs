﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Polly;
using System;

namespace Dibk.Ftpb.Ansako.Api
{
    internal static class PolicyExtensions
    {
        public static IHttpClientBuilder AddRetryPolicy<T>(this IHttpClientBuilder httpClientBuilder, IServiceCollection services)
        {
            return httpClientBuilder.AddTransientHttpErrorPolicy(policyBuilder =>
                                                  policyBuilder.WaitAndRetryAsync(3, retryNumber => TimeSpan.FromSeconds(Math.Pow(2, retryNumber)),
                                                  onRetry: (response, timespan) =>
                                                  {

                                                      var logger = services.BuildServiceProvider().GetRequiredService<ILogger<T>>();
                                                      if (response.Result != null)
                                                          logger.LogError(response.Exception, $"Error occured on {typeof(T).Name} - StatusCode: {(int)response.Result?.StatusCode} - {response.Result?.ReasonPhrase} - Retrying after {timespan.ToString()}");
                                                      else
                                                          logger.LogError(response.Exception, $"Error occured on {typeof(T).Name} - Retrying after {timespan.ToString()}");
                                                  }));
        }
    }
}