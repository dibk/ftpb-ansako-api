using Azure.Identity;
using Dibk.Ftpb.Ansako.Api.Authentication;
using Dibk.Ftpb.Ansako.Api.BackgroundServices;
using Dibk.Ftpb.Ansako.Api.BackgroundServices.QueueWorkers;
using Dibk.Ftpb.Ansako.Api.Logging;
using Dibk.Ftpb.Ansako.App;
using Dibk.Ftpb.Ansako.App.HttpClients.Email;
using Dibk.Ftpb.Ansako.App.HttpClients.Pdf;
using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.App.Services;
using Dibk.Ftpb.Ansako.App.Services.Admin;
using Dibk.Ftpb.Ansako.App.Services.RecurringTaskServices;
using Dibk.Ftpb.Ansako.App.Services.Signing;
using Dibk.Ftpb.Ansako.Authentication;
using Dibk.Ftpb.Ansako.IntegrationTest;
using Dibk.Ftpb.Ansako.Storage;
using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using Dibk.Ftpb.Ansako.Storage.Database;
using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Dibk.Ftpb.Ansako.Storage.Repositories;
using Dibk.Ftpb.Ansako.Storage.SearchEngine;
using Dibk.Ftpb.Ansako.Storage.Service;
using Dibk.Ftpb.Signing;
using Dibk.Ftpb.Signing.eSignering;
using DibkFtpb.Api.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Serilog;
using System;
using WebApi;

namespace Dibk.Ftpb.Ansako.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureApp(services);

            ConfigureAuthentication(services);

            ConfigureCors(services);

            //External API services
            services.AddScoped<ExternalApiFormService>();
            services.Configure<CreateFormSettings>(Configuration.GetSection(CreateFormSettings.ConfigSection));
            services.Configure<AnSaKoSystemSettings>(Configuration.GetSection(AnSaKoSystemSettings.ConfigSection));

            //Admin services
            services.AddScoped<AttachmentSyncService>();
            services.AddScoped<IServiceStateService, ServiceStateService>();
            services.AddScoped<BaseDbRepository<ServiceStateDbModel>>();

            //Internal API services
            services.AddScoped<IFormService, AnsvarsrettService>();
            services.AddScoped<IFormService, KontrollerklaeringService>();
            services.AddScoped<IFormService, SamsvarserklaeringService>();
            services.AddScoped<FormServiceProvider>();
            services.AddScoped<IAttachmentService, AttachmentService>();
            services.AddScoped<PdfGeneratingService>();
            services.AddScoped<InternalApiFormService>();

            //Storage services
            services.AddDbContext<AnSaKoDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
                sqlServerOptionsAction: sqlOptions =>
                {
                    sqlOptions.EnableRetryOnFailure(
                        maxRetryCount: 5,
                        maxRetryDelay: TimeSpan.FromSeconds(5),
                        errorNumbersToAdd: null
                        );
                });
            });

            services.AddScoped<FormRepo>();
            services.AddScoped<FormDbRepository>();

            services.AddScoped<IBlobService, AnSaKoBlobService>();
            services.Configure<AnSaKoBlobStorageAccountSettings>(Configuration.GetSection(AnSaKoBlobStorageAccountSettings.ConfigSection));
            services.AddScoped<BaseDbRepository<FormDbModel>>();

            services.Configure<ElasticSettings>(Configuration.GetSection(SerilogSettings.ConfigSection));
            services.AddScoped<ElasticService>();

            //Sgregistry cache service
            services.Configure<SgRegistrySettings>(Configuration.GetSection(SgRegistrySettings.ConfigSection));
            services.AddHttpClient<SgRegistryService>(o => o.BaseAddress = Configuration.GetValue<Uri>("SgRegistryUrl"))
                .AddRetryPolicy<SgRegistryService>(services)
                .AddHeaderPropagation();

            // ClamAV service
            services.Configure<ClamAVSettings>(Configuration.GetSection("ClamAV"));
            services.AddScoped<ClamAVService>();

            //PDF generator API client
            services.Configure<PdfSettings>(Configuration.GetSection(PdfSettings.SectionName));
            services.AddHttpClient<PdfHttpClient>()
                .AddRetryPolicy<PdfHttpClient>(services)
                .AddHeaderPropagation();

            //Ftpb Unit of Work
            if (!int.TryParse(Configuration["FormProcessAPISettings:TimeoutSeconds"], out var ftpbTimeoutSeconds))
                ftpbTimeoutSeconds = 60;

            services.AddFtpbApiClient(new Uri(Configuration["FormProcessAPISettings:Uri"]),
                                      Configuration["FormProcessAPISettings:BasicAuthUserName"],
                                      Configuration["FormProcessAPISettings:BasicAuthPassword"],
                                      ftpbTimeoutSeconds);

            services.AddLocalStore(Configuration);

            services.AddScoped<FtpbBlobService>();
            services.Configure<FtpbBlobStorageAccountSettings>(Configuration.GetSection(FtpbBlobStorageAccountSettings.ConfigSection));

            //Email API client
            services.AddScoped<EmailService>();
            services.AddHttpClient<EmailHttpClient>(
                client => client.BaseAddress = new Uri(Configuration["AnSaKo:Email:Uri"])
                )
                .AddRetryPolicy<EmailHttpClient>(services);
            services.AddScoped<EmailMessagePublisher>();

            services.AddAzureClients(builder =>
            {
                var servuceBusNamespace = Configuration["ServiceBus:Namespace"];
                builder.AddServiceBusClientWithNamespace(servuceBusNamespace)
                    .WithCredential(GetAzureCredentials(Configuration))
                    .WithName(EmailMessagePublisher.ServiceBusClientName)
                    .ConfigureOptions(opts => { });
            });

            //Signing services
            services.AddScoped<SignerFactory>();
            services.AddScoped<SigneringsService>();
            // -- eSignering
            services.AddESignering(Configuration);

            // -- test signering TO BE DELETED
            services.AddScoped<ISigningMetadataBuilder, TestSigningMetadataBuilder>();
            services.AddScoped<ISigner, TestSigner>();

            //eSignering Integrationtest
            services.AddSigningIntegtaionTest();

            //Clean up..
            services.AddScoped<DeadlineExpiredService>();
            services.AddScoped<StatusChangedService>();
            services.AddHostedService<StatusChangedWorker>();
            services.AddScoped<StatusChangedMessagePublisher>();
            services.AddAzureClients(builder =>
            {
                var servuceBusNamespace = Configuration["ServiceBus:Namespace"];
                builder.AddServiceBusClientWithNamespace(servuceBusNamespace)
                    .WithCredential(GetAzureCredentials(Configuration))
                    .WithName(StatusChangedMessagePublisher.ServiceBusClientName)
                    .ConfigureOptions(opts => { });
            });

            services.AddScoped<DeleteFormCleanUpService>();
            if (bool.TryParse(Configuration["AnSaKo:RecurringJobs:DeleteFormCleanUp:Enabled"], out var parsedEnabled))
                if (parsedEnabled)
                    services.AddHostedService<DeleteFormBackgroundService>();

            //Add hosted services if enabled
            if (bool.TryParse(Configuration["FormProcessAPISettings:EnqueingEnabled"], out var alfaSyncEnqueingEnabled))
                if (alfaSyncEnqueingEnabled)
                    services.AddHostedService<SyncToAlfaQueueWorker>();

            if (bool.TryParse(Configuration["AnSaKo:Email:EnqueingEnabled"], out var emailEnqueingEnabled))
                if (emailEnqueingEnabled)
                    services.AddHostedService<EmailQueueWorker>();
        }

        private void ConfigureApp(IServiceCollection services)
        {
            services.AddControllers();

            //Logging and enrichment
            services.AddHttpLogging(o => o = new Microsoft.AspNetCore.HttpLogging.HttpLoggingOptions());
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddSerilog(SerilogConfiguration.ConfigureLogging(Configuration).CreateLogger());
            });

            Log.Logger = SerilogConfiguration.ConfigureLogging(Configuration).CreateLogger();

            services.AddHttpContextAccessor();
            services.AddHeaderPropagation(o =>
            {
                o.Headers.Add("x-correlation-id");
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Dibk.Ftpb.Ansako.Api", Version = "v3" });
                c.CustomSchemaIds(type => type.FullName);
            });

            services.AddHealthChecks()
                .AddSqlServer(Configuration["ConnectionStrings:DefaultConnection"])
                .AddDbContextCheck<AnSaKoDbContext>()
                .AddAzureServiceBusQueue(Configuration["ServiceBus:Namespace"], Configuration["AnSaKo:Email:QueueName"], GetAzureCredentials(Configuration), name: "EmailQueueHealthCheck")
                .AddAzureServiceBusQueue(Configuration["ServiceBus:Namespace"], Configuration["FormProcessAPISettings:QueueName"], GetAzureCredentials(Configuration), name: "SyncDataToAlfaCheck");

            services.AddAllElasticApm();

            //Formatting
            services.AddMvcCore()
                    .AddNewtonsoftJson(
                    options =>
                    {
                        options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                        options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                        options.SerializerSettings.Converters.Add(new StringEnumConverter(new CamelCaseNamingStrategy()));
                    })
                .AddApiExplorer();
            services.TryAddEnumerable(ServiceDescriptor.Transient<IConfigureOptions<MvcOptions>, FormattersMvcOptionsSetup>());
        }

        private void ConfigureCors(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(name: "AllowAll",
                    builder =>
                    {
                        builder.AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowAnyOrigin()
                        .WithExposedHeaders(new string[] { "Content-Disposition", "Date", "x-correlation-id" });
                    });

                options.AddDefaultPolicy(
                    builder =>
                    {
                        var allowedOriginsSetting = Configuration["AnSaKo:Cors:AllowedOrigins"];
                        var allowedOrigins = allowedOriginsSetting.Split(',', StringSplitOptions.TrimEntries & StringSplitOptions.RemoveEmptyEntries);

                        builder.WithOrigins(allowedOrigins)
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .WithExposedHeaders(new string[] { "Content-Disposition", "Date", "x-correlation-id" });
                    });
            });
        }

        private void ConfigureAuthentication(IServiceCollection services)
        {
            //Authentication - Start
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddIdPortenConfiguration(Configuration["AnSaKo:Authentication:IdPorten:Issuer"], Configuration["AnSaKo:Authentication:IdPorten:Audience"])
                .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("basic-authentication", options => { })
                .AddMaskinPortenConfiguration(Configuration["AnSaKo:Authentication:Maskinporten:Issuer"], Configuration["AnSaKo:Authentication:Maskinporten:Audience"]);

            services.AddHttpClient("ftpbAuthClient", options => { options.BaseAddress = new Uri(Configuration["AnSaKo:Authentication:BasicAuth:Host"]); })
                            .AddRetryPolicy<BasicAuthenticationHandler>(services);

            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder()
                .RequireAuthenticatedUser()
                .AddAuthenticationSchemes("idporten", "basic-authentication", "maskinporten")
                .Build();

                options.AddPolicy("basic-authentication", policy =>
                {
                    policy.AddAuthenticationSchemes("basic-authentication");
                    policy.RequireAuthenticatedUser();
                });
                options.AddPolicy("maskinporten", policy =>
                {
                    policy.AddAuthenticationSchemes("maskinporten");
                    policy.RequireAuthenticatedUser();
                });
                options.AddPolicy("idporten", policy =>
                {
                    policy.AddAuthenticationSchemes("idporten");
                    policy.RequireAuthenticatedUser();
                });
            });

            if (!bool.Parse(Configuration["AnSaKo:Authentication:AuthenticationEnabled"] ?? "false"))
            {
                //Allows auth to be bypassed
                services.AddSingleton<IAuthorizationHandler, AllowAnonymousAuthorizationHandler>();
            }
            //Authentication - End
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UserCorrelationIdMiddleware();
            app.UseSerilogRequestLogging(opts =>
            {
                opts.EnrichDiagnosticContext = (diagnosticContext, httpContext) =>
                {
                    diagnosticContext.Set("UrlQuery", httpContext.Request.QueryString.Value?.Trim('?'));
                };
            });
            app.UseHttpLogging();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Dibk.Ftpb.Ansako.Api v1"));
            }

            app.Use(async (context, next) =>
            {
                context.Response.Headers.Append("x-robots-tag", "noindex, nofollow");
                context.Response.Headers.Append("cache-control", "no-store");
                await next();
            });

            app.UseStaticFiles(); // For the wwwroot folder and hosting robots.txt

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseCors();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseHeaderPropagation();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health");
                endpoints.MapGet("/ping", () => "pong");
            });
        }

        private static DefaultAzureCredential GetAzureCredentials(IConfiguration configuration)
        {
            var tenantId = configuration["Azure:TenantId"];
            if (!string.IsNullOrWhiteSpace(tenantId))
                return new DefaultAzureCredential(new DefaultAzureCredentialOptions() { TenantId = tenantId });
            else
                return new DefaultAzureCredential();
        }
    }
}