using Azure.Messaging.ServiceBus;
using Dibk.Ftpb.Ansako.App.Services.RecurringTaskServices;
using Elastic.Apm;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Api.BackgroundServices.QueueWorkers
{
    internal class StatusChangedWorker : BaseQueueWorker
    {
        protected override string QueueName => "sbq-ftpb-ansako-statuschanged";

        protected override string ApmTransactionName => "StatusChanged";

        public StatusChangedWorker(IApmAgent apmAgent,
                                ILogger<StatusChangedWorker> logger,
                                IConfiguration configuration,
                                IAzureClientFactory<ServiceBusClient> serviceBusClientFactory,
                                IServiceProvider services) : base(apmAgent,
                                                                        logger,
                                                                        configuration,
                                                                        serviceBusClientFactory,
                                                                        services)
        {
            var queueName = configuration["AnSaKo:StatusChanged:QueueName"];
            if (!string.IsNullOrWhiteSpace(queueName))
                QueueName = queueName;
        }

        protected override async Task DoWork(ProcessMessageEventArgs args)
        {
            var body = args.Message.Body.ToString();
            var statusChangedQueueMessage = JsonSerializer.Deserialize<StatusChangedQueueMessage>(body);

            using (var scope = _services.CreateScope())
            {
                var logger = scope.ServiceProvider.GetRequiredService<ILogger<StatusChangedWorker>>();
                using (var loggerscope = logger.BeginScope(new Dictionary<string, string>
                {
                    { "AnSaKoReferenceId", statusChangedQueueMessage.AnSaKoReferenceId }
                }))
                {
                    var client = scope.ServiceProvider.GetRequiredService<StatusChangedService>();

                    await client.SetStatus(statusChangedQueueMessage);
                }
            }
        }
    }
}