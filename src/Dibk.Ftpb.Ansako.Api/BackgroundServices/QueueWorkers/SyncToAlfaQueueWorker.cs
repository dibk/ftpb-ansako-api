using Azure.Messaging.ServiceBus;
using Dibk.Ftpb.Api.Client;
using Dibk.Ftpb.Api.Client.Interfaces;
using Elastic.Apm;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Api.BackgroundServices.QueueWorkers
{
    internal class SyncToAlfaQueueWorker : BaseQueueWorker
    {
        protected override string QueueName => "sbq-ftpb-ansako-alfa-sync";

        protected override string ApmTransactionName => "SyncDataToAlfa";

        public SyncToAlfaQueueWorker(IApmAgent apmAgent,
                                   ILogger<SyncToAlfaQueueWorker> logger,
                                   IConfiguration configuration,
                                   IAzureClientFactory<ServiceBusClient> serviceBusClientFactory,
                                   IServiceProvider services) : base(apmAgent,
                                                                     logger,
                                                                     configuration,
                                                                     serviceBusClientFactory,
                                                                     services)
        {
            var queueName = configuration["FormProcessAPISettings:QueueName"];
            if (!string.IsNullOrWhiteSpace(queueName))
                QueueName = queueName;
        }

        protected override async Task DoWork(ProcessMessageEventArgs args)
        {
            var body = args.Message.Body.ToString();
            var syncToAlfaMessage = JsonSerializer.Deserialize<SyncToAlfaMessage>(body);

            using (var scope = _services.CreateScope())
            {
                var logger = scope.ServiceProvider.GetRequiredService<ILogger<SyncToAlfaQueueWorker>>();

                using (var loggerscope = logger.BeginScope(new Dictionary<string, string>
                {
                    { "AnSaKoReferenceId", syncToAlfaMessage.AnSaKoReferenceId },
                    { "FtpbReferenceId", syncToAlfaMessage.FtpbReferenceId }
                }))
                {
                    var unitOfWork = scope.ServiceProvider.GetRequiredService<IFtpbUnitOfWork>();

                    await unitOfWork.InitiateAsync(syncToAlfaMessage.FtpbReferenceId, syncToAlfaMessage.AnSaKoReferenceId);
                    await unitOfWork.SyncToAlfaAsync();
                }
            }
        }
    }
}