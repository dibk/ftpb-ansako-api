using Azure.Messaging.ServiceBus;
using Dibk.Ftpb.Ansako.App.HttpClients.Email;
using Dibk.Ftpb.Ansako.App.Services;
using Elastic.Apm;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Api.BackgroundServices.QueueWorkers
{
    internal class EmailQueueWorker : BaseQueueWorker
    {
        protected override string QueueName => "sbq-ftpb-ansako-email";

        protected override string ApmTransactionName => "SendEmail";

        public EmailQueueWorker(IApmAgent apmAgent,
                                ILogger<EmailQueueWorker> logger,
                                IConfiguration configuration,
                                IAzureClientFactory<ServiceBusClient> serviceBusClientFactory,
                                IServiceProvider services) : base(apmAgent,
                                                                        logger,
                                                                        configuration,
                                                                        serviceBusClientFactory,
                                                                        services)
        {
            var queueName = configuration["AnSaKo:Email:QueueName"];
            if (!string.IsNullOrWhiteSpace(queueName))
                QueueName = queueName;
        }

        protected override async Task DoWork(ProcessMessageEventArgs args)
        {
            var body = args.Message.Body.ToString();
            var emailQueueMessage = JsonSerializer.Deserialize<EmailQueueMessage>(body);

            using (var scope = _services.CreateScope())
            {
                var logger = scope.ServiceProvider.GetRequiredService<ILogger<EmailQueueWorker>>();
                using (var loggerscope = logger.BeginScope(new Dictionary<string, string>
                {
                    { "AnSaKoReferenceId", emailQueueMessage.AnSaKoReferenceId },
                    { "FtpbReferenceId", emailQueueMessage.FtpbReferenceId }
                }))
                {
                    var client = scope.ServiceProvider.GetRequiredService<EmailHttpClient>();

                    await client.Post(emailQueueMessage.EmailMessage);
                }
            }
        }
    }
}