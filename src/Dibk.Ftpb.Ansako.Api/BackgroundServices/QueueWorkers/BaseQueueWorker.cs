using Azure.Messaging.ServiceBus;
using Dibk.Ftpb.Api.Client;
using Elastic.Apm;
using Elastic.Apm.Api;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Api.BackgroundServices.QueueWorkers
{
    internal abstract class BaseQueueWorker : BackgroundService
    {
        protected abstract string ApmTransactionName { get; }
        protected virtual string QueueName { get; set; }
        protected readonly ILogger<BaseQueueWorker> _logger;
        protected readonly IServiceProvider _services;

        private readonly IAzureClientFactory<ServiceBusClient> _serviceBusClientFactory;
        private readonly IApmAgent _apmAgent;

        private ServiceBusClient _serviceBusClient;
        private ServiceBusProcessor _serviceBusProcessor;

        public BaseQueueWorker(IApmAgent apmAgent,
                               ILogger<BaseQueueWorker> logger,
                               IConfiguration configuration,
                               IAzureClientFactory<ServiceBusClient> serviceBusClientFactory,
                               IServiceProvider services)
        {
            _logger = logger;
            _serviceBusClientFactory = serviceBusClientFactory;
            _apmAgent = apmAgent;
            _services = services;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Starts receiving messages from queue: {QueueName}", QueueName);
            _serviceBusClient = _serviceBusClientFactory.CreateClient(ServiceBusPublisher.ServiceBusClientName);
            _serviceBusProcessor = _serviceBusClient.CreateProcessor(QueueName,
                                                                     new ServiceBusProcessorOptions());
            _serviceBusProcessor.ProcessMessageAsync += MessageHandler;
            _serviceBusProcessor.ProcessErrorAsync += ErrorHandler;

            await _serviceBusProcessor.StartProcessingAsync(stoppingToken).ConfigureAwait(false);
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stop signal received. Worker stops..");
            try
            {
                await _serviceBusProcessor.StopProcessingAsync().ConfigureAwait(false);
            }
            finally
            {
                await _serviceBusClient.DisposeAsync().ConfigureAwait(false);
            }

            await base.StopAsync(cancellationToken);
        }

        protected abstract Task DoWork(ProcessMessageEventArgs args);

        private async Task MessageHandler(ProcessMessageEventArgs args)
        {
            _logger.LogDebug("Message received {MessageId}", args.Message.MessageId);

            var transaction = _apmAgent.Tracer.StartTransaction(ApmTransactionName, ApiConstants.TypeMessaging);

            try
            {
                await DoWork(args);
            }
            catch (ArgumentNullException e)
            {
                _logger.LogError(e, "Required parameters/values aren't provided. {QueueItem}", e.Message);
                transaction.CaptureException(e);
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Unexpected error occurred");
                transaction.CaptureException(e);
                throw;
            }
            finally
            {
                transaction.End();
            }
        }

        // handle any errors when receiving messages
        private Task ErrorHandler(ProcessErrorEventArgs args)
        {
            _logger.LogError(args.Exception, "Error occurred");

            return Task.CompletedTask;
        }
    }
}