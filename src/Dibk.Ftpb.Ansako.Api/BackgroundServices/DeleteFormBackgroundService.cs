using Dibk.Ftpb.Ansako.App.Services.RecurringTaskServices;
using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NCrontab;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Api.BackgroundServices
{
    public class DeleteFormBackgroundService : BackgroundService
    {
        private readonly ILogger<DeleteFormBackgroundService> _logger;

        public DeleteFormBackgroundService(ILogger<DeleteFormBackgroundService> logger, IServiceProvider services)
        {
            _logger = logger;
            Services = services;
        }

        public IServiceProvider Services { get; }

        private int _daysOld = 365;
        private int _batchSize = 10;

        private CrontabSchedule _schedule;
        private DateTime _nextRun;
        private string _cronExpression = "0 1 * * *"; //Default verdi 01:00:00 hver natt
        private int _limitExecutionHours = 2; //Default begrensing i tid jobben kan kjøre hver natt
        private DateTime? _executionLimit;

        private void LoadConfiguration()
        {
            using (var scope = Services.CreateScope())
            {
                var configuration = scope.ServiceProvider.GetRequiredService<IConfiguration>();

                if(!string.IsNullOrEmpty(configuration["AnSaKo:RecurringJobs:DeleteFormCleanUp:ScheduleCronExpression"]))
                    _cronExpression = configuration["AnSaKo:RecurringJobs:DeleteFormCleanUp:ScheduleCronExpression"];

                if (int.TryParse(configuration["AnSaKo:RecurringJobs:DeleteFormCleanUp:LimitExecutionHours"], out var limitHours))
                    _limitExecutionHours = limitHours;

                if (int.TryParse(configuration["AnSaKo:RecurringJobs:DeleteFormCleanUp:DeleteAfterDays"], out var daysOld))
                    _daysOld = daysOld;

                if (int.TryParse(configuration["AnSaKo:RecurringJobs:DeleteFormCleanUp:BatchSize"], out var batchSize))
                    _batchSize = batchSize;
            }
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("DeleteFormBackgroundService starts..");
            LoadConfiguration();

            await EnsureLeaseBlobExists();

            try
            {
                _schedule = CrontabSchedule.Parse(_cronExpression);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured when parsing cron expression {CronExpression}", _cronExpression);
                throw;
            }
            
            UpdateExecutionTime();

            _logger.LogInformation("Waits for next run..");

            do
            {
                if (DateTime.Now > _nextRun)
                {
                    await DoWork(_executionLimit.Value, stoppingToken);

                    UpdateExecutionTime();
                }
                await Task.Delay(5000, stoppingToken); //5 seconds delay
            }
            while (!stoppingToken.IsCancellationRequested);
        }

        private void UpdateExecutionTime()
        {
            _nextRun = _schedule.GetNextOccurrence(DateTime.Now);
            _executionLimit = _nextRun.AddHours(_limitExecutionHours);

            _logger.LogInformation("Next run is scheduled at {NextRun} and is limited to {ExecutionLimit}", _nextRun.ToString(), _executionLimit.ToString());
        }

        private async Task DoWork(DateTime executionLimit, CancellationToken stoppingToken)
        {
            // Tanken her er at dersom det er eit større antall som skal renskast opp i
            // så bør ein ta det litt etter litt. Antagelsen her er at dersom resultatet fra CleanUp
            // er likt antallet ein ber om at skal renskast opp i, så er det fleire å ta av.

            int resultCount = await DoCleanUp(_batchSize, stoppingToken);
            int processedCandidates = resultCount;

            while (_batchSize == resultCount)
            {
                if (DateTime.Now < executionLimit)
                {
                    resultCount = await DoCleanUp(_batchSize, stoppingToken);
                    processedCandidates += resultCount;
                }
                else
                {
                    _logger.LogInformation("Form clean up job reached time limit {ExectuionLimit}.", executionLimit.ToString());
                    break;
                }
            }

            _logger.LogInformation("Total #{FormCount} forms has been cleaned", processedCandidates);
        }

        private bool IsEnabled()
        {
            bool isEnabled = false;
            using (var scope = Services.CreateScope())
            {
                var configuration = scope.ServiceProvider.GetRequiredService<IConfiguration>();
                if (bool.TryParse(configuration["AnSaKo:RecurringJobs:DeleteFormCleanUp:Enabled"], out var parsedEnabled))
                    isEnabled = parsedEnabled;
            }

            _logger.LogInformation($"Form clean up job enabled: {isEnabled}");

            return isEnabled;
        }

        private async Task<int> DoCleanUp(int candidatesPrBatch, CancellationToken stoppingToken)
        {
            if (!IsEnabled())
                return 0;

            BlobLeaseResult leaseResult = await AquireLease(stoppingToken);

            if (leaseResult.SuccessfullLease)
                try
                {
                    _logger.LogInformation("Lease aquired for container {ContainerName}, leaseId: {LeaseId}", _leaseContainerName, leaseResult.LeaseId);

                    using (var scope = Services.CreateScope())
                    {
                        _logger.LogDebug("Resolves service for clean up");
                        var cleanUpService = scope.ServiceProvider.GetRequiredService<DeleteFormCleanUpService>();

                        _logger.LogInformation("Initiates clean up. CandidatesPrBatch: {BatchSize}, DaysOld: {DaysOld}", candidatesPrBatch, _daysOld);
                        return await cleanUpService.DoCleanUp(candidatesPrBatch, deleteAfterDays: _daysOld);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error occured when processing clean up");
                    return 0;
                }
                finally
                {
                    await ReleaseLease(leaseResult, stoppingToken);
                }
            else
                return 0;
        }

        private string _leaseContainerName = "DeleteFormLeaseContainer";

        private async Task EnsureLeaseBlobExists()
        {
            using (var scope = Services.CreateScope())
            {
                _logger.LogInformation("Ensures lease container {ContainerName} exists..", _leaseContainerName);
                var blobService = scope.ServiceProvider.GetRequiredService<IBlobService>();
                await blobService.CreateContainerAsync(_leaseContainerName);
            }
        }

        private async Task ReleaseLease(BlobLeaseResult leaseResult, CancellationToken stoppingToken)
        {
            using (var scope = Services.CreateScope())
            {
                _logger.LogInformation("Releases lease on container {ContainerName}, leaseId: {LeaseId}", _leaseContainerName, leaseResult.LeaseId);
                var blobService = scope.ServiceProvider.GetRequiredService<IBlobService>();
                await blobService.ReleaseLeaseAsync(_leaseContainerName, leaseResult.LeaseId, stoppingToken);
            }
        }

        private async Task<BlobLeaseResult> AquireLease(CancellationToken stoppingToken)
        {
            using (var scope = Services.CreateScope())
            {
                _logger.LogInformation("Requesting lease on container {ContainerName}", _leaseContainerName);
                var blobService = scope.ServiceProvider.GetRequiredService<IBlobService>();
                return await blobService.AquireLeaseAsync(_leaseContainerName, new TimeSpan(0, 0, 60), stoppingToken);
            }
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("DeleteFormBackgroundService is stopping..");
            return base.StopAsync(cancellationToken);
        }
    }
}