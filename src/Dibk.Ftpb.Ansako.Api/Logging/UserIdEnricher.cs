﻿using Microsoft.AspNetCore.Http;
using Serilog.Core;
using Serilog.Events;
using System.Linq;

namespace Dibk.Ftpb.Ansako.Api.Logging
{
    public class UserIdEnricher : ILogEventEnricher
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserIdEnricher(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            var httpContext = _httpContextAccessor.HttpContext;

            if (httpContext != null && httpContext.User.Identity?.IsAuthenticated == true)
            {
                // Get the UserId from claims, adjust claim name as necessary (e.g., "sub" or "UserId")
                var userId = httpContext.User.Claims.FirstOrDefault(c => c.Type == "sub")?.Value ?? "Anonymous";

                // Add UserId as a log property
                var userIdProperty = propertyFactory.CreateProperty("UserId", userId);
                logEvent.AddPropertyIfAbsent(userIdProperty);
            }
        }
    }
}