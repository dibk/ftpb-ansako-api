﻿using Elastic.Apm.SerilogEnricher;
using Elastic.Serilog.Enrichers.Web;
using Elastic.Serilog.Sinks;
using Elastic.Transport;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Enrichers.Claim;
using Serilog.Events;
using System;

namespace Dibk.Ftpb.Ansako.Api.Logging
{
    public static class SerilogConfiguration
    {
        public static LoggerConfiguration ConfigureLogging(IConfiguration configuration)
        {
            var elasticSearchUrl = configuration.GetValue<string>("Serilog:ConnectionUrl");
            var elasticUsername = configuration.GetValue<string>("Serilog:Username");
            var elasticPassword = configuration.GetValue<string>("Serilog:Password");
            var elasticIndexFormat = configuration.GetValue<string>("Serilog:IndexFormat");

            var loggerConfiguration = new LoggerConfiguration()
                .MinimumLevel.Is(LogEventLevel.Debug)
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .MinimumLevel.Override("Elastic.Apm", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .Enrich.WithCorrelationIdHeader()
                .Enrich.WithElasticApmCorrelationInfo()
                .Enrich.WithEcsHttpContext(configuration.Get<HttpContextAccessor>())
                .Enrich.With(new UserIdEnricher(configuration.Get<HttpContextAccessor>()))
                .Enrich.WithClaim(configuration.Get<HttpContextAccessor>(), "clientSystem")
                .Enrich.WithClaim(configuration.Get<HttpContextAccessor>(), "iss")
                .WriteTo.Console(outputTemplate: "{Timestamp:HH:mm:ss.fff} [{Level}] {Scope} {SourceContext} {InnsendingReferanse} {Message}{NewLine}{Exception}");

            if (!string.IsNullOrEmpty(elasticSearchUrl))
                loggerConfiguration.WriteTo.Elasticsearch(new Uri[] { new Uri(elasticSearchUrl) },
                                conf => { conf.DataStream = new Elastic.Ingest.Elasticsearch.DataStreams.DataStreamName(elasticIndexFormat); },
                                tr => { tr.Authentication(new BasicAuthentication(elasticUsername, elasticPassword)); }
        );
            else
                Console.WriteLine("ERROR IN SERILOG CONFIGURATION - Unable to register elastic sink. URL is missing in config");

            return loggerConfiguration;
        }
    }
}