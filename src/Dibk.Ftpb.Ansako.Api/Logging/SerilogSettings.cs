﻿namespace Dibk.Ftpb.Ansako.Api.Logging
{
    public class SerilogSettings
    {
        public static string ConfigSection = "Serilog";
        public string ConnectionUrl { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string IndexFormat { get; set; }
    }
}
