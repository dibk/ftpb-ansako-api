﻿using Dibk.Ftpb.Ansako.App.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Text.RegularExpressions;
using System.Xml;

namespace Dibk.Ftpb.Ansako.Api.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiController]
    public class DibkHelperController : ControllerBase
    {
        [HttpPost]
        [Route("api/v1/xmlTilInnsendingKonvertering")]
        public ActionResult Convert([FromBody] XmlDocument payload)
        {
            var xmlString = payload.InnerXml;
            xmlString = GetLinearizedXml(xmlString);
            //Get datoformatid and version
            var dataFormatId = payload.DocumentElement.Attributes.GetNamedItem("dataFormatId").Value;
            var dataFormatVersion = payload.DocumentElement.Attributes.GetNamedItem("dataFormatVersion").Value;

            var response = new CreateFormPayload()
            {
                DataFormatId = dataFormatId,
                DataFormatVersion = dataFormatVersion,
                FormDataXml = xmlString,
                FormMetadata = new CreateFormMetadata() { Signeringsfrist = System.DateTime.Today.AddDays(28).AddHours(23).AddMinutes(59).AddSeconds(59) }
            };

            var postData = JObject.FromObject(response);

            return Ok(postData);
        }

        /// <summary>
        /// A method that uses Regex to linearize Xml. The regex replaces methods are used.
        /// </summary>
        /// <param name="text">The Xml text</param>
        /// <returns>Linearized Xml string.</returns>
        private string GetLinearizedXml(string text)
        {
            // Replace all white space with a single space
            var halfclean = Regex.Replace(text, @"\s+", " ", RegexOptions.Singleline);

            // Trim after >.
            var clean75 = Regex.Replace(halfclean, @">\s+", ">");

            // Trim before <
            var fullclean = Regex.Replace(clean75, @"\s+<", "<");

            return fullclean;
        }
    }
}
