﻿using Dibk.Ftpb.Ansako.Api.ApiResources.InternalApi;
using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.App.Repositories.Models;
using Dibk.Ftpb.Ansako.App.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Api.Controllers
{
    [EnableCors]
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiController]
    public class InnsendingController : ControllerBase
    {
        private readonly ILogger<InnsendingController> _logger;
        private readonly InternalApiFormService _internalApiFormService;

        public InnsendingController(ILogger<InnsendingController> logger, InternalApiFormService internalApiFormService)
        {
            _logger = logger;
            _internalApiFormService = internalApiFormService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(AnSaKoRepresentation), (int)HttpStatusCode.OK)]
        [Route("api/v1/[controller]/{ansakoReferenceId}")]
        public async Task<IActionResult> Get(string ansakoReferenceId)
        {
            if (string.IsNullOrEmpty(ansakoReferenceId))
                return NotFound();

            AnSaKoModel d = null;
            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId))
                try
                {
                    d = await _internalApiFormService.GetAsync(ansakoReferenceId);
                }
                catch (FormNotFoundException f)
                {
                    return NotFound(f.Message);
                }

            var response = new AnSaKoRepresentation()
            {
                Innsendingstype = d.FormDbData.FormType,
                DataFormatId = d.FormDbData.DataFormatId,
                DataFormatVersion = d.FormDbData.DataFormatVersion,
                MottattDato = DateTime.Now,
                ReferanseId = d.FormDbData.AnSaKoReferenceId,
                Status = d.FormDbData.Status,                
                SlettetDato = d.FormDbData.DateDeleted
            };

            return Ok(response);
        }
    }
}
