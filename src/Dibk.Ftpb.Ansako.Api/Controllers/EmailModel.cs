﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dibk.Ftpb.Ansako.Api.Controllers
{
    public class EmailModel : IValidatableObject
    {
        [EmailAddress]
        public string ToAddress { get; set; }
        [Required]
        public string Subject { get; set; }
        public string HtmlBody { get; set; }
        public string PlainTextBody { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(HtmlBody) && string.IsNullOrEmpty(PlainTextBody))
                yield return new ValidationResult("'HtmlBody' or 'PlainTextBody' must have a value");
        }
    }
}
