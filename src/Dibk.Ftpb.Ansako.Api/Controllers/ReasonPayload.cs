﻿using System.ComponentModel.DataAnnotations;

namespace Dibk.Ftpb.Ansako.Api.Controllers
{
    public class ReasonPayload
    {
        [Required]
        public string Reason { get; set; }
    }
}
