﻿using Dibk.Ftpb.Ansako.Api.Authentication;
using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.App.Repositories.Models;
using Dibk.Ftpb.Ansako.App.Services;
using Dibk.Ftpb.Ansako.App.Services.Signing;
using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Dibk.Ftpb.Signing.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Api.Controllers
{
    [EnableCors]
    [IdportenAuthorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiController]
    public class SigneringController : ControllerBase
    {
        private readonly ILogger<SigneringController> _logger;
        private readonly InternalApiFormService _internalApiFormService;
        private readonly SigneringsService _signeringsService;

        public SigneringController(ILogger<SigneringController> logger,
                                   InternalApiFormService internalApiFormService,
                                   SigneringsService signeringsService
                                   )
        {
            _logger = logger;
            _internalApiFormService = internalApiFormService;
            _signeringsService = signeringsService;
        }

        [HttpPost]
        [Route("api/v1/[controller]/{ansakoReferenceId}/initiate")]
        [ProducesResponseType(typeof(SigningJobResult), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<SigningJobResult>> Initiate(string ansakoReferenceId)
        {
            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId))
                try
                {
                    AnSaKoModel formdata = null;

                    try
                    {
                        formdata = await _internalApiFormService.GetAsync(ansakoReferenceId);

                        var currentState = formdata.FormDbData.Status;

                        if (!StateHelper.CanTransitToState(currentState, FormStatus.TilSignering))
                            throw new InvalidStateChangeException(currentState, FormStatus.TilSignering);

                    }
                    catch (FormNotFoundException ex)
                    {
                        _logger.LogError(ex, "Form doesn't exist");
                        return NotFound(ex.Message);
                    }

                    if (formdata.FormDbData.Status == FormStatus.Signert)
                        return BadRequest("Form is already signed");

                    if (await _signeringsService.DocumentExists(formdata))
                    {
                        var result = await _signeringsService.CreateSigningJob(formdata);

                        return Ok(result);
                    }
                    else return BadRequest("PDF doesn't exist. Please create PDF first using HtmlToPdf-API");
                }
                catch (InvalidStateChangeException ise)
                { return BadRequest(ise.Message); }
                catch (Exception e)
                {
                    _logger.LogError(e, "An error occurred when creating signing job");
                    throw;
                }
        }

        [HttpPost]
        [Route("api/v1/[controller]/{ansakoReferenceId}/failed")]
        public async Task<ActionResult<SigningJobResult>> Failed(string ansakoReferenceId, [FromBody] ReasonPayload payload)
        {
            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId))
                try
                {
                    AnSaKoModel formdata = null;

                    try
                    {
                        formdata = await _internalApiFormService.GetAsync(ansakoReferenceId);
                    }
                    catch (FormNotFoundException ex)
                    {
                        _logger.LogError(ex, "Form doesn't exist");
                        return NotFound(ex.Message);
                    }

                    if (payload == null)
                        return BadRequest("Payload is missing from request");

                    // !!!!---- TODO ----!!!!
                    //Handle failed

                    return Ok();
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "An error occurred when creating signing job");
                    throw;
                }
        }

        [HttpGet]
        [Route("api/v1/[controller]/{ansakoReferenceId}/signed-document")]
        [ProducesResponseType(typeof(FileStreamResult), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetSignedDocument(string ansakoReferenceId)
        {
            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId))
                try
                {
                    var formdata = await _internalApiFormService.GetAsync(ansakoReferenceId);

                    var result = await _signeringsService.GetSignedDocument(formdata);

                    return File(result.ContentStream, result.MimeType, result.FileName);
                }
                catch (FormNotFoundException ex)
                {
                    _logger.LogError(ex, "Form doesn't exist");
                    return NotFound(ex.Message);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "An error occurred when creating signing job");
                    throw;
                }
        }

        [HttpGet]
        [Route("api/v1/[controller]/{ansakoReferenceId}/status")]
        [ProducesResponseType(typeof(SigningJobResult), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<SigningJobResult>> GetSigningJobStatus(string ansakoReferenceId)
        {
            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId))
                try
                {
                    var formdata = await _internalApiFormService.GetAsync(ansakoReferenceId);

                    var result = await _signeringsService.GetSigningJobStatus(formdata);

                    return Ok(result);
                }
                catch (FormNotFoundException ex)
                {
                    _logger.LogError(ex, "Form doesn't exist");
                    return NotFound(ex.Message);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "An error occurred when creating signing job");
                    throw;
                }
        }
    }
}
