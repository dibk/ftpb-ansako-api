﻿using Dibk.Ftpb.Ansako.Api.Authentication;
using Dibk.Ftpb.Ansako.App;
using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.App.Services;
using Dibk.Ftpb.Ansako.App.Services.Signing;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Api.Controllers
{
    [EnableCors]
    [IdportenAuthorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private readonly ILogger<EmailController> _logger;
        private readonly InternalApiFormService _internalApiFormService;
        private readonly SigneringsService _signeringsService;
        private readonly EmailService _emailService;

        public EmailController(ILogger<EmailController> logger,
                                   InternalApiFormService internalApiFormService,
                                   SigneringsService signeringsService,
                                   EmailService emailService
                                   )
        {
            _logger = logger;
            _internalApiFormService = internalApiFormService;
            _signeringsService = signeringsService;
            _emailService = emailService;
        }

        [HttpPost]
        [Route("api/v1/[controller]/{ansakoReferenceId}")]
        public async Task<ActionResult> SendEmail(string ansakoReferenceId, [FromBody] EmailModel email, [FromQuery] string status)
        {
            if (email == null || !ModelState.IsValid)
                return BadRequest(ModelState);

            if (!string.IsNullOrEmpty(status) && !Enum.TryParse<Storage.Database.Models.FormStatus>(status, true, out var formStatus))
                return BadRequest($"status query parameter has illegal value: '{status}'");

            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId))
                try
                {
                    var formdata = await _internalApiFormService.GetAsync(ansakoReferenceId);

                    if (formdata == null)
                        return NotFound();

                    if (string.IsNullOrEmpty(status) || Enum.Parse<Storage.Database.Models.FormStatus>(status, true) != Storage.Database.Models.FormStatus.Signert)
                    {
                        _logger.LogDebug("Requests email for form with status {FormStatus}", formdata.FormDbData.Status);
                        await _emailService.SendEmail(ansakoReferenceId, formdata.FtpbReferenceId, email.ToAddress, email.Subject, email.HtmlBody, email.PlainTextBody);
                    }
                    else if (Enum.Parse<Storage.Database.Models.FormStatus>(status, true) == Storage.Database.Models.FormStatus.Signert)
                    {
                        if (formdata.FormDbData.Status != Storage.Database.Models.FormStatus.Signert)
                            return BadRequest($"Cannot send email containing signed document when form has status '{formdata.FormDbData.Status}'");

                        //Get signed doc                        
                        var url = await _signeringsService.GetSignedDocumentDownloadUrl(formdata);

                        var htmlBody = string.Empty;
                        if (!string.IsNullOrEmpty(email.HtmlBody))
                         htmlBody= email.HtmlBody.Replace("#download-ref#", url, StringComparison.OrdinalIgnoreCase);

                        var plainTextBody = string.Empty;
                        if (!string.IsNullOrEmpty(email.PlainTextBody))
                            plainTextBody = email.PlainTextBody.Replace("#download-ref#", url, StringComparison.OrdinalIgnoreCase);

                        //Send email
                        _logger.LogDebug("Requests email for form with status {FormStatus}", formdata.FormDbData.Status);
                        await _emailService.SendEmail(ansakoReferenceId, formdata.FtpbReferenceId, email.ToAddress, email.Subject, htmlBody, plainTextBody);
                    }

                    return Ok();
                }
                catch (FormNotFoundException f)
                {
                    return NotFound(f.Message);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "An error occurred when sending email");
                    throw;
                }
        }
    }
}
