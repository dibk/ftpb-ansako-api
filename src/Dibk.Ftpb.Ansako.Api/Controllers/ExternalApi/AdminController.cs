﻿using Dibk.Ftpb.Ansako.Api.Authentication;
using Dibk.Ftpb.Ansako.App.Models.InternalApi;
using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.App.Repositories.Models;
using Dibk.Ftpb.Ansako.App.Services;
using Dibk.Ftpb.Ansako.App.Services.Admin;
using Dibk.Ftpb.Ansako.App.Services.Signing;
using Dibk.Ftpb.Api.Client.Interfaces;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Api.Controllers.ExternalApi
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [EnableCors("AllowAll")]
    [BasicAuthorize]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly ILogger<AdminController> _logger;
        private readonly InternalApiFormService _internalApiFormService;
        private readonly SigneringsService _signeringsService;
        private readonly EmailService _emailService;
        private readonly FormServiceProvider _formServiceProvider;
        private readonly IFtpbUnitOfWork _ftpbUnitOfWork;
        private readonly AttachmentSyncService _attachmentSyncService;

        public AdminController(ILogger<AdminController> logger,
                                InternalApiFormService internalApiFormService,
                                SigneringsService signeringsService,
                                EmailService emailService,
                                FormServiceProvider formServiceProvider,
                                IFtpbUnitOfWork ftpbUnitOfWork,
                                AttachmentSyncService attachmentSyncService)
        {
            _logger = logger;
            _internalApiFormService = internalApiFormService;
            _signeringsService = signeringsService;
            _emailService = emailService;
            _formServiceProvider = formServiceProvider;
            _ftpbUnitOfWork = ftpbUnitOfWork;
            _attachmentSyncService = attachmentSyncService;
        }

        [HttpPost]
        [Route("api/v1/[controller]/{ansakoReferenceId}/sync-to-alfa")]
        public async Task<ActionResult> LogSignedToAlfa(string ansakoReferenceId)
        {
            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId))
                try
                {
                    var formdata = await _internalApiFormService.GetAsync(ansakoReferenceId);

                    if (formdata == null)
                        return NotFound();

                    await _signeringsService.ReportSignedDocumentStatus(ansakoReferenceId);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Failed to log signed document to Alfa");
                    return Problem("Failed to log signed document to Alfa", statusCode: 500);
                }

            return Ok();
        }

        [HttpPost]
        [Route("api/v1/[controller]/attachmentsync")]
        public async Task<ActionResult> AttachmentSync()
        {
            var result = await _attachmentSyncService.SyncDatabaseWithAttachments();
            return Ok(result);
        }

        [HttpPost]
        [Route("api/v1/[controller]/{ansakoReferenceId}/email")]
        public async Task<ActionResult> SendEmail(string ansakoReferenceId, [FromBody] EmailModel email, [FromQuery] string receiverAdrSource)
        {
            if (email == null || !ModelState.IsValid)
                return BadRequest(ModelState);

            if (string.IsNullOrEmpty(receiverAdrSource) || !Enum.TryParse<EmailAddressProvider>(receiverAdrSource, true, out var s))
                return BadRequest($"emailSource query parameter er påkrevd og må ha en gyldig verdi: '{receiverAdrSource}'");

            try
            {
                var formdata = await _internalApiFormService.GetAsync(ansakoReferenceId);

                if (formdata == null)
                    return NotFound();

                var toAddress = string.Empty;

                var formSpecificData = await GetFormData(formdata);

                var emailReceiverSource = Enum.Parse<EmailAddressProvider>(receiverAdrSource, true);
                if (emailReceiverSource == EmailAddressProvider.Foretak)
                {
                    var ansvarligForetakEpost = string.Empty;
                    if (!string.IsNullOrEmpty(formSpecificData.KontaktpersonEpostAdresse))
                        ansvarligForetakEpost = formSpecificData.KontaktpersonEpostAdresse;
                    else
                        ansvarligForetakEpost = formSpecificData.ForetakEpostAdresse;

                    if (string.IsNullOrEmpty(ansvarligForetakEpost))
                        return BadRequest($"AnsvarligForetak har ikke epost adresse tilgjengelig i på foretak- eller kontakpersonnivå");

                    toAddress = ansvarligForetakEpost;
                }
                else if (emailReceiverSource == EmailAddressProvider.Input)
                {
                    if (string.IsNullOrEmpty(email.ToAddress))
                        return BadRequest("Query parameter emailSource er 'Input', men det er ikke sendt med noen epost adresse");

                    toAddress = email.ToAddress;
                }

                //Status
                if (formdata.FormDbData.Status == Storage.Database.Models.FormStatus.Signert)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(formSpecificData.ProsjektNavn))
                            email.Subject = $"{formSpecificData.ProsjektNavn} - {email.Subject}";

                        await SignedFormEmailPreparation(email, formdata);
                    }
                    catch (Exception ex)
                    {
                        return Problem($"Kunne ikke finne det signerte dokumentet {ex.Message}", statusCode: 500);
                    }
                }
                else if (formdata.FormDbData.Status == Storage.Database.Models.FormStatus.Avvist)
                {
                    await AvvistFormEmailPreparation(email, formdata);
                }

                //Send email
                _logger.LogDebug("Sends email for form with attachment with status {FormStatus}", formdata.FormDbData.Status);
                await _emailService.SendEmail(ansakoReferenceId, formdata.FtpbReferenceId, toAddress, email.Subject, email.HtmlBody, email.PlainTextBody);

                //Burde bare vært en event som blir sendt
                await _ftpbUnitOfWork.InitiateAsync(formdata.FtpbReferenceId, formdata.AnSaKoReferenceId);
                _ftpbUnitOfWork.LogEntries.AddInfo($"Epost sendt til {toAddress}");
                await _ftpbUnitOfWork.SaveLogEntriesAsync();

                return Ok($"Epost sendt til {toAddress}");
            }
            catch (FormNotFoundException f)
            {
                return NotFound(f.Message);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "An error occurred when sending email");
                throw;
            }
        }

        private async Task AvvistFormEmailPreparation(EmailModel email, AnSaKoModel formdata)
        {
            var formBaseModelData = await GetFormBaseModel(formdata);
            string innsendingsType = GetInnsendingsType(formdata);

            var headerProsjektNavn = string.IsNullOrEmpty(formBaseModelData.Prosjektnavn) ? "" : $" for '{formBaseModelData.Prosjektnavn}'";
            var headerText = $"{formBaseModelData.AnsvarligForetak.Navn} har avvist {innsendingsType}{headerProsjektNavn}";
            email.HtmlBody = email.HtmlBody.Replace("#headerText#", headerText, StringComparison.OrdinalIgnoreCase);

            //Opprett addresse
            var eiendom = formBaseModelData.EiendomByggesteder.FirstOrDefault();
            if (eiendom != null)
            {
                var kommunenavn = string.IsNullOrEmpty(eiendom.Kommunenavn) ? "" : $" {eiendom.Kommunenavn} kommune";
                var gbnr = string.IsNullOrEmpty(eiendom.Gaardsnummer) ? "" : $" Gårdsnr. {eiendom.Gaardsnummer}, bruksnr {eiendom.Bruksnummer}";
                var adresse = $"Adresse: {eiendom.Adresselinje1} {eiendom.Postnr} {eiendom.Poststed}{kommunenavn}{gbnr}";

                email.HtmlBody = email.HtmlBody.Replace("#adresse#", adresse, StringComparison.OrdinalIgnoreCase);
            }
            else
                email.HtmlBody = email.HtmlBody.Replace("#adresse#", string.Empty, StringComparison.OrdinalIgnoreCase);

            //Saksnummer
            var saksnummer = string.IsNullOrEmpty(formBaseModelData.Kommunenssaksaar) ? string.Empty : $"Kommunens saksnummer: {formBaseModelData.Kommunenssaksaar}/{formBaseModelData.Kommunenssakssekvensnummer} </br>";
            email.HtmlBody = email.HtmlBody.Replace("#saksnummer#", saksnummer, StringComparison.OrdinalIgnoreCase);

            //Kontaktperson ansvarligForetak
            var kontaktpersonAnsvarligForetak = string.IsNullOrEmpty(formBaseModelData.AnsvarligForetak.KontaktpersonNavn) ? string.Empty : $"Kontaktperson hos {formBaseModelData.AnsvarligForetak.Navn}: {formBaseModelData.AnsvarligForetak.KontaktpersonNavn}<br />";
            email.HtmlBody = email.HtmlBody.Replace("#kontaktpersonAF#", kontaktpersonAnsvarligForetak, StringComparison.OrdinalIgnoreCase);

            //Begrunnelse for avvisning
            email.HtmlBody = email.HtmlBody.Replace("#begrunnelseForAvvisning#", formdata.FormDbData.StatusDetails, StringComparison.OrdinalIgnoreCase);

            //Melding
            email.HtmlBody = email.HtmlBody.Replace("#ansvarligSoekerNavn#", formBaseModelData.AnsvarligSoeker.Navn, StringComparison.OrdinalIgnoreCase);

            //Kontaktinfo
            var kontaktinfoNavn = $"{formBaseModelData.AnsvarligSoeker.Navn} {formBaseModelData.AnsvarligSoeker.KontaktpersonNavn}";
            var tlf = string.Empty;
            if (!string.IsNullOrEmpty(formBaseModelData.AnsvarligSoeker.KontaktpersonMobilnummer))
                tlf = formBaseModelData.AnsvarligSoeker.KontaktpersonMobilnummer;
            else if (!string.IsNullOrEmpty(formBaseModelData.AnsvarligSoeker.KontaktpersonTelefonnummer))
                tlf = formBaseModelData.AnsvarligSoeker.KontaktpersonTelefonnummer;
            else if (!string.IsNullOrEmpty(formBaseModelData.AnsvarligSoeker.Mobilnummer))
                tlf = formBaseModelData.AnsvarligSoeker.Mobilnummer;
            else if (!string.IsNullOrEmpty(formBaseModelData.AnsvarligSoeker.Telefonnummer))
                tlf = formBaseModelData.AnsvarligSoeker.Telefonnummer;

            var epostAS = string.IsNullOrEmpty(formBaseModelData.AnsvarligSoeker.KontaktpersonEpost) ? formBaseModelData.AnsvarligSoeker.Epost : formBaseModelData.AnsvarligSoeker.KontaktpersonEpost;
            var epostLenke = string.IsNullOrEmpty(epostAS) ? string.Empty : $" på epost <a href ='mailto:{epostAS}'>{epostAS}<a/>";

            var kontakinfo = $"Ta kontakt med {kontaktinfoNavn} på telefon {tlf} eller {epostLenke}";

            email.HtmlBody = email.HtmlBody.Replace("#kontaktinfo#", kontakinfo, StringComparison.OrdinalIgnoreCase);

            //----------------------------------------
            //Tittel
            var prosjektnavn = string.Empty;
            if (!string.IsNullOrEmpty(formBaseModelData.Prosjektnavn))
                prosjektnavn = formBaseModelData.Prosjektnavn;
            else if (!string.IsNullOrEmpty(eiendom.Adresselinje1))
                prosjektnavn = eiendom.Adresselinje1;
            else
                prosjektnavn = string.IsNullOrEmpty(eiendom.Gaardsnummer) ? string.Empty : $"Gårdsnr. {eiendom.Gaardsnummer}, bruksnr. {eiendom.Bruksnummer}";

            prosjektnavn = string.IsNullOrEmpty(prosjektnavn) ? string.Empty : $"{prosjektnavn} - ";

            email.Subject = $"{prosjektnavn}{GetInnsendingsType(formdata)}  har blitt avvist";
        }

        private string GetInnsendingsType(AnSaKoModel formdata)
        {
            switch (formdata.FormDbData.FormType)
            {
                case Storage.Database.Models.ErklaeringType.Ansvarsrett:
                    return "ansvarserklæringen";

                case Storage.Database.Models.ErklaeringType.Samsvarserklaering:
                    return "samsvarserklæringen";

                case Storage.Database.Models.ErklaeringType.Kontrollerklaering:
                    return "kontrollerklæringen";

                default:
                    return string.Empty;
            }
        }

        private async Task SignedFormEmailPreparation(EmailModel email, AnSaKoModel formdata)
        {
            //Get signed document URL
            string url = await _signeringsService.GetSignedDocumentDownloadUrl(formdata);
            var htmlBody = string.Empty;
            if (!string.IsNullOrEmpty(email.HtmlBody))
                email.HtmlBody = email.HtmlBody.Replace("#download-ref#", url, StringComparison.OrdinalIgnoreCase);

            var plainTextBody = string.Empty;
            if (!string.IsNullOrEmpty(email.PlainTextBody))
                email.PlainTextBody = email.PlainTextBody.Replace("#download-ref#", url, StringComparison.OrdinalIgnoreCase);
        }

        private async Task<AnSaKoResponseModelBase> GetFormBaseModel(AnSaKoModel anSaKoModel)
        {
            var formService = _formServiceProvider.GetService(anSaKoModel.FormDbData.FormType);
            var formData = await formService.GetAsync(anSaKoModel.AnSaKoReferenceId);

            AnSaKoResponseModelBase formDataBase = null;
            switch (anSaKoModel.FormDbData.FormType)
            {
                case Storage.Database.Models.ErklaeringType.Ansvarsrett:
                    var a = formData as AnsvarsrettDto;
                    formDataBase = a.FormData as AnSaKoResponseModelBase;
                    break;

                case Storage.Database.Models.ErklaeringType.Samsvarserklaering:
                    var s = formData as SamsvarserklaeringDto;
                    formDataBase = s.FormData as AnSaKoResponseModelBase;
                    break;

                case Storage.Database.Models.ErklaeringType.Kontrollerklaering:
                    var k = formData as KontrollerklaeringDto;
                    formDataBase = k.FormData as AnSaKoResponseModelBase;
                    break;

                default:
                    break;
            }

            return formDataBase;
        }

        private async Task<FormSpecificData> GetFormData(AnSaKoModel anSaKoModel)
        {
            var formService = _formServiceProvider.GetService(anSaKoModel.FormDbData.FormType);
            var formData = await formService.GetAsync(anSaKoModel.AnSaKoReferenceId);

            var formSpecificData = new FormSpecificData();
            switch (anSaKoModel.FormDbData.FormType)
            {
                case Storage.Database.Models.ErklaeringType.Ansvarsrett:
                    var a = formData as AnsvarsrettDto;
                    formSpecificData.KontaktpersonEpostAdresse = a.FormData.AnsvarligForetak.KontaktpersonEpost;
                    formSpecificData.ForetakEpostAdresse = a.FormData.AnsvarligForetak.Epost;
                    formSpecificData.ProsjektNavn = a.FormData.Prosjektnavn;
                    break;

                case Storage.Database.Models.ErklaeringType.Samsvarserklaering:
                    var s = formData as SamsvarserklaeringDto;
                    formSpecificData.KontaktpersonEpostAdresse = s.FormData.AnsvarligForetak.KontaktpersonEpost;
                    formSpecificData.ForetakEpostAdresse = s.FormData.AnsvarligForetak.Epost;
                    formSpecificData.ProsjektNavn = s.FormData.Prosjektnavn;
                    break;

                case Storage.Database.Models.ErklaeringType.Kontrollerklaering:
                    var k = formData as KontrollerklaeringDto;
                    formSpecificData.KontaktpersonEpostAdresse = k.FormData.AnsvarligForetak.KontaktpersonEpost;
                    formSpecificData.ForetakEpostAdresse = k.FormData.AnsvarligForetak.Epost;
                    formSpecificData.ProsjektNavn = k.FormData.Prosjektnavn;
                    break;

                default:
                    break;
            }

            return formSpecificData;
        }

        private class FormSpecificData
        {
            public string ForetakEpostAdresse { get; set; }
            public string KontaktpersonEpostAdresse { get; set; }
            public string ProsjektNavn { get; set; }
        }

        private enum EmailAddressProvider
        {
            Foretak,
            Input
        }
    }
}