﻿using Dibk.Ftpb.Ansako.Api.ApiResources.ExternalApi;
using Dibk.Ftpb.Ansako.Api.Authentication;
using Dibk.Ftpb.Ansako.App;
using Dibk.Ftpb.Ansako.App.Models;
using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.App.Repositories.Models;
using Dibk.Ftpb.Ansako.App.Services;
using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Api.Controllers.ExternalApi
{
    [EnableCors("AllowAll")]
    [BasicAuthorize]
    [ApiController]
    public class SkjemaInnsendingController : ControllerBase
    {
        private readonly ILogger<SkjemaInnsendingController> _logger;
        private readonly ExternalApiFormService _formService;
        private readonly AnSaKoSystemSettings _settings;

        public SkjemaInnsendingController(ILogger<SkjemaInnsendingController> logger, ExternalApiFormService formService, IOptions<AnSaKoSystemSettings> options)
        {
            _logger = logger;
            _formService = formService;
            _settings = options.Value;
        }

        /// <summary>
        /// Søknadsystemets API for å opprette ny erklæring i Fellestjenester Bygg
        /// </summary>
        /// <param name="payload"></param>
        /// <returns></returns>
        //[BasicAuthorize]
        [HttpPost]
        [Route("api/v1/[controller]")]
        [ProducesResponseType(typeof(CreateFormPayload), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Post([FromBody] CreateFormPayload payload)
        {
            if (payload == null || !ModelState.IsValid)
                return BadRequest(ModelState);

            var valRes = _formService.IsValid(payload);
            if (!valRes.IsValid)
                return BadRequest(valRes.ValidationMessage);

            var result = await _formService.CreateAsync(payload);
            var ErklaeringsType = FormTypeHelper.GetErklaeringType(result.DataFormatId, result.DataFormatVersion);

            var response = new ExternalApiFormRepresentation(_settings.FrontendUrl, result.AnSaKoReferenceId)
            {
                FormType = ErklaeringsType,
                ReferanseId = result.FtpbReferenceId,
                DataFormatId = result.DataFormatId,
                DataFormatVersion = result.DataFormatVersion,
                AnsvarsomraadeReferanser = result.Ansvarsomraader,
                FormMetadata = result.FormMetadata
            };

            return Ok(response);
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        [Route("api/v1/[controller]/{ftpbReferenceId}")]
        [ProducesResponseType(typeof(CreateFormPayload), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(string ftpbReferenceId)
        {
            if (string.IsNullOrEmpty(ftpbReferenceId))
                return BadRequest();

            var formdata = await _formService.GetAsync(ftpbReferenceId);

            var response = new CreateFormPayload()
            {
                FormDataXml = formdata.FormXmlData,
                DataFormatId = formdata.FormDbData.DataFormatId,
                DataFormatVersion = formdata.FormDbData.DataFormatVersion,
                FormMetadata = new CreateFormMetadata()
                {
                    Signeringsfrist = formdata.FormDbData.SigneringsFrist
                }
            };

            return Ok(response);
        }

        /// <summary>
        /// Søknadsystemets API for å sette status på en erklæring.
        /// P.t støttes det kun å angi status 'Trukket'
        /// </summary>
        /// <param name="payload"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/v1/[controller]/{ftpbReferenceId}/status")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string),(int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SetStatus(string ftpbReferenceId, [FromBody] StatusPayload payload)
        {
            using (Serilog.Context.LogContext.PushProperty("FtpbReferenceId", ftpbReferenceId))
            {
                if (payload == null || !ModelState.IsValid)
                    return new BadRequestObjectResult(ModelState);

                AnSaKoModel form = null;
                try
                { form = await _formService.GetAsync(ftpbReferenceId); }
                catch (FormNotFoundException fex)
                { _logger.LogError(fex, "Unable to find form"); }


                if (form == null)
                    return NotFound($"Skjema med referanse {ftpbReferenceId} finnes ikke");

                if (!SupportedState(payload.Status.Value))
                    return BadRequest($"Kun status 'trukket', 'signertManuelt', 'avsluttet' og 'feilet' er støttet pt.");


                if (StateHelper.CanTransitToState(form.FormDbData.Status, payload.Status.Value))
                {
                    var formStatus = form.FormDbData.Status;
                    await _formService.SetStatus(form, payload.Status.Value, payload.Aarsak);
                    return Ok($"Status endret fra '{formStatus}' til '{payload.Status}'");
                }
                else
                {
                    _logger.LogError("Bad request: Status change request cannot be handled. FormStatus: '{FormStatus}' Requested status: {RequestedStatus}", form.FormDbData.Status, payload.Status);
                    return BadRequest($"Kan ikke sette status til '{payload.Status}' når skjemaet har status '{form.FormDbData.Status}'");
                }
            }
        }

        private bool SupportedState(FormStatus formStatus)
        {
            FormStatus[] supportedStatuses = new FormStatus[] { FormStatus.Trukket, FormStatus.SignertManuelt, FormStatus.Feilet, FormStatus.Avsluttet };

            return supportedStatuses.Contains(formStatus);
        }

        public class StatusPayload
        {
            /// <summary>
            /// Status erklæringen
            /// </summary>
            [Required]
            public FormStatus? Status { get; set; }
            /// <summary>
            /// Årsak til endring i status
            /// </summary>
            [Required]
            public string Aarsak { get; set; }
        }
    }
}
