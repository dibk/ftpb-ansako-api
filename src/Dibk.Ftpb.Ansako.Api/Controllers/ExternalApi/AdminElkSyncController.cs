﻿using Dibk.Ftpb.Ansako.Api.Authentication;
using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Dibk.Ftpb.Ansako.Storage.Repositories;
using Dibk.Ftpb.Ansako.Storage.Service;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Api.Controllers.ExternalApi
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [EnableCors("AllowAll")]
    [BasicAuthorize]
    [ApiController]
    public class AdminElkSyncController : ControllerBase
    {
        private readonly ILogger<AdminElkSyncController> _logger;
        private readonly ElasticService _elasticService;
        private readonly FormDbRepository _formRepository;

        public AdminElkSyncController(ILogger<AdminElkSyncController> logger, ElasticService elasticService, FormDbRepository formDbRepository)
        {
            _logger = logger;
            _elasticService = elasticService;
            _formRepository = formDbRepository;
        }

        [HttpPost]
        [Route("api/v1/form/sync-to-elk")]
        public async Task<ActionResult> SyncToElk([FromBody] SyncToElkPayload payload = null)
        {
            List<FormDbModel> forms = new List<FormDbModel>();

            var formQuery = _formRepository.GetAll();
            if (payload == null
                || payload.AnSaKoReferenceIds == null
                || payload.AnSaKoReferenceIds?.Count() == 0)
            {
                _logger.LogDebug("No referenceIds provided, syncs all");
            }
            else
                formQuery = formQuery.Where(p => payload.AnSaKoReferenceIds.Contains(p.AnSaKoReferenceId));

            if (payload.StatusToSync?.Count > 0)
            {
                var statusToSync = payload.StatusToSync.Select(p => Enum.Parse<FormStatus>(p, true)).ToList();
                formQuery = formQuery.Where(p => statusToSync.Contains(p.Status));
            }

            forms = formQuery.ToList();

            var elkForms = forms.Select(p => (FormDoc)p).ToList();
            await _elasticService.AddBulkAllAsync(elkForms);

            return Ok($"Finished syncing to ELK for {string.Join(", ", forms.Select(p => p.AnSaKoReferenceId).ToList())}");
        }
    }

    public class SyncToElkPayload
    {
        public List<string> AnSaKoReferenceIds { get; set; }
        public List<string> StatusToSync { get; set; }
    }
}