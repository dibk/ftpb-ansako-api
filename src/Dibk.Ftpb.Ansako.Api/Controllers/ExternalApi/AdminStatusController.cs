﻿using Dibk.Ftpb.Ansako.Api.Authentication;
using Dibk.Ftpb.Ansako.App.Services.Admin;
using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Api.Controllers.ExternalApi
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [EnableCors("AllowAll")]
    [ApiController]
    public class AdminStatusController : ControllerBase
    {
        private readonly IServiceStateService _serviceStateService;

        public AdminStatusController(IServiceStateService serviceStateService)
        {
            _serviceStateService = serviceStateService;
        }

        [HttpGet]
        [Route("api/v1/[controller]/servicestate")]
        public async Task<ActionResult> Get()
        {
            var state = await _serviceStateService.GetCurrentServiceState();
            return Ok(state.ToApiModel());
        }

        [HttpPost]
        [BasicAuthorize]
        [Route("api/v1/[controller]/servicestate")]
        public async Task<ActionResult> Post(ServiceStatusApiModel newServiceStatus)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var state = await _serviceStateService.SetCurrentServiceState(newServiceStatus.ToDbModel());

            return Ok(state.ToApiModel());
        }
    }

    public class ServiceStatusApiModel
    {
        [Required]
        public ServiceState? State { get; set; }
        public string MessageHeader { get; set; }
        public string MessageBody { get; set; }
        public DateTime? StartDate { get; set; }
    }

    public static class ServiceStatusMapper
    {
        public static ServiceStatusApiModel ToApiModel(this ServiceStateDbModel serviceStateDbModel)
        {
            return new ServiceStatusApiModel()
            {
                MessageBody = serviceStateDbModel.MessageBody,
                MessageHeader = serviceStateDbModel.MessageHeader,
                State = serviceStateDbModel.State,
                StartDate = serviceStateDbModel.StartDate
            };
        }

        public static ServiceStateDbModel ToDbModel(this ServiceStatusApiModel serviceStatusApiModel)
        {
            return new ServiceStateDbModel()
            { 
                MessageBody = serviceStatusApiModel.MessageBody,
                MessageHeader = serviceStatusApiModel.MessageHeader,
                StartDate = serviceStatusApiModel.StartDate,
                State = serviceStatusApiModel.State.Value
            };
        }
    }
}
