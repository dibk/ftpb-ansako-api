using Dibk.Ftpb.Ansako.Api.Authentication;
using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.App.Services;
using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Api.Controllers
{
    [EnableCors]
    [ApiExplorerSettings(IgnoreApi = true)]
    [IdportenAuthorize]
    [ApiController]
    public class HtmlToPdfController : ControllerBase
    {
        private readonly PdfGeneratingService _htmlToPdfConverter;
        private readonly InternalApiFormService _internalApiFormService;
        private readonly ILogger<HtmlToPdfController> _logger;

        public HtmlToPdfController(PdfGeneratingService htmlToPdfConverter, InternalApiFormService internalApiFormService, ILogger<HtmlToPdfController> logger)
        {
            _htmlToPdfConverter = htmlToPdfConverter;
            _internalApiFormService = internalApiFormService;
            _logger = logger;
        }

        [HttpPost]
        [Route("api/v1/[controller]/{ansakoReferenceId}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<ActionResult> GeneratePdfFromHtmlAndSave(string ansakoReferenceId, [FromBody] string value)
        {
            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId))
                try
                {
                    var formdata = await _internalApiFormService.GetAsync(ansakoReferenceId);

                    if (formdata == null)
                        return NotFound();

                    if (!StateHelper.CanTransitToState(formdata.FormDbData.Status, FormStatus.TilSignering))
                        throw new InvalidStateChangeException(formdata.FormDbData.Status, FormStatus.TilSignering);

                    var generatedPdfSuccessfully = await _htmlToPdfConverter.GeneratePdfAndSaveToBlob(ansakoReferenceId, value);

                    if (generatedPdfSuccessfully)
                        return Ok();
                    else
                        return BadRequest("Unable to generate PDF.");
                }
                catch (InvalidStateChangeException ise)
                { return BadRequest(ise.Message); }
                catch (FormNotFoundException f)
                { return NotFound(f); }
                catch (Exception e)
                {
                    _logger.LogError(e, "Failed to convert HTML to pdf");
                    return BadRequest("Failed to parse HTML");
                }
        }

        [HttpPost]
        [Route("api/v1/[controller]")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<FileStreamResult> GeneratePdfFromHtml([FromBody] string value)
        {
            try
            {
                var pdfStream = await _htmlToPdfConverter.GeneratePdf(value);
                return new FileStreamResult(pdfStream, "application/pdf");
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to convert HTML to pdf");
                throw;
            }
        }
    }
}