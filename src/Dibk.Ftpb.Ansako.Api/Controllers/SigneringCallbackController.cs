﻿using Dibk.Ftpb.Ansako.App;
using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.App.Repositories.Models;
using Dibk.Ftpb.Ansako.App.Services;
using Dibk.Ftpb.Ansako.App.Services.Signing;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Api.Controllers
{
    [EnableCors("AllowAll")]
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiController]
    public class SigneringCallbackController : ControllerBase
    {
        private readonly ILogger<SigneringCallbackController> _logger;
        private readonly InternalApiFormService _internalApiFormService;
        private readonly SigneringsService _signeringsService;
        private readonly SignerFactory _signerFactory;
        private readonly AnSaKoSystemSettings _settings;

        public SigneringCallbackController(ILogger<SigneringCallbackController> logger,
                                   InternalApiFormService internalApiFormService,
                                   SigneringsService signeringsService,
                                   SignerFactory signerFactory,
                                   IOptions<AnSaKoSystemSettings> settings
                                   )
        {
            _logger = logger;
            _internalApiFormService = internalApiFormService;
            _signeringsService = signeringsService;
            _signerFactory = signerFactory;
            _settings = settings.Value;
        }

        [HttpGet]
        [Route("api/v1/[controller]/{ansakoReferenceId}")]
        public async Task<ActionResult> CallbackFromSigner(string ansakoReferenceId, [FromQuery] string status_query_token, [FromQuery] string signingevent, [FromQuery] string callback_token)
        {
            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId))
                try
                {
                    AnSaKoModel formdata = null;

                    try
                    {
                        formdata = await _internalApiFormService.GetAsync(ansakoReferenceId);
                    }
                    catch (FormNotFoundException ex)
                    {
                        _logger.LogError(ex, "Form doesn't exist");
                        return NotFound(ex.Message);
                    }

                    if (formdata.FormDbData.DateDeleted.HasValue)
                        return new StatusCodeResult((int)HttpStatusCode.Gone);

                    if (formdata.FormDbData.SigneringsjobOpprettetDato.HasValue) //Midlertidig fix for å ikke knekke eksisterende signeringsjobber i test-miljøet
                        if (!callback_token.Equals(formdata.FormDbData.SigneringsjobCallbackToken, StringComparison.OrdinalIgnoreCase))
                            return BadRequest("The callbacktoken is invalid");

                    var metaData = _signerFactory.GetSigner().GetSigningMetadataBuilder().Build(formdata.FormDbData.SigneringsjobMetadata, new System.Collections.Generic.KeyValuePair<string, string>("StatusQueryToken", status_query_token));
                    formdata.FormDbData.SigneringsjobMetadata = metaData;

                    formdata = await _internalApiFormService.Update(formdata);

                    var redirectUrl = string.Empty;

                    try
                    {
                        await _signeringsService.GetSigningJobStatus(formdata);

                        if (signingevent.Equals("signed", StringComparison.OrdinalIgnoreCase))
                        {
                            await _signeringsService.DocumentSigned(formdata);
                            redirectUrl = SigningRedirectUrls.FrontendRedirectOnSuccessUrl(_settings.FrontendUrl, ansakoReferenceId);
                        }
                        else if (signingevent.Equals("rejected", StringComparison.OrdinalIgnoreCase))
                        {
                            await _signeringsService.SigningJobRejected(formdata);
                            redirectUrl = SigningRedirectUrls.FrontendRedirectOnRejectedUrl(_settings.FrontendUrl, ansakoReferenceId);
                        }
                        else if (signingevent.Equals("error", StringComparison.OrdinalIgnoreCase))
                        {
                            await _signeringsService.SigningJobFailed(formdata);
                            redirectUrl = SigningRedirectUrls.FrontendRedirectOnErrorUrl(_settings.FrontendUrl, ansakoReferenceId, "esignering_error");
                        }
                    }
                    catch (SigningJobException s)
                    {
                        _logger.LogError(s, "An exception occured when receiving callback from signer system");
                        if (s.ExceptionType == SigningJobException.SigningJobExceptionType.NoSigningJobData)
                        {
                            redirectUrl = SigningRedirectUrls.FrontendRedirectOnErrorUrl(_settings.FrontendUrl, ansakoReferenceId, "signingjob_deleted");
                        }
                    }

                    return Redirect(redirectUrl);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "An error occurred when handling the callback from signing job");
                    throw;
                }
        }
    }
}
