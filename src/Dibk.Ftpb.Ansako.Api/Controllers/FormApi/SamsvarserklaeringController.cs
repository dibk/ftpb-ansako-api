﻿using Dibk.Ftpb.Ansako.Api.ApiResources.InternalApi;
using Dibk.Ftpb.Ansako.Api.Authentication;
using Dibk.Ftpb.Ansako.App.Models.InternalApi;
using Dibk.Ftpb.Ansako.App.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dibk.Ftpb.Ansako.Api.Controllers
{
    [EnableCors]
    [IdportenAuthorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiController]
    public class SamsvarserklaeringController : ErklaeringBaseController<SamsvarserklaeringDto, SamsvarserklaeringRepresentation>
    {
        public SamsvarserklaeringController(ILogger<SamsvarserklaeringController> logger, FormServiceProvider formServiceProvider, InternalApiFormService internalApiFormService) 
            : base(logger, internalApiFormService)
        {
            base.FormService = formServiceProvider.GetService(Storage.Database.Models.ErklaeringType.Samsvarserklaering);
            base.Mapper = new SamsvarserklaeringRepresentationMapper();
        }
    }
}
