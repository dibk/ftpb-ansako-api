﻿using Dibk.Ftpb.Ansako.Api.ApiResources.InternalApi;
using Dibk.Ftpb.Ansako.App.Models.InternalApi;
using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.App.Services;
using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Dibk.Ftpb.Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Api.Controllers
{
    public class ErklaeringBaseController<TDto, TRep> : ControllerBase where TDto : class, IFormDto where TRep : class, IInnsendingRepresentation
    {
        protected IFormService FormService;
        protected IRepresentationMapper<TRep, TDto> Mapper;
        private readonly InternalApiFormService _internalApiFormService;
        protected readonly ILogger Logger;


        public ErklaeringBaseController(ILogger logger, InternalApiFormService internalApiFormService)
        {
            Logger = logger;
            _internalApiFormService = internalApiFormService;
        }

        internal async Task<bool> HasActiveSigningJob(string ansakoReferenceId)
        {
            var form = await FormService.GetAsync(ansakoReferenceId);

            if (form.SigningJobStatus.HasValue && form.SigningJobStatus.Value == SigningJobStatus.Created)
                return true;
            else
                return false;
        }

        internal async Task RemoveActiveSigningJob(string ansakoReferenceId)
        {
            var form = await _internalApiFormService.GetAsync(ansakoReferenceId);
            form.FormDbData.SigneringsJobStatus = null;
            form.FormDbData.SigneringsJobId = null;
            form.FormDbData.SigneringsUrl = null;
            form.FormDbData.SigneringsjobMetadata = null;
            await _internalApiFormService.Update(form);
        }

        [AllowAnonymous]
        [Route("api/v1/[controller]/{ansakoReferenceId}")]
        [HttpGet]
        public async Task<IActionResult> Get(string ansakoReferenceId)
        {
            if (string.IsNullOrEmpty(ansakoReferenceId))
                return NotFound();

            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId))
                try
                {
                    var form = await FormService.GetAsync(ansakoReferenceId) as TDto;

                    return Ok(Mapper.Map(form));
                }
                catch (FormNotFoundException)
                {
                    return NotFound($"{ansakoReferenceId} doesn't exist");
                }
        }

        [Route("api/v1/[controller]/{ansakoReferenceId}")]
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put([FromBody] TRep formRepresentation, string ansakoReferenceId, [FromQuery] string stage)
        {
            if (string.IsNullOrEmpty(ansakoReferenceId))
                return NotFound();

            if (!formRepresentation.ReferanseId.Equals(ansakoReferenceId))
                return BadRequest("ReferanseId in content doesn't correspond with ReferanseId in URL");

            if (!string.IsNullOrEmpty(stage) && !stage.Equals("signing_preparation", System.StringComparison.OrdinalIgnoreCase))
                return BadRequest($"Stage query parameter value '{stage}' is invalid. Valid vaues are: 'signing_preparation'");

            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId))
                try
                {
                    TRep form;
                    if (await HasActiveSigningJob(ansakoReferenceId) && string.IsNullOrEmpty(stage))
                    {
                        return Conflict("Erklæringen har en aktiv signeringsjobb");
                    }
                    else
                    {
                        formRepresentation.Status = AnSaKoProcessStatusType.iArbeid;
                        TDto updatedForm = null;
                        if (!string.IsNullOrEmpty(stage) && stage.Equals("signing_preparation", System.StringComparison.OrdinalIgnoreCase))
                        {
                            updatedForm = await FormService.PrepareForSigning(Mapper.Map(formRepresentation)) as TDto;
                        }
                        else
                            updatedForm = await FormService.UpdateAsync(Mapper.Map(formRepresentation)) as TDto;

                        form = Mapper.Map(updatedForm);

                        return Ok(form);
                    }
                }
                catch (FormNotFoundException)
                {
                    return NotFound($"{ansakoReferenceId} doesn't exist");
                }
                catch (InvalidStateChangeException ise)
                {
                    return BadRequest(ise.Message);
                }
        }

        [Route("api/v1/[controller]/{ansakoReferenceId}/signering")]
        [HttpDelete]
        public async Task<IActionResult> DeleteSigningJobb(string ansakoReferenceId)
        {
            if (string.IsNullOrEmpty(ansakoReferenceId))
                return NotFound();

            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId))
                try
                {
                    await RemoveActiveSigningJob(ansakoReferenceId);
                    var form = await FormService.GetAsync(ansakoReferenceId) as TDto;
                    return Ok(Mapper.Map(form));
                }
                catch (FormNotFoundException)
                {
                    return NotFound($"{ansakoReferenceId} doesn't exist");
                }
        }

        /// <summary>
        /// Forbereding av skjemadata for signering. Brukes til å rigge dataene klart for signeringsjobb
        /// Dette må endres, da det ikkje kjem skikkelig fram at dette BØR gjerast før ein setter i gang signeringa
        /// </summary>
        /// <param name="ansakoReferenceId"></param>
        /// <returns></returns>
        [Route("api/v1/[controller]/{ansakoReferenceId}/signering/prepare")]
        [HttpGet]
        public async Task<IActionResult> PrepareForSigning(string ansakoReferenceId)
        {
            if (string.IsNullOrEmpty(ansakoReferenceId))
                return NotFound();

            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId))
                try
                {
                    var form = await FormService.GetAsync(ansakoReferenceId) as TDto;
                    return Ok(Mapper.Map(form));
                }
                catch (FormNotFoundException)
                {
                    return NotFound($"{ansakoReferenceId} doesn't exist");
                }
        }

        [Route("api/v1/[controller]/{ansakoReferenceId}/reject")]
        [HttpPost]
        public async Task<IActionResult> Reject(string ansakoReferenceId, [FromBody] ReasonPayload reasonPayload)
        {
            if (string.IsNullOrEmpty(ansakoReferenceId))
                return NotFound();

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId))
                try
                {
                    var form = await FormService.RejectAsync(ansakoReferenceId, reasonPayload.Reason) as TDto;
                    return Ok(Mapper.Map(form));
                }
                catch (FormNotFoundException)
                {
                    return NotFound($"{ansakoReferenceId} doesn't exist");
                }
                catch (InvalidStateChangeException ise)
                {
                    return BadRequest(ise.Message);
                }
        }
    }
}
