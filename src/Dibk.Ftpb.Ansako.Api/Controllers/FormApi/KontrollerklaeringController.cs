using Dibk.Ftpb.Ansako.Api.ApiResources.InternalApi;
using Dibk.Ftpb.Ansako.Api.Authentication;
using Dibk.Ftpb.Ansako.Api.ErrorHandling;
using Dibk.Ftpb.Ansako.App.Models.InternalApi;
using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.App.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Api.Controllers
{
    [EnableCors]
    [IdportenAuthorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiController]
    public class KontrollerklaeringController : ErklaeringBaseController<KontrollerklaeringDto, KontrollerklaeringRepresentation>
    {
        private readonly IAttachmentService _attachmentService;
        private readonly ClamAVService _clam;

        public KontrollerklaeringController(ILogger<KontrollerklaeringController> logger, FormServiceProvider formServiceProvider, IAttachmentService attachmentService, InternalApiFormService internalApiFormService, ClamAVService clam)
            : base(logger, internalApiFormService)
        {
            _attachmentService = attachmentService;
            _clam = clam;
            base.FormService = formServiceProvider.GetService(Storage.Database.Models.ErklaeringType.Kontrollerklaering);
            base.Mapper = new KontrollerklaeringRepresentationMapper();
        }


        [Route("api/v1/[controller]/{ansakoReferenceId}/attachment")]
        [HttpGet]
        public async Task<ActionResult<Stream>> GetAttachment(string ansakoReferenceId)
        {
            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId))
                try
                {
                    var blob = await _attachmentService.GetAttachmentAsync(ansakoReferenceId);
                    if (blob == null)
                        return NotFound();

                    var attachmentStream = blob.ContentStream;
                    if (attachmentStream != null)
                        return Ok(attachmentStream);
                    else
                        return NotFound();
                }
                catch (FormNotFoundException)
                {
                    return NotFound($"{ansakoReferenceId} doesn't exist");
                }
        }

        [Route("api/v1/[controller]/{ansakoReferenceId}/attachment")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> UploadAttachment(string ansakoReferenceId, IFormFile attachment)
        {
            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId))
            {
                if (attachment == null || attachment.Length == 0)
                {
                    Logger.LogWarning("BadRequest - Uploaded file is empty or has no length");
                    return BadRequest(AnSaKoProblemDetails.Create(AttachmentProblemType.AttachmentEmpty, "Filen er tom"));
                }

                if (attachment.FileName.Length > 80)
                {
                    Logger.LogWarning("BadRequest - Filename {FileName} is longer than 80 chars", attachment.FileName.Length);
                    return BadRequest(AnSaKoProblemDetails.Create(AttachmentProblemType.AttachmentFilenameTooLong, $"Filnavnet '{attachment.FileName}' overskrider makslengde på 80 tegn"));
                }


                if (await base.HasActiveSigningJob(ansakoReferenceId))
                {
                    Logger.LogWarning("BadRequest - Has an active signingjob");
                    return Conflict(AnSaKoProblemDetails.Create(SigningProblemType.ActiveSigningJobExists, "Erklæringen har en aktiv signeringsjobb"));
                }


                if (await _clam.FileInfectedAsync(attachment))
                {
                    Logger.LogWarning("BadRequest - ClamAV file scan considers the file as infected in some way");
                    return BadRequest(AnSaKoProblemDetails.Create(AttachmentProblemType.AttachmentInfected, "Filen kan være infisert"));
                }

                //Decide mime type
                new FileExtensionContentTypeProvider().TryGetContentType(attachment.FileName, out var mimeType);

                if (mimeType != "application/pdf")
                {
                    Logger.LogWarning("BadRequest - Uploaded files mimetype is {MimeType}. Only supports application/pdf", mimeType);
                    return BadRequest(AnSaKoProblemDetails.Create(AttachmentProblemType.UnsupportedFileType, "Filen må være en PDF"));
                }


                // Get byte array
                byte[] fileBytes = null;
                using (var ms = new MemoryStream())
                {
                    attachment.CopyTo(ms);
                    fileBytes = ms.ToArray();
                }

                if (IsEncryptedPdf(fileBytes))
                {
                    Logger.LogWarning("BadRequest - The uploaded file is password protected");
                    return BadRequest(AnSaKoProblemDetails.Create(AttachmentProblemType.AttachmentProtected, "Filen er beskyttet"));
                }

                try
                {
                    await _attachmentService.AddAttachmentAsync(ansakoReferenceId, attachment.FileName, mimeType, fileBytes);
                    var persistedAttachment = await _attachmentService.GetAttachmentAsync(ansakoReferenceId);

                    return Ok(new Attachment()
                    {
                        Filnavn = persistedAttachment.AttachmentFileName,
                        MimeType = persistedAttachment.MimeType,
                        Vedleggstype = persistedAttachment.Metadata["AttachmentType"],
                    });
                }
                catch (FormNotFoundException)
                {
                    return NotFound($"{ansakoReferenceId} doesn't exist");
                }
            }
        }

        private bool IsEncryptedPdf(byte[] fileBytes)
        {
            try
            {
                var pdf = new Spire.Pdf.PdfDocument(fileBytes);
                return pdf.IsEncrypted;
            }
            catch (System.Exception ex)
            {
                base.Logger.LogWarning(ex, "Error occurred while checking if PDF is password protected.");
                return false;
            }
        }

        [Route("api/v1/[controller]/{ansakoReferenceId}/attachment")]
        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeleteAttachment(string ansakoReferenceId)
        {
            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId))
                try
                {
                    await _attachmentService.DeleteAttachmentAsync(ansakoReferenceId);
                    return Ok();
                }
                catch (FormNotFoundException)
                {
                    return NotFound($"{ansakoReferenceId} doesn't exist");
                }
        }
    }
}
