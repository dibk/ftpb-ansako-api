﻿using Dibk.Ftpb.Ansako.Api.ApiResources.InternalApi;
using Dibk.Ftpb.Ansako.App.Services.RecurringTaskServices;
using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Api.Controllers
{
    [EnableCors]
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiController]
    public class RecurringTaskController : ControllerBase
    {
        private readonly ILogger<RecurringTaskController> _logger;
        private readonly DeadlineExpiredService _deadlineExpiredService;
        private readonly IBlobService _blobService;
        private readonly string _leaseBlobName = "DeadlineExpiredService";

        public RecurringTaskController(ILogger<RecurringTaskController> logger, DeadlineExpiredService deadlineExpiredService, IBlobService blobService, IConfiguration configuration)
        {
            _leaseBlobName = $"{_leaseBlobName}-{configuration["AnSaKo:Environment"]}";

            _logger = logger;
            _deadlineExpiredService = deadlineExpiredService;
            _blobService = blobService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(AnSaKoRepresentation), (int)HttpStatusCode.OK)]
        [Route("api/v1/[controller]/deadlineExpired")]
        public async Task<IActionResult> Get()
        {
            try
            {
                //Accuire lease to ensure just one instance executes job
                var leaseResult = await _blobService.AquireLeaseAsync(_leaseBlobName, new TimeSpan(0, 0, 60), CancellationToken.None);
                if (!leaseResult.SuccessfullLease)
                    return BadRequest($"Unable to initiate job: {leaseResult.Message}");

                var result = await _deadlineExpiredService.ScheduleDeadlineExpiredStatusChange();

                await _blobService.ReleaseLeaseAsync(_leaseBlobName, leaseResult.LeaseId, CancellationToken.None);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occured when setting status 'Utgått' for expired forms");
                throw;
            }
        }
    }
}