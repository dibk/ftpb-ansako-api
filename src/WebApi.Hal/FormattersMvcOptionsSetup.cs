using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.ObjectPool;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Buffers;
using WebApi.Hal.MediaTypeFormatters;

namespace WebApi
{
    public class FormattersMvcOptionsSetup : IConfigureOptions<MvcOptions>
    {
        private readonly ILoggerFactory _loggerFactory;
        private readonly JsonSerializerSettings _jsonSerializerSettings;
        private readonly ArrayPool<char> _charPool;
        private readonly ObjectPoolProvider _objectPoolProvider;
        private readonly MvcNewtonsoftJsonOptions _jsonOptions;

        public FormattersMvcOptionsSetup(
            ILoggerFactory loggerFactory,
            IOptions<MvcNewtonsoftJsonOptions> jsonOptions,
            ArrayPool<char> charPool,
            ObjectPoolProvider objectPoolProvider)
        {
            if (jsonOptions == null) throw new ArgumentNullException(nameof(jsonOptions));

            _loggerFactory = loggerFactory ?? throw new ArgumentNullException(nameof(loggerFactory));
            _jsonSerializerSettings = jsonOptions.Value.SerializerSettings;
            _charPool = charPool ?? throw new ArgumentNullException(nameof(charPool));
            _objectPoolProvider = objectPoolProvider ?? throw new ArgumentNullException(nameof(objectPoolProvider));
            _jsonOptions = jsonOptions.Value;
        }

        public void Configure(MvcOptions mvcOptions)
        {
            mvcOptions.OutputFormatters.Clear();
            mvcOptions.InputFormatters.Clear();

            mvcOptions.OutputFormatters.Add(new NewtonsoftJsonOutputFormatter(_jsonSerializerSettings, _charPool, mvcOptions, _jsonOptions));
            mvcOptions.OutputFormatters.Add(new JsonHalMediaTypeOutputFormatter(_jsonSerializerSettings, _charPool, mvcOptions, _jsonOptions));
            mvcOptions.OutputFormatters.Add(new StreamOutputFormatter());
            
            mvcOptions.InputFormatters.Add(new HtmlInputFormatter());
            mvcOptions.InputFormatters.Add(new XmlSerializerInputFormatter(mvcOptions));

            // Register JsonPatchInputFormatter before JsonInputFormatter, otherwise
            // JsonInputFormatter would consume "application/json-patch+json" requests
            // before JsonPatchInputFormatter gets to see them.

            var jsonInputPatchLogger = _loggerFactory.CreateLogger<JsonHalMediaTypeInputFormatter>();
            mvcOptions.InputFormatters.Add(new JsonHalMediaTypeInputFormatter(
                                            jsonInputPatchLogger,
                                            new JsonSerializerSettings(),
                                            _charPool,
                                            _objectPoolProvider,
                                            mvcOptions,
                                            _jsonOptions));

            var jsonInputSystemTextLogger = _loggerFactory.CreateLogger<SystemTextJsonInputFormatter>();
            mvcOptions.InputFormatters.Add(new SystemTextJsonInputFormatter(new JsonOptions(), jsonInputSystemTextLogger));
        }
    }
}
