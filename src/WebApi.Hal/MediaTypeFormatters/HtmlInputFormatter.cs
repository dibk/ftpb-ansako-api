﻿using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Net.Http.Headers;
using System;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Hal.MediaTypeFormatters
{
    public class HtmlInputFormatter : TextInputFormatter
    {
        public HtmlInputFormatter()
        {
            SupportedEncodings.Add(Encoding.UTF8);
            SupportedEncodings.Add(Encoding.Unicode);
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }

        public override Task<InputFormatterResult> ReadRequestBodyAsync
                        (InputFormatterContext context, Encoding encoding)
        {
            if (context == null)  throw new ArgumentNullException(nameof(context));
            if (encoding == null) throw new ArgumentNullException(nameof(encoding));
          
            var request = context.HttpContext.Request;
            using var streamReader = context.ReaderFactory(request.Body, encoding);
            var model = streamReader.ReadToEndAsync().Result;
            
            try
            {
                return InputFormatterResult.SuccessAsync(model);
            }
            catch (Exception)
            {
                return InputFormatterResult.FailureAsync();
            }
        }
    }
}
