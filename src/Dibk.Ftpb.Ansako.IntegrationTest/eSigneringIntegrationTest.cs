﻿using Dibk.Ftpb.Ansako.App;
using Dibk.Ftpb.Signing.eSignering;
using Dibk.Ftpb.Signing.Models;
using Microsoft.Extensions.Options;
using System.Reflection;

namespace Dibk.Ftpb.Ansako.IntegrationTest
{
    public class eSigneringIntegrationTest
    {
        private readonly eSigneringSigner _eSignering;
        private readonly AnSaKoSystemSettings _settings;

        public eSigneringIntegrationTest(eSigneringSigner eSignering, IOptions<AnSaKoSystemSettings> options)
        {
            _eSignering = eSignering;
            _settings = options.Value;
        }

        public async Task<SigningJobResult> InitiateSigningJob(string testReferenceId)
        {
            var docs = GetDocumentsToSign();
            var callbackToken = Guid.NewGuid().ToString();


            var signingJob = new SigningJobRequest()
            {
                SigneeName = "Test Signatør",
                SigneeEmail = "test@arkitektum.no",
                JobTitle = "DiBK eSignering Integration Test",
                SigningCallbackUrls = new SigningCallbackUrls()
                {
                    OnSuccessRedirectUrl = IntegrationTestSigningRedirectUrls.SigningJobSuccessdUrl(_settings.BackendUrl, testReferenceId, callbackToken),
                    OnCancelRedirectUrl = IntegrationTestSigningRedirectUrls.SigningJobRejectedUrl(_settings.BackendUrl, testReferenceId, callbackToken),
                    OnErrorRedirectUrl = IntegrationTestSigningRedirectUrls.SigningJobErrorUrl(_settings.BackendUrl, testReferenceId, callbackToken)
                },
                ExternalId = testReferenceId
            };
            signingJob.SigningDocuments.AddRange(docs);

            return await _eSignering.CreateSigningJob(signingJob);
        }

        private IEnumerable<SigningDocument> GetDocumentsToSign()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var documentStream = assembly.GetManifestResourceStream("Dibk.Ftpb.Ansako.IntegrationTest.SigningTestDocument.pdf");

            var signingDocument = new SigningDocument(SigningFileType.MainDocument)
            {
                FileContent = documentStream.GetAsBytes(),
                FileName = "SigningTestDocument.pdf",
                Title = "Integration test signing document"
            };

            return new List<SigningDocument>() { signingDocument };
        }
    }

    public static class IntegrationTestSigningRedirectUrls
    {
        public static string SigningJobSuccessdUrl(string host, string ansakoReferenceId, string callbackToken) => $"{host}/api/v1/signeringintegrationtest/{ansakoReferenceId}?signingevent=signed&callback_token={callbackToken}";
        public static string SigningJobRejectedUrl(string host, string ansakoReferenceId, string callbackToken) => $"{host}/api/v1/signeringintegrationtest/{ansakoReferenceId}?signingevent=rejected&callback_token={callbackToken}";
        public static string SigningJobErrorUrl(string host, string ansakoReferenceId, string callbackToken) => $"{host}/api/v1/signeringintegrationtest/{ansakoReferenceId}?signingevent=error&callback_token={callbackToken}";
    }
}