﻿using Dibk.Ftpb.Signing.eSignering;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Dibk.Ftpb.Ansako.IntegrationTest
{
    public static class IntegrationTestExtensions
    {
        public static IServiceCollection AddSigningIntegtaionTest(this IServiceCollection services)
        {
            services.AddTransient<eSigneringIntegrationTest>();
            services.AddTransient<eSigneringSigner>();
            services.AddMvc().AddApplicationPart(typeof(SigneringIntegrationTestController).GetTypeInfo().Assembly).AddControllersAsServices();
            return services;
        }
    }
}
