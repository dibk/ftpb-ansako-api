﻿using Dibk.Ftpb.Ansako.Api.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dibk.Ftpb.Ansako.IntegrationTest
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiController]
    public class SigneringIntegrationTestController : ControllerBase
    {
        private readonly ILogger<SigneringIntegrationTestController> _logger;
        private readonly eSigneringIntegrationTest _eSigneringIntegrationTest;

        public SigneringIntegrationTestController(ILogger<SigneringIntegrationTestController> logger,
            eSigneringIntegrationTest eSigneringIntegrationTest)
        {
            _logger = logger;
            _eSigneringIntegrationTest = eSigneringIntegrationTest;
        }

        [BasicAuthorize]
        [HttpPost]
        [Route("api/v1/[controller]")]
        public async Task<ActionResult> InitiateIntegrationTest([FromBody] IntegrationSigningTestRequest payload)
        {
            try
            {
                var result = await _eSigneringIntegrationTest.InitiateSigningJob(payload.TestReferenceId);

                _logger.LogInformation("eSignering integration test initated");
                return Ok(result);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "An error occurred when handling the callback from signing job");
                throw;
            }
        }

        [HttpGet]
        [Route("api/v1/[controller]/{ansakoReferenceId}")]
        public async Task<ActionResult> CallbackFromSigner(string ansakoReferenceId, [FromQuery] string status_query_token, [FromQuery] string signingevent, [FromQuery] string callback_token)
        {
            try
            {
                var result = new IntegrationSigningTestResult()
                {
                    SigningEvent = signingevent
                };

                _logger.LogInformation("eSignering integration test for {TestReferenceId} returned {StatusQueryToken} {SigningEvent}", ansakoReferenceId, status_query_token, signingevent);
                return Ok(result);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "An error occurred when handling the callback from test signing job");
                throw;
            }
        }

        public class IntegrationSigningTestResult
        {
            public string SigningEvent { get; set; }
        }
        public class IntegrationSigningTestRequest
        {
            public string TestReferenceId { get; set; }
        }
    }
}
