﻿using Microsoft.AspNetCore.Authorization;

namespace Dibk.Ftpb.Ansako.Api.Authentication
{
    public class IdportenAuthorizeAttribute : AuthorizeAttribute
    {

        public IdportenAuthorizeAttribute()
        {
            Policy = "idporten";
        }
    }
}