﻿using Microsoft.AspNetCore.Authorization;

namespace Dibk.Ftpb.Ansako.Api.Authentication
{
    public class BasicAuthorizeAttribute : AuthorizeAttribute
    {

        public BasicAuthorizeAttribute()
        {
            Policy = "basic-authentication";
        }
    }
}
