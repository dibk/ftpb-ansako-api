﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.JsonWebTokens;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace Dibk.Ftpb.Ansako.Api.Authentication
{
    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly IHttpClientFactory httpClientFactory;

        public BasicAuthenticationHandler(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            IHttpClientFactory httpClientFactory
            ) : base(options, logger, encoder)
        {
            this.httpClientFactory = httpClientFactory;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey("Authorization"))
            {
                return AuthenticateResult.Fail("Authorization header missing.");
            }

            var authorizationHeader = Request.Headers["Authorization"].ToString();
            var authHeaderRegex = new Regex(@"Basic (.*)");

            if (!authHeaderRegex.IsMatch(authorizationHeader))
            {
                return AuthenticateResult.Fail("Authorization code not formatted properly.");
            }

            HttpClient client = httpClientFactory.CreateClient("ftpbAuthClient");
            var authHeader = Request.Headers["Authorization"];
            string encodedUsernamePassword = authHeader.ToString().Substring("Basic ".Length).Trim();
            Encoding encoding = Encoding.GetEncoding("iso-8859-1");
            string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));
            var plainTextBytes = Encoding.UTF8.GetBytes(usernamePassword);

            HttpResponseMessage? response = null;
            string? responseContent = null;

            try
            {
                var message = new HttpRequestMessage(HttpMethod.Get, "api/authenticate");
                message.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(plainTextBytes));

                response = await client.SendAsync(message).ConfigureAwait(false);
                responseContent = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    return AuthenticateResult.Fail(responseContent);

                response.EnsureSuccessStatusCode();
            }
            catch (Exception exception)
            {
                if (response != null)
                    response.Dispose();
                return AuthenticateResult.Fail("Authorization failed. " + exception.Message);
            }

            IEnumerable<Claim>? claims = null;

            if (!string.IsNullOrEmpty(responseContent))
            {
                JsonWebToken? secToken = null;

                var handler = new JsonWebTokenHandler();
                using (var jsonDoc = JsonDocument.Parse(responseContent))
                {
                    var token = jsonDoc.RootElement.GetProperty("Token").GetString();
                    secToken = handler.ReadJsonWebToken(token);
                }

                if (secToken != null)
                    claims = secToken.Claims;
            }

            var authenticatedUser = new AuthenticatedUser("BasicAuthentication", true, null);
            var claimsPrincipal = new ClaimsPrincipal(new ClaimsIdentity(authenticatedUser, claims));

            return AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipal, Scheme.Name));
        }
    }

    public class AuthenticatedUser : IIdentity
    {
        public AuthenticatedUser(string authenticationType, bool isAuthenticated, string name)
        {
            AuthenticationType = authenticationType;
            IsAuthenticated = isAuthenticated;
            Name = name;
        }

        public string AuthenticationType { get; }

        public bool IsAuthenticated { get; }

        public string Name { get; }
    }
}