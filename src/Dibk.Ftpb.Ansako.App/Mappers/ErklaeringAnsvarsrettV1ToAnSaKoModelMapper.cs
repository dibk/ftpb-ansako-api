﻿using Dibk.Ftpb.Ansako.App.Models.InternalApi;
using Dibk.Ftpb.Ansako.App.Repositories.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using no.kxml.skjema.dibk.ansvarsrettAnsako;

namespace Dibk.Ftpb.Ansako.App.Mappers
{
    public static class ErklaeringAnsvarsrettV1ToAnSaKoModelMapper
    {
        public static void MapFromDto(AnsvarsrettDto ansvarsrettDto, ref AnSaKoModel form)
        {
            form.FormXmlData = XmlSerialization.SerializeObject<ErklaeringAnsvarsrettType>(ErklaeringAnsvarsrettV1ModelMapper.Map(ansvarsrettDto.FormData));

            form.FormDbData.DataFormatId = string.IsNullOrEmpty(form.FormDbData.DataFormatId) ? ansvarsrettDto.DataFormatId : form.FormDbData.DataFormatId;
            form.FormDbData.DataFormatVersion = string.IsNullOrEmpty(form.FormDbData.DataFormatVersion) ? ansvarsrettDto.DataFormatVersion : form.FormDbData.DataFormatVersion;
            form.FormDbData.AnsvarligForetakOrgnr = ansvarsrettDto.FormData.AnsvarligForetak.Organisasjonsnummer;
            form.FormDbData.AnsvarligSoekerOrgnr = ansvarsrettDto.FormData.AnsvarligSoeker.Organisasjonsnummer;
            form.FormDbData.ProsjektNavn = ansvarsrettDto.FormData.Prosjektnavn;
            form.FormDbData.Status = ansvarsrettDto.Status;
            form.FormDbData.StatusDetails = ansvarsrettDto.StatusDetails;
            //form.FormDbData.FormData = form.FormXmlData;
            //form.FormDbData.FormDataStoredInDb = true;
            //form.FormDbData.SigneringsFrist = ansvarsrettDto.Signeringsfrist;
            form.Hovedinnsendingsnummer = ansvarsrettDto.FormData.Hovedinnsendingsnummer;

            if (form.FormDbData.Ansvarsomraader == null)
                form.FormDbData.Ansvarsomraader = new List<Storage.Database.Models.AnsvarsomraadeDbModel>();

            foreach (var ansvarsomraade in ansvarsrettDto.FormData.Ansvarsomraader)
            {
                var a = form.FormDbData.Ansvarsomraader.FirstOrDefault(p => p.Soeknadssystemetsreferanse.Equals(ansvarsomraade.SoeknadssystemetsReferanse));

                if (a == null)
                {
                    a = new Storage.Database.Models.AnsvarsomraadeDbModel();
                    form.FormDbData.Ansvarsomraader.Add(a);
                    a.AnsvarsomraadeId = Guid.NewGuid();
                }

                a.AnSaKoReferenceId = form.FormDbData.AnSaKoReferenceId;
                a.Soeknadssystemetsreferanse = ansvarsomraade.SoeknadssystemetsReferanse;
                a.AnsvarsomraadeBeskrivelse = ansvarsomraade.BeskrivelseAvAnsvarsomraade;                
                a.DateUpdated = DateTime.Now;
            }
        }

        public static void MapNewModelFromXml(ErklaeringAnsvarsrettType ansvarsrett, ref AnSaKoModel form)
        {
            form.FormXmlData = XmlSerialization.SerializeObject<ErklaeringAnsvarsrettType>(ansvarsrett);

            if (form.FormDbData == null)
                form.FormDbData = new Storage.Database.Models.FormDbModel();

            form.FormDbData.DataFormatId = ansvarsrett.dataFormatId;
            form.FormDbData.DataFormatVersion = ansvarsrett.dataFormatVersion;
            form.FormDbData.AnsvarligForetakOrgnr = ansvarsrett.ansvarsrett?.foretak?.organisasjonsnummer;
            form.FormDbData.AnsvarligSoekerOrgnr = ansvarsrett.ansvarligSoeker?.organisasjonsnummer;
            form.FormDbData.ProsjektNavn = ansvarsrett.prosjektnavn;
            form.FormDbData.SluttbrukerSystem = ansvarsrett.fraSluttbrukersystem;
            form.Hovedinnsendingsnummer = ansvarsrett.hovedinnsendingsnummer;
            form.FormDbData.Soeknadssystemetsreferanse = ansvarsrett.ansvarsrett.ansvarsomraader.First().soeknadssystemetsReferanse;

            if (form.FormDbData.Ansvarsomraader == null)
                form.FormDbData.Ansvarsomraader = new List<Storage.Database.Models.AnsvarsomraadeDbModel>();

            foreach (var ansvarsomraade in ansvarsrett.ansvarsrett.ansvarsomraader)
            {
                var a = form.FormDbData.Ansvarsomraader.FirstOrDefault(p => p.Soeknadssystemetsreferanse.Equals(ansvarsomraade.soeknadssystemetsReferanse));

                if (a == null)
                {
                    a = new Storage.Database.Models.AnsvarsomraadeDbModel();
                    form.FormDbData.Ansvarsomraader.Add(a);
                }
                a.AnsvarsomraadeId = Guid.NewGuid();
                a.AnSaKoReferenceId = form.FormDbData.AnSaKoReferenceId;
                a.Soeknadssystemetsreferanse = ansvarsomraade.soeknadssystemetsReferanse;
                a.AnsvarsomraadeBeskrivelse = ansvarsomraade.beskrivelseAvAnsvarsomraade;
                a.AnsvarligforetakOrgnr = ansvarsrett.ansvarsrett?.foretak?.organisasjonsnummer;                
                a.DateCreated = DateTime.Now;
                a.DateUpdated = DateTime.Now;
            }
        }
    }
}
