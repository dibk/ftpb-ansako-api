﻿using Dibk.Ftpb.Ansako.App.Models.InternalApi;
using System.Collections.Generic;
using System.Linq;
using no.kxml.skjema.dibk.ansvarsrettAnsako;

namespace Dibk.Ftpb.Ansako.App.Mappers
{
    public static class ErklaeringAnsvarsrettV1ModelMapper
    {
        public static AnsvarsrettModel Map(string ansvarsrettXmlData)
        {
            var source = XmlSerialization.DeserializeFromString<ErklaeringAnsvarsrettType>(ansvarsrettXmlData);
            var ansvarsrett = new AnsvarsrettModel()
            {
                ErklaeringAnsvarligKontrollerende = source.ansvarsrett.erklaeringAnsvarligKontrollerende,
                ErklaeringAnsvarligProsjekterende = source.ansvarsrett.erklaeringAnsvarligProsjekterende,
                ErklaeringAnsvarligUtfoerende = source.ansvarsrett.erklaeringAnsvarligUtfoerende,
                FraSluttbrukerSystem = source.fraSluttbrukersystem,
                Hovedinnsendingsnummer = source.hovedinnsendingsnummer,
                Kommunenssaksaar = source.kommunensSaksnummer?.saksaar,
                Kommunenssakssekvensnummer = source.kommunensSaksnummer?.sakssekvensnummer,
                Prosjektnavn = source.prosjektnavn,
                Signaturdato = source.signatur?.signaturdato,
                SignertAvNavn = source.signatur?.signertAv
            };

            ansvarsrett.AnsvarligForetak = new ForetakModel()
            {
                Adresselinje1 = source.ansvarsrett.foretak?.adresse?.adresselinje1,
                Adresselinje2 = source.ansvarsrett.foretak?.adresse?.adresselinje2,
                Adresselinje3 = source.ansvarsrett.foretak?.adresse?.adresselinje3,
                Epost = source.ansvarsrett.foretak?.epost,
                HarSentralGodkjenning = source.ansvarsrett.foretak?.harSentralGodkjenning,
                KontaktpersonEpost = source.ansvarsrett.foretak?.kontaktperson?.epost,
                KontaktpersonMobilnummer = source.ansvarsrett.foretak?.kontaktperson?.mobilnummer,
                KontaktpersonTelefonnummer = source.ansvarsrett.foretak?.kontaktperson?.telefonnummer,
                KontaktpersonNavn = source.ansvarsrett.foretak?.kontaktperson?.navn,                
                Landkode = source.ansvarsrett.foretak?.adresse?.landkode,
                Mobilnummer = source.ansvarsrett.foretak?.mobilnummer,
                Telefonnummer = source.ansvarsrett.foretak?.telefonnummer,
                Navn = source.ansvarsrett.foretak?.navn,
                Organisasjonsnummer = source.ansvarsrett.foretak?.organisasjonsnummer,
                PartstypeKode = source.ansvarsrett.foretak?.partstype?.kodeverdi,
                PartstypeBeskrivelse = source.ansvarsrett.foretak?.partstype?.kodebeskrivelse,
                Postnr = source.ansvarsrett.foretak?.adresse?.postnr,
                Poststed = source.ansvarsrett.foretak?.adresse?.poststed,                
            };
            ansvarsrett.AnsvarligSoeker = new AnsvarligSoekerModel()
            {
                Adresselinje1 = source.ansvarligSoeker?.adresse?.adresselinje1,
                Adresselinje2 = source.ansvarligSoeker?.adresse?.adresselinje2,
                Adresselinje3 = source.ansvarligSoeker?.adresse?.adresselinje3,
                Epost = source.ansvarligSoeker?.epost,
                Foedselsnummer = source.ansvarligSoeker?.foedselsnummer,
                KontaktpersonEpost = source.ansvarligSoeker?.kontaktperson?.epost,
                KontaktpersonMobilnummer = source.ansvarligSoeker?.kontaktperson?.mobilnummer,
                KontaktpersonTelefonnummer = source.ansvarligSoeker?.kontaktperson?.telefonnummer,
                KontaktpersonNavn = source.ansvarligSoeker?.kontaktperson?.navn,                
                Landkode = source.ansvarligSoeker?.adresse?.landkode,
                Mobilnummer = source.ansvarligSoeker?.mobilnummer,
                Telefonnummer = source.ansvarligSoeker?.telefonnummer,
                Navn = source.ansvarligSoeker?.navn,
                Organisasjonsnummer = source.ansvarligSoeker?.organisasjonsnummer,
                PartstypeKode = source.ansvarligSoeker?.partstype?.kodeverdi,
                PartstypeBeskrivelse = source.ansvarligSoeker?.partstype?.kodebeskrivelse,
                Postnr = source.ansvarligSoeker?.adresse?.postnr,
                Poststed = source.ansvarligSoeker?.adresse?.poststed                
            };
            ansvarsrett.EiendomByggesteder = source.eiendomByggested?.Select(eiendom =>
                new EiendomModel()
                {
                    Adresselinje1 = eiendom?.adresse?.adresselinje1,
                    Adresselinje2 = eiendom.adresse?.adresselinje2,
                    Adresselinje3 = eiendom.adresse?.adresselinje3,
                    Bokstav = eiendom.adresse?.bokstav,
                    Gatenavn = eiendom.adresse?.gatenavn,
                    Husnr = eiendom.adresse?.husnr,
                    Landkode = eiendom.adresse?.landkode,
                    Postnr = eiendom.adresse?.postnr,
                    Poststed = eiendom.adresse?.poststed,
                    Bolignummer = eiendom.bolignummer,
                    Bygningsnummer = eiendom.bygningsnummer,
                    Bruksnummer = eiendom.eiendomsidentifikasjon?.bruksnummer,
                    Festenummer = eiendom.eiendomsidentifikasjon?.festenummer,
                    Gaardsnummer = eiendom.eiendomsidentifikasjon?.gaardsnummer,
                    Kommunenavn = eiendom.kommunenavn,
                    Kommunenummer = eiendom.eiendomsidentifikasjon?.kommunenummer,
                    Seksjonsnummer = eiendom.eiendomsidentifikasjon?.seksjonsnummer
                }).ToList();


            var ansvarsomraader = new List<AnsvarsomraadeModel>();
            for (int i = 0; i < source.ansvarsrett.ansvarsomraader.Count(); i++)
            {
                var srcAnsvarsomraade = source.ansvarsrett.ansvarsomraader[i];
                var destAnsvarsomraade = new AnsvarsomraadeModel()
                {
                    BeskrivelseAvAnsvarsomraade = srcAnsvarsomraade.beskrivelseAvAnsvarsomraade,
                    DekkesOmradetAvSentralGodkjenning = srcAnsvarsomraade.dekkesOmraadeAvSentralGodkjenning,
                    FunksjonBeskrivelse = srcAnsvarsomraade.funksjon?.kodebeskrivelse,
                    FunksjonKode = srcAnsvarsomraade.funksjon?.kodeverdi,
                    SamsvarKontrollVedFerdigattest = srcAnsvarsomraade.samsvarKontrollVedFerdigattest,
                    SamsvarKontrollVedIgangsettingstillatelse = srcAnsvarsomraade.samsvarKontrollVedIgangsettingstillatelse,
                    SamsvarKontrollVedMidlertidigBrukstillatelse = srcAnsvarsomraade.samsvarKontrollVedMidlertidigBrukstillatelse,
                    SamsvarKontrollVedRammetillatelse = srcAnsvarsomraade.samsvarKontrollVedRammetillatelse,
                    SoeknadssystemetsReferanse = srcAnsvarsomraade.soeknadssystemetsReferanse,
                    TiltaksklasseBeskrivelse = srcAnsvarsomraade.tiltaksklasse?.kodebeskrivelse,
                    TiltaksklasseKode = srcAnsvarsomraade.tiltaksklasse?.kodeverdi
                };
                ansvarsomraader.Add(destAnsvarsomraade);
            }

            ansvarsrett.Ansvarsomraader = ansvarsomraader;
            return ansvarsrett;
        }

        public static ErklaeringAnsvarsrettType Map(AnsvarsrettModel ansvarsrettModel)
        {
            var ansvarligSoeker = new PartType()
            {
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = ansvarsrettModel.AnsvarligSoeker.Adresselinje1,
                    adresselinje2 = ansvarsrettModel.AnsvarligSoeker.Adresselinje2,
                    adresselinje3 = ansvarsrettModel.AnsvarligSoeker.Adresselinje3,
                    landkode = ansvarsrettModel.AnsvarligSoeker.Landkode,
                    postnr = ansvarsrettModel.AnsvarligSoeker.Postnr,
                    poststed = ansvarsrettModel.AnsvarligSoeker.Poststed
                },
                epost = ansvarsrettModel.AnsvarligSoeker.Epost,
                foedselsnummer = ansvarsrettModel.AnsvarligSoeker.Foedselsnummer,
                kontaktperson = new KontaktpersonType()
                {
                    epost = ansvarsrettModel.AnsvarligSoeker.KontaktpersonEpost,
                    mobilnummer = ansvarsrettModel.AnsvarligSoeker.KontaktpersonMobilnummer,
                    navn = ansvarsrettModel.AnsvarligSoeker.KontaktpersonNavn,
                    telefonnummer = ansvarsrettModel.AnsvarligSoeker.KontaktpersonTelefonnummer
                },
                mobilnummer = ansvarsrettModel.AnsvarligSoeker.Mobilnummer,
                telefonnummer = ansvarsrettModel.AnsvarligSoeker.Telefonnummer,
                navn = ansvarsrettModel.AnsvarligSoeker.Navn,
                organisasjonsnummer = ansvarsrettModel.AnsvarligSoeker.Organisasjonsnummer,
                partstype = new KodeType()
                {
                    kodebeskrivelse = ansvarsrettModel.AnsvarligSoeker.PartstypeBeskrivelse,
                    kodeverdi = ansvarsrettModel.AnsvarligSoeker.PartstypeKode
                }                
            };

            var ansvarligForetak = new ForetakType()
            {
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = ansvarsrettModel.AnsvarligForetak.Adresselinje1,
                    adresselinje2 = ansvarsrettModel.AnsvarligForetak.Adresselinje2,
                    adresselinje3 = ansvarsrettModel.AnsvarligForetak.Adresselinje3,
                    landkode = ansvarsrettModel.AnsvarligForetak.Landkode,
                    postnr = ansvarsrettModel.AnsvarligForetak.Postnr,
                    poststed = ansvarsrettModel.AnsvarligForetak.Poststed
                },
                epost = ansvarsrettModel.AnsvarligForetak.Epost,
                harSentralGodkjenning = ansvarsrettModel.AnsvarligForetak.HarSentralGodkjenning.HasValue ? ansvarsrettModel.AnsvarligForetak.HarSentralGodkjenning : null,
                harSentralGodkjenningSpecified = ansvarsrettModel.AnsvarligForetak.HarSentralGodkjenning.HasValue,
                kontaktperson = new KontaktpersonType()
                {
                    epost = ansvarsrettModel.AnsvarligForetak.KontaktpersonEpost,
                    mobilnummer = ansvarsrettModel.AnsvarligForetak.KontaktpersonMobilnummer,
                    telefonnummer = ansvarsrettModel.AnsvarligForetak.KontaktpersonTelefonnummer,
                    navn = ansvarsrettModel.AnsvarligForetak.KontaktpersonNavn  
                },
                mobilnummer = ansvarsrettModel.AnsvarligForetak.Mobilnummer,
                telefonnummer = ansvarsrettModel.AnsvarligForetak.Telefonnummer,
                navn = ansvarsrettModel.AnsvarligForetak.Navn,
                organisasjonsnummer = ansvarsrettModel.AnsvarligForetak.Organisasjonsnummer,

                partstype = new KodeType()
                {
                    kodebeskrivelse = ansvarsrettModel.AnsvarligForetak.PartstypeBeskrivelse,
                    kodeverdi = ansvarsrettModel.AnsvarligForetak.PartstypeKode
                }                
            };

            var ansvarsrett = new AnsvarsrettType()
            {

                erklaeringAnsvarligKontrollerende = ansvarsrettModel.ErklaeringAnsvarligKontrollerende.HasValue ? ansvarsrettModel.ErklaeringAnsvarligKontrollerende.Value : null,
                erklaeringAnsvarligKontrollerendeSpecified = ansvarsrettModel.ErklaeringAnsvarligKontrollerende.HasValue,
                erklaeringAnsvarligProsjekterende = ansvarsrettModel.ErklaeringAnsvarligProsjekterende.HasValue ? ansvarsrettModel.ErklaeringAnsvarligProsjekterende.Value : null,
                erklaeringAnsvarligProsjekterendeSpecified = ansvarsrettModel.ErklaeringAnsvarligProsjekterende.HasValue,
                erklaeringAnsvarligUtfoerende = ansvarsrettModel.ErklaeringAnsvarligUtfoerende.HasValue ? ansvarsrettModel.ErklaeringAnsvarligUtfoerende.Value : null,
                erklaeringAnsvarligUtfoerendeSpecified = ansvarsrettModel.ErklaeringAnsvarligUtfoerende.HasValue,
                ansvarsomraader = ansvarsrettModel.Ansvarsomraader.Select(ansvarsomraade => new AnsvarsomraadeType()
                {
                    beskrivelseAvAnsvarsomraade = ansvarsomraade.BeskrivelseAvAnsvarsomraade,
                    dekkesOmraadeAvSentralGodkjenning = ansvarsomraade.DekkesOmradetAvSentralGodkjenning,
                    dekkesOmraadeAvSentralGodkjenningSpecified = ansvarsomraade.DekkesOmradetAvSentralGodkjenning.HasValue,
                    funksjon = new KodeType()
                    {
                        kodebeskrivelse = ansvarsomraade.FunksjonBeskrivelse,
                        kodeverdi = ansvarsomraade.FunksjonKode
                    },
                    samsvarKontrollVedFerdigattest = ansvarsomraade.SamsvarKontrollVedFerdigattest,
                    samsvarKontrollVedFerdigattestSpecified = ansvarsomraade.SamsvarKontrollVedFerdigattest.HasValue,
                    samsvarKontrollVedIgangsettingstillatelse = ansvarsomraade.SamsvarKontrollVedIgangsettingstillatelse,
                    samsvarKontrollVedIgangsettingstillatelseSpecified = ansvarsomraade.SamsvarKontrollVedIgangsettingstillatelse.HasValue,
                    samsvarKontrollVedMidlertidigBrukstillatelse = ansvarsomraade.SamsvarKontrollVedMidlertidigBrukstillatelse,
                    samsvarKontrollVedMidlertidigBrukstillatelseSpecified = ansvarsomraade.SamsvarKontrollVedMidlertidigBrukstillatelse.HasValue,
                    samsvarKontrollVedRammetillatelse = ansvarsomraade.SamsvarKontrollVedRammetillatelse,
                    samsvarKontrollVedRammetillatelseSpecified = ansvarsomraade.SamsvarKontrollVedRammetillatelse.HasValue,
                    tiltaksklasse = new KodeType()
                    {
                        kodebeskrivelse = ansvarsomraade.TiltaksklasseBeskrivelse,
                        kodeverdi = ansvarsomraade.TiltaksklasseKode
                    },
                    soeknadssystemetsReferanse = ansvarsomraade.SoeknadssystemetsReferanse
                }).ToArray(),
                foretak = ansvarligForetak
            };

            var destination = new ErklaeringAnsvarsrettType()
            {
                ansvarligSoeker = ansvarligSoeker,
                ansvarsrett = ansvarsrett,
                eiendomByggested = ansvarsrettModel.EiendomByggesteder.Select(eiendom =>
                    new EiendomType()
                    {
                        adresse = new EiendommensAdresseType()
                        {
                            adresselinje1 = eiendom.Adresselinje1,
                            adresselinje2 = eiendom.Adresselinje2,
                            adresselinje3 = eiendom.Adresselinje3,
                            bokstav = eiendom.Bokstav,
                            gatenavn = eiendom.Gatenavn,
                            husnr = eiendom.Husnr,
                            landkode = eiendom.Landkode,
                            postnr = eiendom.Postnr,
                            poststed = eiendom.Poststed
                        },
                        bolignummer = eiendom.Bolignummer,
                        bygningsnummer = eiendom.Bygningsnummer,
                        kommunenavn = eiendom.Kommunenavn,
                        eiendomsidentifikasjon = new MatrikkelnummerType()
                        {
                            bruksnummer = eiendom.Bruksnummer,
                            festenummer = eiendom.Festenummer,
                            gaardsnummer = eiendom.Gaardsnummer,
                            kommunenummer = eiendom.Kommunenummer,
                            seksjonsnummer = eiendom.Seksjonsnummer
                        }
                    }
                ).ToArray(),
                fraSluttbrukersystem = ansvarsrettModel.FraSluttbrukerSystem,
                hovedinnsendingsnummer = ansvarsrettModel.Hovedinnsendingsnummer,
                kommunensSaksnummer = new SaksnummerType()
                {
                    saksaar = ansvarsrettModel.Kommunenssaksaar,
                    sakssekvensnummer = ansvarsrettModel.Kommunenssakssekvensnummer
                },
                prosjektnavn = ansvarsrettModel.Prosjektnavn,
                signatur = ansvarsrettModel.Signaturdato.HasValue ? new SignaturType()
                {
                    signaturdato = ansvarsrettModel.Signaturdato.Value,
                    signertAv = ansvarsrettModel.SignertAvNavn
                } : null
            };

            return destination;
        }
    }
}