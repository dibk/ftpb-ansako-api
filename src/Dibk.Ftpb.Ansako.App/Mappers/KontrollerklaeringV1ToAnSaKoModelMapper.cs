﻿using Dibk.Ftpb.Ansako.App.Models.InternalApi;
using Dibk.Ftpb.Ansako.App.Repositories.Models;
using no.kxml.skjema.dibk.kontrollerklaeringAnsako;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Ansako.App.Mappers
{
    public static class KontrollerklaeringV1ToAnSaKoModelMapper
    {
        public static void MapFromDto(KontrollerklaeringDto kontrollerklaeringDto, ref AnSaKoModel form)
        {
            form.FormXmlData = XmlSerialization.SerializeObject<KontrollerklaeringType>(KontrollerklaeringV1ModelMapper.Map(kontrollerklaeringDto.FormData));

            form.FormDbData.DataFormatId = string.IsNullOrEmpty(form.FormDbData.DataFormatId) ? kontrollerklaeringDto.DataFormatId : form.FormDbData.DataFormatId;
            form.FormDbData.DataFormatVersion = string.IsNullOrEmpty(form.FormDbData.DataFormatVersion) ? kontrollerklaeringDto.DataFormatVersion : form.FormDbData.DataFormatVersion;
            form.FormDbData.AnsvarligForetakOrgnr = kontrollerklaeringDto.FormData.AnsvarligForetak.Organisasjonsnummer;
            form.FormDbData.AnsvarligSoekerOrgnr = kontrollerklaeringDto.FormData.AnsvarligSoeker.Organisasjonsnummer;
            form.FormDbData.ProsjektNavn = kontrollerklaeringDto.FormData.Prosjektnavn;
            form.FormDbData.Status = kontrollerklaeringDto.Status;
            form.FormDbData.StatusDetails = kontrollerklaeringDto.StatusDetails;
            //form.FormDbData.SigneringsFrist = kontrollerklaeringDto.Signeringsfrist;
            form.Hovedinnsendingsnummer = kontrollerklaeringDto.FormData.Hovedinnsendingsnummer;

            if (form.FormDbData.Ansvarsomraader == null)
                form.FormDbData.Ansvarsomraader = new List<Storage.Database.Models.AnsvarsomraadeDbModel>();

            var a = form.FormDbData.Ansvarsomraader.FirstOrDefault();

            if (a == null)
            {
                a = new Storage.Database.Models.AnsvarsomraadeDbModel();
                form.FormDbData.Ansvarsomraader.Add(a);
                a.AnsvarsomraadeId = Guid.NewGuid();
            }

            a.AnSaKoReferenceId = form.FormDbData.AnSaKoReferenceId;
            a.Soeknadssystemetsreferanse = kontrollerklaeringDto.FormData.SoeknadssystemetsReferanse;
            a.AnsvarsomraadeBeskrivelse = kontrollerklaeringDto.FormData.BeskrivelseAvAnsvarsomraadet;
            a.DateCreated = DateTime.Now;
        }

        public static void MapNewModelFromXml(KontrollerklaeringType kontrollerklaering, ref AnSaKoModel form)
        {
            form.FormXmlData = XmlSerialization.SerializeObject<KontrollerklaeringType>(kontrollerklaering);

            if (form.FormDbData == null)
                form.FormDbData = new Storage.Database.Models.FormDbModel();

            form.FormDbData.DataFormatId = kontrollerklaering.dataFormatId;
            form.FormDbData.DataFormatVersion = kontrollerklaering.dataFormatVersion;
            form.FormDbData.AnsvarligForetakOrgnr = kontrollerklaering.foretak?.organisasjonsnummer;
            form.FormDbData.AnsvarligSoekerOrgnr = kontrollerklaering.ansvarligSoeker?.organisasjonsnummer;
            form.FormDbData.ProsjektNavn = kontrollerklaering.prosjektnavn;
            form.FormDbData.SluttbrukerSystem = kontrollerklaering.fraSluttbrukersystem;
            form.Hovedinnsendingsnummer = kontrollerklaering.hovedinnsendingsnummer;
            form.FormDbData.Soeknadssystemetsreferanse = kontrollerklaering.ansvarsrett.soeknadssystemetsReferanse;

            var ansvarsomraade = new Storage.Database.Models.AnsvarsomraadeDbModel();                            
            ansvarsomraade.AnsvarsomraadeBeskrivelse = kontrollerklaering.ansvarsrett.beskrivelseAvAnsvarsomraadet;
            ansvarsomraade.AnsvarsomraadeId = Guid.NewGuid();
            ansvarsomraade.AnsvarligforetakOrgnr = kontrollerklaering.foretak?.organisasjonsnummer;
            ansvarsomraade.AnSaKoReferenceId = form.AnSaKoReferenceId;
            ansvarsomraade.Soeknadssystemetsreferanse = kontrollerklaering.ansvarsrett.soeknadssystemetsReferanse;
            ansvarsomraade.DateCreated = DateTime.Now;
            ansvarsomraade.DateUpdated = DateTime.Now;

            form.FormDbData.Ansvarsomraader = new List<Storage.Database.Models.AnsvarsomraadeDbModel>() { ansvarsomraade };
        }
    }
}