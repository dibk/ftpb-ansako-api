﻿using Dibk.Ftpb.Ansako.App.Models.InternalApi;
using System.Linq;
using no.kxml.skjema.dibk.kontrollerklaeringAnsako;

namespace Dibk.Ftpb.Ansako.App.Mappers
{
    public static class KontrollerklaeringV1ModelMapper
    {
        public static KontrollerklaeringModel Map(string kontrollerklaeringXmlData)
        {
            var kontrollerklaeringXml = XmlSerialization.DeserializeFromString<KontrollerklaeringType>(kontrollerklaeringXmlData);
            var kontrollerklaeringModel = new KontrollerklaeringModel()
            {
                BeskrivelseAvAnsvarsomraadet = kontrollerklaeringXml.ansvarsrett.beskrivelseAvAnsvarsomraadet,
                FunksjonBeskrivelse = kontrollerklaeringXml.ansvarsrett.funksjon.kodebeskrivelse,
                FunksjonKode = kontrollerklaeringXml.ansvarsrett.funksjon.kodeverdi,
                FraSluttbrukerSystem = kontrollerklaeringXml.fraSluttbrukersystem,
                Hovedinnsendingsnummer = kontrollerklaeringXml.hovedinnsendingsnummer,
                Kommunenssaksaar = kontrollerklaeringXml.kommunensSaksnummer?.saksaar,
                Kommunenssakssekvensnummer = kontrollerklaeringXml.kommunensSaksnummer?.sakssekvensnummer,
                Prosjektnavn = kontrollerklaeringXml.prosjektnavn,
                Prosjektnummer = kontrollerklaeringXml.prosjektnr,
                Signaturdato = kontrollerklaeringXml.signatur?.signaturdato,
                SignertAvNavn = kontrollerklaeringXml.signatur?.signertAv,
                SoeknadssystemetsReferanse = kontrollerklaeringXml.ansvarsrett.soeknadssystemetsReferanse,
                AnsvarligForetak = new ForetakModel()
                {
                    Adresselinje1 = kontrollerklaeringXml.foretak?.adresse?.adresselinje1,
                    Adresselinje2 = kontrollerklaeringXml.foretak?.adresse?.adresselinje2,
                    Adresselinje3 = kontrollerklaeringXml.foretak?.adresse?.adresselinje3,
                    Epost = kontrollerklaeringXml.foretak?.epost,

                    KontaktpersonNavn = kontrollerklaeringXml.foretak?.kontaktperson.navn,
                    KontaktpersonEpost = kontrollerklaeringXml.foretak?.kontaktperson?.epost,
                    KontaktpersonMobilnummer = kontrollerklaeringXml.foretak?.kontaktperson.mobilnummer,                    
                    KontaktpersonTelefonnummer = kontrollerklaeringXml.foretak?.kontaktperson.telefonnummer,
                    Landkode = kontrollerklaeringXml.foretak?.adresse?.landkode,
                    Mobilnummer = kontrollerklaeringXml.foretak?.mobilnummer,
                    Telefonnummer = kontrollerklaeringXml.foretak?.telefonnummer,
                    Navn = kontrollerklaeringXml.foretak?.navn,
                    Organisasjonsnummer = kontrollerklaeringXml.foretak?.organisasjonsnummer,
                    PartstypeKode = kontrollerklaeringXml.foretak?.partstype?.kodeverdi,
                    PartstypeBeskrivelse = kontrollerklaeringXml.foretak?.partstype?.kodebeskrivelse,
                    Postnr = kontrollerklaeringXml.foretak?.adresse?.postnr,
                    Poststed = kontrollerklaeringXml.foretak?.adresse?.poststed
                },
                AnsvarligSoeker = new AnsvarligSoekerModel()
                {
                    Adresselinje1 = kontrollerklaeringXml.ansvarligSoeker?.adresse?.adresselinje1,
                    Adresselinje2 = kontrollerklaeringXml.ansvarligSoeker?.adresse?.adresselinje2,
                    Adresselinje3 = kontrollerklaeringXml.ansvarligSoeker?.adresse?.adresselinje3,
                    Epost = kontrollerklaeringXml.ansvarligSoeker?.epost,
                    Foedselsnummer = kontrollerklaeringXml.ansvarligSoeker?.foedselsnummer,
                    KontaktpersonEpost = kontrollerklaeringXml.ansvarligSoeker?.kontaktperson?.epost,
                    KontaktpersonNavn = kontrollerklaeringXml.ansvarligSoeker?.kontaktperson.navn,
                    KontaktpersonMobilnummer = kontrollerklaeringXml.ansvarligSoeker?.kontaktperson?.mobilnummer,                    
                    KontaktpersonTelefonnummer = kontrollerklaeringXml.ansvarligSoeker?.kontaktperson?.telefonnummer,
                    Landkode = kontrollerklaeringXml.ansvarligSoeker?.adresse?.landkode,
                    Mobilnummer = kontrollerklaeringXml.ansvarligSoeker?.mobilnummer,
                    Telefonnummer = kontrollerklaeringXml.ansvarligSoeker?.telefonnummer,
                    Navn = kontrollerklaeringXml.ansvarligSoeker?.navn,
                    Organisasjonsnummer = kontrollerklaeringXml.ansvarligSoeker?.organisasjonsnummer,
                    PartstypeKode = kontrollerklaeringXml.ansvarligSoeker?.partstype?.kodeverdi,
                    PartstypeBeskrivelse = kontrollerklaeringXml.ansvarligSoeker?.partstype?.kodebeskrivelse,
                    Postnr = kontrollerklaeringXml.ansvarligSoeker?.adresse?.postnr,
                    Poststed = kontrollerklaeringXml.ansvarligSoeker?.adresse?.poststed                   
                },
                EiendomByggesteder = kontrollerklaeringXml.eiendomByggested?.Select(eiendom =>
                new EiendomModel()
                {
                    Adresselinje1 = eiendom?.adresse?.adresselinje1,
                    Adresselinje2 = eiendom.adresse?.adresselinje2,
                    Adresselinje3 = eiendom.adresse?.adresselinje3,
                    Bokstav = eiendom.adresse?.bokstav,
                    Gatenavn = eiendom.adresse?.gatenavn,
                    Husnr = eiendom.adresse?.husnr,
                    Landkode = eiendom.adresse?.landkode,
                    Postnr = eiendom.adresse?.postnr,
                    Poststed = eiendom.adresse?.poststed,
                    Bolignummer = eiendom.bolignummer,
                    Bygningsnummer = eiendom.bygningsnummer,
                    Bruksnummer = eiendom.eiendomsidentifikasjon?.bruksnummer,
                    Festenummer = eiendom.eiendomsidentifikasjon?.festenummer,
                    Gaardsnummer = eiendom.eiendomsidentifikasjon?.gaardsnummer,
                    Kommunenavn = eiendom.kommunenavn,
                    Kommunenummer = eiendom.eiendomsidentifikasjon?.kommunenummer,
                    Seksjonsnummer = eiendom.eiendomsidentifikasjon?.seksjonsnummer
                }).ToList(),
                AnsvarsomraadetAvsluttet = kontrollerklaeringXml.ansvarsrett.ansvarsomraadetAvsluttet,
                AnsvarsrettErklaert = kontrollerklaeringXml.ansvarsrett.ansvarsrettErklaert,                
                AnsvarsrettKontrollerende = new AnsvarsrettKontrollerendeModel()
                {
                    AapneAvvik = kontrollerklaeringXml.ansvarsrett.kontrollerende?.aapneAvvik,
                    IngenAvvik = kontrollerklaeringXml.ansvarsrett.kontrollerende?.ingenAvvik,
                    ObserverteAvvik = kontrollerklaeringXml.ansvarsrett.kontrollerende?.observerteAvvik
                },
                ErklaeringKontroll = kontrollerklaeringXml.erklaeringKontroll
            };
            return kontrollerklaeringModel;
        }

        public static KontrollerklaeringType Map(KontrollerklaeringModel kontrollerklaeringModel)
        {
            var kontrollerklaeringXml = new KontrollerklaeringType()
            {
                ansvarligSoeker = new PartType()
                {
                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = kontrollerklaeringModel.AnsvarligSoeker.Adresselinje1,
                        adresselinje2 = kontrollerklaeringModel.AnsvarligSoeker.Adresselinje2,
                        adresselinje3 = kontrollerklaeringModel.AnsvarligSoeker.Adresselinje3,
                        landkode = kontrollerklaeringModel.AnsvarligSoeker.Landkode,
                        postnr = kontrollerklaeringModel.AnsvarligSoeker.Postnr,
                        poststed = kontrollerklaeringModel.AnsvarligSoeker.Poststed
                    },
                    epost = kontrollerklaeringModel.AnsvarligSoeker.Epost,
                    foedselsnummer = kontrollerklaeringModel.AnsvarligSoeker.Foedselsnummer,
                    kontaktperson = new KontaktpersonType()
                    {
                        epost = kontrollerklaeringModel.AnsvarligSoeker.KontaktpersonEpost,
                        navn = kontrollerklaeringModel.AnsvarligSoeker.KontaktpersonNavn,
                        mobilnummer = kontrollerklaeringModel.AnsvarligSoeker.KontaktpersonMobilnummer,                        
                        telefonnummer = kontrollerklaeringModel.AnsvarligSoeker.KontaktpersonTelefonnummer
                    },
                    mobilnummer = kontrollerklaeringModel.AnsvarligSoeker.Mobilnummer,
                    telefonnummer = kontrollerklaeringModel.AnsvarligSoeker.Telefonnummer,
                    navn = kontrollerklaeringModel.AnsvarligSoeker.Navn,
                    organisasjonsnummer = kontrollerklaeringModel.AnsvarligSoeker.Organisasjonsnummer,
                    partstype = new KodeType()
                    {
                        kodebeskrivelse = kontrollerklaeringModel.AnsvarligSoeker.PartstypeBeskrivelse,
                        kodeverdi = kontrollerklaeringModel.AnsvarligSoeker.PartstypeKode
                    }                    
                },

                ansvarsrett = new AnsvarsomraadeType()
                {
                    ansvarsomraadetAvsluttet = kontrollerklaeringModel.AnsvarsomraadetAvsluttet,
                    ansvarsomraadetAvsluttetSpecified = kontrollerklaeringModel.AnsvarsomraadetAvsluttet.HasValue,
                    kontrollerende = new KontrollerendeType()
                    {
                        aapneAvvikSpecified = kontrollerklaeringModel.AnsvarsrettKontrollerende.AapneAvvik.HasValue,
                        aapneAvvik = kontrollerklaeringModel.AnsvarsrettKontrollerende.AapneAvvik,
                        ingenAvvikSpecified = kontrollerklaeringModel.AnsvarsrettKontrollerende.IngenAvvik.HasValue,
                        ingenAvvik = kontrollerklaeringModel.AnsvarsrettKontrollerende.IngenAvvik,
                        observerteAvvikSpecified = kontrollerklaeringModel.AnsvarsrettKontrollerende.ObserverteAvvik.HasValue,
                        observerteAvvik = kontrollerklaeringModel.AnsvarsrettKontrollerende.ObserverteAvvik
                    },
                    ansvarsrettErklaert = kontrollerklaeringModel.AnsvarsrettErklaert,
                    ansvarsrettErklaertSpecified = kontrollerklaeringModel.AnsvarsrettErklaert.HasValue,
                    beskrivelseAvAnsvarsomraadet = kontrollerklaeringModel.BeskrivelseAvAnsvarsomraadet,
                    funksjon = new KodeType()
                    {
                        kodebeskrivelse = kontrollerklaeringModel.FunksjonBeskrivelse,
                        kodeverdi = kontrollerklaeringModel.FunksjonKode
                    },
                    soeknadssystemetsReferanse = kontrollerklaeringModel.SoeknadssystemetsReferanse
                },
                eiendomByggested = kontrollerklaeringModel.EiendomByggesteder.Select(eiendom =>
                    new EiendomType()
                    {
                        adresse = new EiendommensAdresseType()
                        {
                            adresselinje1 = eiendom.Adresselinje1,
                            adresselinje2 = eiendom.Adresselinje2,
                            adresselinje3 = eiendom.Adresselinje3,
                            bokstav = eiendom.Bokstav,
                            gatenavn = eiendom.Gatenavn,
                            husnr = eiendom.Husnr,
                            landkode = eiendom.Landkode,
                            postnr = eiendom.Postnr,
                            poststed = eiendom.Poststed
                        },
                        bolignummer = eiendom.Bolignummer,
                        bygningsnummer = eiendom.Bygningsnummer,
                        kommunenavn = eiendom.Kommunenavn,
                        eiendomsidentifikasjon = new MatrikkelnummerType()
                        {
                            bruksnummer = eiendom.Bruksnummer,
                            festenummer = eiendom.Festenummer,
                            gaardsnummer = eiendom.Gaardsnummer,
                            kommunenummer = eiendom.Kommunenummer,
                            seksjonsnummer = eiendom.Seksjonsnummer
                        }
                    }
                ).ToArray(),
                fraSluttbrukersystem = kontrollerklaeringModel.FraSluttbrukerSystem,
                hovedinnsendingsnummer = kontrollerklaeringModel.Hovedinnsendingsnummer,
                kommunensSaksnummer = new SaksnummerType()
                {
                    saksaar = kontrollerklaeringModel.Kommunenssaksaar,
                    sakssekvensnummer = kontrollerklaeringModel.Kommunenssakssekvensnummer
                },
                prosjektnavn = kontrollerklaeringModel.Prosjektnavn,
                signatur = kontrollerklaeringModel.Signaturdato.HasValue ? new SignaturType()
                {
                    signaturdato = kontrollerklaeringModel.Signaturdato.Value,
                    signaturdatoSpecified = true,
                    signertAv = kontrollerklaeringModel.SignertAvNavn
                } : null,
                prosjektnr = kontrollerklaeringModel.Prosjektnummer,
                foretak = new PartType()
                {
                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = kontrollerklaeringModel.AnsvarligForetak.Adresselinje1,
                        adresselinje2 = kontrollerklaeringModel.AnsvarligForetak.Adresselinje2,
                        adresselinje3 = kontrollerklaeringModel.AnsvarligForetak.Adresselinje3,
                        landkode = kontrollerklaeringModel.AnsvarligForetak.Landkode,
                        postnr = kontrollerklaeringModel.AnsvarligForetak.Postnr,
                        poststed = kontrollerklaeringModel.AnsvarligForetak.Poststed
                    },
                    epost = kontrollerklaeringModel.AnsvarligForetak.Epost,
                    kontaktperson = new KontaktpersonType()
                    {
                        epost = kontrollerklaeringModel.AnsvarligForetak.KontaktpersonEpost,
                        navn = kontrollerklaeringModel.AnsvarligForetak.KontaktpersonNavn,
                        mobilnummer = kontrollerklaeringModel.AnsvarligForetak.KontaktpersonMobilnummer,                        
                        telefonnummer = kontrollerklaeringModel.AnsvarligForetak.KontaktpersonTelefonnummer
                    },
                    navn = kontrollerklaeringModel.AnsvarligForetak.Navn,
                    mobilnummer = kontrollerklaeringModel.AnsvarligForetak.Mobilnummer,
                    telefonnummer = kontrollerklaeringModel.AnsvarligForetak.Telefonnummer,                    
                    organisasjonsnummer = kontrollerklaeringModel.AnsvarligForetak.Organisasjonsnummer,
                    partstype = new KodeType()
                    {
                        kodebeskrivelse = kontrollerklaeringModel.AnsvarligForetak.PartstypeBeskrivelse,
                        kodeverdi = kontrollerklaeringModel.AnsvarligForetak.PartstypeKode
                    }                    
                },
                erklaeringKontroll = kontrollerklaeringModel.ErklaeringKontroll,
                erklaeringKontrollSpecified = kontrollerklaeringModel.ErklaeringKontroll.HasValue
            };

            return kontrollerklaeringXml;
        }
    }
}
