﻿using Dibk.Ftpb.Ansako.App.Models.InternalApi;
using System.Linq;
using no.kxml.skjema.dibk.samsvarserklaeringAnsako;

namespace Dibk.Ftpb.Ansako.App.Mappers
{
    public static class SamsvarserklaeringV1ModelMapper
    {
        public static SamsvarserklaeringModel Map(string ansvarsrettXmlData)
        {
            var source = XmlSerialization.DeserializeFromString<SamsvarserklaeringType>(ansvarsrettXmlData);
            var destination = new SamsvarserklaeringModel()
            {
                BeskrivelseAvAnsvarsomraadet = source.ansvarsrett.beskrivelseAvAnsvarsomraadet,
                ErklaeringProsjektering = source.erklaeringProsjektering,
                ErklaeringUtfoerelse = source.erklaeringUtfoerelse,
                FunksjonBeskrivelse = source.ansvarsrett.funksjon?.kodebeskrivelse,
                FunksjonKode = source.ansvarsrett.funksjon?.kodeverdi,
                FraSluttbrukerSystem = source.fraSluttbrukersystem,
                Hovedinnsendingsnummer = source.hovedinnsendingsnummer,
                Kommunenssaksaar = source.kommunensSaksnummer?.saksaar,
                Kommunenssakssekvensnummer = source.kommunensSaksnummer?.sakssekvensnummer,
                Prosjektnavn = source.prosjektnavn,
                Prosjektnummer = source.prosjektnr,
                Signaturdato = source.signatur?.signaturdato,
                SignertAvNavn = source.signatur?.signertAv,
                SoeknadssystemetsReferanse = source.ansvarsrett.soeknadssystemetsReferanse,
                AnsvarligForetak = new ForetakModel()
                {
                    Adresselinje1 = source.foretak?.adresse?.adresselinje1,
                    Adresselinje2 = source.foretak?.adresse?.adresselinje2,
                    Adresselinje3 = source.foretak?.adresse?.adresselinje3,
                    Epost = source.foretak?.epost,

                    KontaktpersonNavn = source.foretak?.kontaktperson?.navn,
                    KontaktpersonEpost = source.foretak?.kontaktperson?.epost,
                    KontaktpersonMobilnummer = source.foretak?.kontaktperson?.mobilnummer,                    
                    KontaktpersonTelefonnummer = source.foretak?.kontaktperson?.telefonnummer,
                    Landkode = source.foretak?.adresse?.landkode,
                    Mobilnummer = source.foretak?.mobilnummer,
                    Telefonnummer = source.foretak?.telefonnummer,
                    Navn = source.foretak?.navn,
                    Organisasjonsnummer = source.foretak?.organisasjonsnummer,
                    PartstypeKode = source.foretak?.partstype?.kodeverdi,
                    PartstypeBeskrivelse = source.foretak?.partstype?.kodebeskrivelse,
                    Postnr = source.foretak?.adresse?.postnr,
                    Poststed = source.foretak?.adresse?.poststed                    
                },
                AnsvarligSoeker = new AnsvarligSoekerModel()
                {
                    Adresselinje1 = source.ansvarligSoeker?.adresse?.adresselinje1,
                    Adresselinje2 = source.ansvarligSoeker?.adresse?.adresselinje2,
                    Adresselinje3 = source.ansvarligSoeker?.adresse?.adresselinje3,
                    Epost = source.ansvarligSoeker?.epost,
                    Foedselsnummer = source.ansvarligSoeker?.foedselsnummer,
                    KontaktpersonNavn = source.ansvarligSoeker?.kontaktperson?.navn,
                    KontaktpersonEpost = source.ansvarligSoeker?.kontaktperson?.epost,
                    KontaktpersonMobilnummer = source.ansvarligSoeker?.kontaktperson?.mobilnummer,                    
                    KontaktpersonTelefonnummer = source.ansvarligSoeker?.kontaktperson?.telefonnummer,
                    Landkode = source.ansvarligSoeker?.adresse?.landkode,
                    Mobilnummer = source.ansvarligSoeker?.mobilnummer,
                    Telefonnummer = source.ansvarligSoeker?.telefonnummer,
                    Navn = source.ansvarligSoeker?.navn,
                    Organisasjonsnummer = source.ansvarligSoeker?.organisasjonsnummer,
                    PartstypeKode = source.ansvarligSoeker?.partstype?.kodeverdi,
                    PartstypeBeskrivelse = source.ansvarligSoeker?.partstype?.kodebeskrivelse,
                    Postnr = source.ansvarligSoeker?.adresse?.postnr,
                    Poststed = source.ansvarligSoeker?.adresse?.poststed                    
                },
                EiendomByggesteder = source.eiendomByggested?.Select(eiendom =>
                new EiendomModel()
                {
                    Adresselinje1 = eiendom?.adresse?.adresselinje1,
                    Adresselinje2 = eiendom.adresse?.adresselinje2,
                    Adresselinje3 = eiendom.adresse?.adresselinje3,
                    Bokstav = eiendom.adresse?.bokstav,
                    Gatenavn = eiendom.adresse?.gatenavn,
                    Husnr = eiendom.adresse?.husnr,
                    Landkode = eiendom.adresse?.landkode,
                    Postnr = eiendom.adresse?.postnr,
                    Poststed = eiendom.adresse?.poststed,
                    Bolignummer = eiendom.bolignummer,
                    Bygningsnummer = eiendom.bygningsnummer,
                    Bruksnummer = eiendom.eiendomsidentifikasjon?.bruksnummer,
                    Festenummer = eiendom.eiendomsidentifikasjon?.festenummer,
                    Gaardsnummer = eiendom.eiendomsidentifikasjon?.gaardsnummer,
                    Kommunenavn = eiendom.kommunenavn,
                    Kommunenummer = eiendom.eiendomsidentifikasjon?.kommunenummer,
                    Seksjonsnummer = eiendom.eiendomsidentifikasjon?.seksjonsnummer
                }).ToList(),
                AnsvarsomraadetAvsluttet = source.ansvarsrett.ansvarsomraadetAvsluttet,
                AnsvarsrettErklaert = source.ansvarsrett.ansvarsrettErklaert,
                AnsvarsrettProsjekterende = source.ansvarsrett.prosjekterende != null ? new AnsvarsrettProsjekterendeModel()
                {
                    OkForFerdigattest = source.ansvarsrett.prosjekterende?.okForFerdigattest,
                    OkForIgangsetting = source.ansvarsrett.prosjekterende?.okForIgangsetting,
                    OkForMidlertidigBrukstillatelse = source.ansvarsrett.prosjekterende?.okForMidlertidigBrukstillatelse,
                    OkForRammetillatelse = source.ansvarsrett.prosjekterende?.okForRammetillatelse
                } : null,
                AnsvarsrettUtfoerende = source.ansvarsrett.utfoerende != null ? new AnsvarsrettUtfoerendeModel()
                {
                    HarTilstrekkeligSikkerhet = source.ansvarsrett.utfoerende?.harTilstrekkeligSikkerhet,
                    MidlertidigBrukstillatelseGjennstaaendeInnenfor = source.ansvarsrett.utfoerende?.midlertidigBrukstillatelseGjenstaaende?.gjenstaaendeInnenfor,
                    MidlertidigBrukstillatelseGjennstaaendeUtenfor = source.ansvarsrett.utfoerende?.midlertidigBrukstillatelseGjenstaaende?.gjenstaaendeUtenfor,
                    OkForFerdigattest = source.ansvarsrett.utfoerende?.okForFerdigattest,
                    OkForMidlertidigBrukstillatelse = source.ansvarsrett.utfoerende?.okMidlertidigBrukstillatelse,
                    TypeArbeider = source.ansvarsrett.utfoerende?.typeArbeider,
                    UtfoertInnen = source.ansvarsrett.utfoerende?.utfoertInnen
                } : null
            };
            return destination;
        }

        public static SamsvarserklaeringType Map(SamsvarserklaeringModel samsvarserklaeringModel)
        {
            var destination = new SamsvarserklaeringType()
            {
                ansvarligSoeker = new PartType()
                {
                    adresse = new EnkelAdresseType()
                    {
                        adresselinje1 = samsvarserklaeringModel.AnsvarligSoeker.Adresselinje1,
                        adresselinje2 = samsvarserklaeringModel.AnsvarligSoeker.Adresselinje2,
                        adresselinje3 = samsvarserklaeringModel.AnsvarligSoeker.Adresselinje3,
                        landkode = samsvarserklaeringModel.AnsvarligSoeker.Landkode,
                        postnr = samsvarserklaeringModel.AnsvarligSoeker.Postnr,
                        poststed = samsvarserklaeringModel.AnsvarligSoeker.Poststed
                    },
                    epost = samsvarserklaeringModel.AnsvarligSoeker.Epost,
                    foedselsnummer = samsvarserklaeringModel.AnsvarligSoeker.Foedselsnummer,
                    kontaktperson = new KontaktpersonType()
                    {
                        epost = samsvarserklaeringModel.AnsvarligSoeker.KontaktpersonEpost,
                        navn = samsvarserklaeringModel.AnsvarligSoeker.KontaktpersonNavn,
                        mobilnummer = samsvarserklaeringModel.AnsvarligSoeker.KontaktpersonMobilnummer,                        
                        telefonnummer = samsvarserklaeringModel.AnsvarligSoeker.KontaktpersonTelefonnummer
                    },
                    mobilnummer = samsvarserklaeringModel.AnsvarligSoeker.Mobilnummer,
                    telefonnummer = samsvarserklaeringModel.AnsvarligSoeker.Telefonnummer,
                    navn = samsvarserklaeringModel.AnsvarligSoeker.Navn,
                    organisasjonsnummer = samsvarserklaeringModel.AnsvarligSoeker.Organisasjonsnummer,
                    partstype = new KodeType()
                    {
                        kodebeskrivelse = samsvarserklaeringModel.AnsvarligSoeker.PartstypeBeskrivelse,
                        kodeverdi = samsvarserklaeringModel.AnsvarligSoeker.PartstypeKode
                    }                    
                }
            };

            destination.ansvarsrett = new AnsvarsomraadeType()
            {                
                ansvarsomraadetAvsluttetSpecified = samsvarserklaeringModel.AnsvarsomraadetAvsluttet.HasValue,
                ansvarsomraadetAvsluttet = samsvarserklaeringModel.AnsvarsomraadetAvsluttet,
                ansvarsrettErklaertSpecified = samsvarserklaeringModel.AnsvarsrettErklaert.HasValue,                
                ansvarsrettErklaert = samsvarserklaeringModel.AnsvarsrettErklaert,
                beskrivelseAvAnsvarsomraadet = samsvarserklaeringModel.BeskrivelseAvAnsvarsomraadet,
                funksjon = new KodeType()
                {
                    kodebeskrivelse = samsvarserklaeringModel.FunksjonBeskrivelse,
                    kodeverdi = samsvarserklaeringModel.FunksjonKode
                }
            };


            if (samsvarserklaeringModel.AnsvarsrettProsjekterende != null)
            {
                destination.ansvarsrett.prosjekterende = new ProsjekterendeType()
                {
                    okForFerdigattestSpecified = samsvarserklaeringModel.AnsvarsrettProsjekterende.OkForFerdigattest.HasValue,
                    okForFerdigattest = samsvarserklaeringModel.AnsvarsrettProsjekterende.OkForFerdigattest,
                    okForIgangsettingSpecified = samsvarserklaeringModel.AnsvarsrettProsjekterende.OkForIgangsetting.HasValue,
                    okForIgangsetting = samsvarserklaeringModel.AnsvarsrettProsjekterende.OkForIgangsetting,
                    okForMidlertidigBrukstillatelseSpecified = samsvarserklaeringModel.AnsvarsrettProsjekterende.OkForMidlertidigBrukstillatelse.HasValue,
                    okForMidlertidigBrukstillatelse = samsvarserklaeringModel.AnsvarsrettProsjekterende.OkForMidlertidigBrukstillatelse,
                    okForRammetillatelseSpecified = samsvarserklaeringModel.AnsvarsrettProsjekterende.OkForRammetillatelse.HasValue,
                    okForRammetillatelse = samsvarserklaeringModel.AnsvarsrettProsjekterende.OkForRammetillatelse
                };
            }

            if (samsvarserklaeringModel.AnsvarsrettUtfoerende != null)
            {
                destination.ansvarsrett.utfoerende = new UtfoerendeType()
                {
                    harTilstrekkeligSikkerhetSpecified = samsvarserklaeringModel.AnsvarsrettUtfoerende.HarTilstrekkeligSikkerhet.HasValue,
                    harTilstrekkeligSikkerhet = samsvarserklaeringModel.AnsvarsrettUtfoerende.HarTilstrekkeligSikkerhet,
                    midlertidigBrukstillatelseGjenstaaende = new DelsoeknadMidlertidigBrukstillatelseType()
                    {
                        gjenstaaendeInnenfor = samsvarserklaeringModel.AnsvarsrettUtfoerende.MidlertidigBrukstillatelseGjennstaaendeInnenfor,
                        gjenstaaendeUtenfor = samsvarserklaeringModel.AnsvarsrettUtfoerende.MidlertidigBrukstillatelseGjennstaaendeUtenfor
                    },
                    okForFerdigattestSpecified = samsvarserklaeringModel.AnsvarsrettUtfoerende.OkForFerdigattest.HasValue,
                    okForFerdigattest = samsvarserklaeringModel.AnsvarsrettUtfoerende.OkForFerdigattest,
                    okMidlertidigBrukstillatelseSpecified = samsvarserklaeringModel.AnsvarsrettUtfoerende.OkForMidlertidigBrukstillatelse.HasValue,
                    okMidlertidigBrukstillatelse = samsvarserklaeringModel.AnsvarsrettUtfoerende.OkForMidlertidigBrukstillatelse,
                    typeArbeider = samsvarserklaeringModel.AnsvarsrettUtfoerende.TypeArbeider,
                    utfoertInnenSpecified = samsvarserklaeringModel.AnsvarsrettUtfoerende.UtfoertInnen.HasValue,
                    utfoertInnen = samsvarserklaeringModel.AnsvarsrettUtfoerende.UtfoertInnen
                };
            }
            destination.ansvarsrett.soeknadssystemetsReferanse = samsvarserklaeringModel.SoeknadssystemetsReferanse;

            destination.eiendomByggested = samsvarserklaeringModel.EiendomByggesteder.Select(eiendom =>
                    new EiendomType()
                    {
                        adresse = new EiendommensAdresseType()
                        {
                            adresselinje1 = eiendom.Adresselinje1,
                            adresselinje2 = eiendom.Adresselinje2,
                            adresselinje3 = eiendom.Adresselinje3,
                            bokstav = eiendom.Bokstav,
                            gatenavn = eiendom.Gatenavn,
                            husnr = eiendom.Husnr,
                            landkode = eiendom.Landkode,
                            postnr = eiendom.Postnr,
                            poststed = eiendom.Poststed
                        },
                        bolignummer = eiendom.Bolignummer,
                        bygningsnummer = eiendom.Bygningsnummer,
                        kommunenavn = eiendom.Kommunenavn,
                        eiendomsidentifikasjon = new MatrikkelnummerType()
                        {
                            bruksnummer = eiendom.Bruksnummer,
                            festenummer = eiendom.Festenummer,
                            gaardsnummer = eiendom.Gaardsnummer,
                            kommunenummer = eiendom.Kommunenummer,
                            seksjonsnummer = eiendom.Seksjonsnummer
                        }
                    }
                ).ToArray();
            destination.fraSluttbrukersystem = samsvarserklaeringModel.FraSluttbrukerSystem;
            destination.hovedinnsendingsnummer = samsvarserklaeringModel.Hovedinnsendingsnummer;
            destination.kommunensSaksnummer = new SaksnummerType()
            {
                saksaar = samsvarserklaeringModel.Kommunenssaksaar,
                sakssekvensnummer = samsvarserklaeringModel.Kommunenssakssekvensnummer
            };
            destination.prosjektnavn = samsvarserklaeringModel.Prosjektnavn;
            destination.signatur = samsvarserklaeringModel.Signaturdato.HasValue ? new SignaturType()
            {
                signaturdato = samsvarserklaeringModel.Signaturdato.Value,
                signaturdatoSpecified = true,
                signertAv = samsvarserklaeringModel.SignertAvNavn
            } : null;
            destination.erklaeringProsjektering = samsvarserklaeringModel.ErklaeringProsjektering;
            destination.erklaeringProsjekteringSpecified = samsvarserklaeringModel.ErklaeringProsjektering.HasValue;
            destination.erklaeringUtfoerelse = samsvarserklaeringModel.ErklaeringUtfoerelse;
            destination.erklaeringUtfoerelseSpecified = samsvarserklaeringModel.ErklaeringUtfoerelse.HasValue;            
            destination.prosjektnr = samsvarserklaeringModel.Prosjektnummer;
            destination.foretak = new PartType()
            {
                adresse = new EnkelAdresseType()
                {
                    adresselinje1 = samsvarserklaeringModel.AnsvarligForetak.Adresselinje1,
                    adresselinje2 = samsvarserklaeringModel.AnsvarligForetak.Adresselinje2,
                    adresselinje3 = samsvarserklaeringModel.AnsvarligForetak.Adresselinje3,
                    landkode = samsvarserklaeringModel.AnsvarligForetak.Landkode,
                    postnr = samsvarserklaeringModel.AnsvarligForetak.Postnr,
                    poststed = samsvarserklaeringModel.AnsvarligForetak.Poststed
                },
                epost = samsvarserklaeringModel.AnsvarligForetak.Epost,
                kontaktperson = new KontaktpersonType()
                {
                    epost = samsvarserklaeringModel.AnsvarligForetak.KontaktpersonEpost,
                    navn = samsvarserklaeringModel.AnsvarligForetak.KontaktpersonNavn,
                    mobilnummer = samsvarserklaeringModel.AnsvarligForetak.KontaktpersonMobilnummer,                    
                    telefonnummer = samsvarserklaeringModel.AnsvarligForetak.KontaktpersonTelefonnummer
                },
                mobilnummer = samsvarserklaeringModel.AnsvarligForetak.Mobilnummer,
                telefonnummer = samsvarserklaeringModel.AnsvarligForetak.Telefonnummer,
                navn = samsvarserklaeringModel.AnsvarligForetak.Navn,
                organisasjonsnummer = samsvarserklaeringModel.AnsvarligForetak.Organisasjonsnummer,
                partstype = new KodeType()
                {
                    kodebeskrivelse = samsvarserklaeringModel.AnsvarligForetak.PartstypeBeskrivelse,
                    kodeverdi = samsvarserklaeringModel.AnsvarligForetak.PartstypeKode
                }                
            };

            return destination;
        }
    }
}
