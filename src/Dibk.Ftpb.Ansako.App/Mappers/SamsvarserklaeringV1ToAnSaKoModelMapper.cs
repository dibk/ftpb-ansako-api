﻿using Dibk.Ftpb.Ansako.App.Models.InternalApi;
using Dibk.Ftpb.Ansako.App.Repositories.Models;
using System.Collections.Generic;
using no.kxml.skjema.dibk.samsvarserklaeringAnsako;
using System;
using System.Linq;

namespace Dibk.Ftpb.Ansako.App.Mappers
{
    public static class SamsvarserklaeringV1ToAnSaKoModelMapper
    {
        public static void MapFromDto(SamsvarserklaeringDto samsvarserklaeringDto, ref AnSaKoModel form)
        {
            form.FormXmlData = XmlSerialization.SerializeObject<SamsvarserklaeringType>(SamsvarserklaeringV1ModelMapper.Map(samsvarserklaeringDto.FormData));

            form.FormDbData.DataFormatId = string.IsNullOrEmpty(form.FormDbData.DataFormatId) ? samsvarserklaeringDto.DataFormatId : form.FormDbData.DataFormatId;
            form.FormDbData.DataFormatVersion = string.IsNullOrEmpty(form.FormDbData.DataFormatVersion) ? samsvarserklaeringDto.DataFormatVersion : form.FormDbData.DataFormatVersion;
            form.FormDbData.AnsvarligForetakOrgnr = samsvarserklaeringDto.FormData.AnsvarligForetak.Organisasjonsnummer;
            form.FormDbData.AnsvarligSoekerOrgnr = samsvarserklaeringDto.FormData.AnsvarligSoeker.Organisasjonsnummer;            
            form.FormDbData.ProsjektNavn = samsvarserklaeringDto.FormData.Prosjektnavn;
            form.FormDbData.Status = samsvarserklaeringDto.Status;
            form.FormDbData.StatusDetails = samsvarserklaeringDto.StatusDetails;
            //form.FormDbData.SigneringsFrist = samsvarserklaeringDto.Signeringsfrist;
            form.Hovedinnsendingsnummer = samsvarserklaeringDto.FormData.Hovedinnsendingsnummer;

            if (form.FormDbData.Ansvarsomraader == null)
                form.FormDbData.Ansvarsomraader = new List<Storage.Database.Models.AnsvarsomraadeDbModel>();

            var a = form.FormDbData.Ansvarsomraader.FirstOrDefault();

            if (a == null)
            {
                a = new Storage.Database.Models.AnsvarsomraadeDbModel();
                form.FormDbData.Ansvarsomraader.Add(a);
            }

            a.AnSaKoReferenceId = form.FormDbData.AnSaKoReferenceId;
            a.Soeknadssystemetsreferanse = samsvarserklaeringDto.FormData.SoeknadssystemetsReferanse;
            a.AnsvarsomraadeBeskrivelse = samsvarserklaeringDto.FormData.BeskrivelseAvAnsvarsomraadet;
            a.DateCreated = DateTime.Now;
        }

        public static void MapNewModelFromXml(SamsvarserklaeringType samsvarserklaering, ref AnSaKoModel form)
        {
            form.FormXmlData = XmlSerialization.SerializeObject<SamsvarserklaeringType>(samsvarserklaering);

            if (form.FormDbData == null)
                form.FormDbData = new Storage.Database.Models.FormDbModel();

            form.FormDbData.DataFormatId = samsvarserklaering.dataFormatId;
            form.FormDbData.DataFormatVersion = samsvarserklaering.dataFormatVersion;
            form.FormDbData.AnsvarligForetakOrgnr = samsvarserklaering.foretak?.organisasjonsnummer;
            form.FormDbData.AnsvarligSoekerOrgnr = samsvarserklaering.ansvarligSoeker?.organisasjonsnummer;            
            form.FormDbData.ProsjektNavn = samsvarserklaering.prosjektnavn;
            form.FormDbData.SluttbrukerSystem = samsvarserklaering.fraSluttbrukersystem;
            form.Hovedinnsendingsnummer = samsvarserklaering.hovedinnsendingsnummer;
            form.FormDbData.Soeknadssystemetsreferanse = samsvarserklaering.ansvarsrett.soeknadssystemetsReferanse;

            var ansvarsomraade = new Storage.Database.Models.AnsvarsomraadeDbModel();
            ansvarsomraade.AnsvarsomraadeBeskrivelse = samsvarserklaering.ansvarsrett.beskrivelseAvAnsvarsomraadet;
            ansvarsomraade.AnsvarsomraadeId = Guid.NewGuid();
            ansvarsomraade.AnsvarligforetakOrgnr = samsvarserklaering.foretak?.organisasjonsnummer;
            ansvarsomraade.AnSaKoReferenceId = form.AnSaKoReferenceId;
            ansvarsomraade.Soeknadssystemetsreferanse = samsvarserklaering.ansvarsrett.soeknadssystemetsReferanse;
            ansvarsomraade.DateCreated = DateTime.Now;
            ansvarsomraade.DateUpdated = DateTime.Now;

            form.FormDbData.Ansvarsomraader = new List<Storage.Database.Models.AnsvarsomraadeDbModel>() { ansvarsomraade };
        }
    }
}
