﻿using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Dibk.Ftpb.Api.Models;

namespace Dibk.Ftpb.Ansako.App.Mappers
{
    public class FormStatusToAnSaKoProcessStatusMapper
    {
        public static AnSaKoProcessStatusType GetStatus(FormStatus status)
        {
            switch (status)
            {
                case FormStatus.TilSignering:
                case FormStatus.Opprettet:
                    return AnSaKoProcessStatusType.tilSignering;
                case FormStatus.IArbeid:
                    return AnSaKoProcessStatusType.iArbeid;
                case FormStatus.Signert:
                    return AnSaKoProcessStatusType.signert;
                case FormStatus.Avvist:
                    return AnSaKoProcessStatusType.avvist;
                case FormStatus.Utgått:
                    return AnSaKoProcessStatusType.utgått;
                case FormStatus.Trukket:
                    return AnSaKoProcessStatusType.trukket;
                case FormStatus.Feilet:
                    return AnSaKoProcessStatusType.feilet;
                case FormStatus.SignertManuelt:
                    return AnSaKoProcessStatusType.signertManuelt;
                case FormStatus.Avsluttet:
                    return AnSaKoProcessStatusType.avsluttet;
                default:
                    throw new System.ArgumentException("FormStatus isn't mapped to a corresponding AnSaKoProcessStatusType", "status");
            }
        }

        public static FormStatus GetStatus(AnSaKoProcessStatusType status)
        {
            switch (status)
            {
                case AnSaKoProcessStatusType.iArbeid:
                    return FormStatus.IArbeid;
                case AnSaKoProcessStatusType.tilSignering:
                    return FormStatus.TilSignering;
                case AnSaKoProcessStatusType.signert:
                    return FormStatus.Signert;
                case AnSaKoProcessStatusType.avvist:
                    return FormStatus.Avvist;
                case AnSaKoProcessStatusType.utgått:
                    return FormStatus.Utgått;
                case AnSaKoProcessStatusType.trukket:
                    return FormStatus.Trukket;
                case AnSaKoProcessStatusType.feilet:
                    return FormStatus.Feilet;
                case AnSaKoProcessStatusType.signertManuelt:
                    return FormStatus.SignertManuelt;
                case AnSaKoProcessStatusType.avsluttet:
                    return FormStatus.Avsluttet;
                default:
                    throw new System.ArgumentException("AnSaKoProcessStatusType isn't mapped to a corresponding FormStatus", "status");
            }
        }
    }
}
