﻿using Dibk.Ftpb.Ansako.Storage.Database.Models;
using System;

namespace Dibk.Ftpb.Ansako.App.Models.InternalApi
{
    public class AnsvarsrettDto : FormTypeDtoBase<AnsvarsrettModel>
    { }
    public class KontrollerklaeringDto : FormTypeDtoBase<KontrollerklaeringModel>
    {
        public KontrollerklaeringAttachmentDto Attachment { get; set; }
    }
    public class KontrollerklaeringAttachmentDto
    {
        public string FileName { get; set; }
        public string AttachmentType { get; set; }
        public string MimeType { get; set; }
    }
    public class SamsvarserklaeringDto : FormTypeDtoBase<SamsvarserklaeringModel>
    { }
    public class FormTypeDtoBase<TFormDataModel> : IFormDto
    {
        public TFormDataModel FormData { get; set; }
        public string AnSaKoReferenceId { get; set; }
        public string DataFormatId { get; set; }
        public string DataFormatVersion { get; set; }
        public FormStatus Status { get; set; }
        public string StatusDetails { get; set; }
        public SigningJobStatus? SigningJobStatus { get; set; }
        public DateTime? Signeringsfrist { get; set; }
        public DateTime? SignertDato { get; set; }
        public string FtpbReferenceId { get; set; }
    }

    public interface IFormDto
    {
        string AnSaKoReferenceId { get; set; }
        string DataFormatId { get; set; }
        string DataFormatVersion { get; set; }
        FormStatus Status { get; set; }
        string StatusDetails { get; set; }
        SigningJobStatus? SigningJobStatus { get; set; }
        DateTime? Signeringsfrist { get; set; }
        DateTime? SignertDato { get; set; }
        string FtpbReferenceId { get; set; }
    }
}