﻿namespace Dibk.Ftpb.Ansako.App.Models.InternalApi
{
    public class AnsvarsrettProsjekterendeModel
    {
        public bool? OkForFerdigattest { get; set; }
        public bool? OkForRammetillatelse { get; set; }
        public bool? OkForIgangsetting { get; set; }
        public bool? OkForMidlertidigBrukstillatelse { get; set; }
    }
}
