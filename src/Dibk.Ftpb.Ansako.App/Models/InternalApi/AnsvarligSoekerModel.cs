﻿namespace Dibk.Ftpb.Ansako.App.Models.InternalApi
{
    public class AnsvarligSoekerModel
    {
        public string PartstypeKode { get; set; }
        public string PartstypeBeskrivelse { get; set; }
        public string Foedselsnummer { get; set; }
        public string Organisasjonsnummer { get; set; }
        public string Navn { get; set; }
        public string Telefonnummer { get; set; }
        public string Mobilnummer { get; set; }
        public string Epost { get; set; }
        public string Adresselinje1 { get; set; }
        public string Adresselinje2 { get; set; }
        public string Adresselinje3 { get; set; }
        public string Postnr { get; set; }
        public string Poststed { get; set; }
        public string Landkode { get; set; }
        public string KontaktpersonNavn { get; set; }
        public string KontaktpersonTelefonnummer { get; set; }
        public string KontaktpersonMobilnummer { get; set; }
        public string KontaktpersonEpost { get; set; }
    }
}
