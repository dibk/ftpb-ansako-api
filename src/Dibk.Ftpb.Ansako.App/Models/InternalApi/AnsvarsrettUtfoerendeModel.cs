﻿using System;

namespace Dibk.Ftpb.Ansako.App.Models.InternalApi
{
    public class AnsvarsrettUtfoerendeModel
    {
        public bool? OkForFerdigattest { get; set; }
        public bool? HarTilstrekkeligSikkerhet { get; set; }
        public string TypeArbeider { get; set; }
        public DateTime? UtfoertInnen { get; set; }
        public bool? OkForMidlertidigBrukstillatelse { get; set; }
        public string MidlertidigBrukstillatelseGjennstaaendeInnenfor { get; set; }
        public string MidlertidigBrukstillatelseGjennstaaendeUtenfor { get; set; }
    }
}
