﻿using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Ansako.App.Models.InternalApi
{
    public class SamsvarserklaeringModel : AnSaKoResponseModelBase
    {
        public string Prosjektnummer { get; set; }
        public bool? ErklaeringUtfoerelse { get; set; }
        public bool? ErklaeringProsjektering { get; set; }
        public bool? ErTek10 { get; set; }
        public string SluttbrukersystemUrl { get; set; }

        //Ansvarsrett
        public bool? AnsvarsomraadetAvsluttet { get; set; }
        public string BeskrivelseAvAnsvarsomraadet { get; set; }
        public string FunksjonKode { get; set; }
        public string FunksjonBeskrivelse { get; set; }
        public DateTime? AnsvarsrettErklaert { get; set; }
        public string SoeknadssystemetsReferanse { get; set; }
        public AnsvarsrettProsjekterendeModel AnsvarsrettProsjekterende { get; set; }
        public AnsvarsrettUtfoerendeModel AnsvarsrettUtfoerende { get; set; }
        public IEnumerable<AnsvarsomraadeModel> Ansvarsomraader { get; set; }
    }
}
