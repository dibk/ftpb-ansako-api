﻿using System;
using System.Collections.Generic;

namespace Dibk.Ftpb.Ansako.App.Models.InternalApi
{
    public class AnSaKoResponseModelBase
    {
        public string FraSluttbrukerSystem { get; set; }
        public string Hovedinnsendingsnummer { get; set; }
        public string Kommunenssaksaar { get; set; }
        public string Kommunenssakssekvensnummer { get; set; }
        public IEnumerable<EiendomModel> EiendomByggesteder { get; set; }
        public ForetakModel AnsvarligForetak { get; set; }
        public AnsvarligSoekerModel AnsvarligSoeker { get; set; }
        public string Prosjektnavn { get; set; }
        public DateTime? Signaturdato { get; set; }
        public string SignertAvNavn { get; set; }
    }
}
