﻿namespace Dibk.Ftpb.Ansako.App.Models.InternalApi
{

    public class EiendomModel
    {
        public string Adresselinje1 { get; set; }
        public string Adresselinje2 { get; set; }
        public string Adresselinje3 { get; set; }
        public string Postnr { get; set; }
        public string Poststed { get; set; }
        public string Landkode { get; set; }
        public string Gatenavn { get; set; }
        public string Husnr { get; set; }
        public string Bokstav { get; set; }
        public string Bygningsnummer { get; set; }
        public string Bolignummer { get; set; }
        public string Kommunenavn { get; set; }
        public string Kommunenummer { get; set; }
        public string Gaardsnummer { get; set; }
        public string Bruksnummer { get; set; }
        public string Festenummer { get; set; }
        public string Seksjonsnummer { get; set; }
    }
}
