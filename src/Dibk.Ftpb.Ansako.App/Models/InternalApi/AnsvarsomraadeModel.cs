﻿using System;

namespace Dibk.Ftpb.Ansako.App.Models.InternalApi
{
    public class AnsvarsomraadeModel
    {
        public string FunksjonKode { get; set; }
        public string FunksjonBeskrivelse { get; set; }
        public string BeskrivelseAvAnsvarsomraade { get; set; }
        public bool? DekkesOmradetAvSentralGodkjenning { get; set; }
        public bool? SamsvarKontrollVedFerdigattest { get; set; }
        public bool? SamsvarKontrollVedIgangsettingstillatelse { get; set; }
        public bool? SamsvarKontrollVedMidlertidigBrukstillatelse { get; set; }
        public bool? SamsvarKontrollVedRammetillatelse { get; set; }
        public string SoeknadssystemetsReferanse { get; set; }
        public string TiltaksklasseKode { get; set; }
        public string TiltaksklasseBeskrivelse { get; set; }
    }
}
