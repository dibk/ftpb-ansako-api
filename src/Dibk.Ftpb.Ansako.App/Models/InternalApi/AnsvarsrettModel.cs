﻿using System.Collections.Generic;

namespace Dibk.Ftpb.Ansako.App.Models.InternalApi
{
    public class AnsvarsrettModel : AnSaKoResponseModelBase
    {
        public AnsvarsrettModel()
        {
            Ansvarsomraader = new List<AnsvarsomraadeModel>();
        }
        //Ansvarsrett
        public bool? ErklaeringAnsvarligProsjekterende { get; set; }
        public bool? ErklaeringAnsvarligUtfoerende { get; set; }
        public bool? ErklaeringAnsvarligKontrollerende { get; set; }

        public bool ErSamsvarKontrollFeltRedigerbare { get; set; }
        public IEnumerable<AnsvarsomraadeModel> Ansvarsomraader { get; set; }
    }
}
