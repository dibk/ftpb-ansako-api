﻿namespace Dibk.Ftpb.Ansako.App.Models.InternalApi
{
    public class AnsvarsrettKontrollerendeModel
    {
        public bool? ObserverteAvvik { get; set; }
        public bool? AapneAvvik { get; set; }
        public bool? IngenAvvik { get; set; }
    }
}
