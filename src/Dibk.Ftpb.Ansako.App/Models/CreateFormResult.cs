﻿using System.Collections.Generic;

namespace Dibk.Ftpb.Ansako.App.Models
{
    public class CreateFormResult
    {
        public string DataFormatVersion { get; private set; }
        public string DataFormatId { get; private set; }
        public string FtpbReferenceId  { get; private set; }
        public string AnSaKoReferenceId { get; private set; }
        public CreateFormMetadata FormMetadata { get; set; }
        public IEnumerable<AnsvarsomraadeResult> Ansvarsomraader { get; set; }
        public CreateFormResult(string dataFormatId, string dataFormatVersion, string ftpbReferenceId, string ansakoReferenceId, CreateFormMetadata formMetadata, IEnumerable<AnsvarsomraadeResult> ansvarsomraader = null)
        {
            DataFormatId = dataFormatId;
            DataFormatVersion = dataFormatVersion;
            FtpbReferenceId = ftpbReferenceId;
            AnSaKoReferenceId = ansakoReferenceId;
            if (ansvarsomraader != null)
                Ansvarsomraader = ansvarsomraader;
            FormMetadata = formMetadata;
        }
        public class AnsvarsomraadeResult
        {
            public AnsvarsomraadeResult(string soeknadsystemetsReferanse, string beskrivelseAvAnsvarsomraade, string anSaKoAnsvarsomraadeId)
            {
                SoeknadsystemetsReferanse = soeknadsystemetsReferanse;
                BeskrivelseAvAnsvarsomraade = beskrivelseAvAnsvarsomraade;
                AnSaKoAnsvarsomraadeId = anSaKoAnsvarsomraadeId;
            }
            public string SoeknadsystemetsReferanse { get; set; }
            public string BeskrivelseAvAnsvarsomraade { get; set; }
            public string AnSaKoAnsvarsomraadeId { get; set; }
        }
    }
}
