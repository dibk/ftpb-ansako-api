﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dibk.Ftpb.Ansako.App.Models
{
    public class CreateFormPayload
    {
        [Required]
        public string DataFormatId { get; set; }
        [Required]
        public string DataFormatVersion { get; set; }
        [Required]
        public string FormDataXml { get; set; }        
        public CreateFormMetadata FormMetadata { get; set; }
    }

    public class CreateFormMetadata
    {
        public DateTime? Signeringsfrist { get; set; }
    }
}
