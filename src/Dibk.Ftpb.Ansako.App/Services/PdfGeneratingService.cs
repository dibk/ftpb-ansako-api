﻿using Dibk.Ftpb.Ansako.App.HttpClients.Pdf;
using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public class PdfGeneratingService
    {
        private readonly PdfHttpClient _htmlToPdfConverter;
        private readonly InternalApiFormService _internalApiFormService;
        private readonly ILogger<PdfGeneratingService> _logger;
        private readonly IBlobService _blobService;

        public PdfGeneratingService(PdfHttpClient htmlToPdfConverter, InternalApiFormService internalApiFormService, ILogger<PdfGeneratingService> logger, IBlobService blobService)
        {
            _htmlToPdfConverter = htmlToPdfConverter;
            _internalApiFormService = internalApiFormService;
            _logger = logger;
            _blobService = blobService;
        }
        public async Task<bool> GeneratePdfAndSaveToBlob(string ansakoReferenceId, string htmlString)
        {            
            var formdata = await _internalApiFormService.GetAsync(ansakoReferenceId.ToString());

            if (formdata.FormDbData.Status != Storage.Database.Models.FormStatus.Signert)
            {
                using var pdfAsStream = await _htmlToPdfConverter.GeneratePdfDocumentAsync(htmlString);

                var fileName = $"{formdata.FormDbData.FormType.ToString()}.pdf";
                await _blobService.AddOrUpdateFileAsync(formdata.FormDbData.BlobStorageContainerName, fileName, "application/pdf", pdfAsStream);
                return true;
            }
            
            _logger.LogWarning("Couldn't generate PDF for {AnsakoReferenceId} with status {FormStatus}", ansakoReferenceId, formdata.FormDbData.Status);
            return false;
        }

        public async Task<Stream> GeneratePdf(string htmlString)
        {
            return await _htmlToPdfConverter.GeneratePdfDocumentAsync(htmlString);
        }
    }
}
