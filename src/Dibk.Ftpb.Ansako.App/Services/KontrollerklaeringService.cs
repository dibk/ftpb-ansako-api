﻿using Dibk.Ftpb.Ansako.App.Mappers;
using Dibk.Ftpb.Ansako.App.Models;
using Dibk.Ftpb.Ansako.App.Models.InternalApi;
using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.App.Repositories.Models;
using Dibk.Ftpb.Ansako.App.Services.Signing;
using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Dibk.Ftpb.Api.Client.Interfaces;
using Dibk.Ftpb.Signing.Models;
using Microsoft.Extensions.Logging;
using no.kxml.skjema.dibk.kontrollerklaeringAnsako;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public partial class KontrollerklaeringService : FormServiceBase, IFormService
    {
        public static string SignedPdfFileName => "Kontrollerklaering.pdf";

        public override string DocumentTitle
        { get { return "Kontrollerklæring"; } }

        public ErklaeringType FormType => ErklaeringType.Kontrollerklaering;

        private readonly ILogger<KontrollerklaeringService> _logger;
        private readonly FormRepo _formRepo;
        private readonly IAttachmentService _attachmentService;

        public KontrollerklaeringService(ILogger<KontrollerklaeringService> logger, FormRepo formRepo, IFtpbUnitOfWork ftpbUnitOfWork, IBlobService blobService, IAttachmentService attachmentService) :
            base(logger, ftpbUnitOfWork, blobService, formRepo)
        {
            _logger = logger;
            _formRepo = formRepo;
            _attachmentService = attachmentService;
        }

        public async Task<IFormDto> GetAsync(string ansakoReferenceId)
        {
            return MapToDto(await Get(ansakoReferenceId));
        }

        protected override KontrollerklaeringDto MapToDto(AnSaKoModel model)
        {
            if (string.IsNullOrEmpty(model.FormXmlData))
                throw new Exception($"FormXmlData is null or empty. Status: {model.FormDbData.Status.ToString()} DateDeleted: {model.FormDbData.DateDeleted}");

            KontrollerklaeringModel kontrollerklaeringModel = KontrollerklaeringV1ModelMapper.Map(model.FormXmlData);

            var kontrollerklaering = new KontrollerklaeringDto()
            {
                AnSaKoReferenceId = model.AnSaKoReferenceId,
                Status = model.FormDbData.Status,
                StatusDetails = model.FormDbData.StatusDetails,
                FormData = kontrollerklaeringModel,
                SigningJobStatus = model.FormDbData.SigneringsJobStatus,
                Signeringsfrist = model.FormDbData.SigneringsFrist,
                SignertDato = model.FormDbData.SignertDato,
                FtpbReferenceId = model.FtpbReferenceId
            };

            if (model.FormDbData.HasAttachment.HasValue && model.FormDbData.HasAttachment.Value)
            {
                kontrollerklaering.Attachment = new KontrollerklaeringAttachmentDto()
                {
                    AttachmentType = model.FormDbData.AttachmentFtpbType,
                    FileName = model.FormDbData.AttachmentFileName,
                    MimeType = model.FormDbData.AttachmentMimeType
                };
            }

            return kontrollerklaering;
        }

        protected override void MapFromDto(IFormDto dto, AnSaKoModel model)
        {
            KontrollerklaeringV1ToAnSaKoModelMapper.MapFromDto(dto as KontrollerklaeringDto, ref model);
        }

        public async Task<string> CreateAsync(CreateFormPayload payload, string ansakoReferenceId)
        {
            _logger.LogDebug("Deserializes payload");
            var kontrollerklaering = XmlSerialization.DeserializeFromString<KontrollerklaeringType>(payload.FormDataXml);
            _logger.LogDebug("Clears values for signatur and ansvarsomraadetAvsluttet");
            kontrollerklaering.signatur = null;
            kontrollerklaering.ansvarsrett.ansvarsomraadetAvsluttet = null;
            kontrollerklaering.ansvarsrett.ansvarsomraadetAvsluttetSpecified = false;
            kontrollerklaering.erklaeringKontroll = null;
            kontrollerklaering.erklaeringKontrollSpecified = false;

            var ansakoModel = new AnSaKoModel(ansakoReferenceId);
            KontrollerklaeringV1ToAnSaKoModelMapper.MapNewModelFromXml(kontrollerklaering, ref ansakoModel);
            ansakoModel.FormDbData.FtpbDistributionFormsId = Guid.NewGuid();
            ansakoModel.FormDbData.Status = FormStatus.Opprettet;
            ansakoModel.FormDbData.FormType = ErklaeringType.Kontrollerklaering;
            ansakoModel.FormDbData.SigneringsFrist = payload.FormMetadata?.Signeringsfrist;
            ansakoModel.FormDbData.FormDataFileName = "Kontrollerklaering.xml";

            ansakoModel = await _formRepo.InsertAsync(ansakoModel);

            await AddToFtpb(ansakoModel);

            return ansakoModel.FtpbReferenceId;
        }

        public ValidationResult IsValid(CreateFormPayload payload)
        {
            var valRes = new ValidationResult() { IsValid = true, ValidationMessage = "Ok" };
            try
            {
                XmlSerialization.DeserializeFromString<KontrollerklaeringType>(payload.FormDataXml);
            }
            catch (Exception ex)
            {
                var validationMessage = $"Payload with given dataformatid and -version {payload.DataFormatId}/{payload.DataFormatVersion} failed.";
                _logger.LogError(ex, validationMessage);
                valRes.IsValid = false;
                valRes.ValidationMessage = $"{validationMessage} - {ex.Message}";
            }
            return valRes;
        }

        public async Task<IFormDto> RejectAsync(string ansakoReferenceId, string reason)
        {
            await base.RejectForm(ansakoReferenceId, reason);

            return await this.GetAsync(ansakoReferenceId);
        }

        public override async Task<IEnumerable<SigningDocument>> GetDocumentsToSign(AnSaKoModel anSaKoModel)
        {
            var result = await base.GetDocumentsToSign(anSaKoModel);
            List<SigningDocument> docs = result.ToList();
            var attachmentDoc = await _attachmentService.GetAttachmentAsync(anSaKoModel.AnSaKoReferenceId);
            if (attachmentDoc != null)
            {
                var attachmentSigningDocument = new SigningDocument(SigningFileType.Attachment)
                {
                    FileContent = attachmentDoc.ContentStream.GetAsBytes(),
                    FileName = attachmentDoc.AttachmentFileName,
                    Title = attachmentDoc.AttachmentFileName
                };

                docs.Add(attachmentSigningDocument);
            }

            return docs;
        }

        public async Task<FormSigningJobData> GetFormSigningJobData(string ansakoReferenceId)
        {
            var kontrollerklaering = MapToDto(await Get(ansakoReferenceId));

            return new FormSigningJobData()
            {
                SignerName = kontrollerklaering.FormData.AnsvarligForetak.KontaktpersonNavn,
                SignerEmail = kontrollerklaering.FormData.AnsvarligForetak.Epost,
                SigningJobTitle = $"Kontrollerklæring fra {kontrollerklaering.FormData.AnsvarligForetak.Navn}"
            };
        }

        public async Task<IFormDto> PrepareForSigning(IFormDto formDto)
        {
            return await UpdateAsync(formDto);
        }
    }
}