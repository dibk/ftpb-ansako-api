﻿using Dibk.Ftpb.Ansako.App.Models;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public class SgRegistryResponse
    {
        public HttpStatusCode StatusCode { get; set; }
        public SgRegForetak? Foretak;
    }

    public class SgRegistryService
    {
        private readonly HttpClient _httpclient;

        public SgRegistryService(HttpClient httpclient) 
        {
            _httpclient = httpclient;
        }
        public async Task<SgRegistryResponse> GetSgRegForetak(string orgnr)
        {
            var requestUri = $"registry?orgnr={orgnr}";
            var response = await _httpclient.GetAsync(requestUri);
            SgRegistryResponse foretak = new();
            foretak.StatusCode = response.StatusCode;
            if (response.IsSuccessStatusCode)
            {
                var data = await response.Content.ReadAsStreamAsync();
                foretak.Foretak = await JsonSerializer.DeserializeAsync<SgRegForetak>(data, options: new() { PropertyNameCaseInsensitive = true });
            }
            return foretak;
        }
        public async Task<bool> HarSentralGodkjenning(string orgnr)
        {
            var res = await GetSgRegForetak(orgnr);
            switch (res.StatusCode)
            {
                case HttpStatusCode.OK:
                    return res.Foretak.Status.Approved;
                case HttpStatusCode.NotFound:
                default:
                    return false;
            }
            
        }
    }
    public class SgRegistrySettings
    {
        public static string ConfigSection = "SgRegistry";
        public string SgRegUrl { get; set; }
    }
}
