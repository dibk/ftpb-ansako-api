﻿using Dibk.Ftpb.Ansako.Storage.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public class FormServiceProvider
    {
        private readonly IEnumerable<IFormService> _formServiceImpls;

        public FormServiceProvider(IEnumerable<IFormService> formServiceImpls)
        {
            _formServiceImpls = formServiceImpls;
        }

        public IFormService GetService(string dataFormatId, string dataFormatVersion)
        {
            if (string.IsNullOrEmpty(dataFormatId))
                throw new ArgumentNullException("DataFormatId must be provided to select correct IFormService implementation");

            var formType = FormTypeHelper.GetErklaeringType(dataFormatId, dataFormatVersion);
            var formService = _formServiceImpls.FirstOrDefault(p => p.FormType == formType);
            return formService;
        }

        public IFormService GetService(ErklaeringType formType)
        {
            var formService = _formServiceImpls.FirstOrDefault(p => p.FormType == formType);
            return formService;
        }
    }
}
