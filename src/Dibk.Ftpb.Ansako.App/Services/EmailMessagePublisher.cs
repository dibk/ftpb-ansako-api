﻿using Azure.Messaging.ServiceBus;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public class EmailMessagePublisher
    {
        private readonly ILogger<EmailMessagePublisher> _logger;
        private readonly IAzureClientFactory<ServiceBusClient> _azureClientFactory;
        public static readonly string ServiceBusClientName = "Ftpb-Email";
        private readonly string _serviceBusQueueName = "sbq-ftpb-ansako-email";

        public EmailMessagePublisher(ILogger<EmailMessagePublisher> logger,
                                     IAzureClientFactory<ServiceBusClient> azureClientFactory,
                                     IConfiguration configuration)
        {
            _logger = logger;
            _azureClientFactory = azureClientFactory;

            var serviceBusQueueName = configuration["AnSaKo:Email:QueueName"];
            if (!string.IsNullOrWhiteSpace(serviceBusQueueName))
                _serviceBusQueueName = serviceBusQueueName;

        }

        public async Task PublishAsync(EmailQueueMessage message)
        {
            await PublishAsync(message, CancellationToken.None);
        }

        public async Task PublishAsync(EmailQueueMessage message, CancellationToken cancellationToken)
        {
            var client = _azureClientFactory.CreateClient(ServiceBusClientName);
            var sender = client.CreateSender(_serviceBusQueueName);

            var opts = new JsonSerializerOptions() { WriteIndented = false, Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping };
            var messageBody = JsonSerializer.Serialize(message, opts);
            var serviceBusMessage = new ServiceBusMessage(messageBody);

            try
            {
                _logger.LogDebug("Publishing message to ServiceBus queue {QueueName}", _serviceBusQueueName);
                await sender.SendMessageAsync(serviceBusMessage, cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while publishing message to ServiceBus queue {QueueName}", _serviceBusQueueName);
                throw;
            }
        }
    }
}
