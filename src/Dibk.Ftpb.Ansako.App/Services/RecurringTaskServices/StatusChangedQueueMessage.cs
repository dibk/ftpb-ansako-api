﻿using Dibk.Ftpb.Ansako.Storage.Database.Models;

namespace Dibk.Ftpb.Ansako.App.Services.RecurringTaskServices
{
    public class StatusChangedQueueMessage
    {
        public string AnSaKoReferenceId { get; set; }
        public FormStatus Status { get; set; }
        public string StatusDetails { get; set; }
        public string LogEntryMessage { get; set; }
    }
}