﻿using Dibk.Ftpb.Ansako.App.Mappers;
using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Api.Client.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services.RecurringTaskServices
{
    public class StatusChangedService
    {
        private readonly FormRepo _formRepo;
        private readonly IFtpbUnitOfWork _ftpbUnitOfWork;
        private readonly ILogger<StatusChangedService> _logger;

        public StatusChangedService(ILogger<StatusChangedService> logger,
                                    IFtpbUnitOfWork ftpbUnitOfWork,
                                    FormRepo formRepo)
        {
            _logger = logger;
            _ftpbUnitOfWork = ftpbUnitOfWork;
            _formRepo = formRepo;
        }

        public async Task SetStatus(StatusChangedQueueMessage statusChanged)
        {
            var candidate = await _formRepo.GetAsync(statusChanged.AnSaKoReferenceId);
            candidate.FormDbData.Status = statusChanged.Status;
            candidate.FormDbData.StatusDetails = statusChanged.StatusDetails;

            await _ftpbUnitOfWork.InitiateAsync(candidate.FtpbReferenceId, candidate.AnSaKoReferenceId);

            try
            {
                var df = await _ftpbUnitOfWork.DistributionForms.GetAsync(candidate.FormDbData.FtpbDistributionFormsId);
                if (df != null)
                {
                    df.AnSaKoStatus.AnSaKoProcessStatus = FormStatusToAnSaKoProcessStatusMapper.GetStatus(statusChanged.Status);
                    df.AnSaKoStatus.StatusDetails = candidate.FormDbData.StatusDetails;
                    _ftpbUnitOfWork.LogEntries.AddInfo(statusChanged.LogEntryMessage);

                    await _ftpbUnitOfWork.SaveAsync();
                    await _formRepo.UpdateAsync(candidate);
                    await _formRepo.SyncToELK(candidate);
                }
                else
                    _logger.LogError("Couldn't find distribution form");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to update status to '{FormStatus}'", statusChanged.Status);
            }
        }
    }
}