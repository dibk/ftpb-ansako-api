﻿using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Dibk.Ftpb.Ansako.Storage.Repositories;
using Dibk.Ftpb.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services.RecurringTaskServices
{
    public class DeadlineExpiredService
    {
        private readonly ILogger<DeadlineExpiredService> _logger;
        private readonly StatusChangedMessagePublisher _statusChangedMessagePublisher;
        private readonly FormDbRepository _formDbRepository;
        private static readonly string _dateTimeFormat = "yyyy-MM-dd HH:mm:ss";

        public DeadlineExpiredService(ILogger<DeadlineExpiredService> logger,
                                      StatusChangedMessagePublisher statusChangedMessagePublisher,
                                      FormDbRepository formDbRepository)
        {
            _logger = logger;
            _statusChangedMessagePublisher = statusChangedMessagePublisher;
            _formDbRepository = formDbRepository;
        }

        public async Task<string> ScheduleDeadlineExpiredStatusChange()
        {
            var currentDate = DateTime.Now;
            //Find all forms that have expired

            var statusCandidates = new List<FormStatus> { FormStatus.Opprettet, FormStatus.TilSignering, FormStatus.IArbeid };

            var candidates = _formDbRepository.GetAll().Where(p => EF.Constant(statusCandidates).Contains(p.Status) && p.SigneringsFrist < currentDate).ToList();
            _logger.LogDebug("#{DeadlineReachedCandidates} candidates found", candidates.Count);

            var updatedCandidates = new List<Tuple<string, string, DateTime?>>();
            var failedToUpdatedCandidates = new List<Tuple<string, string, DateTime?>>();

            foreach (var candidate in candidates)
            {
                var statusQueueMessage = new StatusChangedQueueMessage()
                {
                    AnSaKoReferenceId = candidate.AnSaKoReferenceId,
                    Status = FormStatus.Utgått,
                    StatusDetails = "Frist for signering er utløpt",
                    LogEntryMessage = $"Status maskinelt satt til '{AnSaKoProcessStatusType.utgått}' da fristen utløp {candidate.SigneringsFrist.Value.ToString(_dateTimeFormat)}"
                };

                await _statusChangedMessagePublisher.PublishAsync(statusQueueMessage);
            }

            return $"{candidates.Count} found with expired deadline";
        }
    }
}