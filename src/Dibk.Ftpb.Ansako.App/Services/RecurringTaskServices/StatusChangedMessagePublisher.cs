﻿using Azure.Messaging.ServiceBus;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services.RecurringTaskServices
{
    public class StatusChangedMessagePublisher
    {
        private readonly ILogger<StatusChangedMessagePublisher> _logger;
        private readonly IAzureClientFactory<ServiceBusClient> _azureClientFactory;
        public static readonly string ServiceBusClientName = "Ftpb-StatusMessage";
        private readonly string _serviceBusQueueName = "sbq-ftpb-ansako-statuschanged";

        public StatusChangedMessagePublisher(ILogger<StatusChangedMessagePublisher> logger,
                                             IAzureClientFactory<ServiceBusClient> azureClientFactory,
                                             IConfiguration configuration)
        {
            _logger = logger;
            _azureClientFactory = azureClientFactory;

            var serviceBusQueueName = configuration["AnSaKo:StatusChanged:QueueName"];
            if (!string.IsNullOrWhiteSpace(serviceBusQueueName))
                _serviceBusQueueName = serviceBusQueueName;
        }

        public async Task PublishAsync(StatusChangedQueueMessage message)
        {
            await PublishAsync(message, CancellationToken.None);
        }

        public async Task PublishAsync(StatusChangedQueueMessage message, CancellationToken cancellationToken)
        {
            var client = _azureClientFactory.CreateClient(ServiceBusClientName);
            var sender = client.CreateSender(_serviceBusQueueName);

            var opts = new JsonSerializerOptions() { WriteIndented = false, Encoder = System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping };
            var messageBody = JsonSerializer.Serialize(message, opts);
            var serviceBusMessage = new ServiceBusMessage(messageBody);

            try
            {
                _logger.LogDebug("Publishing message to ServiceBus queue {QueueName}", _serviceBusQueueName);
                await sender.SendMessageAsync(serviceBusMessage, cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while publishing message to ServiceBus queue {QueueName}", _serviceBusQueueName);
                throw;
            }
        }
    }
}