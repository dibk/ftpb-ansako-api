﻿using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Dibk.Ftpb.Ansako.Storage.Repositories;
using Dibk.Ftpb.Api.Clients.HttpClients;
using Dibk.Ftpb.Api.Models;
using DibkFtpb.Api.Client;
using Elastic.Apm;
using Elastic.Apm.Azure.Storage;
using Elastic.Apm.DiagnosticSource;
using Elastic.Apm.EntityFrameworkCore;
using Elastic.Apm.Instrumentations.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services.RecurringTaskServices
{
    public class DeleteFormCleanUpService
    {
        private readonly ILogger<DeleteFormCleanUpService> _logger;
        private readonly FormDbRepository _formDbRepository;
        private readonly IBlobService _blobService;
        private readonly FtpbBlobService _ftpbBlobService;
        private readonly FileDownloadStatusHttpClient _alfaFileDownloadStatusClient;
        private readonly FormMetadataHttpClient _alfaFormMetadataClient;

        public DeleteFormCleanUpService(ILogger<DeleteFormCleanUpService> logger,
                                          FormDbRepository formDbRepository,
                                          IBlobService blobService,
                                          FileDownloadStatusHttpClient alfaFileDownloadStatusClient,
                                          FormMetadataHttpClient alfaFormMetadataClient,
                                          FtpbBlobService ftpbBlobService)
        {
            _logger = logger;
            _formDbRepository = formDbRepository;
            _blobService = blobService;
            _alfaFileDownloadStatusClient = alfaFileDownloadStatusClient;
            _alfaFormMetadataClient = alfaFormMetadataClient;
            _ftpbBlobService = ftpbBlobService;
        }

        /// <summary>
        /// </summary>
        /// <param name="maxCandidates">Limitation of candidates to clean up</param>
        /// <param name="deleteAfterDays">Number of days old..</param>
        /// <returns>Number of candidates that has been clean up</returns>
        public async Task<int> DoCleanUp(int maxCandidates = 100, int deleteAfterDays = 365)
        {
            int candidateCount = 0;

            Agent.Subscribe(new HttpDiagnosticsSubscriber());
            Agent.Subscribe(new EfCoreDiagnosticsSubscriber());
            Agent.Subscribe(new SqlClientDiagnosticSubscriber());
            Agent.Subscribe(new AzureBlobStorageDiagnosticsSubscriber());

            await Agent.Tracer.CaptureTransaction("CleanUp", "backgroundJob", async (t) =>
            {
                var candidates = await GetCandidatesForCleanUp(maxCandidates, deleteAfterDays);

                if (candidates?.Count() == 0)
                {
                    _logger.LogInformation("No candidates found for clean up");
                    candidateCount = 0;
                }
                else
                {
                    _logger.LogInformation("Found {CleanUpCandidates} candidates", candidates.Count());

                    foreach (var candidate in candidates)
                    {
                        try
                        {
                            await CleanUp(candidate);
                        }
                        catch (Exception ex)
                        {
                            _logger.LogCritical(ex, "Error during cleanup of {AnSaKoReferenceId}/{FtpbReferenceId}", candidate.AnSaKoReferenceId, candidate.FtpbReferenceId);
                        }
                    }

                    candidateCount = candidates.Count();
                }
            });

            return candidateCount;
        }

        private async Task<IEnumerable<FormDbModel>> GetCandidatesForCleanUp(int maxCandidates, int? obsoletionLimitDays)
        {
            IEnumerable<FormDbModel> res = null;

            await Agent.Tracer.CurrentTransaction.CaptureSpan("GetCandidatesForCleanUp", "database", async span =>
            {
                IQueryable<ObsoleteForm> qry = _formDbRepository.DbContext.ObsoleteForms.AsQueryable();

                if (obsoletionLimitDays != null && obsoletionLimitDays < 365)
                {
                    var obsoletionDate = DateTime.UtcNow.Subtract(new TimeSpan(obsoletionLimitDays.Value, 0, 0, 0));
                    qry = qry.Where(o => o.DateCreated <= obsoletionDate);
                }

                var ids = await qry.Take(maxCandidates)
                                   .Select(c => c.AnSaKoReferenceId)
                                   .ToListAsync().ConfigureAwait(false);

                res = await _formDbRepository.GetAll()
                    .Include(f => f.Ansvarsomraader)
                    .Where(f => ids.Contains(f.AnSaKoReferenceId))
                    .ToListAsync().ConfigureAwait(false);
            });

            return res;
        }

        private async Task CleanUp(FormDbModel formDbModel)
        {
            _logger.LogInformation("Initiates clean up for {AnSaKoReferenceId}/{FtpbReferenceId}", formDbModel.AnSaKoReferenceId, formDbModel.FtpbReferenceId);

            await Agent.Tracer.CurrentTransaction.CaptureSpan("DeleteBlobs", "Storage", async span =>
            {
                // ** Blob **
                //Slett container i blobstorage
                if (await _blobService.DeleteContainerAsync(formDbModel.BlobStorageContainerName))
                    _logger.LogDebug("Container {ContainerName} deleted", formDbModel.BlobStorageContainerName);

                if (await _ftpbBlobService.DeleteContainerAsync(formDbModel.BlobStorageContainerName))
                    _logger.LogDebug("Container {ContainerName} deleted in ftbpBlobService", formDbModel.BlobStorageContainerName);
            });

            await Agent.Tracer.CurrentTransaction.CaptureSpan("UpdateDb", "database", async span =>
            {
                // ** DB  **
                //Oppdater databaserad med ny status
                formDbModel.DateDeleted = DateTime.UtcNow;

                //Slett eventuelle ansvarsområder
                if (formDbModel.Ansvarsomraader?.Count() > 0)
                {
                    _logger.LogDebug("Form has {AnsvarsomraaderCount} ansvarsområder. Deletes these..", formDbModel.Ansvarsomraader.Count);
                    _formDbRepository.DbContext.Ansvarsomraader.RemoveRange(formDbModel.Ansvarsomraader);
                }

                await _formDbRepository.UpdateAsync(formDbModel);

                _logger.LogDebug("AnSaKo internal-data clean up done for {AnSaKoReferenceId}/{FtpbReferenceId}", formDbModel.AnSaKoReferenceId, formDbModel.FtpbReferenceId);
            });

            await Agent.Tracer.CurrentTransaction.CaptureSpan("UpdateAlfa", "Alfa", async span =>
            {
                _logger.LogDebug("Updates Alfa regarding clean up for {FtpbReferenceId}", formDbModel.FtpbReferenceId);

                //**SLETTEJOBB **
                // Her kan Alfa oppdateres...
                //Hovedskjema
                var mainForm = await _alfaFormMetadataClient.GetAsync(formDbModel.FtpbReferenceId);
                if (mainForm != null)
                {
                    mainForm.Slettet = formDbModel.DateDeleted;
                    await _alfaFormMetadataClient.UpdateAsync(mainForm);
                }
                else
                    _logger.LogWarning("Couldn't find formmetadata for ref: {FtpbReferenceId}", formDbModel.FtpbReferenceId);

                //Hent FileDownloadStatus..
                //Oppdater FDS
                //Slett container
                if (formDbModel.FtpbSignedReferenceId != null)
                {
                    _logger.LogDebug("Updates Alfa regarding clean up of signed form {FtpbReferenceId}", formDbModel.FtpbSignedReferenceId);
                    var signedForm = await _alfaFormMetadataClient.GetAsync(formDbModel.FtpbSignedReferenceId);
                    if (signedForm != null)
                    {
                        signedForm.Slettet = formDbModel.DateDeleted;
                        await _alfaFormMetadataClient.UpdateAsync(signedForm);
                    }
                    else
                        _logger.LogWarning("Couldn't find formmetadata for ref: {FtpbSignedReferenceId}", formDbModel.FtpbSignedReferenceId);

                    List<FileDownloadStatusApiModel> fdss = null;
                    try
                    {
                        var result = await _alfaFileDownloadStatusClient.GetAsync(formDbModel.FtpbSignedReferenceId);

                        if (result != null)
                        {
                            fdss = result;
                            _logger.LogDebug("Found {FileDownloadStatusCount} FileDownloadStatus for {FtpbSignedReferenceId}", fdss.Count, formDbModel.FtpbSignedReferenceId);
                        }
                    }
                    catch (HttpRequestException ex)
                    {
                        _logger.LogDebug(ex, "Unable to find FiledownloadStatus for {FtpbSignedReferenceId}", formDbModel.FtpbSignedReferenceId);
                    }

                    if (fdss != null)
                        foreach (var fds in fdss)
                        {
                            fds.IsDeleted = true;
                            await _alfaFileDownloadStatusClient.UpdateAsync(fds);

                            await _ftpbBlobService.DeleteContainerAsync(fds.ContainerName);
                        }
                }
            });

            _logger.LogInformation("Clean up for {AnSaKoReferenceId}/{FtpbReferenceId} done", formDbModel.AnSaKoReferenceId, formDbModel.FtpbReferenceId);
        }
    }
}