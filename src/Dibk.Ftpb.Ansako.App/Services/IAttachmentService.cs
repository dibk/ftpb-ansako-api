﻿using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public interface IAttachmentService
    {
        Task AddAttachmentAsync(string ansakoReferenceId, string fileName, string mimeType, byte[] fileBytes);
        Task DeleteAttachmentAsync(string ansakoReferenceId);
        Task<AttachmentBlobItem> GetAttachmentAsync(string ansakoReferenceId);
    }
}