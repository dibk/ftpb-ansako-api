﻿using Dibk.Ftpb.Ansako.App.Models;
using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.App.Repositories.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Dibk.Ftpb.Ansako.App.Models.CreateFormResult;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public class CreateFormSettings
    {
        public static string ConfigSection => "CreateForm";
        public int DefaultDaysToSigningDeadline { get; set; }
    }

    public class ExternalApiFormService
    {
        private readonly ILogger<ExternalApiFormService> _logger;
        private readonly FormServiceProvider _formServiceProvider;
        private readonly FormRepo _formRepo;
        private readonly CreateFormSettings _settings;
        public ExternalApiFormService(ILogger<ExternalApiFormService> logger, FormServiceProvider formServiceProvider, FormRepo formRepo, IOptions<CreateFormSettings> settings)
        {
            _logger = logger;
            _formServiceProvider = formServiceProvider;
            _formRepo = formRepo;
            _settings = settings.Value;
        }

        public async Task<AnSaKoModel> GetAsync(string ftpbReferenceId)
        {
            var ansakoReferenceId = _formRepo.GetAnSaKoReferenceId(ftpbReferenceId);
            return await _formRepo.GetAsync(ansakoReferenceId);
        }

        public async Task<CreateFormResult> CreateAsync(CreateFormPayload payload)
        {
            var ansakoReferenceId = Guid.NewGuid().ToString();
            var ftpbReferenceId = string.Empty;

            List<AnsvarsomraadeResult> ansvarsomraadeRefs = null;
            using (Serilog.Context.LogContext.PushProperty("AnSaKoReferenceId", ansakoReferenceId, true))
            {
                var formService = _formServiceProvider.GetService(payload.DataFormatId, payload.DataFormatVersion);

                _logger.LogDebug($"Received signeringsfrist: {payload.FormMetadata?.Signeringsfrist?.ToString()}");
                FormPayloadDataEnricher.SetSigningDeadlineIfMissing(payload, _settings.DefaultDaysToSigningDeadline);
                _logger.LogDebug($"Signeringsfrist set to: {payload.FormMetadata?.Signeringsfrist?.ToString()}");

                ftpbReferenceId = await formService.CreateAsync(payload, ansakoReferenceId);

                _logger.LogDebug("Successfully created form");
                var ansvarsOmraader = _formRepo.GetAnsvarsomraade(ansakoReferenceId);
                ansvarsomraadeRefs = ansvarsOmraader.Select(p => new AnsvarsomraadeResult(p.Soeknadssystemetsreferanse, p.AnsvarsomraadeBeskrivelse, p.AnsvarsomraadeId.ToString())).ToList();
                return new CreateFormResult(payload.DataFormatId, payload.DataFormatVersion, ftpbReferenceId, ansakoReferenceId, payload.FormMetadata, ansvarsomraadeRefs);
            }
        }

        public ValidationResult IsValid(CreateFormPayload payload)
        {
            var valResult = new ValidationResult();

            if (payload.FormMetadata?.Signeringsfrist != null && payload.FormMetadata.Signeringsfrist.HasValue && payload.FormMetadata.Signeringsfrist.Value.Date <= DateTime.Now.Date)
            {
                valResult.IsValid = false;
                valResult.ValidationMessage = $"'SigneringsFrist' må være fremover i tid";
                return valResult;
            }

            try
            {
                var formService = _formServiceProvider.GetService(payload.DataFormatId, payload.DataFormatVersion);
                valResult = formService.IsValid(payload);
            }
            catch (System.ArgumentException)
            {
                valResult.IsValid = false;
                valResult.ValidationMessage = $"DataformatId '{payload.DataFormatId}' and DataFormatVersion '{payload.DataFormatVersion}' isn't supported";
            }

            //Kall valideringsservice

            return valResult;
        }

        public async Task SetStatus(AnSaKoModel form, Storage.Database.Models.FormStatus formStatus, string reason)
        {
            var formService = _formServiceProvider.GetService(form.FormDbData.DataFormatId, form.FormDbData.DataFormatVersion);
            if (formStatus == Storage.Database.Models.FormStatus.Trukket)
                await formService.WithdrawForm(form, reason);
            else if (formStatus == Storage.Database.Models.FormStatus.SignertManuelt)
                await formService.SignedManually(form, reason);
            else if (formStatus == Storage.Database.Models.FormStatus.Avsluttet)
                await formService.ReportedCompleted(form, reason);
            else if (formStatus == Storage.Database.Models.FormStatus.Feilet)
                await formService.FormFailed(form, reason);
            else
                throw new ArgumentOutOfRangeException("Setting state other than 'trukket', 'signertManuelt', 'avsluttet' and 'feilet' isn't implemented");
        }
    }

    public class ValidationResult
    {
        public bool IsValid { get; set; }
        public string ValidationMessage { get; set; }
    }
}