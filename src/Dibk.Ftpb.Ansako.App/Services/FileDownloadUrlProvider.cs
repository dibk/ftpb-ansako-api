﻿using Dibk.Ftpb.Api.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Dibk.Ftpb.Ansako.App.Services
{

    public class FileDownloadUrlProvider
    {
        private readonly string _host;

        public FileDownloadUrlProvider(string host)
        {
            _host = host.TrimEnd(new[] { '/' });
        }

        public string GenerateDownloadUri(FileDownloadStatus fileDownload)
        {
            return GenerateUri(fileDownload, _host);
        }

        private static string GenerateUri(FileDownloadStatus fileDownloadStatus, string host)
        {
            var urlEncodedFilename = HttpUtility.UrlPathEncode(Uri.EscapeDataString(fileDownloadStatus.Filename));
            return $"{host}/api/download/{fileDownloadStatus.ContainerName}/{urlEncodedFilename}";
        }
    }
}
