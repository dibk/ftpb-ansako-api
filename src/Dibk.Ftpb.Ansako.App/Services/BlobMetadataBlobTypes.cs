﻿namespace Dibk.Ftpb.Ansako.App.Services
{
    public static class BlobMetadata
    {
        public static string BlobTypeKey => "BlobType";
        public static class BlobTypes
        {
            public static string Attachment => "Attachment";
            public static string SignedDocument => "SignedDocument";
            public static string FormXmlDataOriginal => "FormXmlDataOriginal";
            public static string FormXmlDataEditable => "FormXmlDataEditable";
        }

        public static string AttachmentTypeKey => "AttachmentType";
        public static class AttachmentTypes
        {
            public static string PlanForUavhengigKontrol => "PlanForUavhengigKontroll";
        }
    }
}
