﻿using Dibk.Ftpb.Signing;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Ansako.App.Services.Signing
{
    public class SignerFactory
    {
        private AnSaKoSystemSettings _settings;
        private readonly IEnumerable<ISigner> _signerImpls;

        public SignerFactory(IOptions<AnSaKoSystemSettings> settings, IEnumerable<ISigner> signerImpls)
        {
            _settings = settings.Value;
            _signerImpls = signerImpls;
        }

        public ISigner GetSigner()
        {
            var signer = _signerImpls.FirstOrDefault(s => s.GetSigningProviderName().Equals(_settings.SignerType, StringComparison.InvariantCultureIgnoreCase));
            return signer;
        }
    }
}
