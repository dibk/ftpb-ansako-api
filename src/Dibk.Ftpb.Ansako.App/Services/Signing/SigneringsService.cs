using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.App.Repositories.Models;
using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using Dibk.Ftpb.Api.Client.Interfaces;
using Dibk.Ftpb.Api.Client.Models;
using Dibk.Ftpb.Api.Models;
using Dibk.Ftpb.Signing;
using Dibk.Ftpb.Signing.Models;
using DibkFtpb.Api.Client;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services.Signing
{
    public partial class SigneringsService
    {
        private readonly ILogger<SigneringsService> _logger;
        private readonly FormServiceProvider _formServiceProvider;
        private readonly IBlobService _blobService;
        private readonly FtpbBlobService _ftpbBlobService;
        private readonly FormRepo _formRepo;
        private readonly IFtpbUnitOfWork _ftpbUnitOfWork;
        private readonly SignerFactory _signerFactory;
        private readonly AnSaKoSystemSettings _settings;
        private readonly IAttachmentService _attachmentService;

        public SigneringsService(ILogger<SigneringsService> logger,
            FormServiceProvider formServiceProvider,
            IBlobService blobService,
            IOptions<AnSaKoSystemSettings> settings,
            FormRepo formRepo,
            IFtpbUnitOfWork ftpbUnitOfWork,
            SignerFactory signerFactory,
            FtpbBlobService ftpbBlobService,
            IAttachmentService attachmentService)
        {
            _logger = logger;
            _formServiceProvider = formServiceProvider;
            _blobService = blobService;
            _formRepo = formRepo;
            _ftpbUnitOfWork = ftpbUnitOfWork;
            _signerFactory = signerFactory;
            _settings = settings.Value;
            _ftpbBlobService = ftpbBlobService;
            _attachmentService = attachmentService;
        }

        public async Task<bool> DocumentExists(AnSaKoModel formdata)
        {
            try
            {
                await GetDocumentsToSign(formdata);
            }
            catch (FileNotFoundException ex)
            {
                _logger.LogWarning(ex, "Document for signing doesn't exist");
                return false;
            }
            return true;
        }

        public async Task<SigningJobResult> CreateSigningJob(AnSaKoModel anSaKoModel)
        {
            SigningJobResult result = null;

            if (SigningJobExists(anSaKoModel) && await SigningJobIsAccesible(anSaKoModel))
                result = await RequestNewSigningRedirectUrl(anSaKoModel);
            else
            {
                string callbackToken = GenerateCallbackToken();
                anSaKoModel.FormDbData.SigneringsjobCallbackToken = callbackToken;
                anSaKoModel.FormDbData.SigneringsjobOpprettetDato = DateTime.Now;
                result = await RequestNewSigningJob(anSaKoModel, callbackToken);
            }

            anSaKoModel.FormDbData.SigneringsJobId = result.Id;
            anSaKoModel.FormDbData.SigneringsJobStatus = result.Status.ToSigningJobStatus();
            anSaKoModel.FormDbData.SigneringsUrl = result.SigningUrl;
            anSaKoModel.FormDbData.SigneringsjobMetadata = result.SigningMetadata;
            anSaKoModel.FormDbData.Signeringstjeneste = result.SigningProvider;

            await _formRepo.UpdateAsync(anSaKoModel);

            await _ftpbUnitOfWork.InitiateAsync(anSaKoModel.FtpbReferenceId, anSaKoModel.AnSaKoReferenceId);
            _ftpbUnitOfWork.LogEntries.AddInfo("Signeringsjobb opprettet");
            await _ftpbUnitOfWork.LogEntries.SaveAsync();
            return result;
        }

        private string GenerateCallbackToken()
        {
            return Guid.NewGuid().ToString();
        }

        private async Task<SigningJobResult> RequestNewSigningRedirectUrl(AnSaKoModel anSaKoModel)
        {
            var result = new SigningJobResult();
            result.Id = anSaKoModel.FormDbData.SigneringsJobId;
            result.SigningMetadata = anSaKoModel.FormDbData.SigneringsjobMetadata;
            result.SigningProvider = anSaKoModel.FormDbData.Signeringstjeneste;
            result.Status = anSaKoModel.FormDbData.SigneringsJobStatus.ToSigningStatus();

            result.SigningUrl = await _signerFactory.GetSigner().RequestNewRedirectUri(anSaKoModel.FormDbData.SigneringsjobMetadata);

            return result;
        }

        private async Task<SigningJobResult> RequestNewSigningJob(AnSaKoModel anSaKoModel, string callbackToken)
        {
            var docs = await GetDocumentsToSign(anSaKoModel);
            var formSigningJob = await GetFormSigningData(anSaKoModel);

            var signingJob = new SigningJobRequest()
            {
                SigneeName = formSigningJob.SignerName,
                SigneeEmail = formSigningJob.SignerEmail,
                JobTitle = formSigningJob.SigningJobTitle,
                SigningCallbackUrls = new SigningCallbackUrls()
                {
                    OnSuccessRedirectUrl = SigningRedirectUrls.SigningJobSuccessdUrl(_settings.BackendUrl, anSaKoModel.AnSaKoReferenceId, callbackToken),
                    OnCancelRedirectUrl = SigningRedirectUrls.SigningJobRejectedUrl(_settings.BackendUrl, anSaKoModel.AnSaKoReferenceId, callbackToken),
                    OnErrorRedirectUrl = SigningRedirectUrls.SigningJobErrorUrl(_settings.BackendUrl, anSaKoModel.AnSaKoReferenceId, callbackToken)
                },
                ExternalId = anSaKoModel.AnSaKoReferenceId
            };
            signingJob.SigningDocuments.AddRange(docs);
            return await _signerFactory.GetSigner().CreateSigningJob(signingJob);
        }

        private async Task<bool> SigningJobIsAccesible(AnSaKoModel model)
        {
            var status = await GetSigningJobStatus(model);
            _logger.LogInformation($"Job has status {status.Status}");
            if (status.Status == SigningStatus.Created ||
                status.Status == SigningStatus.InProgress)
                return true;

            return false;
        }

        private bool SigningJobExists(AnSaKoModel anSaKoModel)
        {
            if (!string.IsNullOrEmpty(anSaKoModel.FormDbData.SigneringsJobId))
                return true;

            return false;
        }

        public async Task<SigningJobResult> GetSigningJobStatus(AnSaKoModel anSaKoModel)
        {
            if (string.IsNullOrEmpty(anSaKoModel.FormDbData.SigneringsJobId))
                throw new SigningJobException(SigningJobException.SigningJobExceptionType.NoSigningJobData, "Erklæringen har ingen aktiv signeringsjob");

            SigningStatus status;
            try
            {
                status = await _signerFactory.GetSigner().GetStatusForSigningJob(new SigningJobStatusRequest(
                    anSaKoModel.FormDbData.SigneringsJobId,
                    anSaKoModel.FormDbData.SigneringsjobMetadata));

                anSaKoModel.FormDbData.SigneringsJobStatus = status.ToSigningJobStatus();
                await _formRepo.UpdateAsync(anSaKoModel);
            }
            catch (ArgumentNullException)
            {
                status = anSaKoModel.FormDbData.SigneringsJobStatus.ToSigningStatus();
            }

            return new SigningJobResult()
            {
                Status = status,
                Id = anSaKoModel.FormDbData.SigneringsJobId,
                SigningProvider = anSaKoModel.FormDbData.Signeringstjeneste,
                SigningUrl = anSaKoModel.FormDbData.SigneringsUrl,
                SigningMetadata = anSaKoModel.FormDbData.SigneringsjobMetadata
            };
        }

        public async Task<string> GetSignedDocumentDownloadUrl(AnSaKoModel anSaKoModel)
        {
            var signedDocumentMetadata = await GetFileDownloadStatusFromFtpb(anSaKoModel);
            var urlProvider = new FileDownloadUrlProvider(_settings.FtpbPublicHost);
            var url = urlProvider.GenerateDownloadUri(signedDocumentMetadata);
            return url;
        }

        private async Task<FileDownloadStatus> GetFileDownloadStatusFromFtpb(AnSaKoModel anSaKoModel)
        {
            await _ftpbUnitOfWork.InitiateAsync(anSaKoModel.FormDbData.FtpbSignedReferenceId, anSaKoModel.AnSaKoReferenceId);
            var fdss = await _ftpbUnitOfWork.FileDownloadStatuses.GetAsync();
            var signedDocumentMetadata = fdss.FirstOrDefault(p => p.FileType == FilTyperForNedlasting.SkjemaPdf);
            if (signedDocumentMetadata == null)
            {
                _logger.LogError("Signed document wasn't found for {ReferenceId}", anSaKoModel.FormDbData.FtpbSignedReferenceId);
                throw new Exception($"Signed document wasn't found for {anSaKoModel.FormDbData.FtpbSignedReferenceId}");
            }
            return signedDocumentMetadata;
        }

        public async Task<BlobItem> GetSignedDocument(AnSaKoModel anSaKoModel)
        {
            var signedDocumentMetadata = await GetFileDownloadStatusFromFtpb(anSaKoModel);

            var blob = await _ftpbBlobService.GetFileAsync(signedDocumentMetadata.ContainerName, signedDocumentMetadata.Filename);
            return blob;
        }

        public async Task SigningJobRejected(AnSaKoModel anSaKoModel, string reason = null)
        {
            anSaKoModel.FormDbData.SigneringsJobStatus = Storage.Database.Models.SigningJobStatus.Failed;
            anSaKoModel.FormDbData.StatusDetails = reason;
            await _formRepo.UpdateAsync(anSaKoModel);
        }

        public async Task SigningJobFailed(AnSaKoModel anSaKoModel, string reason = null)
        {
            anSaKoModel.FormDbData.SigneringsJobStatus = Storage.Database.Models.SigningJobStatus.Failed;
            anSaKoModel.FormDbData.StatusDetails = reason;
            await _formRepo.UpdateAsync(anSaKoModel);
        }

        public async Task SigningFailed(AnSaKoModel anSaKoModel, string reason = null)
        {
            anSaKoModel.FormDbData.Status = Storage.Database.Models.FormStatus.Feilet;
            anSaKoModel.FormDbData.SigneringsJobStatus = Storage.Database.Models.SigningJobStatus.Failed;
            await _formRepo.UpdateAsync(anSaKoModel);

            await _ftpbUnitOfWork.InitiateAsync(anSaKoModel.FtpbReferenceId, anSaKoModel.AnSaKoReferenceId);
            var df = await _ftpbUnitOfWork.DistributionForms.GetAsync(anSaKoModel.FormDbData.FtpbDistributionFormsId);
            if (df.AnSaKoStatus == null) df.AnSaKoStatus = new AnSaKoStatus();
            df.AnSaKoStatus.AnSaKoProcessStatus = AnSaKoProcessStatusType.feilet;
            df.AnSaKoStatus.StatusDetails = reason;
            await _ftpbUnitOfWork.SaveAsync();
        }

        private async Task<SignedDocument> DownloadAndStoreSignedDocument(AnSaKoModel anSaKoModel)
        {
            var signedDocument = await _signerFactory.GetSigner().GetSignedDocument(new SigningJobStatusRequest(
                                            anSaKoModel.FormDbData.SigneringsJobId,
                                            anSaKoModel.FormDbData.SigneringsjobMetadata));

            var metadata = new Dictionary<string, string>()
            {
                {BlobMetadata.BlobTypeKey, BlobMetadata.BlobTypes.SignedDocument }
            };

            var signedPdfFileName = GetSignedDocumentFilename(anSaKoModel);
            await _blobService.AddOrUpdateFileAsync(anSaKoModel.AnSaKoReferenceId, signedPdfFileName, "application/pdf", signedDocument.FileContent, metadata);

            return signedDocument;
        }

        public async Task<SigningJobResult> DocumentSigned(AnSaKoModel anSaKoModel)
        {
            //Get next ftpbReferenceId (FRxxxxx) from DB...
            var ftpbSignedReferenceId = await _formRepo.GetNextFtpbReferenceId();

            //Henter signert dokument fra signeringstjeneste
            var signedDocument = await DownloadAndStoreSignedDocument(anSaKoModel);
            _logger.LogDebug("Retreived signed dorcument from {SignerProvider}", _signerFactory.GetSigner().GetSigningProviderName());

            anSaKoModel.FormDbData.FtpbSignedReferenceId = ftpbSignedReferenceId;
            anSaKoModel.FormDbData.Status = Storage.Database.Models.FormStatus.Signert;
            anSaKoModel.FormDbData.SignertDato = signedDocument.SignedDate;
            anSaKoModel.FormDbData.SigneringsJobStatus = Storage.Database.Models.SigningJobStatus.CompletedSuccessfully;
            await _formRepo.UpdateAsync(anSaKoModel);

            await ReportSignedDocumentStatus(anSaKoModel.AnSaKoReferenceId);

            return new SigningJobResult()
            {
                Id = anSaKoModel.FormDbData.SigneringsJobId,
                SigningUrl = anSaKoModel.FormDbData.SigneringsUrl,
                SigningProvider = anSaKoModel.FormDbData.Signeringstjeneste,
                Status = SigningStatus.CompletedSuccessfully
            };
        }

        public async Task ReportSignedDocumentStatus(string ansakoReferenceId)
        {
            var anSaKoModel = await _formRepo.GetAsync(ansakoReferenceId);

            //Sett distrform for initiell formmetadata til signert
            _logger.LogDebug("Updates Ftpb DistributionForm for main form: {FtpbReferenceId}", anSaKoModel.FtpbReferenceId);
            await _ftpbUnitOfWork.InitiateAsync(anSaKoModel.FtpbReferenceId, anSaKoModel.AnSaKoReferenceId);
            var df = await _ftpbUnitOfWork.DistributionForms.GetAsync(anSaKoModel.FormDbData.FtpbDistributionFormsId);
            df.Signed = anSaKoModel.FormDbData.SignertDato;
            df.SignedArchiveReference = anSaKoModel.FormDbData.FtpbSignedReferenceId;
            df.DistributionStatus = DistributionStatus.receiptSent;
            if (df.AnSaKoStatus == null) df.AnSaKoStatus = new AnSaKoStatus();
            df.AnSaKoStatus.AnSaKoProcessStatus = AnSaKoProcessStatusType.signert;
            _ftpbUnitOfWork.LogEntries.AddInfo($"Erklæringen er signert. Registreres med arkivreferanse: {anSaKoModel.FormDbData.FtpbSignedReferenceId}");
            await _ftpbUnitOfWork.SaveAsync();

            // Oppretter ny formMetadata-rad med tilhørende Filedownloadstatus rader for signert erklæring og xml
            _logger.LogDebug("Creates Ftpb FormMetadata for signed document with {ArchiveReference}", anSaKoModel.FormDbData.FtpbSignedReferenceId);
            await _ftpbUnitOfWork.InitiateAsync(anSaKoModel.FormDbData.FtpbSignedReferenceId, anSaKoModel.AnSaKoReferenceId);
                        
            var fmmd = await _ftpbUnitOfWork.FormMetadata.GetAsync();
            if (fmmd == null)
                fmmd = new FormMetadata(anSaKoModel.FormDbData.FtpbSignedReferenceId);

            fmmd.FormType = AnsakoToFtpb.GetFtpbDistributionType(anSaKoModel.FormDbData.FormType);
            fmmd.ArchiveTimestamp = anSaKoModel.FormDbData.SignertDato;
            fmmd.Status = "I kø";
            fmmd.SenderSystem = "AnSaKo";
            fmmd.Application = anSaKoModel.FormDbData.SluttbrukerSystem;

            fmmd = await _ftpbUnitOfWork.FormMetadata.AddOrUpdateAsync(fmmd);

            _ftpbUnitOfWork.LogEntries.AddInfo("Signert erklæring mottatt");
            _ftpbUnitOfWork.LogEntries.AddInfoInternal($"Signert registrert for AnSaKo intern id {anSaKoModel.AnSaKoReferenceId} Distribusjonens arkivreferanse: {anSaKoModel.FtpbReferenceId}", "AnSaKoSigned");

            //Tilgjengeliggjør XML og signert dokument i StatuAPIet
            //MÅ ENDRES TIL Å VÆRE REKJØRBART - sjekk om container ekisterer, Filedownloadstatus eksisterer, osv
            // Legg signert XML og dokument i ny container
            _ftpbUnitOfWork.LogEntries.AddInfo("Lagrer xml skjema");
            var signedFormBlobContainerName = Guid.NewGuid();

            var signedXml = await _blobService.GetFileAsync(anSaKoModel.FormDbData.BlobStorageContainerName, anSaKoModel.FormDbData.FormDataFileName);
            await AddBlobAndFileDownloadStatus(anSaKoModel.FormDbData.FtpbSignedReferenceId, signedFormBlobContainerName, signedXml.FileName, signedXml.MimeType, signedXml.ContentStream.GetAsBytes(), anSaKoModel.FormDbData.FormType, FilTyperForNedlasting.MaskinlesbarXml);

            _ftpbUnitOfWork.LogEntries.AddInfo("Lagrer signert pdf");
            var signedPdfFileName = GetSignedDocumentFilename(anSaKoModel);
            var signedDocument = await _blobService.GetFileAsync(ansakoReferenceId, signedPdfFileName);
            await AddBlobAndFileDownloadStatus(anSaKoModel.FormDbData.FtpbSignedReferenceId, signedFormBlobContainerName, signedPdfFileName, "application/pdf", signedDocument.ContentStream.GetAsBytes(), anSaKoModel.FormDbData.FormType, FilTyperForNedlasting.SkjemaPdf);

            //Legg til PlanForUavhengigKontrol vedlegg i dersom dette eksisterer
            var attachment = await _attachmentService.GetAttachmentAsync(anSaKoModel.AnSaKoReferenceId);
            if (attachment != null)
            {
                var attachmentBlobContainerName = Guid.NewGuid();
                _ftpbUnitOfWork.LogEntries.AddInfo($"Lagrer vedlegg: {anSaKoModel.FormDbData.AttachmentFileName}");
                await AddBlobAndFileDownloadStatus(anSaKoModel.FormDbData.FtpbSignedReferenceId, attachmentBlobContainerName, attachment.AttachmentFileName, attachment.MimeType, attachment.ContentStream.GetAsBytes(), anSaKoModel.FormDbData.FormType, FilTyperForNedlasting.PlanForUavhengigKontroll);
            }
            fmmd.Status = "Ok";
            await _ftpbUnitOfWork.SaveAsync();
        }

        private async Task<FileDownloadStatus> AddBlobAndFileDownloadStatus(string ftpbReferenceId, Guid containerName, string fileName, string mimeType, byte[] content, Storage.Database.Models.ErklaeringType formType, FilTyperForNedlasting fileType)
        {
            var blobUrl = await _ftpbBlobService.AddOrUpdateFileAsync(containerName.ToString(), fileName, mimeType, content);

            var fds = new FileDownloadStatus()
            {
                ArchiveReference = ftpbReferenceId,
                BlobLink = blobUrl,
                Filename = fileName,
                FileType = fileType,
                FormName = AnsakoToFtpb.GetFtpbDistributionType(formType),
                MimeType = mimeType,
                ContainerName = containerName.ToString()
            };
            return await _ftpbUnitOfWork.FileDownloadStatuses.AddOrUpdateAsync(fds);
        }

        private string GetSignedDocumentFilename(AnSaKoModel anSaKoModel)
        {
            var documentFilename = string.Empty;

            switch (anSaKoModel.FormDbData.FormType)
            {
                case Storage.Database.Models.ErklaeringType.Ansvarsrett:
                    documentFilename = AnsvarsrettService.SignedPdfFileName;
                    break;

                case Storage.Database.Models.ErklaeringType.Samsvarserklaering:
                    documentFilename = SamsvarserklaeringService.SignedPdfFileName;
                    break;

                case Storage.Database.Models.ErklaeringType.Kontrollerklaering:
                    documentFilename = KontrollerklaeringService.SignedPdfFileName;
                    break;

                default:
                    break;
            }
            return documentFilename;
        }

        private async Task<IEnumerable<SigningDocument>> GetDocumentsToSign(AnSaKoModel anSaKoModel)
        {
            var retVal = new List<SigningDocument>();
            var service = _formServiceProvider.GetService(anSaKoModel.FormDbData.FormType);
            var attachments = await service.GetDocumentsToSign(anSaKoModel);
            retVal.AddRange(attachments);
            return retVal;
        }

        private async Task<FormSigningJobData> GetFormSigningData(AnSaKoModel anSaKoModel)
        {
            var formService = _formServiceProvider.GetService(anSaKoModel.FormDbData.FormType);
            var formSigningJob = await formService.GetFormSigningJobData(anSaKoModel.AnSaKoReferenceId); new FormSigningJobData();

            return formSigningJob;
        }
    }

    public class SigningJobException : Exception
    {
        public enum SigningJobExceptionType
        {
            NoSigningJobData
        }

        public SigningJobExceptionType ExceptionType { get; set; }

        public SigningJobException(SigningJobExceptionType exceptionType, string message) : base(message)
        {
            ExceptionType = exceptionType;
        }

        public SigningJobException(SigningJobExceptionType exceptionType, string message, Exception innerException) : base(message, innerException)
        {
            ExceptionType = exceptionType;
        }
    }

    public static class SigningJobStatusExtensions
    {
        public static Storage.Database.Models.SigningJobStatus ToSigningJobStatus(this SigningStatus status)
        {
            switch (status)
            {
                case SigningStatus.Created:
                    return Storage.Database.Models.SigningJobStatus.Created;

                case SigningStatus.NoChanges:
                    return Storage.Database.Models.SigningJobStatus.NoChanges;

                case SigningStatus.CompletedSuccessfully:
                    return Storage.Database.Models.SigningJobStatus.CompletedSuccessfully;

                case SigningStatus.Failed:
                    return Storage.Database.Models.SigningJobStatus.Failed;

                case SigningStatus.InProgress:
                    return Storage.Database.Models.SigningJobStatus.InProgress;

                default:
                    throw new ArgumentException("Value of status doesn't exist in SigningJobStatus");
            }
        }

        public static SigningStatus ToSigningStatus(this Storage.Database.Models.SigningJobStatus? status)
        {
            if (status.HasValue)
                switch (status)
                {
                    case Storage.Database.Models.SigningJobStatus.Created:
                        return SigningStatus.Created;

                    case Storage.Database.Models.SigningJobStatus.NoChanges:
                        return SigningStatus.NoChanges;

                    case Storage.Database.Models.SigningJobStatus.CompletedSuccessfully:
                        return SigningStatus.CompletedSuccessfully;

                    case Storage.Database.Models.SigningJobStatus.Failed:
                        return SigningStatus.Failed;

                    case Storage.Database.Models.SigningJobStatus.InProgress:
                        return SigningStatus.InProgress;

                    default:
                        throw new ArgumentException("Value of status doesn't exist in SigningStatus");
                }
            else
                return SigningStatus.Created;
        }
    }
}