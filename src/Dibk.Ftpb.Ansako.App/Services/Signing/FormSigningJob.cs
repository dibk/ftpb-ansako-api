﻿namespace Dibk.Ftpb.Ansako.App.Services.Signing
{
    public class FormSigningJobData
    {
        public string SignerName { get; set; }
        public string SignerEmail { get; set; }

        private string _signingJobTitle;
        public string SigningJobTitle
        {
            get
            {
                if (_signingJobTitle.Length > 80) { return _signingJobTitle.Substring(0, 80); }
                else return _signingJobTitle;
            }
            set { _signingJobTitle = value; }
        }
    }
}
