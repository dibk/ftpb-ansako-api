﻿namespace Dibk.Ftpb.Ansako.App.Services.Signing
{
    public static class SigningRedirectUrls
    {
        public static string SigningJobSuccessdUrl(string host, string reference, string callbackToken) => $"{host}/api/v1/signeringcallback/{reference}?signingevent=signed&callback_token={callbackToken}";
        public static string SigningJobRejectedUrl(string host, string reference, string callbackToken) => $"{host}/api/v1/signeringcallback/{reference}?signingevent=rejected&callback_token={callbackToken}";
        public static string SigningJobErrorUrl(string host, string reference, string callbackToken) => $"{host}/api/v1/signeringcallback/{reference}?signingevent=error&callback_token={callbackToken}";
        public static string FrontendRedirectOnSuccessUrl(string host, string reference) => $"{host}/skjema/{reference}/signert";
        public static string FrontendRedirectOnRejectedUrl(string host, string reference) => $"{host}/skjema/{reference}/rediger";
        public static string FrontendRedirectOnErrorUrl(string host, string reference, string errorType) => $"{host}/skjema/{reference}?error_type={errorType}";
    }
}
