﻿using ClamAV.Net.Client;
using ClamAV.Net.Client.Results;
using ClamAV.Net.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public class ClamAVService
    {
        private readonly ILogger<ClamAVService> _logger;
        private readonly ClamAVSettings _config;
        private IClamAvClient _clamAvClient;

        public ClamAVService(IOptions<ClamAVSettings> config, ILoggerFactory loggerFactory)
        {
            _config = config.Value;
            _logger = loggerFactory.CreateLogger<ClamAVService>();            
            _clamAvClient = ClamAvClient.Create(new Uri(_config.Host), loggerFactory);
        }

        public async Task<bool> FileInfectedAsync(IFormFile file)
        {
            try
            {
                await TestConnectivity();
                await LogVersionInfo();

                var memoryStream = new MemoryStream();
                await file.CopyToAsync(memoryStream).ConfigureAwait(false);
                                
                _logger.LogInformation($"ClamAV - starts scanning file {file.FileName}");                
                ScanResult res = await _clamAvClient.ScanDataAsync(memoryStream).ConfigureAwait(false);

                if (res.Infected)
                    _logger.LogWarning($"ClamAV - Virus scan of '{file.FileName}' indicates file infected by: {res.VirusName}");
                else
                    _logger.LogInformation(($"ClamAV - File '{file.FileName}' is OK"));

                return res.Infected;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ClamAV - An exception occurred when scanning file.");
                return false;
            }
        }

        private async Task TestConnectivity()
        {
            if (_config.TestConnection)
            {
                try
                {
                    //Send PING command to ClamAV
                    await _clamAvClient.PingAsync().ConfigureAwait(false);
                }
                catch (ClamAvException ex)
                {
                    _logger.LogError(ex, "ClamAV - Exception occurred when pinging {ClamAVHost}", _config.Host);
                    throw;
                }
            }
        }

        private async Task LogVersionInfo()
        {
            if (_config.LogVersionInfo)
            {
                try
                {
                    //Get ClamAV engine and virus database version
                    VersionResult result = await _clamAvClient.GetVersionAsync().ConfigureAwait(false);
                    _logger.LogInformation($"ClamAV version - {result.ProgramVersion} , virus database version {result.VirusDbVersion}");
                }
                catch (ClamAvException ex)
                {
                    _logger.LogError(ex, "ClamAV - Exception occurred when checking for version {ClamAVHost}", _config.Host);
                    throw;
                }
            }
        }
    }

    public class ClamAVSettings
    {
        public string Host { get; set; }
        public bool TestConnection { get; set; }
        public bool LogVersionInfo { get; set; }
    }
}