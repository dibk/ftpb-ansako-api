using Dibk.Ftpb.Ansako.App.Mappers;
using Dibk.Ftpb.Ansako.App.Models;
using Dibk.Ftpb.Ansako.App.Models.InternalApi;
using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.App.Repositories.Models;
using Dibk.Ftpb.Ansako.App.Services.Signing;
using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Dibk.Ftpb.Api.Client.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using no.kxml.skjema.dibk.ansvarsrettAnsako;
using System;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public class AnsvarsrettService : FormServiceBase, IFormService
    {
        public static string SignedPdfFileName => "ErklaeringOmAnsvarsrett.pdf";

        public override string DocumentTitle
        { get { return "Erklæring ansvarsrett"; } }

        public ErklaeringType FormType => ErklaeringType.Ansvarsrett;

        private readonly ILogger<AnsvarsrettService> _logger;
        private readonly FormRepo _formRepo;
        private readonly SgRegistryService _sgRegistryService;

        // TMP løsning
        private readonly string _samsvarKontrollIkkeRedigerbareSluttbrukerSystem = string.Empty;

        public AnsvarsrettService(ILogger<AnsvarsrettService> logger, FormRepo formRepo, IFtpbUnitOfWork unitOfWork, IBlobService blobService, SgRegistryService sgRegistryService, IOptions<AnSaKoSystemSettings> options)
            : base(logger, unitOfWork, blobService, formRepo)
        {
            _logger = logger;
            _formRepo = formRepo;
            _sgRegistryService = sgRegistryService;
            _samsvarKontrollIkkeRedigerbareSluttbrukerSystem = options.Value.SamsvarKontrollFeltIkkeRedigerbare;
        }

        public async Task<IFormDto> GetAsync(string ansakoReferenceId)
        {
            return MapToDto(await Get(ansakoReferenceId));
        }

        protected override AnsvarsrettDto MapToDto(AnSaKoModel model)
        {
            if (string.IsNullOrEmpty(model.FormXmlData))
                throw new Exception($"FormXmlData is null or empty. Status: {model.FormDbData.Status.ToString()} DateDeleted: {model.FormDbData.DateDeleted}");

            AnsvarsrettModel ansvarsrettModel = ErklaeringAnsvarsrettV1ModelMapper.Map(model.FormXmlData);

            if (ansvarsrettModel.FraSluttbrukerSystem.Equals(_samsvarKontrollIkkeRedigerbareSluttbrukerSystem, StringComparison.OrdinalIgnoreCase))
                ansvarsrettModel.ErSamsvarKontrollFeltRedigerbare = false;
            else
                ansvarsrettModel.ErSamsvarKontrollFeltRedigerbare = true;

            return new AnsvarsrettDto()
            {
                AnSaKoReferenceId = model.AnSaKoReferenceId,
                Status = model.FormDbData.Status,
                StatusDetails = model.FormDbData.StatusDetails,
                FormData = ansvarsrettModel,
                SigningJobStatus = model.FormDbData.SigneringsJobStatus,
                Signeringsfrist = model.FormDbData.SigneringsFrist,
                SignertDato = model.FormDbData.SignertDato,
                FtpbReferenceId = model.FtpbReferenceId
            };
        }

        protected override void MapFromDto(IFormDto dto, AnSaKoModel model)
        {
            ErklaeringAnsvarsrettV1ToAnSaKoModelMapper.MapFromDto(dto as AnsvarsrettDto, ref model);
        }

        public async Task<string> CreateAsync(CreateFormPayload payload, string ansakoReferenceId)
        {
            _logger.LogDebug("Deserializes payload");
            var ansvarsrett = XmlSerialization.DeserializeFromString<ErklaeringAnsvarsrettType>(payload.FormDataXml);

            _logger.LogDebug("Sets harSentalGodkjenning value if missing");
            ansvarsrett.ansvarsrett.foretak.harSentralGodkjenning ??= await _sgRegistryService.HarSentralGodkjenning(ansvarsrett.ansvarsrett.foretak.organisasjonsnummer);
            ansvarsrett.ansvarsrett.foretak.harSentralGodkjenningSpecified = ansvarsrett.ansvarsrett.foretak.harSentralGodkjenning.HasValue;
            ansvarsrett.signatur = null;

            _logger.LogDebug("Clears values for signatur, signaturdato, dekkesOmraadeAvSentralGodkjenning, ansvarsrett.erklaeringAnsvarligKontrollerende, ansvarsrett.erklaeringAnsvarligProsjekterende, ansvarsrett.erklaeringAnsvarligUtfoerende");
            ansvarsrett.signatur = null;
            ansvarsrett.ansvarsrett.foretak.signaturdato = null;
            ansvarsrett.ansvarsrett.foretak.signaturdatoSpecified = false;

            foreach (var ansvarsomraade in ansvarsrett.ansvarsrett.ansvarsomraader)
            {
                ansvarsomraade.dekkesOmraadeAvSentralGodkjenning = null;
                ansvarsomraade.dekkesOmraadeAvSentralGodkjenningSpecified = false;

                if (ansvarsomraade.funksjon.kodeverdi.Equals("SØK"))
                    ansvarsomraade.beskrivelseAvAnsvarsomraade = "Endring av ansvarlig søker";
            }

            ansvarsrett.ansvarsrett.erklaeringAnsvarligKontrollerende = null;
            ansvarsrett.ansvarsrett.erklaeringAnsvarligKontrollerendeSpecified = false;
            ansvarsrett.ansvarsrett.erklaeringAnsvarligProsjekterende = null;
            ansvarsrett.ansvarsrett.erklaeringAnsvarligProsjekterendeSpecified = false;
            ansvarsrett.ansvarsrett.erklaeringAnsvarligUtfoerende = null;
            ansvarsrett.ansvarsrett.erklaeringAnsvarligUtfoerendeSpecified = false;

            var ansakoModel = new AnSaKoModel(ansakoReferenceId);

            ErklaeringAnsvarsrettV1ToAnSaKoModelMapper.MapNewModelFromXml(ansvarsrett, ref ansakoModel);
            ansakoModel.FormDbData.FtpbDistributionFormsId = Guid.NewGuid();
            ansakoModel.FormDbData.Status = FormStatus.Opprettet;
            ansakoModel.FormDbData.FormType = ErklaeringType.Ansvarsrett;
            ansakoModel.FormDbData.SigneringsFrist = payload.FormMetadata?.Signeringsfrist;
            ansakoModel.FormDbData.FormDataFileName = "ErklaeringOmAnsvarsrett.xml";

            ansakoModel = await _formRepo.InsertAsync(ansakoModel);

            await AddToFtpb(ansakoModel);

            return ansakoModel.FtpbReferenceId;
        }

        public async Task<IFormDto> RejectAsync(string ansakoReferenceId, string reason)
        {
            await base.RejectForm(ansakoReferenceId, reason);

            return await this.GetAsync(ansakoReferenceId);
        }

        public ValidationResult IsValid(CreateFormPayload payload)
        {
            var valRes = new ValidationResult() { IsValid = true, ValidationMessage = "Ok" };
            try
            {
                XmlSerialization.DeserializeFromString<ErklaeringAnsvarsrettType>(payload.FormDataXml);
            }
            catch (Exception ex)
            {
                var validationMessage = $"Payload with given dataformatid and -version {payload.DataFormatId}/{payload.DataFormatVersion} failed.";
                _logger.LogError(ex, validationMessage);
                valRes.IsValid = false;
                valRes.ValidationMessage = $"{validationMessage} - {ex.Message}";
            }
            return valRes;
        }

        public async Task<FormSigningJobData> GetFormSigningJobData(string ansakoReferenceId)
        {
            var ansvarsrett = MapToDto(await Get(ansakoReferenceId));

            return new FormSigningJobData()
            {
                SignerName = ansvarsrett.FormData.AnsvarligForetak.KontaktpersonNavn,
                SignerEmail = ansvarsrett.FormData.AnsvarligForetak.Epost,
                SigningJobTitle = $"Ansvarserklæring fra {ansvarsrett.FormData.AnsvarligForetak.Navn}"
            };
        }

        public async Task<IFormDto> PrepareForSigning(IFormDto formDto)
        {
            return await UpdateAsync(formDto);
        }
    }
}