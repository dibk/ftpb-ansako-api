﻿using Dibk.Ftpb.Ansako.Storage.Database.Models;
using System;
using System.Linq;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public static class StateHelper
    {
        public static bool CanTransitToState(FormStatus currentState, FormStatus requestedState)
        {
            switch (currentState)
            {
                case FormStatus.Opprettet:
                    return true; //Kan endres til alle
                case FormStatus.IArbeid:
                    return requestedState != FormStatus.Opprettet; //Kan ikke gå tilbake til opprettet status
                case FormStatus.TilSignering:
                    var validForTilSignering = new[] { FormStatus.IArbeid, FormStatus.Signert, FormStatus.Avvist, FormStatus.Utgått, FormStatus.Trukket, FormStatus.Feilet, FormStatus.SignertManuelt, FormStatus.Avsluttet };
                    return validForTilSignering.Contains(requestedState);
                case FormStatus.Signert:
                    var validForSignering = new[] { FormStatus.Trukket};
                    return validForSignering.Contains(requestedState);
                case FormStatus.Avvist:
                case FormStatus.Utgått:
                case FormStatus.Trukket:
                case FormStatus.Feilet:
                case FormStatus.SignertManuelt:
                case FormStatus.Avsluttet:
                    return false; //Endelige statuser som ikke kan endres
                default:
                    throw new ArgumentOutOfRangeException(nameof(currentState));
            }
        }
    }
}
