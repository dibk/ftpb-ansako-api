﻿using Dibk.Ftpb.Ansako.App.Mappers;
using Dibk.Ftpb.Ansako.App.Models.InternalApi;
using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.App.Repositories.Models;
using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Dibk.Ftpb.Api.Client.Interfaces;
using Dibk.Ftpb.Api.Client.Models;
using Dibk.Ftpb.Api.Models;
using Dibk.Ftpb.Signing.Models;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public abstract class FormServiceBase
    {
        private readonly ILogger _logger;
        private readonly IFtpbUnitOfWork _ftpbUnitOfWork;
        private readonly IBlobService _blobService;
        private readonly FormRepo _formRepo;

        public FormServiceBase(ILogger logger, IFtpbUnitOfWork ftpbUnitOfWork, IBlobService blobService, FormRepo formRepo)
        {
            _logger = logger;
            _ftpbUnitOfWork = ftpbUnitOfWork;
            _blobService = blobService;
            _formRepo = formRepo;
        }

        protected AnSaKoModel CurrentForm;

        protected async Task<AnSaKoModel> Get(string ansakoReferenceId)
        {
            if (CurrentForm == null)
            {
                CurrentForm = await _formRepo.GetAsync(ansakoReferenceId);
            }
            return CurrentForm;
        }

        public async Task<IFormDto> UpdateAsync(IFormDto formDto)
        {
            var storedAnsvarsrett = await Get(formDto.AnSaKoReferenceId);
            var currentState = storedAnsvarsrett.FormDbData.Status;

            ThrowIfStateIsInvalid(storedAnsvarsrett, formDto.Status);

            //Synkronisér data mellom Dto og database/xml data
            MapFromDto(formDto, storedAnsvarsrett);

            if (storedAnsvarsrett.FormDbData.SigneringsJobStatus.HasValue &&
                (storedAnsvarsrett.FormDbData.SigneringsJobStatus.Value == SigningJobStatus.Created
                || storedAnsvarsrett.FormDbData.SigneringsJobStatus.Value == SigningJobStatus.Failed
                || storedAnsvarsrett.FormDbData.SigneringsJobStatus.Value == SigningJobStatus.InProgress
                )
                && (storedAnsvarsrett.FormDbData.Status == FormStatus.TilSignering
                || storedAnsvarsrett.FormDbData.Status == FormStatus.IArbeid))
            {
                storedAnsvarsrett.FormDbData.SigneringsJobStatus = null;
                storedAnsvarsrett.FormDbData.SigneringsJobId = null;
                storedAnsvarsrett.FormDbData.SigneringsUrl = null;
                storedAnsvarsrett.FormDbData.SigneringsjobMetadata = null;
            }

            CurrentForm = await _formRepo.UpdateAsync(storedAnsvarsrett);

            if (currentState != storedAnsvarsrett.FormDbData.Status)
                await HandleStatusChanged(storedAnsvarsrett);

            return MapToDto(CurrentForm);
        }

        private async Task HandleStatusChanged(AnSaKoModel model)
        {
            _logger.LogInformation("Form state has changed to {FormState}", model.FormDbData.Status);
            await SetDistributionStatus(model);
        }

        protected abstract IFormDto MapToDto(AnSaKoModel model);

        protected async Task AddToFtpb(AnSaKoModel ansakoModel)
        {
            await _ftpbUnitOfWork.InitiateAsync(ansakoModel.FtpbReferenceId, ansakoModel.AnSaKoReferenceId);
            _ftpbUnitOfWork.LogEntries.AddInfo($"{AnsakoToFtpb.GetFtpbFormTypeName(ansakoModel.FormDbData.FormType)} mottatt");

            var formMetaData = new FormMetadata(ansakoModel.FtpbReferenceId)
            {
                ArchiveTimestamp = ansakoModel.FormDbData.DateCreated,
                FormType = AnsakoToFtpb.GetFtpbFormTypeName(ansakoModel.FormDbData.FormType),
                Application = ansakoModel.FormDbData.SluttbrukerSystem,
                ServiceCode = CreateServiceCode(ansakoModel),
                ServiceEditionCode = 1,
                Status = "Ok",
                SenderSystem = "AnSaKo"
            };

            await _ftpbUnitOfWork.FormMetadata.AddOrUpdateAsync(formMetaData);

            var distributionForm = new DistributionForm()
            {
                Id = ansakoModel.FormDbData.FtpbDistributionFormsId,
                InitialArchiveReference = ansakoModel.FtpbReferenceId,
                DistributionStatus = DistributionStatus.submittedPrefilled,
                SubmitAndInstantiatePrefilled = ansakoModel.FormDbData.DateCreated,
                DistributionType = AnsakoToFtpb.GetFtpbDistributionType(ansakoModel.FormDbData.FormType),
                ExternalSystemReference = ansakoModel.FormDbData.Soeknadssystemetsreferanse,
                InitialExternalSystemReference = ansakoModel.Hovedinnsendingsnummer,
                AnSaKoStatus = new AnSaKoStatus()
                {
                    AnSaKoProcessStatus = AnSaKoProcessStatusType.tilSignering,
                    SigningDeadline = ansakoModel.FormDbData.SigneringsFrist
                }
            };

            _ftpbUnitOfWork.DistributionForms.AddAsync(distributionForm);

            _ftpbUnitOfWork.LogEntries.AddInfo($"{ansakoModel.FormDbData.FormType} {ansakoModel.FormDbData.DataFormatId}/{ansakoModel.FormDbData.DataFormatVersion} opprettet og distribuert");
            _ftpbUnitOfWork.LogEntries.AddInfoInternal($"AnSaKo intern id: {ansakoModel.AnSaKoReferenceId}", "AnSaKoCreate");

            await _ftpbUnitOfWork.SaveAsync();
        }

        protected abstract void MapFromDto(IFormDto dto, AnSaKoModel model);

        private string CreateServiceCode(AnSaKoModel anSaKoModel)
        {
            var serviceCode = string.Empty;
            switch (anSaKoModel.FormDbData.FormType)
            {
                case ErklaeringType.Ansvarsrett:
                    serviceCode = $"A{anSaKoModel.FormDbData.DataFormatId}";
                    break;

                case ErklaeringType.Samsvarserklaering:
                    serviceCode = $"S{anSaKoModel.FormDbData.DataFormatId}";
                    break;

                case ErklaeringType.Kontrollerklaering:
                    serviceCode = $"K{anSaKoModel.FormDbData.DataFormatId}";
                    break;

                default:
                    break;
            }

            return serviceCode;
        }

        //Avvist
        protected async Task RejectForm(string ansakoReferenceId, string reason)
        {
            using (Serilog.Context.LogContext.PushProperty("AnsakoReferenceId", ansakoReferenceId))
            {
                _logger.LogDebug("Form is rejected");
                var f = await _formRepo.GetAsync(ansakoReferenceId);

                var state = FormStatus.Avvist;
                ThrowIfStateIsInvalid(f, state);

                f.FormDbData.Status = state;
                f.FormDbData.StatusDetails = reason;
                _logger.LogDebug("Updates database");
                f = await _formRepo.UpdateAsync(f);

                await HandleStatusChanged(f);
            }
        }

        //Trukket
        public virtual async Task WithdrawForm(AnSaKoModel anSaKoModel, string reason)
        {
            await SetReportedStatus(anSaKoModel, reason, FormStatus.Trukket);
        }

        //Signert manuelt
        public virtual async Task SignedManually(AnSaKoModel anSaKoModel, string reason)
        {
            await SetReportedStatus(anSaKoModel, reason, FormStatus.SignertManuelt);
        }

        //Avsluttet
        public virtual async Task ReportedCompleted(AnSaKoModel anSaKoModel, string reason)
        {
            await SetReportedStatus(anSaKoModel, reason, FormStatus.Avsluttet);
        }

        //Feilet
        public virtual async Task FormFailed(AnSaKoModel anSaKoModel, string reason)
        {
            await SetReportedStatus(anSaKoModel, reason, FormStatus.Feilet);
        }

        private void ThrowIfStateIsInvalid(AnSaKoModel anSaKoModel, FormStatus status)
        {
            var currentState = anSaKoModel.FormDbData.Status;
            if (!StateHelper.CanTransitToState(currentState, status))
                throw new InvalidStateChangeException(currentState, status);
        }

        private async Task SetReportedStatus(AnSaKoModel anSaKoModel, string reason, FormStatus status)
        {
            using (Serilog.Context.LogContext.PushProperty("FtpbReferenceId", anSaKoModel.FtpbReferenceId))
            {
                _logger.LogDebug("Form is {FormStatus}", status);
                var f = await _formRepo.GetAsync(anSaKoModel.AnSaKoReferenceId);

                ThrowIfStateIsInvalid(anSaKoModel, status);

                f.FormDbData.Status = status;
                f.FormDbData.StatusDetails = reason;
                _logger.LogDebug("Updates database");
                f = await _formRepo.UpdateAsync(f);

                await HandleStatusChanged(f);
            }
        }

        protected async Task SetDistributionStatus(AnSaKoModel ansakoModel)
        {
            await _ftpbUnitOfWork.InitiateAsync(ansakoModel.FtpbReferenceId, ansakoModel.AnSaKoReferenceId);
            var df = await _ftpbUnitOfWork.DistributionForms.GetAsync(ansakoModel.FormDbData.FtpbDistributionFormsId);
            var status = FormStatusToAnSaKoProcessStatusMapper.GetStatus(ansakoModel.FormDbData.Status);
            if (df != null)
            {
                if (df.AnSaKoStatus == null) df.AnSaKoStatus = new AnSaKoStatus();
                if (df.AnSaKoStatus.AnSaKoProcessStatus != status)
                {
                    _logger.LogDebug("Updates Ftpb with status change");
                    df.AnSaKoStatus.AnSaKoProcessStatus = status;
                    df.AnSaKoStatus.StatusDetails = ansakoModel.FormDbData.StatusDetails;
                    if (string.IsNullOrEmpty(ansakoModel.FormDbData.StatusDetails))
                        _ftpbUnitOfWork.LogEntries.AddInfo($"Erklæringens status er endret til {status}");
                    else
                        _ftpbUnitOfWork.LogEntries.AddInfo($"Erklæringens status er endret til {status} - {ansakoModel.FormDbData.StatusDetails}");
                    _logger.LogInformation("Status set to {AnSaKoStatus}", status);
                    await _ftpbUnitOfWork.SaveAsync();
                }
            }
            else
                _logger.LogWarning("Unable to set status {AnSaKoStatus}. DistributionForm was null", status);
        }

        public abstract string DocumentTitle { get; }

        public virtual async Task<IEnumerable<SigningDocument>> GetDocumentsToSign(AnSaKoModel anSaKoModel)
        {
            var fileName = $"{anSaKoModel.FormDbData.FormType}.pdf";
            var blob = await _blobService.GetFileAsync(anSaKoModel.FormDbData.BlobStorageContainerName, fileName);

            if (blob == null)
                throw new FileNotFoundException($"Unable to find file {fileName} in container {anSaKoModel.FormDbData.BlobStorageContainerName}");
            //Get byte array
            byte[] fileBytes = blob.ContentStream.GetAsBytes();

            return new List<SigningDocument>(){ new SigningDocument(SigningFileType.MainDocument)
            {
                FileName = blob.FileName,
                Title = DocumentTitle,
                FileContent = fileBytes
            } };
        }
    }
}