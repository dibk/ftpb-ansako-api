﻿using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using DibkFtpb.Api.Client;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public class AttachmentService : IAttachmentService
    {
        private readonly ILogger<AttachmentService> _logger;
        private readonly FormRepo _formRepo;
        private readonly IBlobService _blobService;
        private readonly FtpbBlobService _ftpbBlobService;

        public AttachmentService(ILogger<AttachmentService> logger, FormRepo formRepo, IBlobService blobService, FtpbBlobService ftpbBlobService)
        {
            _logger = logger;
            _formRepo = formRepo;
            _blobService = blobService;
            _ftpbBlobService = ftpbBlobService;
        }

        public async Task AddAttachmentAsync(string ansakoReferenceId, string fileName, string mimeType, byte[] fileBytes)
        {
            var form = await _formRepo.GetAsync(ansakoReferenceId);

            if (!form.FormDbData.HasAttachment.HasValue || form.FormDbData.HasAttachment.Value)
                await DeleteAttachmentAsync(ansakoReferenceId);

            form.FormDbData.AttachmentBlobName = $"attachment/{fileName}";
            form.FormDbData.HasAttachment = true;
            form.FormDbData.AttachmentFileName = fileName;
            form.FormDbData.AttachmentMimeType = mimeType;
            form.FormDbData.AttachmentFtpbType = BlobMetadata.AttachmentTypes.PlanForUavhengigKontrol.ToString();

            var metadata = new Dictionary<string, string>()
            {
                { BlobMetadata.AttachmentTypeKey, BlobMetadata.AttachmentTypes.PlanForUavhengigKontrol },
                { BlobMetadata.BlobTypeKey, BlobMetadata.BlobTypes.Attachment }
            };

            Task addBlob = _blobService.AddOrUpdateFileAsync(form.FormDbData.BlobStorageContainerName, form.FormDbData.AttachmentBlobName, mimeType, fileBytes, metadata);
            Task updateDb = _formRepo.UpdateAsync(form);

            await Task.WhenAll(addBlob, updateDb).ConfigureAwait(false);
        }

        public async Task<AttachmentBlobItem> GetAttachmentAsync(string ansakoReferenceId)
        {
            return await GetAttachment(ansakoReferenceId);
        }

        private async Task<AttachmentBlobItem> GetAttachment(string ansakoReferenceId)
        {
            var form = await _formRepo.GetAsync(ansakoReferenceId);
            if (form.FormDbData.HasAttachment.HasValue && form.FormDbData.HasAttachment.Value)
            {
                BlobItem blobItem = await _blobService.GetFileAsync(form.FormDbData.BlobStorageContainerName, form.FormDbData.AttachmentBlobName);
                                
                if (blobItem == null)
                {
                    //Forsøker å hente fra Alfa-storage og flytt over
                    _logger.LogInformation("Unable to find attachment for {ansakoReferenceId}. Tries locating it in Alfa-storage", ansakoReferenceId);
                    var ftpbBlob = await _ftpbBlobService.GetFileAsync(form.FormDbData.BlobStorageContainerName, form.FormDbData.AttachmentBlobName);

                    if (ftpbBlob != null)
                    {
                        _logger.LogInformation("Found attachement {attachmentBlobName} in Alfa-storage, syncs it to AnSaKo-storage", form.FormDbData.AttachmentBlobName);
                        await _blobService.AddOrUpdateFileAsync(form.FormDbData.BlobStorageContainerName, form.FormDbData.AttachmentBlobName, ftpbBlob.MimeType, ftpbBlob.ContentStream, ftpbBlob.Metadata);
                        blobItem = await _blobService.GetFileAsync(form.FormDbData.BlobStorageContainerName, form.FormDbData.AttachmentBlobName);
                    }
                }

                var attachmentBlobItem = new AttachmentBlobItem(blobItem, blobItem.FileName)
                {
                    FileName = form.FormDbData.AttachmentBlobName,
                };
                attachmentBlobItem.AttachmentFileName = form.FormDbData.AttachmentFileName;

                return attachmentBlobItem;
            }
            else
                return null;
        }

        public async Task DeleteAttachmentAsync(string ansakoReferenceId)
        {
            var blob = await GetAttachmentAsync(ansakoReferenceId);
            if (blob != null)
            {
                var form = await _formRepo.GetAsync(ansakoReferenceId);
                form.FormDbData.HasAttachment = false;
                form.FormDbData.AttachmentFileName = string.Empty;
                form.FormDbData.AttachmentBlobName = string.Empty;
                form.FormDbData.AttachmentFtpbType = string.Empty;
                form.FormDbData.AttachmentMimeType = string.Empty;

                Task deleteBlob = _blobService.DeleteFileAsync(ansakoReferenceId, blob.FileName);
                Task updateForm = _formRepo.UpdateAsync(form);

                await Task.WhenAll(deleteBlob, updateForm).ConfigureAwait(false);
            }
        }
    }

    public class AttachmentBlobItem : BlobItem
    {
        public string AttachmentFileName { get; set; }

        public AttachmentBlobItem(BlobItem blob, string attachmentFileName) : base(blob.FileName, blob.MimeType, blob.Metadata, blob.ContentStream, blob.LastModified)
        {
            AttachmentFileName = attachmentFileName;
        }
    }
}