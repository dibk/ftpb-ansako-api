﻿using Dibk.Ftpb.Ansako.App.HttpClients.Email;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public class EmailService
    {
        private readonly ILogger<EmailService> _logger;
        private readonly EmailHttpClient _client;
        private readonly EmailMessagePublisher _emailMessagePublisher;
        private readonly bool _enqueueEmails;
        public EmailService(ILogger<EmailService> logger,
                            EmailMessagePublisher emailMessagePublisher,
                            EmailHttpClient client,
                            IConfiguration configuration)
        {
            _logger = logger;
            _client = client;

            var useEnqueingConfig = configuration["AnSaKo:Email:EnqueingEnabled"];

            if (!string.IsNullOrWhiteSpace(useEnqueingConfig))
                _enqueueEmails = bool.Parse(useEnqueingConfig);

            _emailMessagePublisher = emailMessagePublisher;
        }

        public async Task SendEmail(string anSaKoReferenceId, string ftpbReferenceId, string toAddress, string subject, string htmlBody, string plainTextBody)
        {
            await SendEmail(anSaKoReferenceId, ftpbReferenceId, toAddress, subject, htmlBody, plainTextBody, null, null, null);
        }

        public async Task SendEmail(string anSaKoReferenceId, string ftpbReferenceId, string toAddress, string subject, string htmlBody, string plainTextBody, byte[] attachmentContent, string fileName, string mimeType)
        {
            _logger.LogDebug("Creates email message for emailAPI for {EmailToAddress}", toAddress);
            var emailMessage = new EmailMessage();
            emailMessage.To = new List<EmailAddress>() { new EmailAddress() { Address = toAddress } };
            emailMessage.Subject = subject;
            emailMessage.HtmlBody = htmlBody;
            emailMessage.Body = plainTextBody;

            if (attachmentContent != null)
            {
                emailMessage.Attachments = new List<EmailAttachment>()
                { new EmailAttachment()
                    { Content = attachmentContent, FileName = fileName, MimeType = mimeType }
                };
            }

            _logger.LogDebug("Initiates sending of email for {EmailToAddress}", toAddress);
            try
            {
                if (_enqueueEmails)
                {
                    await _emailMessagePublisher.PublishAsync(new EmailQueueMessage()
                    {
                        AnSaKoReferenceId = anSaKoReferenceId,
                        FtpbReferenceId = ftpbReferenceId,
                        EmailMessage = emailMessage
                    });
                    _logger.LogDebug("Email enqueued for {EmailToAddress}", toAddress);
                    return;
                }
                else
                {
                    await _client.Post(emailMessage);
                    _logger.LogDebug("Email sent to {EmailToAddress}", toAddress);
                }
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, "An exception occured when sending email to {EmailToAddress}", toAddress);
                throw;
            }
        }
    }
}
