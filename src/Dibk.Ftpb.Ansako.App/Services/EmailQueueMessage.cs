﻿using Dibk.Ftpb.Ansako.App.HttpClients.Email;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public class EmailQueueMessage
    {
        public string AnSaKoReferenceId { get; set; }
        public string FtpbReferenceId { get; set; }
        public EmailMessage EmailMessage { get; set; }
    }
}
