﻿using Dibk.Ftpb.Ansako.Storage.Database.Models;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services.Admin
{
    public interface IServiceStateService
    {
        Task<ServiceStateDbModel> GetCurrentServiceState();
        Task<ServiceStateDbModel> SetCurrentServiceState(ServiceStateDbModel serviceStateDbModel);
    }
}