﻿using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using Dibk.Ftpb.Ansako.Storage.Repositories;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services.Admin
{
    public class AttachmentSyncService
    {
        private readonly ILogger<AttachmentSyncService> _logger;
        private readonly IBlobService _blobService;
        private readonly FormDbRepository _formDbRepository;

        public AttachmentSyncService(ILogger<AttachmentSyncService> logger, IBlobService blobService, FormDbRepository formDbRepository)
        {
            _logger = logger;
            _blobService = blobService;
            _formDbRepository = formDbRepository;
        }
        public async Task<List<string>> SyncDatabaseWithAttachments()
        {
            var result = new List<string>();
            var kontrollerklaeringer = _formDbRepository.GetAll().Where(p => p.FormType == Storage.Database.Models.ErklaeringType.Kontrollerklaering && !p.HasAttachment.HasValue).ToList();

            var metadata = new Dictionary<string, string>()
                                    {
                                        {BlobMetadata.AttachmentTypeKey, BlobMetadata.AttachmentTypes.PlanForUavhengigKontrol },
                                        {BlobMetadata.BlobTypeKey, BlobMetadata.BlobTypes.Attachment }
                                    };
            foreach (var k in kontrollerklaeringer)
            {
                var blobs = await _blobService.GetFilesByFilterAsync(k.BlobStorageContainerName, metadata);
                if (blobs != null && blobs.Count() > 0)
                {                    
                    BlobItem blobItem = blobs?.OrderByDescending(p => p.LastModified).First();

                    result.Add($"{k.AnSaKoReferenceId}/{k.FtpbReferenceId} attachment in blob {blobItem.FileName} synced to database");

                    k.HasAttachment = true;
                    k.AttachmentFileName = blobItem.FileName;
                    k.AttachmentMimeType = blobItem.MimeType;
                    k.AttachmentFtpbType = BlobMetadata.AttachmentTypes.PlanForUavhengigKontrol.ToString();
                    
                    _logger.LogInformation("Found attachment blob {AttachmentFileName} for {AnSaKoReferenceId}/{FtpbReferenceId} - syncs it to DB", blobItem.FileName, k.AnSaKoReferenceId, k.FtpbReferenceId);
                }
                else
                {
                    k.HasAttachment = false;
                    result.Add($"{k.AnSaKoReferenceId}/{k.FtpbReferenceId} no blob found");
                    _logger.LogInformation("Unable to locate attachment blob for {AnSaKoReferenceId}/{FtpbReferenceId}", k.AnSaKoReferenceId, k.FtpbReferenceId);
                }

                await _formDbRepository.UpdateAsync(k);
            }
            return result;
        }
    }
}
