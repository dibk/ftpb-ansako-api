﻿using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Dibk.Ftpb.Ansako.Storage.Repositories;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services.Admin
{
    public class ServiceStateService : IServiceStateService
    {
        private readonly ILogger<ServiceStateService> _logger;
        private readonly BaseDbRepository<ServiceStateDbModel> _repository;

        public ServiceStateService(ILogger<ServiceStateService> logger, BaseDbRepository<ServiceStateDbModel> repository)
        {
            _logger = logger;
            _repository = repository;
        }

        private readonly ServiceStateDbModel defaultOkState = new ServiceStateDbModel() { State = ServiceState.Online };

        public async Task<ServiceStateDbModel> GetCurrentServiceState()
        {
            var result = _repository.GetAll().OrderByDescending(p => p.StartDate).FirstOrDefault();
            if (result == null)
                return defaultOkState;

            return result;
        }
        public async Task<ServiceStateDbModel> SetCurrentServiceState(ServiceStateDbModel serviceStateDbModel)
        {            
            if (!serviceStateDbModel.StartDate.HasValue)
            {
                _logger.LogDebug("No start date provided; sets to now");
                serviceStateDbModel.StartDate = System.DateTime.Now;
            }

            if (!serviceStateDbModel.CreatedDate.HasValue)
                serviceStateDbModel.CreatedDate = System.DateTime.Now;

            _logger.LogInformation("Sets service state to: {ServiceState}", serviceStateDbModel.ToString());
            _logger.LogDebug("Persists service state to database");
            return await _repository.AddAsync(serviceStateDbModel, syncToELK: false);
        }
    }
}
