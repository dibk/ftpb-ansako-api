﻿using Dibk.Ftpb.Ansako.Storage.Database.Models;
using System;

namespace Dibk.Ftpb.Ansako.App.Services
{
    [Serializable]
    public class InvalidStateChangeException : Exception
    {
        public FormStatus CurrentState { get; set; }
        public FormStatus RequestedState { get; set; }
        public InvalidStateChangeException(FormStatus currentState, FormStatus requestedState) 
            : base($"Form cannot change state from '{currentState}' to {requestedState}")
        {
            CurrentState = currentState;
            RequestedState = requestedState;            
        }
    }
}