﻿using Dibk.Ftpb.Ansako.App.Models;
using Dibk.Ftpb.Ansako.App.Models.InternalApi;
using Dibk.Ftpb.Ansako.App.Repositories.Models;
using Dibk.Ftpb.Ansako.App.Services.Signing;
using Dibk.Ftpb.Signing.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public interface IFormService
    {
        Storage.Database.Models.ErklaeringType FormType { get; }
        Task<IFormDto> GetAsync(string ansakoReferenceId);
        Task<IFormDto> UpdateAsync(IFormDto formDto);
        Task<IFormDto> PrepareForSigning(IFormDto formDto);
        Task<string> CreateAsync(CreateFormPayload payload, string ansakoReferenceId);
        
        Task<IFormDto> RejectAsync(string ansakoReferenceId, string reason);
        ValidationResult IsValid(CreateFormPayload payload);
        Task<IEnumerable<SigningDocument>> GetDocumentsToSign(AnSaKoModel anSaKoModel);
        Task<FormSigningJobData> GetFormSigningJobData(string ansakoReferenceId);
        string DocumentTitle { get; }

        Task WithdrawForm(AnSaKoModel form, string reason);
        Task SignedManually(AnSaKoModel form, string reason);
        Task ReportedCompleted(AnSaKoModel form, string reason);
        Task FormFailed(AnSaKoModel form, string reason);
    }
}
