﻿using Dibk.Ftpb.Ansako.Storage.Database.Models;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public class AnsakoToFtpb
    {
        public static string GetFtpbDistributionType(ErklaeringType erklaeringType)
        {
            var ftpbFormType = string.Empty;
            switch (erklaeringType)
            {
                case ErklaeringType.Ansvarsrett:
                    ftpbFormType = "Erklæring om ansvarsrett";
                    break;
                case ErklaeringType.Samsvarserklaering:
                    ftpbFormType = "Samsvarserklaering";
                    break;
                case ErklaeringType.Kontrollerklaering:
                    ftpbFormType = "Kontrollerklæring";
                    break;
                default:
                    break;
            }
            return ftpbFormType;
        }

        public static string GetFtpbFormTypeName(ErklaeringType erklaeringType)
        {
            var formTypeName = string.Empty;
            switch (erklaeringType)
            {
                case ErklaeringType.Ansvarsrett:
                    formTypeName = "Distribusjon av ansvarsrett";
                    break;
                case ErklaeringType.Samsvarserklaering:
                    formTypeName = "Distribusjon av samsvarserklæring";
                    break;
                case ErklaeringType.Kontrollerklaering:
                    formTypeName = "Distribusjon av kontrollerklæring";
                    break;
                default:
                    break;
            }
            return formTypeName;
        }
    }
}
