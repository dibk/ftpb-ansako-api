﻿using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.App.Repositories.Models;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public class InternalApiFormService
    {
        private readonly ILogger<InternalApiFormService> _logger;
        private readonly FormRepo _formRepo;
        public InternalApiFormService(ILogger<InternalApiFormService> logger, FormRepo formRepo)
        {
            _logger = logger;
            _formRepo = formRepo;
        }

        public async Task<AnSaKoModel> GetAsync(string ansakoReferenceId)
        {
            return await _formRepo.GetAsync(ansakoReferenceId);
        }

        public async Task<AnSaKoModel> Update(AnSaKoModel formModel)
        {
            return await _formRepo.UpdateAsync(formModel);
        }
    }
}