﻿using Dibk.Ftpb.Ansako.App.Mappers;
using Dibk.Ftpb.Ansako.App.Models;
using Dibk.Ftpb.Ansako.App.Models.InternalApi;
using Dibk.Ftpb.Ansako.App.Repositories;
using Dibk.Ftpb.Ansako.App.Repositories.Models;
using Dibk.Ftpb.Ansako.App.Services.Signing;
using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Dibk.Ftpb.Api.Client.Interfaces;
using Microsoft.Extensions.Logging;
using no.kxml.skjema.dibk.samsvarserklaeringAnsako;
using System;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public class SamsvarserklaeringService : FormServiceBase, IFormService
    {
        public ErklaeringType FormType => ErklaeringType.Samsvarserklaering;

        public override string DocumentTitle
        { get { return "Samsvarserklæring"; } }

        public static string SignedPdfFileName => "Samsvarserklaering.pdf";

        private readonly ILogger<SamsvarserklaeringService> _logger;
        private readonly FormRepo _formRepo;

        public SamsvarserklaeringService(ILogger<SamsvarserklaeringService> logger, FormRepo formRepo, IFtpbUnitOfWork ftpbUnitOfWork, IBlobService blobService)
            : base(logger, ftpbUnitOfWork, blobService, formRepo)
        {
            _logger = logger;
            _formRepo = formRepo;
        }

        public async Task<IFormDto> GetAsync(string ansakoReferenceId)
        {
            return MapToDto(await Get(ansakoReferenceId));
        }

        protected override SamsvarserklaeringDto MapToDto(AnSaKoModel model)
        {
            if (string.IsNullOrEmpty(model.FormXmlData))
                throw new Exception($"FormXmlData is null or empty. Status: {model.FormDbData.Status.ToString()} DateDeleted: {model.FormDbData.DateDeleted}");

            SamsvarserklaeringModel samsvarserklaeringModel = SamsvarserklaeringV1ModelMapper.Map(model.FormXmlData);

            var samsvarserklaeringDto = new SamsvarserklaeringDto()
            {
                AnSaKoReferenceId = model.AnSaKoReferenceId,
                Status = model.FormDbData.Status,
                StatusDetails = model.FormDbData.StatusDetails,
                FormData = samsvarserklaeringModel,
                SigningJobStatus = model.FormDbData.SigneringsJobStatus,
                Signeringsfrist = model.FormDbData.SigneringsFrist,
                SignertDato = model.FormDbData.SignertDato,
                FtpbReferenceId = model.FtpbReferenceId
            };
            return samsvarserklaeringDto;
        }

        protected override void MapFromDto(IFormDto dto, AnSaKoModel model)
        {
            SamsvarserklaeringV1ToAnSaKoModelMapper.MapFromDto(dto as SamsvarserklaeringDto, ref model);
        }

        public async Task<string> CreateAsync(CreateFormPayload payload, string ansakoReferenceId)
        {
            _logger.LogDebug("Deserializes payload");
            var samsvarserklaering = XmlSerialization.DeserializeFromString<SamsvarserklaeringType>(payload.FormDataXml);
            _logger.LogDebug("Clears values for signatur");
            samsvarserklaering.signatur = null;
            samsvarserklaering.erklaeringProsjektering = null;
            samsvarserklaering.erklaeringProsjekteringSpecified = false;
            samsvarserklaering.erklaeringUtfoerelse = null;
            samsvarserklaering.erklaeringUtfoerelseSpecified = false;

            if (samsvarserklaering.ansvarsrett.funksjon.kodeverdi.Equals("PRO", StringComparison.OrdinalIgnoreCase))
            {
                if (samsvarserklaering.ansvarsrett.prosjekterende == null)
                    samsvarserklaering.ansvarsrett.prosjekterende = new ProsjekterendeType();

                samsvarserklaering.ansvarsrett.prosjekterende.okForFerdigattest = samsvarserklaering.ansvarsrett.prosjekterende.okForFerdigattestSpecified == true
                    ? samsvarserklaering.ansvarsrett.prosjekterende.okForFerdigattest : false;
                samsvarserklaering.ansvarsrett.prosjekterende.okForFerdigattestSpecified = true;

                samsvarserklaering.ansvarsrett.prosjekterende.okForIgangsetting = samsvarserklaering.ansvarsrett.prosjekterende.okForIgangsettingSpecified == true
                    ? samsvarserklaering.ansvarsrett.prosjekterende.okForIgangsetting : false;
                samsvarserklaering.ansvarsrett.prosjekterende.okForIgangsettingSpecified = true;

                samsvarserklaering.ansvarsrett.prosjekterende.okForMidlertidigBrukstillatelse = samsvarserklaering.ansvarsrett.prosjekterende.okForMidlertidigBrukstillatelseSpecified == true
                    ? samsvarserklaering.ansvarsrett.prosjekterende.okForMidlertidigBrukstillatelse : false;
                samsvarserklaering.ansvarsrett.prosjekterende.okForMidlertidigBrukstillatelseSpecified = true;

                samsvarserklaering.ansvarsrett.prosjekterende.okForRammetillatelse = samsvarserklaering.ansvarsrett.prosjekterende.okForRammetillatelseSpecified == true
                    ? samsvarserklaering.ansvarsrett.prosjekterende.okForRammetillatelse : false;
                samsvarserklaering.ansvarsrett.prosjekterende.okForRammetillatelseSpecified = true;

                samsvarserklaering.ansvarsrett.utfoerende = null;
            }

            if (samsvarserklaering.ansvarsrett.funksjon.kodeverdi.Equals("UTF", StringComparison.OrdinalIgnoreCase))
            {
                if (samsvarserklaering.ansvarsrett.utfoerende == null)
                    samsvarserklaering.ansvarsrett.utfoerende = new UtfoerendeType();

                samsvarserklaering.ansvarsrett.utfoerende.harTilstrekkeligSikkerhet = samsvarserklaering.ansvarsrett.utfoerende.harTilstrekkeligSikkerhetSpecified == true
                    ? samsvarserklaering.ansvarsrett.utfoerende.harTilstrekkeligSikkerhet : false;
                samsvarserklaering.ansvarsrett.utfoerende.harTilstrekkeligSikkerhetSpecified = true;

                samsvarserklaering.ansvarsrett.utfoerende.okForFerdigattest = samsvarserklaering.ansvarsrett.utfoerende.okForFerdigattestSpecified == true
                    ? samsvarserklaering.ansvarsrett.utfoerende.okForFerdigattest : false;
                samsvarserklaering.ansvarsrett.utfoerende.okForFerdigattestSpecified = true;

                samsvarserklaering.ansvarsrett.utfoerende.okMidlertidigBrukstillatelse = samsvarserklaering.ansvarsrett.utfoerende.okMidlertidigBrukstillatelseSpecified == true
                    ? samsvarserklaering.ansvarsrett.utfoerende.okMidlertidigBrukstillatelse : false;
                samsvarserklaering.ansvarsrett.utfoerende.okMidlertidigBrukstillatelseSpecified = true;

                if (samsvarserklaering.ansvarsrett.utfoerende.midlertidigBrukstillatelseGjenstaaende == null)
                    samsvarserklaering.ansvarsrett.utfoerende.midlertidigBrukstillatelseGjenstaaende = new DelsoeknadMidlertidigBrukstillatelseType();

                samsvarserklaering.ansvarsrett.prosjekterende = null;
            }

            var ansakoModel = new AnSaKoModel(ansakoReferenceId);
            SamsvarserklaeringV1ToAnSaKoModelMapper.MapNewModelFromXml(samsvarserklaering, ref ansakoModel);
            ansakoModel.FormDbData.FtpbDistributionFormsId = Guid.NewGuid();
            ansakoModel.FormDbData.Status = FormStatus.Opprettet;
            ansakoModel.FormDbData.FormType = ErklaeringType.Samsvarserklaering;
            ansakoModel.FormDbData.SigneringsFrist = payload.FormMetadata?.Signeringsfrist;
            ansakoModel.FormDbData.FormDataFileName = "Samsvarserklaering.xml";

            ansakoModel = await _formRepo.InsertAsync(ansakoModel);

            await AddToFtpb(ansakoModel);

            return ansakoModel.FtpbReferenceId;
        }

        public async Task<IFormDto> PrepareForSigning(IFormDto formDto)
        {
            var form = formDto as SamsvarserklaeringDto;
            if (form.FormData.AnsvarsrettUtfoerende != null)
            {
                if (form.FormData.AnsvarsrettUtfoerende.OkForFerdigattest.HasValue
                    && form.FormData.AnsvarsrettUtfoerende.OkForFerdigattest == true)
                {
                    form.FormData.AnsvarsrettUtfoerende.MidlertidigBrukstillatelseGjennstaaendeInnenfor = null;
                    form.FormData.AnsvarsrettUtfoerende.MidlertidigBrukstillatelseGjennstaaendeUtenfor = null;
                    form.FormData.AnsvarsrettUtfoerende.HarTilstrekkeligSikkerhet = null;

                    form.FormData.AnsvarsrettUtfoerende.TypeArbeider = null;
                    form.FormData.AnsvarsrettUtfoerende.UtfoertInnen = null;
                }

                if (form.FormData.AnsvarsrettUtfoerende.OkForMidlertidigBrukstillatelse.HasValue
                    && form.FormData.AnsvarsrettUtfoerende.OkForMidlertidigBrukstillatelse == true)
                {
                    if (form.FormData.AnsvarsrettUtfoerende.HarTilstrekkeligSikkerhet.HasValue &&
                        form.FormData.AnsvarsrettUtfoerende.HarTilstrekkeligSikkerhet == true)
                    {
                        form.FormData.AnsvarsrettUtfoerende.TypeArbeider = null;
                        form.FormData.AnsvarsrettUtfoerende.UtfoertInnen = null;
                    }
                }

                return await base.UpdateAsync(form);
            }

            return await base.UpdateAsync(form);
        }

        public async Task<IFormDto> RejectAsync(string ansakoReferenceId, string reason)
        {
            await base.RejectForm(ansakoReferenceId, reason);

            return await this.GetAsync(ansakoReferenceId);
        }

        public ValidationResult IsValid(CreateFormPayload payload)
        {
            var valRes = new ValidationResult() { IsValid = true, ValidationMessage = "Ok" };
            try
            {
                XmlSerialization.DeserializeFromString<SamsvarserklaeringType>(payload.FormDataXml);
            }
            catch (Exception ex)
            {
                var validationMessage = $"Payload with given dataformatid and -version {payload.DataFormatId}/{payload.DataFormatVersion} failed.";
                _logger.LogError(ex, validationMessage);
                valRes.IsValid = false;
                valRes.ValidationMessage = $"{validationMessage} - {ex.Message}";
            }
            return valRes;
        }

        public async Task<FormSigningJobData> GetFormSigningJobData(string ansakoReferenceId)
        {
            var samsvarserklaering = MapToDto(await Get(ansakoReferenceId));

            return new FormSigningJobData()
            {
                SignerName = samsvarserklaering.FormData.AnsvarligForetak.KontaktpersonNavn,
                SignerEmail = samsvarserklaering.FormData.AnsvarligForetak.Epost,
                SigningJobTitle = $"Samsvarserklæring fra {samsvarserklaering.FormData.AnsvarligForetak.Navn}"
            };
        }
    }
}