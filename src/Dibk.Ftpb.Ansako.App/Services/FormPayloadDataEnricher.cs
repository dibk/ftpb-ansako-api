﻿using Dibk.Ftpb.Ansako.App.Models;

namespace Dibk.Ftpb.Ansako.App.Services
{
    public static class FormPayloadDataEnricher
    {
        public static void SetSigningDeadlineIfMissing(CreateFormPayload payload, int daysToSigningDeadline)
        {
            if (payload.FormMetadata == null) payload.FormMetadata = new CreateFormMetadata();
            if (payload.FormMetadata.Signeringsfrist == null || !payload.FormMetadata.Signeringsfrist.HasValue)
                payload.FormMetadata.Signeringsfrist = System.DateTime.Today.AddDays(daysToSigningDeadline).AddHours(23).AddMinutes(59).AddSeconds(59);
            else
                payload.FormMetadata.Signeringsfrist = payload.FormMetadata.Signeringsfrist.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59);

        }
    }
}
