﻿using System.IO;
using System.Xml.Serialization;

namespace Dibk.Ftpb.Ansako.App
{
    public class XmlSerialization
    {
        public static T DeserializeFromString<T>(string objectData)
        {
            using var reader = new StringReader(objectData);
            var xmlDeserializer = new XmlSerializer(typeof(T));
            var ansvarsrett = xmlDeserializer.Deserialize(reader);

            return (T)ansvarsrett;
        }

        public static string SerializeObject<T>(object data)
        {
            using var writer = new StringWriter();
            var xmlDeserializer = new XmlSerializer(typeof(T));
            xmlDeserializer.Serialize(writer, data);

            var result = writer.ToString();
            return result;
        }
    }
}
