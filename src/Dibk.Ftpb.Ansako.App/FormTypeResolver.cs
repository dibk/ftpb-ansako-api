﻿using System;
using System.Linq;
using System.Reflection;
using System.Xml;

namespace Dibk.Ftpb.Ansako.App
{
    public class FormTypeResolver
    {
        public static Type GetFormType(string xmlString, string formType)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);

            var ns = xmlDoc.DocumentElement.NamespaceURI;

            var ft =
                Assembly.GetExecutingAssembly().GetTypes().Where(p =>
                p.GetCustomAttribute<System.Xml.Serialization.XmlRootAttribute>() != null &&
                p.GetCustomAttribute<System.Xml.Serialization.XmlRootAttribute>().ElementName.Equals(formType, StringComparison.OrdinalIgnoreCase) &&
                p.GetCustomAttribute<System.Xml.Serialization.XmlRootAttribute>().Namespace.Equals(ns, StringComparison.OrdinalIgnoreCase)
                    ).FirstOrDefault();

            return ft;
        }
    }
}
