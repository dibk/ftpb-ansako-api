﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.HttpClients.Pdf
{
    public class PdfHttpClient
    {
        private readonly PdfSettings _settings;
        private readonly ILogger<PdfHttpClient> _logger;
        private readonly HttpClient _httpClient;

        public PdfHttpClient(
                                            HttpClient httpClient,
                                            IOptions<PdfSettings> settings,
                                            ILogger<PdfHttpClient> logger)
        {
            _httpClient = httpClient;
            _settings = settings.Value;
            _logger = logger;
        }

        public async Task<MemoryStream> GeneratePdfDocumentAsync(string htmlString)
        {
            using var request = new HttpRequestMessage(HttpMethod.Post, _settings.ApiUrls.FromHtmlString)
            {
                Content = CreateFormData(htmlString)
            };

            return await PostRequestAsync(request);
        }

        public async Task<MemoryStream> GeneratePdfDocumentAsync(PdfInputData inputData)
        {
            using var request = new HttpRequestMessage(HttpMethod.Post, _settings.ApiUrls.FromImage)
            {
                Content = CreateFormData(inputData)
            };

            return await PostRequestAsync(request);
        }

        private async Task<MemoryStream> PostRequestAsync(HttpRequestMessage request)
        {
            try
            {
                request.Headers.Add("X-API-KEY", _settings.ApiKey);

                if (!string.IsNullOrWhiteSpace(_settings.UserAgent))
                    request.Headers.Add("UserAgent", _settings.UserAgent);

                var response = await _httpClient.SendAsync(request).ConfigureAwait(false); ;

                response.EnsureSuccessStatusCode();

                using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false); ;
                var memoryStream = new MemoryStream();
                await responseStream.CopyToAsync(memoryStream);
                memoryStream.Position = 0;

                return memoryStream;
            }
            catch (HttpRequestException hre)
            {
                _logger.LogError(hre, "HTTP Status Code {StatusCode}: Could not generate PDF", hre.StatusCode);
                throw;
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "An error occured for generation of PDF");
                throw;
            }
        }

        private MultipartFormDataContent CreateFormData(string htmlString)
        {
            var formData = new MultipartFormDataContent
            {
                { new StringContent(htmlString), "htmlString" },
                { new StringContent(JObject.FromObject(_settings.Paper).ToString(), Encoding.UTF8, "application/json"), "options" }
            };

            formData.Headers.ContentType.MediaType = "multipart/form-data";

            return formData;
        }

        private MultipartFormDataContent CreateFormData(PdfInputData inputData)
        {
            var formData = new MultipartFormDataContent
            {
                { CreateStreamContent(inputData.Content, inputData.MimeType), "image", inputData.FileName },
                { new StringContent(JObject.FromObject(_settings.Paper).ToString(), Encoding.UTF8, "application/json"), "options" }
            };

            if (inputData.Title != null)
                formData.Add(new StringContent(inputData.Title), "title");

            formData.Headers.ContentType.MediaType = "multipart/form-data";

            return formData;
        }

        private static StreamContent CreateStreamContent(Stream stream, string mimeType)
        {
            var streamContent = new StreamContent(stream);

            streamContent.Headers.ContentType = new MediaTypeHeaderValue(mimeType);

            return streamContent;
        }
    }
}