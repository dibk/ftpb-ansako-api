﻿namespace Dibk.Ftpb.Ansako.App.HttpClients.Pdf
{
    public class PdfSettings
    {
        public static string SectionName => "Pdf";
        public string ApiKey { get; set; }
        public ApiUrlsSettings ApiUrls { get; set; } = new ApiUrlsSettings();
        public string UserAgent { get; set; }
        public PaperSettings Paper { get; set; } = new PaperSettings();

        public class PaperSettings
        {
            public string Format { get; set; }
            public string PaperWidth { get; set; }
            public string PaperHeight { get; set; }
            public string MarginTop { get; set; }
            public string MarginRight { get; set; }
            public string MarginBottom { get; set; }
            public string MarginLeft { get; set; }
            public bool? PrintBackground { get; set; }
        }

        public class ApiUrlsSettings
        {
            public string FromHtmlString { get; set; }
            public string FromImage { get; set; }
        }
    }
}
