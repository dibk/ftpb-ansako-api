﻿using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.HttpClients.Email
{
    public class EmailHttpClient
    {
        private HttpClient _client;
        private readonly ILogger<EmailHttpClient> _logger;

        public EmailHttpClient(HttpClient httpClient, ILogger<EmailHttpClient> logger)
        {
            _logger = logger;
            _client = httpClient;
        }

        public async Task Post(EmailMessage emailMessage)
        {
            try
            {
                var stringPayload = Newtonsoft.Json.JsonConvert.SerializeObject(emailMessage);
                HttpContent requestContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");

                var response = await _client.PostAsync("api/email", requestContent).ConfigureAwait(false); ;
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    _logger.LogInformation("Email sent to {emailaddress}", emailMessage.To.First().Address);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred when sending email to emailApi");

                throw;
            }
        }
    }
}