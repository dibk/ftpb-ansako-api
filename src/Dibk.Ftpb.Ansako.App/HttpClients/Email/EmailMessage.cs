﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dibk.Ftpb.Ansako.App.HttpClients.Email
{
    public class EmailMessage
    {
        public IEnumerable<EmailAddress> To { get; set; }
        public IEnumerable<EmailAddress> CC { get; set; }
        public IEnumerable<EmailAddress> Bcc { get; set; }
        public EmailAddress From { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string HtmlBody { get; set; }
        public IEnumerable<EmailAttachment> Attachments { get; set; }
    }
    public class EmailAddress
    {
        [EmailAddress]
        public string Address { get; set; }

        public string DisplayName { get; set; }
    }

    public class EmailAttachment
    {
        public string FileName { get; set; }
        public byte[] Content { get; set; }
        public string MimeType { get; set; }
    }
}