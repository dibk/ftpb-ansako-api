﻿using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using Dibk.Ftpb.Signing;
using Dibk.Ftpb.Signing.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App
{
    public class TestSigner : ISigner
    {
        private readonly IBlobService blobService;

        public TestSigner(IBlobService blobService)
        {
            this.blobService = blobService;
        }
        public async Task<SigningJobResult> CreateSigningJob(SigningJobRequest signingJob)
        {
            var jobid = $"testsigning-{Guid.NewGuid()}";
            var filename = $"{jobid}.pdf";
            var doc = signingJob.SigningDocuments.First(p => p.FileType == SigningFileType.MainDocument);
            var documentUrl = await blobService.AddOrUpdateFileAsync("signingTestStorage", filename, "application.pdf", new MemoryStream(doc.FileContent));

            var url = "https://arkitektum.github.io/dibk.ftpb.broop.dummySigning/";

            return new SigningJobResult()
            {
                Id = jobid,
                SigningProvider = "TestSigner",
                Status = SigningStatus.Created,
                SigningUrl = url
            };
        }

        public async Task<SignedDocument> GetSignedDocument(SigningJobStatusRequest request)
        {
            var doc = await blobService.GetFileAsync("signingTestStorage", $"{request.JobId}.pdf");

            return new SignedDocument()
            {
                FileContent = doc.ContentStream.GetAsBytes(),
                SignedDate = DateTime.Now
            };
        }

        public string GetSigningProviderName()
        { return "TestSigner"; }

        public ISigningMetadataBuilder GetSigningMetadataBuilder()
        { return new TestSigningMetadataBuilder(); }

        public Task<SigningStatus> GetStatusForSigningJob(SigningJobStatusRequest request)
        { return Task.FromResult(SigningStatus.CompletedSuccessfully); }

        public Task<string> RequestNewRedirectUri(string redirectUri)
        { return Task.FromResult(redirectUri); }
    }

    public class TestSigningMetadataBuilder : ISigningMetadataBuilder
    {
        public string Build(string metaData, params KeyValuePair<string, string>[] values)
        {
            return string.Empty;
        }
    }
}
