﻿using Dibk.Ftpb.Ansako.App.Repositories.Models;
using Dibk.Ftpb.Ansako.App.Services;
using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Dibk.Ftpb.Ansako.Storage.Repositories;
using DibkFtpb.Api.Client;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.App.Repositories
{
    public class FormRepo
    {
        private readonly ILogger<FormRepo> _logger;
        private readonly IBlobService _blobService;
        private readonly FormDbRepository _formRepository;
        private readonly FtpbBlobService _ftpbBlobService;

        public FormRepo(ILogger<FormRepo> logger,
                        IBlobService blobService,
                        FormDbRepository formRepository,
                        FtpbBlobService ftpbBlobService)
        {
            _logger = logger;
            _blobService = blobService;
            _formRepository = formRepository;
            _ftpbBlobService = ftpbBlobService;
        }

        /// <summary>
        /// Gets form with data
        /// </summary>
        /// <param name="ansakoReferenceId"></param>
        /// <returns>AnSaKoModel</returns>
        /// <exception cref="FormNotFoundException"></exception>
        public async Task<AnSaKoModel> GetAsync(string ansakoReferenceId)
        {
            var formDbModel = _formRepository.GetAll().Where(x => x.AnSaKoReferenceId.Equals(ansakoReferenceId)).Include("Ansvarsomraader").FirstOrDefault();
            if (formDbModel == null)
                throw new FormNotFoundException($"Unable to find form with id: {ansakoReferenceId}");

            var serializedFormData = string.Empty;

            if (!formDbModel.DateDeleted.HasValue) //If form is deleted, no need to fetch data from blob
            {
                var blobItem = await _blobService.GetFileAsync(formDbModel.BlobStorageContainerName, formDbModel.FormDataFileName);

                //If item is null; it might exist in old storage
                // tries to fetch it and copy it
                if (blobItem == null)
                {
                    _logger.LogInformation("Couldn't find {ansakoReferenceId} blob. Tries to locate it in ftpbStorage", ansakoReferenceId);
                    var ftpbBlobItem = await _ftpbBlobService.GetFileAsync(formDbModel.BlobStorageContainerName, formDbModel.FormDataFileName);

                    if (ftpbBlobItem != null)
                    {
                        _logger.LogInformation("Found {ContainerName}/{Filename} in ftpbStorage.", formDbModel.BlobStorageContainerName, ftpbBlobItem.FileName);
                        //get origin as well
                        var originFtpbBlobItem = await _ftpbBlobService.GetFileAsync(formDbModel.BlobStorageContainerName, $"origin-{formDbModel.FormDataFileName}");
                        if (originFtpbBlobItem != null)
                        {
                            _logger.LogInformation("Found backup file in ftpbStorage. Starts copying it to new location {ContainerName}/{Filename}", formDbModel.BlobStorageContainerName, originFtpbBlobItem.FileName);
                            await _blobService.AddOrUpdateFileAsync(formDbModel.BlobStorageContainerName, originFtpbBlobItem.FileName, originFtpbBlobItem.MimeType, originFtpbBlobItem.ContentStream, originFtpbBlobItem.Metadata);
                            _logger.LogInformation("{ContainerName}/{Filename} successfully copied", formDbModel.BlobStorageContainerName, originFtpbBlobItem.FileName);
                        }

                        _logger.LogInformation("Starts copying source file to new location {ContainerName}/{Filename}", formDbModel.BlobStorageContainerName, ftpbBlobItem.FileName);
                        await _blobService.AddOrUpdateFileAsync(formDbModel.BlobStorageContainerName, formDbModel.FormDataFileName, ftpbBlobItem.MimeType, ftpbBlobItem.ContentStream, ftpbBlobItem.Metadata);
                        _logger.LogInformation("{ContainerName}/{Filename} successfully copied", formDbModel.BlobStorageContainerName, ftpbBlobItem.FileName);

                        //Reloads
                        _logger.LogInformation("Reloads file after copying");
                        blobItem = await _blobService.GetFileAsync(formDbModel.BlobStorageContainerName, formDbModel.FormDataFileName);
                    }
                }

                if (blobItem != null)
                    using (var reader = new StreamReader(blobItem.ContentStream, Encoding.UTF8))
                        serializedFormData = reader.ReadToEnd();
            }

            return new AnSaKoModel() { FormDbData = formDbModel, FormXmlData = serializedFormData };
        }

        /// <summary>
        /// Gets form with data
        /// </summary>
        /// <param name="ftpbReferenceId"></param>
        /// <returns>AnSaKoModel</returns>
        /// <exception cref="FormNotFoundException"></exception>
        public string GetAnSaKoReferenceId(string ftpbReferenceId)
        {
            var formDbModel = _formRepository.GetAll().Where(x => x.FtpbReferenceId.Equals(ftpbReferenceId)).FirstOrDefault();
            if (formDbModel == null)
                throw new FormNotFoundException($"Unable to find form with id: {ftpbReferenceId}");
            return formDbModel.AnSaKoReferenceId;
        }

        /// <summary>
        /// Updates form in database and data in blob storage
        /// </summary>
        /// <param name="formModel"></param>
        /// <returns>AnSaKoModel</returns>
        /// <exception cref="FormNotFoundException"></exception>
        public async Task<AnSaKoModel> UpdateAsync(AnSaKoModel formModel)
        {
            try
            {
                Task databaseUpdate = _formRepository.UpdateFormAsync(formModel.FormDbData);

                var blobBytes = Encoding.UTF8.GetBytes(formModel.FormXmlData);
                Task blobUpdate = _blobService.AddOrUpdateFileAsync(formModel.FormDbData.BlobStorageContainerName, formModel.FormDbData.FormDataFileName, "application/xml", blobBytes);
                Task syncToElk = SyncToELK(formModel);
                await Task.WhenAll(databaseUpdate, blobUpdate, syncToElk).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while updating blob content for form {AnSaKoReferenceId}", formModel.FormDbData.AnSaKoReferenceId);
                throw;
            }

            return formModel;
        }

        public async Task<AnSaKoModel> InsertAsync(AnSaKoModel formModel)
        {
            if (string.IsNullOrEmpty(formModel.FormDbData.AnSaKoReferenceId))
                throw new ArgumentNullException("Form.AnSaKoReferenceId cannot be null");

            if (string.IsNullOrEmpty(formModel.FormXmlData))
                throw new ArgumentNullException("FormXmlData cannot be null");

            try
            {
                formModel.FormDbData.BlobStorageContainerName = formModel.FormDbData.AnSaKoReferenceId;
                if (string.IsNullOrEmpty(formModel.FormDbData.FormDataFileName))
                    formModel.FormDbData.FormDataFileName = $"{formModel.FormDbData.DataFormatId}-{formModel.FormDbData.DataFormatVersion}_FormData.xml";

                var formDbModel = await _formRepository.AddAsync(formModel.FormDbData);

                var blobBytes = Encoding.UTF8.GetBytes(formModel.FormXmlData);

                _logger.LogDebug("Saves editable xml to blobstorage");

                Task addEditableXmlFile = _blobService.AddOrUpdateFileAsync(formDbModel.BlobStorageContainerName, formDbModel.FormDataFileName, "application/xml", blobBytes, new Dictionary<string, string>
                    {
                        { BlobMetadata.BlobTypeKey,BlobMetadata.BlobTypes.FormXmlDataEditable }
                    });

                _logger.LogDebug("Saves original xml to blobstorage");
                Task addBackupXmlFile = _blobService.AddOrUpdateFileAsync(formDbModel.BlobStorageContainerName, $"origin-{formDbModel.FormDataFileName}", "application/xml", blobBytes,
                  new Dictionary<string, string>
                    {
                            { BlobMetadata.BlobTypeKey,BlobMetadata.BlobTypes.FormXmlDataOriginal }
                    });

                await Task.WhenAll(addEditableXmlFile, addBackupXmlFile).ConfigureAwait(false);

                _logger.LogDebug("Database row and blob stored");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while updating database row or blob for form {AnSaKoReferenceId}", formModel.FormDbData.AnSaKoReferenceId);
                throw;
            }

            return await GetAsync(formModel.FormDbData.AnSaKoReferenceId);
        }

        public IEnumerable<AnsvarsomraadeDbModel> GetAnsvarsomraade(string ansakoReferenceId)
        {
            var ansvarsomraader = _formRepository.DbContext.Set<AnsvarsomraadeDbModel>().Where(p => p.AnSaKoReferenceId.Equals(ansakoReferenceId)).ToList();

            return ansvarsomraader;
        }

        public async Task UpdateAnsvarsomraadeAsync(AnsvarsomraadeDbModel ansvarsomraade)
        {
            var entry = _formRepository.DbContext.Entry<AnsvarsomraadeDbModel>(ansvarsomraade);
            entry.CurrentValues.SetValues(ansvarsomraade);
            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            await _formRepository.DbContext.SaveChangesAsync();
        }

        public async Task<string> GetNextFtpbReferenceId()
        {
            return await _formRepository.GetNextFtpbReferenceId();
        }

        public async Task SyncToELK(AnSaKoModel model)
        {
            await _formRepository.SyncToElasticSearch(model.FormDbData);
        }
    }

    public class FormNotFoundException : Exception
    {
        public FormNotFoundException(string message) : base(message)
        {
        }
    }
}