﻿using Dibk.Ftpb.Ansako.Storage.Database.Models;

namespace Dibk.Ftpb.Ansako.App.Repositories.Models
{
    public class AnSaKoModel
    {
        public AnSaKoModel() { }
        public AnSaKoModel(string ansakoReferenceId)
        {
            FormDbData = new FormDbModel();
            FormDbData.AnSaKoReferenceId = ansakoReferenceId;
            FormDbData.DateCreated = System.DateTime.Now;
            FormDbData.DateUpdated = System.DateTime.Now;
        }
        public FormDbModel FormDbData { get; set; }
        public string FormXmlData { get; set; }
        public string AnSaKoReferenceId { get => FormDbData.AnSaKoReferenceId; }
        public string FtpbReferenceId { get => FormDbData.FtpbReferenceId; }
        public string Hovedinnsendingsnummer { get; set; }
    }
}
