﻿using Dibk.Ftpb.Ansako.Storage.Database.Models;
using System;

namespace Dibk.Ftpb.Ansako.App
{
    public static class FormTypeHelper
    {
        public static ErklaeringType GetErklaeringType(string dataFormatId, string dataformatversion)
        {
            if (dataFormatId.Equals("10000"))
            {
                return ErklaeringType.Ansvarsrett;
            }
            else if (dataFormatId.Equals("10001"))
            {
                return ErklaeringType.Samsvarserklaering;
            }
            else if (dataFormatId.Equals("10002"))
            {
                return ErklaeringType.Kontrollerklaering;
            }

            throw new ArgumentOutOfRangeException($"Unable to identiyfy ErklaeringType for {dataFormatId}/{dataformatversion}");
        }
    }
}
