﻿namespace Dibk.Ftpb.Ansako.App
{
    public class AnSaKoSystemSettings
    {
        public static string ConfigSection = "AnSaKo";
        public string FrontendUrl { get; set; }
        public string BackendUrl { get; set; }
        public string SignerType { get; set; }
        public string FtpbPublicHost { get; set; }
        public string SamsvarKontrollFeltIkkeRedigerbare { get; set; }
    }
}
