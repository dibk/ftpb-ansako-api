﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by xsd, Version=4.8.3928.0.
// 
namespace no.kxml.skjema.dibk.kontrollerklaeringAnsako {
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0")]
    [System.Xml.Serialization.XmlRootAttribute("Kode", Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0", IsNullable=false)]
    public partial class KodeType {
        
        private string kodeverdiField;
        
        private string kodebeskrivelseField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string kodeverdi {
            get {
                return this.kodeverdiField;
            }
            set {
                this.kodeverdiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string kodebeskrivelse {
            get {
                return this.kodebeskrivelseField;
            }
            set {
                this.kodebeskrivelseField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0")]
    [System.Xml.Serialization.XmlRootAttribute("Ansvarsomraade", Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0", IsNullable=false)]
    public partial class AnsvarsomraadeType {
        
        private System.Nullable<bool> ansvarsomraadetAvsluttetField;
        
        private bool ansvarsomraadetAvsluttetFieldSpecified;
        
        private string beskrivelseAvAnsvarsomraadetField;
        
        private KodeType funksjonField;
        
        private KontrollerendeType kontrollerendeField;
        
        private System.Nullable<System.DateTime> ansvarsrettErklaertField;
        
        private bool ansvarsrettErklaertFieldSpecified;
        
        private string soeknadssystemetsReferanseField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<bool> ansvarsomraadetAvsluttet {
            get {
                return this.ansvarsomraadetAvsluttetField;
            }
            set {
                this.ansvarsomraadetAvsluttetField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ansvarsomraadetAvsluttetSpecified {
            get {
                return this.ansvarsomraadetAvsluttetFieldSpecified;
            }
            set {
                this.ansvarsomraadetAvsluttetFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string beskrivelseAvAnsvarsomraadet {
            get {
                return this.beskrivelseAvAnsvarsomraadetField;
            }
            set {
                this.beskrivelseAvAnsvarsomraadetField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public KodeType funksjon {
            get {
                return this.funksjonField;
            }
            set {
                this.funksjonField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public KontrollerendeType kontrollerende {
            get {
                return this.kontrollerendeField;
            }
            set {
                this.kontrollerendeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<System.DateTime> ansvarsrettErklaert {
            get {
                return this.ansvarsrettErklaertField;
            }
            set {
                this.ansvarsrettErklaertField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ansvarsrettErklaertSpecified {
            get {
                return this.ansvarsrettErklaertFieldSpecified;
            }
            set {
                this.ansvarsrettErklaertFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string soeknadssystemetsReferanse {
            get {
                return this.soeknadssystemetsReferanseField;
            }
            set {
                this.soeknadssystemetsReferanseField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0")]
    [System.Xml.Serialization.XmlRootAttribute("Kontrollerende", Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0", IsNullable=false)]
    public partial class KontrollerendeType {
        
        private System.Nullable<bool> observerteAvvikField;
        
        private bool observerteAvvikFieldSpecified;
        
        private System.Nullable<bool> aapneAvvikField;
        
        private bool aapneAvvikFieldSpecified;
        
        private System.Nullable<bool> ingenAvvikField;
        
        private bool ingenAvvikFieldSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<bool> observerteAvvik {
            get {
                return this.observerteAvvikField;
            }
            set {
                this.observerteAvvikField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool observerteAvvikSpecified {
            get {
                return this.observerteAvvikFieldSpecified;
            }
            set {
                this.observerteAvvikFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<bool> aapneAvvik {
            get {
                return this.aapneAvvikField;
            }
            set {
                this.aapneAvvikField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool aapneAvvikSpecified {
            get {
                return this.aapneAvvikFieldSpecified;
            }
            set {
                this.aapneAvvikFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<bool> ingenAvvik {
            get {
                return this.ingenAvvikField;
            }
            set {
                this.ingenAvvikField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ingenAvvikSpecified {
            get {
                return this.ingenAvvikFieldSpecified;
            }
            set {
                this.ingenAvvikFieldSpecified = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0")]
    [System.Xml.Serialization.XmlRootAttribute("Ansvarsrett", Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0", IsNullable=false)]
    public partial class AnsvarsrettType {
        
        private AnsvarsomraadeType[] ansvarsomraaderField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(IsNullable=true)]
        [System.Xml.Serialization.XmlArrayItemAttribute("ansvarsomraade", IsNullable=false)]
        public AnsvarsomraadeType[] ansvarsomraader {
            get {
                return this.ansvarsomraaderField;
            }
            set {
                this.ansvarsomraaderField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0")]
    [System.Xml.Serialization.XmlRootAttribute("Eiendom", Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0", IsNullable=false)]
    public partial class EiendomType {
        
        private MatrikkelnummerType eiendomsidentifikasjonField;
        
        private EiendommensAdresseType adresseField;
        
        private string bygningsnummerField;
        
        private string bolignummerField;
        
        private string kommunenavnField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public MatrikkelnummerType eiendomsidentifikasjon {
            get {
                return this.eiendomsidentifikasjonField;
            }
            set {
                this.eiendomsidentifikasjonField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public EiendommensAdresseType adresse {
            get {
                return this.adresseField;
            }
            set {
                this.adresseField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string bygningsnummer {
            get {
                return this.bygningsnummerField;
            }
            set {
                this.bygningsnummerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string bolignummer {
            get {
                return this.bolignummerField;
            }
            set {
                this.bolignummerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string kommunenavn {
            get {
                return this.kommunenavnField;
            }
            set {
                this.kommunenavnField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0")]
    [System.Xml.Serialization.XmlRootAttribute("Matrikkelnummer", Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0", IsNullable=false)]
    public partial class MatrikkelnummerType {
        
        private string kommunenummerField;
        
        private string gaardsnummerField;
        
        private string bruksnummerField;
        
        private string festenummerField;
        
        private string seksjonsnummerField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string kommunenummer {
            get {
                return this.kommunenummerField;
            }
            set {
                this.kommunenummerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="integer", IsNullable=true)]
        public string gaardsnummer {
            get {
                return this.gaardsnummerField;
            }
            set {
                this.gaardsnummerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="integer", IsNullable=true)]
        public string bruksnummer {
            get {
                return this.bruksnummerField;
            }
            set {
                this.bruksnummerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="integer", IsNullable=true)]
        public string festenummer {
            get {
                return this.festenummerField;
            }
            set {
                this.festenummerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="integer", IsNullable=true)]
        public string seksjonsnummer {
            get {
                return this.seksjonsnummerField;
            }
            set {
                this.seksjonsnummerField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0")]
    [System.Xml.Serialization.XmlRootAttribute("EiendommensAdresse", Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0", IsNullable=false)]
    public partial class EiendommensAdresseType {
        
        private string adresselinje1Field;
        
        private string adresselinje2Field;
        
        private string adresselinje3Field;
        
        private string postnrField;
        
        private string poststedField;
        
        private string landkodeField;
        
        private string gatenavnField;
        
        private string husnrField;
        
        private string bokstavField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string adresselinje1 {
            get {
                return this.adresselinje1Field;
            }
            set {
                this.adresselinje1Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string adresselinje2 {
            get {
                return this.adresselinje2Field;
            }
            set {
                this.adresselinje2Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string adresselinje3 {
            get {
                return this.adresselinje3Field;
            }
            set {
                this.adresselinje3Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string postnr {
            get {
                return this.postnrField;
            }
            set {
                this.postnrField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string poststed {
            get {
                return this.poststedField;
            }
            set {
                this.poststedField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string landkode {
            get {
                return this.landkodeField;
            }
            set {
                this.landkodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string gatenavn {
            get {
                return this.gatenavnField;
            }
            set {
                this.gatenavnField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string husnr {
            get {
                return this.husnrField;
            }
            set {
                this.husnrField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string bokstav {
            get {
                return this.bokstavField;
            }
            set {
                this.bokstavField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0")]
    [System.Xml.Serialization.XmlRootAttribute("EnkelAdresse", Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0", IsNullable=false)]
    public partial class EnkelAdresseType {
        
        private string adresselinje1Field;
        
        private string adresselinje2Field;
        
        private string adresselinje3Field;
        
        private string postnrField;
        
        private string poststedField;
        
        private string landkodeField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string adresselinje1 {
            get {
                return this.adresselinje1Field;
            }
            set {
                this.adresselinje1Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string adresselinje2 {
            get {
                return this.adresselinje2Field;
            }
            set {
                this.adresselinje2Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string adresselinje3 {
            get {
                return this.adresselinje3Field;
            }
            set {
                this.adresselinje3Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string postnr {
            get {
                return this.postnrField;
            }
            set {
                this.postnrField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string poststed {
            get {
                return this.poststedField;
            }
            set {
                this.poststedField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string landkode {
            get {
                return this.landkodeField;
            }
            set {
                this.landkodeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0")]
    [System.Xml.Serialization.XmlRootAttribute("Kontaktperson", Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0", IsNullable=false)]
    public partial class KontaktpersonType {
        
        private string navnField;
        
        private string telefonnummerField;
        
        private string mobilnummerField;
        
        private string epostField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string navn {
            get {
                return this.navnField;
            }
            set {
                this.navnField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string telefonnummer {
            get {
                return this.telefonnummerField;
            }
            set {
                this.telefonnummerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string mobilnummer {
            get {
                return this.mobilnummerField;
            }
            set {
                this.mobilnummerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string epost {
            get {
                return this.epostField;
            }
            set {
                this.epostField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0")]
    [System.Xml.Serialization.XmlRootAttribute("Kontrollerklaering", Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0", IsNullable=false)]
    public partial class KontrollerklaeringType {
        
        private string fraSluttbrukersystemField;
        
        private EiendomType[] eiendomByggestedField;
        
        private SaksnummerType kommunensSaksnummerField;
        
        private string prosjektnrField;
        
        private PartType foretakField;
        
        private AnsvarsomraadeType ansvarsrettField;
        
        private SignaturType signaturField;
        
        private string hovedinnsendingsnummerField;
        
        private string prosjektnavnField;
        
        private PartType ansvarligSoekerField;
        
        private System.Nullable<bool> erklaeringKontrollField;
        
        private bool erklaeringKontrollFieldSpecified;
        
        private string dataFormatProviderField;
        
        private string dataFormatIdField;
        
        private string dataFormatVersionField;
        
        public KontrollerklaeringType() {
            this.dataFormatProviderField = "ANSAKO";
            this.dataFormatIdField = "10002";
            this.dataFormatVersionField = "1";
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string fraSluttbrukersystem {
            get {
                return this.fraSluttbrukersystemField;
            }
            set {
                this.fraSluttbrukersystemField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(IsNullable=true)]
        [System.Xml.Serialization.XmlArrayItemAttribute("eiendom", IsNullable=false)]
        public EiendomType[] eiendomByggested {
            get {
                return this.eiendomByggestedField;
            }
            set {
                this.eiendomByggestedField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public SaksnummerType kommunensSaksnummer {
            get {
                return this.kommunensSaksnummerField;
            }
            set {
                this.kommunensSaksnummerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string prosjektnr {
            get {
                return this.prosjektnrField;
            }
            set {
                this.prosjektnrField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public PartType foretak {
            get {
                return this.foretakField;
            }
            set {
                this.foretakField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public AnsvarsomraadeType ansvarsrett {
            get {
                return this.ansvarsrettField;
            }
            set {
                this.ansvarsrettField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public SignaturType signatur {
            get {
                return this.signaturField;
            }
            set {
                this.signaturField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string hovedinnsendingsnummer {
            get {
                return this.hovedinnsendingsnummerField;
            }
            set {
                this.hovedinnsendingsnummerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string prosjektnavn {
            get {
                return this.prosjektnavnField;
            }
            set {
                this.prosjektnavnField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public PartType ansvarligSoeker {
            get {
                return this.ansvarligSoekerField;
            }
            set {
                this.ansvarligSoekerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<bool> erklaeringKontroll {
            get {
                return this.erklaeringKontrollField;
            }
            set {
                this.erklaeringKontrollField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool erklaeringKontrollSpecified {
            get {
                return this.erklaeringKontrollFieldSpecified;
            }
            set {
                this.erklaeringKontrollFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string dataFormatProvider {
            get {
                return this.dataFormatProviderField;
            }
            set {
                this.dataFormatProviderField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string dataFormatId {
            get {
                return this.dataFormatIdField;
            }
            set {
                this.dataFormatIdField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string dataFormatVersion {
            get {
                return this.dataFormatVersionField;
            }
            set {
                this.dataFormatVersionField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0")]
    [System.Xml.Serialization.XmlRootAttribute("Saksnummer", Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0", IsNullable=false)]
    public partial class SaksnummerType {
        
        private string saksaarField;
        
        private string sakssekvensnummerField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="integer", IsNullable=true)]
        public string saksaar {
            get {
                return this.saksaarField;
            }
            set {
                this.saksaarField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="integer", IsNullable=true)]
        public string sakssekvensnummer {
            get {
                return this.sakssekvensnummerField;
            }
            set {
                this.sakssekvensnummerField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0")]
    [System.Xml.Serialization.XmlRootAttribute("Part", Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0", IsNullable=false)]
    public partial class PartType {
        
        private KodeType partstypeField;
        
        private string foedselsnummerField;
        
        private string organisasjonsnummerField;
        
        private string navnField;
        
        private EnkelAdresseType adresseField;
        
        private string telefonnummerField;
        
        private string mobilnummerField;
        
        private string epostField;
        
        private KontaktpersonType kontaktpersonField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public KodeType partstype {
            get {
                return this.partstypeField;
            }
            set {
                this.partstypeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string foedselsnummer {
            get {
                return this.foedselsnummerField;
            }
            set {
                this.foedselsnummerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string organisasjonsnummer {
            get {
                return this.organisasjonsnummerField;
            }
            set {
                this.organisasjonsnummerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string navn {
            get {
                return this.navnField;
            }
            set {
                this.navnField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public EnkelAdresseType adresse {
            get {
                return this.adresseField;
            }
            set {
                this.adresseField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string telefonnummer {
            get {
                return this.telefonnummerField;
            }
            set {
                this.telefonnummerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string mobilnummer {
            get {
                return this.mobilnummerField;
            }
            set {
                this.mobilnummerField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string epost {
            get {
                return this.epostField;
            }
            set {
                this.epostField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public KontaktpersonType kontaktperson {
            get {
                return this.kontaktpersonField;
            }
            set {
                this.kontaktpersonField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0")]
    [System.Xml.Serialization.XmlRootAttribute("Signatur", Namespace="http://skjema.kxml.no/dibk/ansakoKontrollerklaering/v1.0", IsNullable=false)]
    public partial class SignaturType {
        
        private System.Nullable<System.DateTime> signaturdatoField;
        
        private bool signaturdatoFieldSpecified;
        
        private string signertAvField;
        
        private string signertPaaVegneAvField;
        
        private string signeringsstegField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public System.Nullable<System.DateTime> signaturdato {
            get {
                return this.signaturdatoField;
            }
            set {
                this.signaturdatoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool signaturdatoSpecified {
            get {
                return this.signaturdatoFieldSpecified;
            }
            set {
                this.signaturdatoFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string signertAv {
            get {
                return this.signertAvField;
            }
            set {
                this.signertAvField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string signertPaaVegneAv {
            get {
                return this.signertPaaVegneAvField;
            }
            set {
                this.signertPaaVegneAvField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string signeringssteg {
            get {
                return this.signeringsstegField;
            }
            set {
                this.signeringsstegField = value;
            }
        }
    }
}
