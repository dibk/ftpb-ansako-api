﻿using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace Dibk.Ftpb.Ansako.Storage.Database
{
    public class AnSaKoDbContext : DbContext
    {
        public AnSaKoDbContext(DbContextOptions<AnSaKoDbContext> options) : base(options)
        { }

        public DbSet<FormDbModel> Forms { get; set; }
        public DbSet<AnsvarsomraadeDbModel> Ansvarsomraader { get; set; }
        public DbSet<ObsoleteForm> ObsoleteForms { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FormDbModel>().ToTable("Forms");
            modelBuilder.HasSequence<int>("SeqFormId").StartsAt(10000);
            modelBuilder.Entity<FormDbModel>().HasIndex(p => p.FtpbReferenceId).IsUnique();

            modelBuilder.Entity<AnsvarsomraadeDbModel>()
                .ToTable("Ansvarsomraader")
                .HasKey(p => new { p.AnsvarsomraadeId });

            modelBuilder.Entity<AnsvarsomraadeDbModel>()
                .HasOne<FormDbModel>(a => a.Form)
                .WithMany(a => a.Ansvarsomraader)
                .HasForeignKey(a => a.AnSaKoReferenceId);

            modelBuilder.Entity<ServiceStateDbModel>().ToTable("ServiceState");
            modelBuilder.Entity<ServiceStateDbModel>().HasIndex(p => p.StartDate);

            modelBuilder.Entity<ObsoleteForm>().ToView("View_ObsoleteForms");
            modelBuilder.Entity<ObsoleteForm>().HasNoKey();
        }
    }
}
