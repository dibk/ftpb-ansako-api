﻿using Microsoft.EntityFrameworkCore;

namespace Dibk.Ftpb.Ansako.Storage.Database
{
    public static class DbInitializer
    {
        public static bool Initialize(AnSaKoDbContext context)
        {
            context.Database.Migrate();
            return true;
        }
    }
}