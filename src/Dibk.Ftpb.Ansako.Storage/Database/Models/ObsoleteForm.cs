﻿using System;

namespace Dibk.Ftpb.Ansako.Storage.Database.Models
{
    public class ObsoleteForm
    {
        public string AnSaKoReferenceId { get; set; }
        public string FtpbReferenceId { get; set; }
        public string FtpbSignedReferenceId { get; set; }
        public string BlobStorageContainerName { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateDeleted { get; set; }
    }
}