﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dibk.Ftpb.Ansako.Storage.Database.Models
{
    public class AnsvarsomraadeDbModel
    {
        [Key]
        public Guid AnsvarsomraadeId { get; set; }        
        public string Soeknadssystemetsreferanse { get; set; }
        public string AnSaKoReferenceId { get; set; }
        public FormDbModel Form { get; set; }        
        public string AnsvarsomraadeBeskrivelse { get; set; }
        public string AnsvarligforetakOrgnr { get; set; }
        public string Status { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}
