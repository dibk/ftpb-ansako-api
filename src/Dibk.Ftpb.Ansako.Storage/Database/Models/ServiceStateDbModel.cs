﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Dibk.Ftpb.Ansako.Storage.Database.Models
{
    public class ServiceStateDbModel
    {
        [Key]
        public Guid Id { get; set; }
        public ServiceState State { get; set; }
        public string MessageHeader { get; set; }
        public string MessageBody { get; set; }        
        public DateTime? StartDate { get; set; }
        public DateTime? CreatedDate { get; set; }

        public override string ToString()
        {
            var startDateValue = StartDate.HasValue ? StartDate.Value.ToString() : null;
            return $"State: {State} - StartDate: {startDateValue} - MessageHeader: {MessageHeader} - MessageBody: {MessageBody}";
        }
    }
    public enum ServiceState
    {
        Online,
        Offline,
        Degraded
    }
}
