﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Dibk.Ftpb.Ansako.Storage.Database.Models
{
    public class FormDbModel
    {
        [Key]
        public string AnSaKoReferenceId { get; set; }

        [MaxLength(50)]
        public string FtpbReferenceId { get; set; }

        public string DataFormatId { get; set; }
        public string DataFormatVersion { get; set; }
        public ErklaeringType FormType { get; set; }
        public FormStatus Status { get; set; }
        public string StatusDetails { get; set; }
        public string FormDataFileName { get; set; }
        public string BlobStorageContainerName { get; set; }
        public string AnsvarligSoekerOrgnr { get; set; }
        public string AnsvarligForetakOrgnr { get; set; }
        public string AnsvarligForetakFunksjon { get; set; }
        public string GjennomforeringsplanId { get; set; }
        public string FtbId { get; set; }
        public string ProsjektNavn { get; set; }
        public string Prosjektnummer { get; set; }
        public string FtpbSignedReferenceId { get; set; }
        public string Soeknadssystemetsreferanse { get; set; }
        public SigningJobStatus? SigneringsJobStatus { get; set; }
        public string SigneringsJobId { get; set; }
        public string SigneringsUrl { get; set; }
        public string Signeringstjeneste { get; set; }
        public string SigneringsjobMetadata { get; set; }
        public string SigneringsjobCallbackToken { get; set; }
        public DateTime? SigneringsjobOpprettetDato { get; set; }
        public DateTime? SignertDato { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public DateTime? SigneringsFrist { get; set; }
        public Guid FtpbDistributionFormsId { get; set; }
        public string SluttbrukerSystem { get; set; }
        public bool? HasAttachment { get; set; }
        public string AttachmentFileName { get; set; }
        public string AttachmentBlobName { get; set; }
        public string AttachmentMimeType { get; set; }
        public string AttachmentFtpbType { get; set; }
        public List<AnsvarsomraadeDbModel> Ansvarsomraader { get; set; }
        public DateTime? DateDeleted { get; set; }
    }

    public enum SigningJobStatus
    {
        Created,
        NoChanges,
        CompletedSuccessfully,
        Failed,
        InProgress
    }

    public enum ErklaeringType
    {
        Ansvarsrett,
        Samsvarserklaering,
        Kontrollerklaering
    }

    public enum FormStatus
    {
        /// <summary>
        /// Erklæring er opprettet i Ftpb, men ikke åpnet av foretak
        /// </summary>
        Opprettet,

        /// <summary>
        /// Erklæring er åpnet av ansvarlig foretak, men ikke signert eller avvist.
        /// </summary>
        IArbeid,

        /// <summary>
        /// Erklæring er sendt til signering, men ikke signert eller avvist.
        /// </summary>
        TilSignering,

        /// <summary>
        /// Erklæring er signert med eller uten endringer fra ansvarlig foretak.
        /// </summary>
        Signert,

        /// <summary>
        /// Erklæring er avvist og returnert fra ansvarlig foretak.
        /// </summary>
        Avvist,

        /// <summary>
        /// Erklæring er ikke signert eller avvist innen tidsfristen.
        /// </summary>
        Utgått,

        /// <summary>
        /// Erklæring er trukket tilbake fra ansvarlig søker.
        /// </summary>
        Trukket,

        /// <summary>
        /// Sendinger som har feilet grunnet tekniske problemer.
        /// </summary>
        Feilet,

        SignertManuelt,
        Avsluttet
    }
}