﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Dibk.Ftpb.Ansako.Storage.database.migrations
{
    public partial class RenamingColumnFormReferenceId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ansvarsomraader_Forms_FormReferenceId",
                table: "Ansvarsomraader");

            migrationBuilder.RenameColumn(
                name: "FormReferenceId",
                table: "Forms",
                newName: "AnSaKoReferenceId");

            migrationBuilder.RenameColumn(
                name: "FormReferenceId",
                table: "Ansvarsomraader",
                newName: "AnSaKoReferenceId");

            migrationBuilder.RenameIndex(
                name: "IX_Ansvarsomraader_FormReferenceId",
                table: "Ansvarsomraader",
                newName: "IX_Ansvarsomraader_AnSaKoReferenceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Ansvarsomraader_Forms_AnSaKoReferenceId",
                table: "Ansvarsomraader",
                column: "AnSaKoReferenceId",
                principalTable: "Forms",
                principalColumn: "AnSaKoReferenceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ansvarsomraader_Forms_AnSaKoReferenceId",
                table: "Ansvarsomraader");

            migrationBuilder.RenameColumn(
                name: "AnSaKoReferenceId",
                table: "Forms",
                newName: "FormReferenceId");

            migrationBuilder.RenameColumn(
                name: "AnSaKoReferenceId",
                table: "Ansvarsomraader",
                newName: "FormReferenceId");

            migrationBuilder.RenameIndex(
                name: "IX_Ansvarsomraader_AnSaKoReferenceId",
                table: "Ansvarsomraader",
                newName: "IX_Ansvarsomraader_FormReferenceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Ansvarsomraader_Forms_FormReferenceId",
                table: "Ansvarsomraader",
                column: "FormReferenceId",
                principalTable: "Forms",
                principalColumn: "FormReferenceId");
        }
    }
}
