﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Dibk.Ftpb.Ansako.Storage.database.migrations
{
    public partial class MigratesExistingRows : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"CREATE PROCEDURE #CursorGenerateId
                AS
                BEGIN
                    DECLARE @AnSaKoReferenceId NVARCHAR(450)
                        , @GeneratedId INT = 9999;

                    DECLARE cursor_forms CURSOR
                    FOR
                    SELECT f.AnSaKoReferenceId
                    FROM Forms f
                    WHERE FtpbReferenceId IS NULL
                    ORDER BY DateCreated DESC;

                    OPEN cursor_forms

                    FETCH NEXT
                    FROM cursor_forms
                    INTO @AnSaKoReferenceId

                    WHILE @@FETCH_STATUS = 0
                    BEGIN
                        UPDATE Forms
                        SET FtpbReferenceId = 'FR' + CAST(@GeneratedId AS VARCHAR)
                        WHERE AnSaKoReferenceId = @AnSaKoReferenceId;

                        SET @GeneratedId = @GeneratedId - 1;

                        FETCH NEXT
                        FROM cursor_forms
                        INTO @AnSaKoReferenceId
                    END

                    CLOSE cursor_forms

                    DEALLOCATE cursor_forms
                END;
                GO

                EXEC #CursorGenerateId;
                GO

                DROP PROCEDURE #CursorGenerateId
                GO
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
