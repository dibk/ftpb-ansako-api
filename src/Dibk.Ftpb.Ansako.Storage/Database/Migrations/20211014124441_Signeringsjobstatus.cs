﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Dibk.Ftpb.Ansako.Storage.database.migrations
{
    public partial class Signeringsjobstatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SigneringsJobStatus",
                table: "Forms",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SigneringsJobStatus",
                table: "Forms");
        }
    }
}
