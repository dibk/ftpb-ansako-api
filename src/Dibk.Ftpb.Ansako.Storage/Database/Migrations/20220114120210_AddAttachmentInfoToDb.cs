﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Dibk.Ftpb.Ansako.Storage.database.migrations
{
    public partial class AddAttachmentInfoToDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AttachmentFileName",
                table: "Forms",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AttachmentFtpbType",
                table: "Forms",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AttachmentMimeType",
                table: "Forms",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "HasAttachment",
                table: "Forms",
                type: "bit",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AttachmentFileName",
                table: "Forms");

            migrationBuilder.DropColumn(
                name: "AttachmentFtpbType",
                table: "Forms");

            migrationBuilder.DropColumn(
                name: "AttachmentMimeType",
                table: "Forms");

            migrationBuilder.DropColumn(
                name: "HasAttachment",
                table: "Forms");
        }
    }
}
