﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Dibk.Ftpb.Ansako.Storage.database.migrations
{
    public partial class MigratesAnsvarsomraader : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"GO
                    CREATE PROCEDURE #CursorMigrateAnsvarsomraader
                    AS
                    BEGIN
                        DECLARE
                    @Soeknadssystemetsreferanse nvarchar(450),
                    @FormReferenceId  nvarchar(450),
                    @SamsvarserklaeringFormReferenceId  nvarchar(450),
                    @KontrollerklaeringFormReferenceId  nvarchar(450),
                    @AnsvarsomraadeBeskrivelse nvarchar(max);

                        DECLARE cursor_ansvarsomraader CURSOR FOR
                    SELECT Soeknadssystemetsreferanse, FormReferenceId, AnsvarsomraadeBeskrivelse, SamsvarserklaeringFormReferenceId, KontrollerklaeringFormReferenceId
                        FROM Ansvarsomraader
                        WHERE SamsvarserklaeringFormReferenceId IS NOT NULL OR KontrollerklaeringFormReferenceId IS NOT NULL

                        OPEN cursor_ansvarsomraader
                        FETCH NEXT FROM cursor_ansvarsomraader INTO @Soeknadssystemetsreferanse, @FormReferenceId, @AnsvarsomraadeBeskrivelse, @SamsvarserklaeringFormReferenceId, @KontrollerklaeringFormReferenceId
                        WHILE @@FETCH_STATUS=0
                    BEGIN
                            IF(@SamsvarserklaeringFormReferenceId IS NOT NULL) 
                            BEGIN
                                INSERT INTO Ansvarsomraader (AnsvarsomraadeId, Soeknadssystemetsreferanse, FormReferenceId, AnsvarsomraadeBeskrivelse) 
                                        VALUES (NEWID(), @Soeknadssystemetsreferanse, @KontrollerklaeringFormReferenceId, @AnsvarsomraadeBeskrivelse);
                            END
                            IF (@KontrollerklaeringFormReferenceId IS NOT NULL)
                            BEGIN
                                INSERT INTO Ansvarsomraader (AnsvarsomraadeId, Soeknadssystemetsreferanse, FormReferenceId, AnsvarsomraadeBeskrivelse) 
                                        VALUES (NEWID(), @Soeknadssystemetsreferanse, @KontrollerklaeringFormReferenceId, @AnsvarsomraadeBeskrivelse);
                            END

                            FETCH NEXT FROM cursor_ansvarsomraader INTO @Soeknadssystemetsreferanse, @FormReferenceId, @AnsvarsomraadeBeskrivelse, @SamsvarserklaeringFormReferenceId, @KontrollerklaeringFormReferenceId
                        END
                        CLOSE cursor_ansvarsomraader
                        DEALLOCATE cursor_ansvarsomraader
                    END;

                    GO
                    EXEC #CursorMigrateAnsvarsomraader;

                    GO
                    DROP PROCEDURE #CursorMigrateAnsvarsomraader
                    ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
