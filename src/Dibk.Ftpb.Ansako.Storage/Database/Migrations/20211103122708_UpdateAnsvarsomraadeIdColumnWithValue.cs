﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Dibk.Ftpb.Ansako.Storage.database.migrations
{
    public partial class UpdateAnsvarsomraadeIdColumnWithValue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"GO
                    CREATE PROCEDURE #CursorAnsvarsomraader
                    AS
                    BEGIN
                        DECLARE
                    @Soeknadssystemetsreferanse nvarchar(450),
                    @FormReferenceId  nvarchar(450),
                    @SamsvarserklaeringFormReferenceId  nvarchar(450),
                    @KontrollerklaeringFormReferenceId  nvarchar(450),
                    @AnsvarsomraadeBeskrivelse nvarchar(max);

                        DECLARE cursor_ansvarsomraader CURSOR FOR
                    SELECT Soeknadssystemetsreferanse, FormReferenceId, AnsvarsomraadeBeskrivelse, SamsvarserklaeringFormReferenceId, KontrollerklaeringFormReferenceId
                        FROM Ansvarsomraader

                        OPEN cursor_ansvarsomraader
                        FETCH NEXT FROM cursor_ansvarsomraader INTO @Soeknadssystemetsreferanse, @FormReferenceId, @AnsvarsomraadeBeskrivelse, @SamsvarserklaeringFormReferenceId, @KontrollerklaeringFormReferenceId
                        WHILE @@FETCH_STATUS=0
                    BEGIN
                            UPDATE Ansvarsomraader set AnsvarsomraadeId = NEWID() where Soeknadssystemetsreferanse = @Soeknadssystemetsreferanse and FormReferenceId = @FormReferenceId
                            FETCH NEXT FROM cursor_ansvarsomraader INTO @Soeknadssystemetsreferanse, @FormReferenceId, @AnsvarsomraadeBeskrivelse, @SamsvarserklaeringFormReferenceId, @KontrollerklaeringFormReferenceId
                        END
                        CLOSE cursor_ansvarsomraader
                        DEALLOCATE cursor_ansvarsomraader
                    END;

                    GO
                    EXEC #CursorAnsvarsomraader;

                    GO
                    DROP PROCEDURE #CursorAnsvarsomraader"
                );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
