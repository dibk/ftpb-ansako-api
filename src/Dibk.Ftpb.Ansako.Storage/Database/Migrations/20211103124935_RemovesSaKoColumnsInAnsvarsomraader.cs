﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Dibk.Ftpb.Ansako.Storage.database.migrations
{
    public partial class RemovesSaKoColumnsInAnsvarsomraader : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "KontrollerklaeringFormReferenceId",
                table: "Ansvarsomraader");

            migrationBuilder.DropColumn(
                name: "SamsvarserklaeringFormReferenceId",
                table: "Ansvarsomraader");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "KontrollerklaeringFormReferenceId",
                table: "Ansvarsomraader",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SamsvarserklaeringFormReferenceId",
                table: "Ansvarsomraader",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
