﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Dibk.Ftpb.Ansako.Storage.database.migrations
{
    public partial class AddedIndexForFtpbReferenceIdColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Forms_FtpbReferenceId",
                table: "Forms",
                column: "FtpbReferenceId",
                unique: true,
                filter: "[FtpbReferenceId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Forms_FtpbReferenceId",
                table: "Forms");
        }
    }
}
