﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Dibk.Ftpb.Ansako.Storage.database.migrations
{
    public partial class AddsFormTypeAndChangedStatusFieldFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FormStatus",
                table: "Forms",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Forms");

            migrationBuilder.RenameColumn(
                name: "FormStatus",
                table: "Forms",
                newName: "Status");

            migrationBuilder.AddColumn<int>(
                name: "FormType",
                table: "Forms",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FormType",
                table: "Forms");

            migrationBuilder.AlterColumn<string>(
                name: "Status",
                table: "Forms",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }
    }
}
