﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Dibk.Ftpb.Ansako.Storage.database.migrations
{
    /// <inheritdoc />
    public partial class AddDateCreatedAndDateDeletedIndexes : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                    IF NOT EXISTS (
                        SELECT 1
                    FROM sys.indexes
                    WHERE name = 'IX_Forms_DateDeleted_DateCreated' AND object_id = OBJECT_ID('dbo.Forms')
                    )
                    BEGIN
                        PRINT 'Creates Index IX_Forms_DateDeleted_DateCreated..';
                        CREATE NONCLUSTERED INDEX IX_Forms_DateDeleted_DateCreated
                        ON dbo.Forms (DateDeleted, DateCreated);
                    END
                    ELSE
                    BEGIN
                        PRINT 'Index IX_Forms_DateDeleted_DateCreated already exists';
                    END");

            migrationBuilder.Sql(@"
                    IF NOT EXISTS (
                        SELECT 1
                    FROM sys.indexes
                    WHERE name = 'IX_DateCreated' AND object_id = OBJECT_ID('dbo.Forms')
                    )
                    BEGIN
                        PRINT 'Creates Index IX_DateCreated..';
                        CREATE NONCLUSTERED INDEX IX_DateCreated
                        ON dbo.Forms (DateCreated);
                    END
                    ELSE
                    BEGIN
                        PRINT 'Index IX_DateCreated already exists';
                    END");

            migrationBuilder.Sql(@"
                    IF NOT EXISTS (
                        SELECT 1
                    FROM sys.indexes
                    WHERE name = 'IX_Status_SigneringsFrist' AND object_id = OBJECT_ID('dbo.Forms')
                    )
                    BEGIN
                        PRINT 'Creates Index IX_Status_SigneringsFrist..';
                        CREATE NONCLUSTERED INDEX IX_Status_SigneringsFrist
                        ON dbo.Forms (Status, SigneringsFrist);
                    END
                    ELSE
                    BEGIN
                        PRINT 'Index IX_Status_SigneringsFrist already exists';
                    END");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Forms_DateDeleted_DateCreated",
                table: "Forms");

            migrationBuilder.DropIndex(
                name: "IX_DateCreated",
                table: "Forms");

            migrationBuilder.DropIndex(
                name: "IX_Status_SigneringsFrist",
                table: "Forms");
        }
    }
}
