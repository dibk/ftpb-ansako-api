﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Dibk.Ftpb.Ansako.Storage.database.migrations
{
    public partial class ChangesKeyColumnForAnsvarsomraader : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ansvarsomraader_Forms_FormReferenceId",
                table: "Ansvarsomraader");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Ansvarsomraader",
                table: "Ansvarsomraader");

            migrationBuilder.AlterColumn<string>(
                name: "Soeknadssystemetsreferanse",
                table: "Ansvarsomraader",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<string>(
                name: "FormReferenceId",
                table: "Ansvarsomraader",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Ansvarsomraader",
                table: "Ansvarsomraader",
                column: "AnsvarsomraadeId");

            migrationBuilder.CreateIndex(
                name: "IX_Ansvarsomraader_FormReferenceId",
                table: "Ansvarsomraader",
                column: "FormReferenceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Ansvarsomraader_Forms_FormReferenceId",
                table: "Ansvarsomraader",
                column: "FormReferenceId",
                principalTable: "Forms",
                principalColumn: "FormReferenceId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ansvarsomraader_Forms_FormReferenceId",
                table: "Ansvarsomraader");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Ansvarsomraader",
                table: "Ansvarsomraader");

            migrationBuilder.DropIndex(
                name: "IX_Ansvarsomraader_FormReferenceId",
                table: "Ansvarsomraader");

            migrationBuilder.AlterColumn<string>(
                name: "Soeknadssystemetsreferanse",
                table: "Ansvarsomraader",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FormReferenceId",
                table: "Ansvarsomraader",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Ansvarsomraader",
                table: "Ansvarsomraader",
                columns: new[] { "FormReferenceId", "Soeknadssystemetsreferanse" });

            migrationBuilder.AddForeignKey(
                name: "FK_Ansvarsomraader_Forms_FormReferenceId",
                table: "Ansvarsomraader",
                column: "FormReferenceId",
                principalTable: "Forms",
                principalColumn: "FormReferenceId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
