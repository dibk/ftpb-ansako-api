﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Dibk.Ftpb.Ansako.Storage.database.migrations
{
    /// <inheritdoc />
    public partial class AddViewObsoleteForm : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                IF OBJECT_ID('dbo.View_ObsoleteForms', 'V') IS NOT NULL
                    DROP VIEW dbo.View_ObsoleteForms;"
            );

            migrationBuilder.Sql(@"
                CREATE VIEW View_ObsoleteForms AS
                SELECT AnSaKoReferenceId, FtpbReferenceId, FtpbSignedReferenceId, BlobStorageContainerName, DateCreated, DateDeleted
                FROM Forms WITH (INDEX (IX_Forms_DateDeleted_DateCreated))
                WHERE DateCreated <= GETDATE()-365 AND DateDeleted IS NULL;"
            );
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP VIEW dbo.View_ObsoleteForms;");
        }
    }
}
