﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Dibk.Ftpb.Ansako.Storage.database.migrations
{
    public partial class AddsSigningFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SigneringsJobId",
                table: "Forms",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SigneringsUrl",
                table: "Forms",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Signeringstjeneste",
                table: "Forms",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SigneringsJobId",
                table: "Forms");

            migrationBuilder.DropColumn(
                name: "SigneringsUrl",
                table: "Forms");

            migrationBuilder.DropColumn(
                name: "Signeringstjeneste",
                table: "Forms");
        }
    }
}
