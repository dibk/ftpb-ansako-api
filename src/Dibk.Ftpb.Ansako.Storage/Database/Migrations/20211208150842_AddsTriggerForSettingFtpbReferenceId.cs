﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Dibk.Ftpb.Ansako.Storage.database.migrations
{
    public partial class AddsTriggerForSettingFtpbReferenceId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
              @"CREATE OR ALTER TRIGGER TR_INSERT_FORMS ON Forms
                AFTER INSERT
                AS
                BEGIN
                    DECLARE @NextId INT;

                    SET @NextId = NEXT VALUE
                    FOR SeqFormId;

                    UPDATE Forms
                    SET FtpbReferenceId = 'FR' + Cast(@NextId AS VARCHAR)
                    FROM inserted
                    WHERE Forms.AnSaKoReferenceId = inserted.AnSaKoReferenceId;
                END
                GO");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP TRIGGER IF EXISTS TR_INSERT_FORMS
                GO");
        }
    }
}
