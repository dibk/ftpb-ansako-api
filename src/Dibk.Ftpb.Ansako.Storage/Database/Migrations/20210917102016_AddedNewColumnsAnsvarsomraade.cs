﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Dibk.Ftpb.Ansako.Storage.database.migrations
{
    public partial class AddedNewColumnsAnsvarsomraade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "KontrollerklaeringFormReferenceId",
                table: "Ansvarsomraader",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SamsvarserklaeringFormReferenceId",
                table: "Ansvarsomraader",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "KontrollerklaeringFormReferenceId",
                table: "Ansvarsomraader");

            migrationBuilder.DropColumn(
                name: "SamsvarserklaeringFormReferenceId",
                table: "Ansvarsomraader");
        }
    }
}
