﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Dibk.Ftpb.Ansako.Storage.database.migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Forms",
                columns: table => new
                {
                    FormReferenceId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    DataFormatId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DataFormatVersion = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FormDataFileName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BlobStorageContainerName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AnsvarligSoekerOrgnr = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AnsvarligForetakOrgnr = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AnsvarligForetakFunksjon = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GjennomforeringsplanId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FtbId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ProsjektNavn = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Prosjektnummer = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FtpbSignedReferenceId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Soeknadssystemetsreferanse = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime2", nullable: true),
                    SigneringsFrist = table.Column<DateTime>(type: "datetime2", nullable: true),
                    FtpbDistributionFormsId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Forms", x => x.FormReferenceId);
                });

            migrationBuilder.CreateTable(
                name: "Ansvarsomraader",
                columns: table => new
                {
                    Soeknadssystemetsreferanse = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    FormReferenceId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    AnsvarsomraadeBeskrivelse = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AnsvarligforetakOrgnr = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DateUpdated = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ansvarsomraader", x => new { x.FormReferenceId, x.Soeknadssystemetsreferanse });
                    table.ForeignKey(
                        name: "FK_Ansvarsomraader_Forms_FormReferenceId",
                        column: x => x.FormReferenceId,
                        principalTable: "Forms",
                        principalColumn: "FormReferenceId",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Ansvarsomraader");

            migrationBuilder.DropTable(
                name: "Forms");
        }
    }
}
