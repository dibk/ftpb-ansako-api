﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Dibk.Ftpb.Ansako.Storage.database.migrations
{
    public partial class AddedSigningJobMetadata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SigneringsjobMetadata",
                table: "Forms",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SigneringsjobMetadata",
                table: "Forms");
        }
    }
}
