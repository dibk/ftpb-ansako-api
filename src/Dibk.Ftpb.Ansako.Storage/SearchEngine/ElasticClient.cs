﻿using Microsoft.Extensions.Options;
using Serilog;
using System;
using Elastic.Clients.Elasticsearch;
using Elastic.Transport;

namespace Dibk.Ftpb.Ansako.Storage.SearchEngine
{
    public class ElasticUtil
    {
        private static readonly ILogger Log = Serilog.Log.ForContext<ElasticUtil>();
  
        /// <summary>
        /// Return a new instance of the ElasticClient.
        /// </summary>
        /// <param name="indexName">Name of the index to use as default in all operations.</param>
        /// <returns></returns>
        /// 

        public static ElasticsearchClient GetClient(IOptions<ElasticSettings> elasticSettings)
        {
            Log.Verbose("Creating new ElasticClient with url: " + elasticSettings.Value.ConnectionUrl);

            string index = string.Format(elasticSettings.Value.DocIndex, DateTime.Now);

            ElasticsearchClientSettings settings = new ElasticsearchClientSettings(new Uri(elasticSettings.Value.ConnectionUrl))
                .DefaultIndex(index)
                .DisableDirectStreaming() // enable debugging information of request queries
                .ThrowExceptions();

            if (elasticSettings.Value.Username != null)
            {
                settings.Authentication(new BasicAuthentication(elasticSettings.Value.Username, elasticSettings.Value.Password));
            }

            return new ElasticsearchClient(settings);
        }
    }
}
