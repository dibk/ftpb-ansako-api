﻿namespace Dibk.Ftpb.Ansako.Storage.SearchEngine
{
    public class ElasticSettings
    {
        public string ConnectionUrl { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string IndexFormat { get; set; }
        public string DocIndex { get; set; }
    }
}
