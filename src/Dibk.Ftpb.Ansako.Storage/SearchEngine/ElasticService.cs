﻿using Dibk.Ftpb.Ansako.Storage.SearchEngine;
using Elastic.Clients.Elasticsearch;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Storage.Service
{
    public class ElasticService
    {
        private readonly ILogger _logger;
        private readonly IOptions<ElasticSettings> _elasticSettings;
        public ElasticService(ILogger<ElasticService> logger, IOptions<ElasticSettings> elasticSettings)
        {
            _logger = logger;
            _elasticSettings = elasticSettings;
        }

        public void Add(object entity)
        {
            IndexDocument(entity);
        }

        public async Task AddAsync(object entity)
        {
            await IndexDocumentAsync(entity);
        }

        public async Task AddManyAsync(IEnumerable<object> items)
        {
            await IndexManyAsync(items);
        }

        public async Task AddBulkAllAsync(IEnumerable<object> items)
        {
            await BulkAllAsync(items);
        }

        private async Task IndexDocumentAsync<TDoc>(TDoc doc) where TDoc : class
        {
            try
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                var jsontext = JsonSerializer.Serialize(doc);
                await ElasticUtil.GetClient(_elasticSettings).IndexAsync(doc).ConfigureAwait(false);
                stopWatch.Stop();
                _logger.LogDebug("Finished indexing document in: {ElapsedMilliseconds} ms", stopWatch.ElapsedMilliseconds);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error while indexing document");
            }
        }

        private void IndexDocument<TDoc>(TDoc doc) where TDoc : class
        {
            try
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                ElasticUtil.GetClient(_elasticSettings).IndexAsync(doc).ConfigureAwait(false);
                stopWatch.Stop();
                _logger.LogDebug("Finished indexing document in: {ElapsedMilliseconds} ms", stopWatch.ElapsedMilliseconds);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error while indexing document");
            }
        }

        private async Task IndexManyAsync<TDoc>(IEnumerable<TDoc> docs) where TDoc : class
        {
            try
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();

                var client = ElasticUtil.GetClient(_elasticSettings);

                await client.IndexManyAsync(docs).ConfigureAwait(false);

                stopWatch.Stop();
                _logger.LogDebug("Finished indexing {IndexedDocumentsCount} documents in: {ElapsedMilliseconds} ms", docs?.Count(), stopWatch.ElapsedMilliseconds);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error while indexing documents");
            }
        }

        private async Task BulkAllAsync<TDoc>(IEnumerable<TDoc> docs) where TDoc : class
        {
            try
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();

                var client = ElasticUtil.GetClient(_elasticSettings);

                List<string> errors = new List<string>();
                int seenPages = 0;
                int requests = 0;
                CancellationTokenSource tokenSource = new CancellationTokenSource();
                ConcurrentBag<BulkResponse> bulkResponses = new ConcurrentBag<BulkResponse>();
                ConcurrentBag<BulkAllResponse> bulkAllResponses = new ConcurrentBag<BulkAllResponse>();
                ConcurrentBag<TDoc> deadLetterQueue = new ConcurrentBag<TDoc>();
                BulkAllObservable<TDoc> observableBulk = client.BulkAll(docs, f => f
                        .MaxDegreeOfParallelism(2)
                        .BulkResponseCallback(r =>
                        {
                            bulkResponses.Add(r);
                            Interlocked.Increment(ref requests);
                        })
                        .ContinueAfterDroppedDocuments()
                        .DroppedDocumentCallback((r, o) =>
                        {
                            if (r.Error != null)
                                errors.Add(r.Error.Reason);
                            deadLetterQueue.Add(o);
                        })
                        .BackOffTime(TimeSpan.FromSeconds(20))
                        .BackOffRetries(2)
                        .Size(10000)
                        .RefreshOnCompleted()
                        .Index(_elasticSettings.Value.DocIndex)
                        .BufferToBulk((r, buffer) => r.IndexMany(buffer))
                    , tokenSource.Token);

                try
                {
                    observableBulk.Wait(TimeSpan.FromMinutes(4), b =>
                    {
                        bulkAllResponses.Add(b);
                        Interlocked.Increment(ref seenPages);
                    });
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Error occured");
                }

                if (errors.Count == 0)
                    _logger.LogInformation($"Indexing of {docs.Count()} source docs done by {requests} bulk requests");
                else
                {
                    _logger.LogInformation($"Indexing of {docs.Count()} source docs finished with {errors.Count()} errors");
                    foreach (var err in errors)
                    {
                        _logger.LogError($"Error ==> {err}");
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error while indexing documents");
                throw;
            }
        }
    }
}
