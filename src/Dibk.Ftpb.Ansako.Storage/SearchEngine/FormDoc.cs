﻿using Dibk.Ftpb.Ansako.Storage.Database.Models;
using System;

namespace Dibk.Ftpb.Ansako.Storage.Service
{
    public class FormDoc
    {
        public string Id { get { return AnSaKoReferenceId; } set { } }
        public string AnSaKoReferenceId { get; set; }
        public string FtpbReferenceId { get; set; }
        public string DataFormatId { get; set; }
        public string DataFormatVersion { get; set; }
        public string FormType { get; set; }
        public string Status { get; set; }
        public string StatusDetails { get; set; }
        public string FormDataFileName { get; set; }
        public string BlobStorageContainerName { get; set; }
        public string AnsvarligSoekerOrgnr { get; set; }
        public string AnsvarligForetakOrgnr { get; set; }
        public string AnsvarligForetakFunksjon { get; set; }
        public string GjennomforeringsplanId { get; set; }
        public string FtbId { get; set; }
        public string ProsjektNavn { get; set; }
        public string Prosjektnummer { get; set; }
        public string FtpbSignedReferenceId { get; set; }
        public string Soeknadssystemetsreferanse { get; set; }
        public string SigneringsJobStatus { get; set; }
        public string SigneringsJobId { get; set; }
        public string SigneringsUrl { get; set; }
        public string Signeringstjeneste { get; set; }
        public string SigneringsjobMetadata { get; set; }
        public string SigneringsjobCallbackToken { get; set; }
        public DateTime? SigneringsjobOpprettetDato { get; set; }
        public DateTime? SignertDato { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public DateTime? DateDeleted { get; set; }
        public DateTime? SigneringsFrist { get; set; }
        public Guid FtpbDistributionFormsId { get; set; }
        public string SluttbrukerSystem { get; set; }

        public static explicit operator FormDoc(FormDbModel formDbModel)
        {
            return new FormDoc()
            {
                AnSaKoReferenceId = formDbModel.AnSaKoReferenceId,
                FtpbReferenceId = formDbModel.FtpbReferenceId,
                DataFormatId = formDbModel.DataFormatId,
                DataFormatVersion = formDbModel.DataFormatVersion,
                FormType = formDbModel.FormType.ToString(),
                Status = formDbModel.Status.ToString(),
                StatusDetails = formDbModel.StatusDetails,
                FormDataFileName = formDbModel.FormDataFileName,
                BlobStorageContainerName = formDbModel.BlobStorageContainerName,
                AnsvarligSoekerOrgnr = formDbModel.AnsvarligSoekerOrgnr,
                AnsvarligForetakOrgnr = formDbModel.AnsvarligForetakOrgnr,
                AnsvarligForetakFunksjon = formDbModel.AnsvarligForetakFunksjon,
                GjennomforeringsplanId = formDbModel.GjennomforeringsplanId,
                FtbId = formDbModel.FtbId,
                ProsjektNavn = formDbModel.ProsjektNavn,
                Prosjektnummer = formDbModel.Prosjektnummer,
                FtpbSignedReferenceId = formDbModel.FtpbSignedReferenceId,
                Soeknadssystemetsreferanse = formDbModel.Soeknadssystemetsreferanse,
                SigneringsJobStatus = formDbModel.SigneringsJobStatus.ToString(),
                SigneringsJobId = formDbModel.SigneringsJobId,
                SigneringsUrl = formDbModel.SigneringsUrl,
                Signeringstjeneste = formDbModel.Signeringstjeneste,
                SigneringsjobMetadata = formDbModel.SigneringsjobMetadata,
                SigneringsjobCallbackToken = formDbModel.SigneringsjobCallbackToken,
                SigneringsjobOpprettetDato = formDbModel.SigneringsjobOpprettetDato.HasValue ? formDbModel.SigneringsjobOpprettetDato.Value.ToUniversalTime() : null,
                SignertDato = formDbModel.SignertDato.HasValue ? formDbModel.SignertDato.Value.ToUniversalTime() : null,
                DateCreated = formDbModel.DateCreated.HasValue ? formDbModel.DateCreated.Value.ToUniversalTime() : null,
                DateUpdated = formDbModel.DateUpdated.HasValue ? formDbModel.DateUpdated.Value.ToUniversalTime() : null,
                DateDeleted = formDbModel.DateDeleted.HasValue ? formDbModel.DateDeleted.Value.ToUniversalTime() : null,
                SigneringsFrist = formDbModel.SigneringsFrist.HasValue ? formDbModel.SigneringsFrist.Value.ToUniversalTime() : null,
                FtpbDistributionFormsId = formDbModel.FtpbDistributionFormsId,
                SluttbrukerSystem = formDbModel.SluttbrukerSystem
            };
        }
    }
}
