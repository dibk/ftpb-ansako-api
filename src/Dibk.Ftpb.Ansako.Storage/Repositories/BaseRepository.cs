﻿using Dibk.Ftpb.Ansako.Storage.Database;
using Dibk.Ftpb.Ansako.Storage.Service;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Storage.Repositories
{

    public class BaseDbRepository<TEntity> : IRepository<TEntity> where TEntity : class, new()
    {
        public readonly AnSaKoDbContext DbContext;
        public readonly ElasticService _elasticService;

        public BaseDbRepository(AnSaKoDbContext dbContext, ElasticService elasticService)
        {
            DbContext = dbContext;
            _elasticService = elasticService;
        }

        public IQueryable<TEntity> GetAll()
        {
            try
            {
                return DbContext.Set<TEntity>();
            }
            catch (Exception ex)
            {
                throw new Exception($"Couldn't retrieve entities: {ex.Message}");
            }
        }
        public virtual Task SyncToElasticSearch(TEntity entity)
        {
            return Task.FromResult(0);
        }

        public async Task<TEntity> AddAsync(TEntity entity, bool syncToELK = true)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
            }

            try
            {
                await DbContext.AddAsync(entity).ConfigureAwait(false);
                await DbContext.SaveChangesAsync().ConfigureAwait(false);
                await DbContext.Entry(entity).ReloadAsync().ConfigureAwait(false);
                if (syncToELK)
                    await SyncToElasticSearch(entity);
                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be saved: {ex.Message}");
            }
        }

        public async Task<TEntity> UpdateAsync(TEntity entity, bool syncToELK = true)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
            }

            try
            {
                DbContext.Update(entity);
                await DbContext.SaveChangesAsync().ConfigureAwait(false);
                if (syncToELK)
                    await SyncToElasticSearch(entity);

                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be updated: {ex.Message}");
            }
        }
    }
}
