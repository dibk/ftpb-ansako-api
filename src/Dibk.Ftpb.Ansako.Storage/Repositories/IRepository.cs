﻿using System.Linq;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Storage.Repositories
{
    public interface IRepository<TEntity> where TEntity : class, new()
    {
        IQueryable<TEntity> GetAll();
        Task<TEntity> AddAsync(TEntity entity, bool syncToELK = true);
        Task<TEntity> UpdateAsync(TEntity entity, bool syncToELK = true);
    }
}
