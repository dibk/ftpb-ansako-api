﻿using Dibk.Ftpb.Ansako.Storage.Database;
using Dibk.Ftpb.Ansako.Storage.Database.Models;
using Dibk.Ftpb.Ansako.Storage.Service;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Ansako.Storage.Repositories
{
    public class FormDbRepository : BaseDbRepository<FormDbModel>
    {
        public FormDbRepository(AnSaKoDbContext dbContext, ElasticService elasticService) : base(dbContext, elasticService) { }

        public async Task UpdateFormAsync(FormDbModel formDbModel)
        {
            var entity = DbContext.Entry(formDbModel);

            entity.CurrentValues.SetValues(formDbModel);
            entity.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            if (entity.Entity.Ansvarsomraader != null)
                foreach (var ansvarsomraade in entity.Entity.Ansvarsomraader)
                {
                    var ansvarsomraadeDb = DbContext.Entry(ansvarsomraade);
                    ansvarsomraadeDb.CurrentValues.SetValues(ansvarsomraade);
                    ansvarsomraadeDb.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                }
            

            await DbContext.SaveChangesAsync().ConfigureAwait(false);
        }
        public override async Task SyncToElasticSearch(FormDbModel entity)
        {
            await _elasticService.AddAsync((FormDoc)entity);
        }

        public async Task<string> GetNextFtpbReferenceId()
        {
            var p = new SqlParameter("@result", System.Data.SqlDbType.Int);
            p.Direction = System.Data.ParameterDirection.Output;
            await DbContext.Database.ExecuteSqlRawAsync("SET @result = NEXT VALUE FOR SeqFormId", p).ConfigureAwait(false); ;
            return $"FR{(int)p.Value}"; 
        }
    }
}

