﻿using Dibk.Ftpb.Ansako.Storage.BlobStorage;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Dibk.Ftpb.Ansako.Storage
{
    public class AnSaKoBlobService : BlobService
    {
        public AnSaKoBlobService(ILogger<BlobService> logger, IOptions<AnSaKoBlobStorageAccountSettings> options) : base(logger, options)
        { }
    }

    public class AnSaKoBlobStorageAccountSettings : StorageAccountSettings
    {
        public static string ConfigSection => "AzureStorage";
    }
}