using Dibk.Ftpb.Signing.Certificate;
using Dibk.Ftpb.Signing.Models;
using Digipost.Signature.Api.Client.Core;
using Digipost.Signature.Api.Client.Core.Exceptions;
using Digipost.Signature.Api.Client.Direct;
using Digipost.Signature.Api.Client.Direct.Enums;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Polly;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Wmhelp.XPath2;

namespace Dibk.Ftpb.Signing.eSignering
{
    public class eSigneringSigner : ISigner
    {
        private readonly ILoggerFactory _loggerFactory;
        private readonly ICertificateReader _certificateReader;
        
        private readonly SigningJobBuilderFactory _signingJobBuilderFactory;
        private readonly eSigneringSettings _settings;
        private readonly ILogger<eSigneringSigner> _logger;

        public eSigneringSigner(ILoggerFactory loggerFactory,
                                IOptions<eSigneringSettings> options,
                                ICertificateReader certificateReader,
                                SigningJobBuilderFactory signingJobBuilderFactory)
        {
            _loggerFactory = loggerFactory;
            _certificateReader = certificateReader;
            _signingJobBuilderFactory = signingJobBuilderFactory;
            _settings = options.Value;
            _logger = _loggerFactory.CreateLogger<eSigneringSigner>();
        }
        private Digipost.Signature.Api.Client.Core.Environment GetEnvironment()
        {
            if (_settings.eSigneringEnvironment.Equals("prod", StringComparison.InvariantCultureIgnoreCase))
                return Digipost.Signature.Api.Client.Core.Environment.Production;
            else
                return Digipost.Signature.Api.Client.Core.Environment.DifiTest;
        }

        private DirectClient GetClient()
        {
            var sender = new Sender(_settings.GlobalSenderOrgnr);
            var cert = _certificateReader.GetCertificate();
            var config = new ClientConfiguration(GetEnvironment(), cert, globalSender: sender);
            config.HttpClientTimeoutInMilliseconds = _settings.TimeoutMilliseconds; 
            _logger.LogDebug($"Creates eSignering client for environment {config.Environment.Url.AbsoluteUri}");
            return new DirectClient(config, _loggerFactory);
        }

        private SigningJobResult GetJobResult(JobResponse response)
        {
            var signer = response.Signers.First();

            //Get URL for status requests
            var statusUrl = response.StatusUrl.StatusBaseUrl;

            var meta = new eSigneringMetadata() { StatusUrl = statusUrl.AbsoluteUri, SignerUrl = signer.SignerUrl.AbsoluteUri };

            return new SigningJobResult()
            {
                Id = response.JobId.ToString(),
                SigningProvider = GetSigningProviderName(),
                Status = SigningStatus.Created,
                SigningUrl = signer.RedirectUrl.AbsoluteUri,
                SigningMetadata = System.Text.Json.JsonSerializer.Serialize(meta)
            };
        }

        public async Task<SigningJobResult> CreateSigningJob(SigningJobRequest signingJob)
        {
            var client = GetClient();
            var job = _signingJobBuilderFactory.GetSigningJobProvider().CreateJobPayload(signingJob);

            JobResponse response = null;
            try
            {
                response = await GetExecutionPolicy().ExecuteAsync(async () => await client.Create(job)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                throw HandleException(ex);                
            }

            return GetJobResult(response);
        }

        public async Task<string> RequestNewRedirectUri(string signeringMetadata)
        {
            var meta = GetMetadata(signeringMetadata);

            Uri persistedSignerUrl = new Uri(meta.SignerUrl);
            SignerResponse signerWithUpdatedRedirectUrl;
            try
            {
            signerWithUpdatedRedirectUrl = await GetExecutionPolicy().ExecuteAsync(async () => await GetClient()
                .RequestNewRedirectUrl(
                    NewRedirectUrlRequest
                        .FromSignerUrl(persistedSignerUrl)
                ).ConfigureAwait(false));
            }
            catch (Exception ex)
            {
                throw HandleException(ex);                
            }
            return signerWithUpdatedRedirectUrl.RedirectUrl.AbsoluteUri;
        }

        public async Task<SignedDocument> GetSignedDocument(SigningJobStatusRequest request)
        {
            var client = GetClient();

            var jobStatusResponse = await GetJobStatus(request);

            var document = new SignedDocument();
            if (jobStatusResponse.Status == JobStatus.CompletedSuccessfully)
            {
                Stream padesByteStream = null;

                try
                {
                    padesByteStream = await GetExecutionPolicy().ExecuteAsync(async () => await client.GetPades(jobStatusResponse.References.Pades).ConfigureAwait(false));
                }
                catch (Exception ex)
                {
                    throw HandleException(ex);
                }

                document.SignedDate = jobStatusResponse.Signatures.First().DateTimeForStatus;
                document.FileContent = padesByteStream.GetAsBytes();

                document.SignerDisplayName = await GetSignerName(jobStatusResponse);

                var statusQueryToken = GetMetadata(request.SigningJobMetadata).StatusQueryToken;

                if (document.FileContent.Length == 0)
                    _logger.LogError("Signed document for {STATUS_QUERY_TOKEN} not found", statusQueryToken);
            }

            return document;
        }

        private async Task<string> GetSignerName(JobStatusResponse jobStatusResponse)
        {
            var client = GetClient();
            Stream xadesStream = null;
            var signerName = string.Empty;
            try
            {
                xadesStream = await GetExecutionPolicy().ExecuteAsync(async () => await client.GetXades(jobStatusResponse.Signatures.First().XadesReference).ConfigureAwait(false));
                using var reader = new StreamReader(xadesStream);
                string text = reader.ReadToEnd();
                var doc = XDocument.Parse(text);
                signerName = doc.XPath2SelectElement("*:LtvSdo/*:Description/*:SignerDescription/*:SignerDisplayName").Value;
            }
            catch (Exception ex)
            {                
                _logger.LogWarning(ex, "Unable to retreive signer name from Xades. Sets name based on the signer set when creating the job.");
                HandleException(ex);
            }

            return signerName;

        }

        private Exception HandleException(Exception ex)
        {
            if (ex is BrokerNotAuthorizedException notAuthorizedException)
            {
                _logger.LogError(notAuthorizedException, "eSignering - Not authorized");
            }
            else if (ex is UnexpectedResponseException unexpectedResponseException)
            {
                _logger.LogError(unexpectedResponseException, $"eSignering - An unexpected response was received: {unexpectedResponseException.Error}");
            }
            else if (ex is SignatureException exception)
                _logger.LogError(exception, "eSignering - A signature exception occurred");
            else if(ex is System.TimeoutException || ex is System.Threading.Tasks.TaskCanceledException)
            {
                return new eSigneringException("eSignering - The request experienced a timeout on the eSigneringAPI", true, ex);
            }
            else
                _logger.LogError(ex, "eSignering - Unexpected error occurred");

            return ex;
        }

        public async Task<SigningStatus> GetStatusForSigningJob(SigningJobStatusRequest request)
        {
            var status = await GetJobStatus(request);
            return GetSigningStatus(status.Status);
        }

        private SigningStatus GetSigningStatus(JobStatus eSigneringStatus)
        {
            switch (eSigneringStatus)
            {
                case JobStatus.Failed:
                    return SigningStatus.Failed;
                case JobStatus.InProgress:
                    return SigningStatus.InProgress;
                case JobStatus.CompletedSuccessfully:
                    return SigningStatus.CompletedSuccessfully;
                case JobStatus.NoChanges:
                    return SigningStatus.NoChanges;
                default:
                    return SigningStatus.Created;
            }
        }

        private async Task<JobStatusResponse> GetJobStatus(SigningJobStatusRequest request)
        {
            var meta = GetMetadata(request.SigningJobMetadata);

            if (string.IsNullOrEmpty(meta.StatusQueryToken))
                throw new ArgumentNullException("Cannot request status. The StatusQueryToken has no value.");

            var client = GetClient();
            JobStatusResponse jobstatusResponse = null;
            try
            {
                jobstatusResponse = await GetExecutionPolicy().ExecuteAsync(async ()=> await client.GetStatus(new StatusReference(new Uri(meta.StatusUrl), meta.StatusQueryToken)).ConfigureAwait(false));
            }
            catch (Exception ex)
            {
                throw HandleException(ex);
            }
            return jobstatusResponse;
        }


        private eSigneringMetadata GetMetadata(string signingJobMetadata)
        {
            return System.Text.Json.JsonSerializer.Deserialize<eSigneringMetadata>(signingJobMetadata);
        }

        public string GetSigningProviderName()
        {
            return "eSignering";
        }

        public ISigningMetadataBuilder GetSigningMetadataBuilder()
        {
            return new eSigneringMetadataBuilder();
        }

        public AsyncPolicy GetExecutionPolicy()
        {
            return Policy.Handle<System.TimeoutException>()
                .Or<System.Threading.Tasks.TaskCanceledException>(ex => 
                {
                    return ex.InnerException != null && ex.InnerException is System.TimeoutException;
                })
                .Or<UnexpectedResponseException>(ex => (int)ex.StatusCode >= 500 || ex.StatusCode == System.Net.HttpStatusCode.RequestTimeout)
                .WaitAndRetryAsync(6,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                    (ex, timespan) => { _logger.LogError(ex, $"Exception occured in eSignering integration. Waits {timespan.TotalSeconds} seconds "); });
        }

    }

    public class eSigneringException : Exception
    {
        public eSigneringException(string message, bool retryable, Exception innerException) : base(message, innerException)
        {
            Retryable = retryable;
        }

        public bool Retryable { get; set; }
    }
}
