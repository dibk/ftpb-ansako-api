﻿namespace Dibk.Ftpb.Signing.eSignering
{
    public class eSigneringSettings
    {
        public static string ConfigSection => "eSignering";
        public string GlobalSenderOrgnr { get; set; }
        public string eSigneringEnvironment { get; set; }
        public int TimeoutMilliseconds { get; set; }
        public string SigningIdentifier { get; set; }
    }
}
