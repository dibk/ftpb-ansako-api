﻿namespace Dibk.Ftpb.Signing.eSignering
{
    internal class eSigneringMetadata
    {
        public string StatusQueryToken { get; set; }
        public string StatusUrl { get; set; }
        public string SignerUrl { get; set; }
    }
}
