﻿using Dibk.Ftpb.Signing.Models;
using Digipost.Signature.Api.Client.Core.Enums;
using Digipost.Signature.Api.Client.Direct;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json.Serialization;

namespace Dibk.Ftpb.Signing.eSignering
{
    public class PidSigningJobBuilder : SigningJobBuilder
    {
        private readonly IHttpContextAccessor _context;

        public PidSigningJobBuilder(ILogger<SigningJobBuilder> logger, IHttpContextAccessor context) : base(logger)
        {
            _context = context;
        }

        public override IdentifierInSignedDocuments SignedDocumentsIdentifier => IdentifierInSignedDocuments.PersonalIdentificationNumberAndName;

        protected override IEnumerable<Signer> GetSigners(SigningJobRequest signingJobRequest)
        {
            var userInfo = new UserInfo()
            {
                Id = _context.HttpContext.User.GetId(),
                Claims = _context.HttpContext.User.Claims.ToDictionary(claim => claim.Type, claim => claim.Value)
            };

            var pid = userInfo.Claims.FirstOrDefault(p => p.Key.Equals("PID", StringComparison.OrdinalIgnoreCase));

            var signers = new List<Signer>();
            signers.Add(new Signer(new Digipost.Signature.Api.Client.Core.Identifier.PersonalIdentificationNumber(pid.Value)));
            return signers;
        }
        public class UserInfo
        {
            [JsonPropertyName("id")]
            public string Id { get; set; }

            [JsonPropertyName("claims")]
            public Dictionary<string, string> Claims { get; set; }
        }
    }
    public static class UserHelpers
    {
        public static string GetId(this ClaimsPrincipal principal)
        {
            var userIdClaim = principal.FindFirst(c => c.Type == ClaimTypes.NameIdentifier) ?? principal.FindFirst(c => c.Type == "sub");
            if (userIdClaim != null && !string.IsNullOrEmpty(userIdClaim.Value))
            {
                return userIdClaim.Value;
            }

            return null;
        }
    }
}
