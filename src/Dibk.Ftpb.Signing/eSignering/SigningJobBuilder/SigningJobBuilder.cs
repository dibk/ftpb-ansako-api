﻿using Dibk.Ftpb.Signing.Models;
using Digipost.Signature.Api.Client.Core;
using Digipost.Signature.Api.Client.Core.Enums;
using Digipost.Signature.Api.Client.Direct;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Signing.eSignering
{
    public class SigningJobBuilder : ISigningJobBuilder
    {
        private readonly ILogger<SigningJobBuilder> _logger;

        public virtual IdentifierInSignedDocuments SignedDocumentsIdentifier => IdentifierInSignedDocuments.Name;
        public SigningJobBuilder(ILogger<SigningJobBuilder> logger)
        {
            _logger = logger;
        }

        public SigningJobBuilder()
        {
        }

        public Job CreateJobPayload(SigningJobRequest signingJobRequest)
        {
            var documents = new List<Document>();
            var mainDoc = signingJobRequest.SigningDocuments.First(p => p.FileType == SigningFileType.MainDocument);
            documents.Add(new Document(mainDoc.Title, FileType.Pdf, mainDoc.FileContent));
            var attachments = signingJobRequest.SigningDocuments.Where(p => p.FileType == SigningFileType.Attachment).ToList();
            documents.AddRange(attachments.Select(p => new Document(p.Title, FileType.Pdf, p.FileContent)));

            var signers = GetSigners(signingJobRequest);

            var exitUrl = new ExitUrls(
                new Uri(signingJobRequest.SigningCallbackUrls.OnSuccessRedirectUrl),
                new Uri(signingJobRequest.SigningCallbackUrls.OnCancelRedirectUrl),
                new Uri(signingJobRequest.SigningCallbackUrls.OnErrorRedirectUrl));

            var job = new Job(signingJobRequest.JobTitle, documents, signers, signingJobRequest.ExternalId, exitUrl)
            {
                IdentifierInSignedDocuments = SignedDocumentsIdentifier,
                AuthenticationLevel = Digipost.Signature.Api.Client.Core.Enums.AuthenticationLevel.Four
            };

            _logger.LogDebug($"SigningJob created with {SignedDocumentsIdentifier.ToString()} as identifier");

            return job;
        }

        protected virtual IEnumerable<Signer> GetSigners(SigningJobRequest signingJobRequest)
        {
            List<Signer> signers = new List<Signer>();

            signers.Add(new Signer(new Digipost.Signature.Api.Client.Core.Identifier.CustomIdentifier(signingJobRequest.SigneeName)));
            return signers;
        }
    }
}
