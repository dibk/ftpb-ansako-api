﻿using Dibk.Ftpb.Signing.Models;
using Digipost.Signature.Api.Client.Core.Enums;
using Digipost.Signature.Api.Client.Direct;

namespace Dibk.Ftpb.Signing.eSignering
{
    public interface ISigningJobBuilder
    {
        IdentifierInSignedDocuments SignedDocumentsIdentifier { get; }

        Job CreateJobPayload(SigningJobRequest signingJobRequest);
    }
}
