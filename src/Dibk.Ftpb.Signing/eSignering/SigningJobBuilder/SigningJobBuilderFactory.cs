﻿using Digipost.Signature.Api.Client.Core.Enums;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Signing.eSignering
{
    public class SigningJobBuilderFactory
    {
        private readonly eSigneringSettings _settings;
        private readonly IEnumerable<ISigningJobBuilder> _signingJobProviders;

        public SigningJobBuilderFactory(IOptions<eSigneringSettings> options, IEnumerable<ISigningJobBuilder> signingJobProviders)
        {
            _settings = options.Value;
            _signingJobProviders = signingJobProviders;
        }

        public ISigningJobBuilder GetSigningJobProvider()
        {
            if (_settings.SigningIdentifier.Equals("pid", StringComparison.OrdinalIgnoreCase))
                return ResolveService(IdentifierInSignedDocuments.PersonalIdentificationNumberAndName);
            else
                return ResolveService(IdentifierInSignedDocuments.Name);

        
        }
        private ISigningJobBuilder ResolveService(IdentifierInSignedDocuments identifierType)
        {
            return _signingJobProviders.First(p => p.SignedDocumentsIdentifier == identifierType);
        }
    }
}
