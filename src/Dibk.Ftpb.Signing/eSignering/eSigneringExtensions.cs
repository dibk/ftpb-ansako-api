﻿using Dibk.Ftpb.Signing.Certificate;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Dibk.Ftpb.Signing.eSignering
{
    public static class eSigneringExtensions
    {
        public static IServiceCollection AddESignering(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<CertificateSettings>(configuration.GetSection(CertificateSettings.ConfigSection));

            var certificateStoreType = configuration.GetValue<string>($"{CertificateSettings.ConfigSection}:StoreType");

            if (certificateStoreType == "AzureKeyVault")
                services.AddSingleton<ICertificateReader, KeyVaultCertificateReader>();
            else
                services.AddSingleton<ICertificateReader, CertificateReader>();
            
            services.Configure<eSigneringSettings>(configuration.GetSection(eSigneringSettings.ConfigSection));
            services.AddScoped<ISigner, eSigneringSigner>();
            services.AddScoped<ISigningMetadataBuilder, eSigneringMetadataBuilder>();
            services.AddScoped<SigningJobBuilderFactory>();
            services.AddScoped<ISigningJobBuilder, SigningJobBuilder>();
            services.AddScoped<ISigningJobBuilder, PidSigningJobBuilder>();
            return services;
        }
    }
}
