﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dibk.Ftpb.Signing.eSignering
{
    public class eSigneringMetadataBuilder : ISigningMetadataBuilder
    {
        public string Build(string metaData, params KeyValuePair<string, string>[] values)
        {
            eSigneringMetadata eSigneringMetadata = null;
            if (string.IsNullOrEmpty(metaData))
                eSigneringMetadata = new eSigneringMetadata();
            else
                eSigneringMetadata = System.Text.Json.JsonSerializer.Deserialize<eSigneringMetadata>(metaData);

            if (values.Length > 0)
            {
                var token = values.ToList().FirstOrDefault(p => p.Key.Equals("StatusQueryToken", StringComparison.InvariantCultureIgnoreCase));

                if (!string.IsNullOrEmpty(token.Key))
                    eSigneringMetadata.StatusQueryToken = token.Value;
            }

            return System.Text.Json.JsonSerializer.Serialize(eSigneringMetadata);
        }
    }
}
