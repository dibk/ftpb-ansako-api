﻿using Dibk.Ftpb.Signing.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dibk.Ftpb.Signing
{
    public interface ISigner
    {
        string GetSigningProviderName();
        Task<SigningJobResult> CreateSigningJob(SigningJobRequest request);
        Task<SigningStatus> GetStatusForSigningJob(SigningJobStatusRequest request);
        Task<SignedDocument> GetSignedDocument(SigningJobStatusRequest request);
        Task<string> RequestNewRedirectUri(string signerMetadata);
        ISigningMetadataBuilder GetSigningMetadataBuilder();
    }

    public interface ISigningMetadataBuilder
    {
        string Build(string metaData, params KeyValuePair<string, string>[] values);
    }

    public class SigningJobStatusRequest
    {
        public SigningJobStatusRequest(string jobId, string signingJobMetadata)
        {
            JobId = jobId;
            SigningJobMetadata = signingJobMetadata;
        }
        public string JobId { get; set; }
        public string SigningJobMetadata { get; set; }
    }
}
