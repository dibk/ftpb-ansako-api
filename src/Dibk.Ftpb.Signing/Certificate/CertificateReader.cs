﻿using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace Dibk.Ftpb.Signing.Certificate
{
    public class CertificateReader : ICertificateReader
    {
        private readonly X509Certificate2 _certificate;
        private readonly string _certThumbprint;

        public CertificateReader(IOptions<CertificateSettings> settings)
        {
            _certThumbprint = settings.Value.LocalStore.CetificateThumbprint;
            _certificate = LoadCertificate();
        }

        public X509Certificate2 GetCertificate() => _certificate;

        private X509Certificate2 LoadCertificate()
        {
            if (string.IsNullOrEmpty(_certThumbprint))
                throw new ArgumentException($"Parameter 'certThumbprint' must be set");

            X509Certificate2 cert = null;
            using (var store = new X509Store(StoreLocation.CurrentUser))
            {
                store.Open(OpenFlags.ReadOnly);
                var certificates = store.Certificates.Find(X509FindType.FindByThumbprint, _certThumbprint, false);

                cert = certificates.OfType<X509Certificate2>().FirstOrDefault();

                if (cert is null)
                    throw new Exception($"Certificate with thumbprint {_certThumbprint} was not found");
            }

            return cert;
        }
    }
}