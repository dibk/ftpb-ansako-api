﻿namespace Dibk.Ftpb.Signing.Certificate
{
    public class CertificateSettings
    {
        public const string ConfigSection = "EnterpriseCertificate";
        public string StoreType { get; set; }
        public LocalStoreSettings LocalStore { get; set; }

        public AzureKeyVaultSettings AzureKeyVault { get; set; }
    }

    public class LocalStoreSettings
    {
        public string CetificateThumbprint { get; set; }
    }

    public class AzureKeyVaultSettings
    {
        public string TenantId { get; set; }
        public string VaultUrl { get; set; }
        public string CertificateName { get; set; }
    }
}