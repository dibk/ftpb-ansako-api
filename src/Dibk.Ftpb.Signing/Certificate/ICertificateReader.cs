﻿using System.Security.Cryptography.X509Certificates;

namespace Dibk.Ftpb.Signing.Certificate
{
    public interface ICertificateReader
    {
        X509Certificate2 GetCertificate();
    }
}