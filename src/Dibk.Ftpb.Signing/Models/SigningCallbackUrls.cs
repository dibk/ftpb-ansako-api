﻿namespace Dibk.Ftpb.Signing.Models
{
    public class SigningCallbackUrls
    {
        public string OnErrorRedirectUrl { get; set; }
        public string OnSuccessRedirectUrl { get; set; }
        public string OnCancelRedirectUrl { get; set; }
    }
}
