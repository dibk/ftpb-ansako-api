﻿using System;

namespace Dibk.Ftpb.Signing.Models
{
    public class SignedDocument
    {
        public string SignerDisplayName { get; set; }
        public DateTime SignedDate { get; set; }
        public byte[] FileContent { get; set; }
    }
}
