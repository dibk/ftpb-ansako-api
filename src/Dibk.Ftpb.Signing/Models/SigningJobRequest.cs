﻿using System.Collections.Generic;

namespace Dibk.Ftpb.Signing.Models
{
    public class SigningJobRequest
    {
        public SigningCallbackUrls SigningCallbackUrls { get; set; }
        public string ExternalId { get; set; } 
        public string SigneeName { get; set; }
        public string SigneeEmail { get; set; }
        public string JobTitle { get; set; }
        public List<SigningDocument> SigningDocuments { get; set; }
        public SigningJobRequest()
        {
            SigningDocuments = new List<SigningDocument>();
        }
    }
}
