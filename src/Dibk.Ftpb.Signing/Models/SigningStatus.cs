﻿namespace Dibk.Ftpb.Signing
{
    public enum SigningStatus
    {
        Created,
        NoChanges,
        CompletedSuccessfully,
        Failed,
        InProgress
    }
}