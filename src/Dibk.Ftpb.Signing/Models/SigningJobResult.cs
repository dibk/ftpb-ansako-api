﻿namespace Dibk.Ftpb.Signing.Models
{
    public class SigningJobResult
    {
        public string Id { get; set; }
        public SigningStatus Status { get; set; }
        public string SigningUrl { get; set; }        
        public string SigningProvider { get; set; }
        public string SigningMetadata { get; set; }
    }
}
