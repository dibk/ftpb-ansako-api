﻿namespace Dibk.Ftpb.Signing.Models
{
    public class SigningDocument
    {
        public SigningDocument(SigningFileType signingFileType)
        {
            FileType = signingFileType;
        }
        public string Title { get; set; }
        public string FileName { get; set; }
        public SigningFileType FileType { get; set; }
        public byte[] FileContent { get; set; }
    }

    public enum SigningFileType
    {
        MainDocument,
        Attachment
    }
}
