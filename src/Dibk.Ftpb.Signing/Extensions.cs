﻿using System.IO;

namespace Dibk.Ftpb.Signing
{
    public static class StreamExtension
    {
        public static byte[] GetAsBytes(this Stream stream)
        {
            byte[] fileBytes = null;
            using (var ms = new MemoryStream())
            {
                stream.CopyTo(ms);
                fileBytes = ms.ToArray();
            }

            return fileBytes;
        }
    }
}
